﻿using SocketTester.Entities;
using SocketTester.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Util.Data;
using Util.Threads;

namespace SocketTester
{
    internal class Program
    {
        private static List<Task> Tasks = new List<Task>();
        private static void Main(string[] args)
        {
            try
            {
                string ipAddress = ConfigurationManager.AppSettings["IpAddress"];
                string port = ConfigurationManager.AppSettings["Port"];
                string runTaskWithSingleMessage = ConfigurationManager.AppSettings["RunTaskWithSingleMessage"];

                string delayMilliseconds = ConfigurationManager.AppSettings["DelayMilliseconds"];
                int delayTime = 0;
                if (!string.IsNullOrEmpty(delayMilliseconds))
                {
                    if (!int.TryParse(delayMilliseconds, out delayTime))
                    {
                        delayTime = 1;
                    }
                }

                string timeoutResponseMilliseconds = ConfigurationManager.AppSettings["TimeoutResponseMilliseconds"];

                int timeoutResponse = 0;
                if (!string.IsNullOrEmpty(timeoutResponseMilliseconds))
                {
                    if (!int.TryParse(timeoutResponseMilliseconds, out timeoutResponse))
                    {
                        timeoutResponse = 30000;
                    }
                }

                string batchSizeCongig = ConfigurationManager.AppSettings["BatchSize"];

                int batchSize = 0;
                if (!string.IsNullOrEmpty(batchSizeCongig))
                {
                    if (!int.TryParse(batchSizeCongig, out batchSize) || batchSize <= 0)
                    {
                        batchSize = 50; //Default
                    }
                }

                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                Tasks.Clear();

                var entities = new TransactionHostEntities();
                entities.Database.CommandTimeout = Int32.MaxValue;
                var currentDate = DateTime.Now.Date;

                #region Loading data to test
                var watcher = new Stopwatch();
                watcher.Start();
                var transactions = entities.usp_SocketTesterTool_GetTransactions(currentDate, batchSize).ToList();
                var lstBatches = transactions.Select(n => new
                {
                    n.BatchNumber,
                    n.TerminalID
                }).Distinct().ToList();

                foreach (var item in lstBatches)
                {
                    var socketDetails = transactions.Where(x => x.TerminalID == item.TerminalID && x.BatchNumber == item.BatchNumber && x.ResponseData == null)
                        .Select(n => new BatchDataToSendMessageModel()
                        {
                            ID = n.ID,
                            MessageTypeID = n.MessageTypeID,
                            DataToSendMessage = DataHelper.HexStringToByteArray(n.RequestData),
                            TerminalID = n.TerminalID,
                            BatchNumber = n.BatchNumber
                        }).ToList();

                    Task task = new Task(() =>
                    {
                        SendAndReceiveTask tsk = new SendAndReceiveTask(socketDetails, ipAddress, Convert.ToInt32(port), timeoutResponse)
                        {
                            ThreadType = WorkerType.StandardThread
                        };
                        if (string.IsNullOrEmpty(runTaskWithSingleMessage))
                        {
                            tsk.RunTask();
                        }
                        else
                        {
                            tsk.RunTaskWithSingleMessage();
                        }
                    });

                    Tasks.Add(task);
                }

                watcher.Stop();
                Console.WriteLine($"Loading time = {watcher.ElapsedMilliseconds / 1000} s");
                #endregion

                #region Run tasks
                var taskWatcher = new Stopwatch();
                taskWatcher.Start();

                foreach (var task in Tasks)
                {
                    task.Start();
                    Thread.Sleep(delayTime);
                }

                Task.WaitAll(Tasks.ToArray());

                taskWatcher.Stop();
                Console.WriteLine($"Total running time = {taskWatcher.ElapsedMilliseconds / 1000} s");
                #endregion

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.ToString());
            var args = new string[] { };
            Main(args);
        }
    }
}