﻿namespace SocketTester.Models
{
    public class BatchDataToSendMessageModel
    {
        public int ID { get; set; }
        public string MessageTypeID { get; set; }
        public byte[] DataToSendMessage { get; set; }
        public string TerminalID { get; set; }
        public string BatchNumber { get; set; }
    }
}