﻿using Comms.Connection.Common;
using Comms.Connection.Sockets;
using Comms.Message.AS2805;
using Comms.Protocol;
using Comms.Protocol.Common;
using SocketTester.Entities;
using SocketTester.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using Util.Data;
using Util.Threads;

namespace SocketTester
{
    public class SendAndReceiveTask : ThreadWorker
    {
        private List<BatchDataToSendMessageModel> batchDataToSendMessages = new List<BatchDataToSendMessageModel>();

        private string ConnectionString = new TransactionHostEntities().Database.Connection.ConnectionString;
        private string SqlCommandUpdateSocketTesterDetail = "UPDATE dbo.SocketTesterDetail SET RequestDatetime = @RequestDatetime, ResponseData = @ResponseData, ResponseDatetime = @ResponseDatetime, ResponseCode = @ResponseCode WHERE ID = @ID";
        private string SqlCommandUpdateStatus = "UPDATE dbo.SocketTesterSummary SET IsSent = 1 WHERE TerminalID = @TerminalID AND BatchNumber = @BatchNumber";

        private string IpAddress = "localhost";
        private int Port = 3006;
        private int TimeoutResponse = 30000;

        private int bytesReceived = 0;
        private AS805Packer packer = new AS805Packer();

        private AutoResetEvent dataReceivedEvent = new AutoResetEvent(false);
        private AutoResetEvent dataReceivedEvent0800 = new AutoResetEvent(false);
        private AutoResetEvent dataReceivedEvent0220 = new AutoResetEvent(false);
        private AutoResetEvent dataReceivedEvent0500 = new AutoResetEvent(false);

        private void protocol_OnFrameReceived(int bytesReceived)
        {
            this.bytesReceived = bytesReceived;
            dataReceivedEvent.Set();
        }

        private void protocol_OnFrameReceived0800(int bytesReceived)
        {
            this.bytesReceived = bytesReceived;
            dataReceivedEvent0800.Set();
        }

        private void protocol_OnFrameReceived0220(int bytesReceived)
        {
            this.bytesReceived = bytesReceived;
            dataReceivedEvent0220.Set();
        }

        private void protocol_OnFrameReceived0500(int bytesReceived)
        {
            this.bytesReceived = bytesReceived;
            dataReceivedEvent0500.Set();
        }

        public SendAndReceiveTask(List<BatchDataToSendMessageModel> batchDataToSendMessages, string ipAddress, int port, int timeoutResponse)
        {
            this.batchDataToSendMessages = batchDataToSendMessages;
            IpAddress = ipAddress;
            Port = port;
            TimeoutResponse = timeoutResponse;
        }

        protected override void Run()
        {
            try
            {
                TcpConnection conn = new TcpConnection(IpAddress, Port);
                BcdProtocol protocol = new BcdProtocol(conn);

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    #region Message 0800

                    conn = new TcpConnection(IpAddress, Port);
                    protocol = new BcdProtocol(conn);

                    #region Get message 0800

                    var messageDetail0800 = batchDataToSendMessages.FirstOrDefault(x => x.MessageTypeID == "0800");

                    #endregion Get message 0800

                    UpdateStatusBatchWhenSentMessage(connection, messageDetail0800.TerminalID, messageDetail0800.BatchNumber);

                    try
                    {
                        if (conn.Status != ConnectionStatus.Connected)
                            conn.Connect();

                        #region Send data message 0800 to host

                        //send
                        Console.WriteLine("Send message 0800");
                        protocol.SendFrame(messageDetail0800.DataToSendMessage, 0, messageDetail0800.DataToSendMessage.Length);
                        var requestDatetime0800 = DateTime.Now;

                        #endregion Send data message 0800 to host

                        #region Receive data message 0810 from host

                        //receive
                        byte[] receiveBuffer0800 = new byte[4096];
                        protocol.OnFrameReceived += new ProtocolHandler.FrameReceived(protocol_OnFrameReceived0800);
                        protocol.WaitFrameAsync(receiveBuffer0800);
                        dataReceivedEvent0800.WaitOne(TimeoutResponse);

                        string receiveMessage0800 = DataHelper.ByteArrayToHexString(receiveBuffer0800, 0, bytesReceived);
                        Console.WriteLine($"Receive message 0800: {receiveMessage0800}");

                        var responseData0800 = receiveMessage0800;
                        var responseDatetime0800 = DateTime.Now;

                        #endregion Receive data message 0810 from host

                        #region Unpack message 0810 to get response code

                        //unpack message
                        var messageResponse0810 = (AS2805Message)packer.Unpack(receiveBuffer0800, 0, bytesReceived);

                        string responseCode0800 = null;
                        if (!string.IsNullOrEmpty(receiveMessage0800))
                        {
                            if (messageResponse0810.IsValidMessage)
                            {
                                responseCode0800 = messageResponse0810.ResponseCode;
                            }
                            else
                            {
                                responseCode0800 = "Error"; //In the case: receiveBuffer0800 = {0, 0, 0, 0} => receiveMessage0800 = "0000";
                            }
                        }
                        else
                        {
                            responseCode0800 = "N/A"; //timeout
                        }

                        #endregion Unpack message 0810 to get response code

                        #region Save

                        UpdateDataResponse(connection, requestDatetime0800, responseData0800, responseDatetime0800, responseCode0800, messageDetail0800.ID);

                        #endregion Save
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error 0800: {e.Message}");

                        UpdateDataResponse(connection, DateTime.Now, e.Message, DateTime.Now, "Error", messageDetail0800.ID);
                    }
                    finally
                    {
                        if (conn.Status == ConnectionStatus.Connected)
                            conn.Disconnect();
                    }

                    #endregion Message 0800

                    #region Message 0220

                    #region Get message 0220

                    var batchMessage0220s = batchDataToSendMessages.Where(x => x.MessageTypeID == "0220").ToList();

                    #endregion Get message 0220

                    foreach (var messageDetail0220 in batchMessage0220s)
                    {
                        conn = new TcpConnection(IpAddress, Port);
                        protocol = new BcdProtocol(conn);

                        try
                        {
                            if (conn.Status != ConnectionStatus.Connected)
                                conn.Connect();

                            #region Send data message 0220 to host

                            //send
                            protocol.SendFrame(messageDetail0220.DataToSendMessage, 0, messageDetail0220.DataToSendMessage.Length);
                            var requestDatetime0220 = DateTime.Now;
                            Console.WriteLine("Send message 0220");

                            #endregion Send data message 0220 to host

                            #region Receive data message 0230 from host

                            //receive
                            byte[] receiveBuffer0220 = new byte[8192];

                            protocol.OnFrameReceived += new ProtocolHandler.FrameReceived(protocol_OnFrameReceived0220);
                            protocol.WaitFrameAsync(receiveBuffer0220);
                            dataReceivedEvent0220.WaitOne(TimeoutResponse);

                            var responseData0220 = string.Empty;
                            var receiveMessage0220 = string.Empty;
                            var responseDatetime0220 = DateTime.Now;

                            #endregion Receive data message 0230 from host

                            #region Unpack message 0230 to get response code

                            Thread.Sleep(100);
                            var messageResponse0230 = new AS2805Message();

                            receiveMessage0220 = DataHelper.ByteArrayToHexString(receiveBuffer0220, 0, bytesReceived);
                            Console.WriteLine($"Receive message 0220: {receiveMessage0220} ");
                            responseData0220 = receiveMessage0220;
                            //unpack message
                            messageResponse0230 = (AS2805Message)packer.Unpack(receiveBuffer0220, 0, bytesReceived);
                            
                            string responseCode0230 = null;
                            if (!string.IsNullOrEmpty(receiveMessage0220))
                            {
                                if (messageResponse0230.IsValidMessage)
                                {
                                    responseCode0230 = messageResponse0230.ResponseCode;
                                }
                                else
                                {
                                    responseCode0230 = "Error"; //In the case: receiveBuffer0220 = {0, 0, 0, 0} => receiveMessage0220 = "0000";
                                }  
                            }
                            else
                            {
                                responseCode0230 = "N/A"; //timeout
                            }

                            #endregion Unpack message 0230 to get response code

                            #region Save

                            //save
                            UpdateDataResponse(connection, requestDatetime0220, responseData0220, responseDatetime0220, responseCode0230, messageDetail0220.ID);

                            #endregion Save
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"Error 0220: {e.Message}");

                            UpdateDataResponse(connection, DateTime.Now, e.Message, DateTime.Now, "Error", messageDetail0220.ID);

                            continue;
                        }
                        finally
                        {
                            if (conn.Status == ConnectionStatus.Connected)
                                conn.Disconnect();
                        }
                    }

                    #endregion Message 0220

                    #region Message 0500

                    conn = new TcpConnection(IpAddress, Port);
                    protocol = new BcdProtocol(conn);

                    var messageDetail0500 = batchDataToSendMessages.FirstOrDefault(x => x.MessageTypeID == "0500");

                    try
                    {
                        if (conn.Status != ConnectionStatus.Connected)
                            conn.Connect();



                        #region Send data message 0500 to host

                        //send
                        protocol.SendFrame(messageDetail0500.DataToSendMessage, 0, messageDetail0500.DataToSendMessage.Length);
                        var requestDatetime0500 = DateTime.Now;

                        #endregion Send data message 0500 to host

                        #region Receive data message 0510 from host

                        //receive
                        byte[] receiveBuffer0500 = new byte[4096];
                        protocol.OnFrameReceived += new ProtocolHandler.FrameReceived(protocol_OnFrameReceived0500);
                        protocol.WaitFrameAsync(receiveBuffer0500);
                        dataReceivedEvent0500.WaitOne(TimeoutResponse);

                        string receiveMessage0500 = DataHelper.ByteArrayToHexString(receiveBuffer0500, 0, bytesReceived);
                        var responseData0500 = receiveMessage0500;
                        var responseDatetime0500 = DateTime.Now;

                        #endregion Receive data message 0510 from host

                        #region Unpack message 0510 to get response code

                        //unpack message
                        var messageResponse0510 = (AS2805Message)packer.Unpack(receiveBuffer0500, 0, bytesReceived);

                        string responseCode0500 = null;
                        if (!string.IsNullOrEmpty(receiveMessage0500))
                        {
                            if (messageResponse0510.IsValidMessage)
                            {
                                responseCode0500 = messageResponse0510.ResponseCode;
                            }
                            else
                            {
                                responseCode0500 = "Error"; //In the case: receiveBuffer0500 = {0, 0, 0, 0} => receiveMessage0500 = "0000";
                            } 
                        }
                        else
                        {
                            responseCode0500 = "N/A"; //timeout
                        }

                        #endregion Unpack message 0510 to get response code

                        #region Save

                        //save
                        UpdateDataResponse(connection, requestDatetime0500, responseData0500, responseDatetime0500, responseCode0500, messageDetail0500.ID);

                        #endregion Save

                        if (conn.Status == ConnectionStatus.Connected)
                            conn.Disconnect();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error 0500: {e.Message}");

                        //save

                        UpdateDataResponse(connection, DateTime.Now, e.Message, DateTime.Now, "Error", messageDetail0500.ID);
                    }
                    finally
                    {
                        if (conn.Status == ConnectionStatus.Connected)
                            conn.Disconnect();
                    }

                    #endregion Message 0500
                }

                if (conn.Status == ConnectionStatus.Connected)
                    conn.Disconnect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RunTask()
        {
            try
            {
                TcpConnection conn = new TcpConnection(IpAddress, Port);
                BcdProtocol protocol = new BcdProtocol(conn);

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    #region Message 0800

                    conn = new TcpConnection(IpAddress, Port);
                    protocol = new BcdProtocol(conn);

                    #region Get message 0800

                    var messageDetail0800 = batchDataToSendMessages.FirstOrDefault(x => x.MessageTypeID == "0800");

                    #endregion Get message 0800

                    UpdateStatusBatchWhenSentMessage(connection, messageDetail0800.TerminalID, messageDetail0800.BatchNumber);

                    try
                    {
                        if (conn.Status != ConnectionStatus.Connected)
                            conn.Connect();

                        #region Send data message 0800 to host

                        //send
                        Console.WriteLine("Send message 0800");
                        protocol.SendFrame(messageDetail0800.DataToSendMessage, 0, messageDetail0800.DataToSendMessage.Length);
                        var requestDatetime0800 = DateTime.Now;

                        #endregion Send data message 0800 to host

                        #region Receive data message 0810 from host

                        //receive
                        byte[] receiveBuffer0800 = new byte[4096];
                        protocol.OnFrameReceived += new ProtocolHandler.FrameReceived(protocol_OnFrameReceived0800);
                        protocol.WaitFrameAsync(receiveBuffer0800);
                        dataReceivedEvent0800.WaitOne(TimeoutResponse);

                        string receiveMessage0800 = DataHelper.ByteArrayToHexString(receiveBuffer0800, 0, bytesReceived);
                        Console.WriteLine($"Receive message 0800: {receiveMessage0800}");

                        var responseData0800 = receiveMessage0800;
                        var responseDatetime0800 = DateTime.Now;

                        #endregion Receive data message 0810 from host

                        #region Unpack message 0810 to get response code

                        //unpack message
                        var messageResponse0810 = (AS2805Message)packer.Unpack(receiveBuffer0800, 0, bytesReceived);

                        string responseCode0800 = null;
                        if (!string.IsNullOrEmpty(receiveMessage0800))
                        {
                            if (messageResponse0810.IsValidMessage)
                            {
                                responseCode0800 = messageResponse0810.ResponseCode;
                            }
                            else
                            {
                                responseCode0800 = "Error"; //In the case: receiveBuffer0800 = {0, 0, 0, 0} => receiveMessage0800 = "0000";
                            }
                        }
                        else
                        {
                            responseCode0800 = "N/A"; //timeout
                        }

                        #endregion Unpack message 0810 to get response code

                        #region Save

                        UpdateDataResponse(connection, requestDatetime0800, responseData0800, responseDatetime0800, responseCode0800, messageDetail0800.ID);

                        #endregion Save
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error 0800: {e.Message}");

                        UpdateDataResponse(connection, DateTime.Now, e.Message, DateTime.Now, "Error", messageDetail0800.ID);
                    }
                    finally
                    {
                        if (conn.Status == ConnectionStatus.Connected)
                            conn.Disconnect();
                    }

                    #endregion Message 0800

                    #region Message 0220

                    #region Get message 0220

                    var batchMessage0220s = batchDataToSendMessages.Where(x => x.MessageTypeID == "0220").ToList();

                    #endregion Get message 0220

                    foreach (var messageDetail0220 in batchMessage0220s)
                    {
                        conn = new TcpConnection(IpAddress, Port);
                        protocol = new BcdProtocol(conn);

                        try
                        {
                            if (conn.Status != ConnectionStatus.Connected)
                                conn.Connect();

                            #region Send data message 0220 to host

                            //send
                            protocol.SendFrame(messageDetail0220.DataToSendMessage, 0, messageDetail0220.DataToSendMessage.Length);
                            var requestDatetime0220 = DateTime.Now;
                            Console.WriteLine("Send message 0220");

                            #endregion Send data message 0220 to host

                            #region Receive data message 0230 from host

                            //receive
                            byte[] receiveBuffer0220 = new byte[8192];

                            protocol.OnFrameReceived += new ProtocolHandler.FrameReceived(protocol_OnFrameReceived0220);
                            protocol.WaitFrameAsync(receiveBuffer0220);
                            dataReceivedEvent0220.WaitOne(TimeoutResponse);

                            var responseData0220 = string.Empty;
                            var receiveMessage0220 = string.Empty;
                            var responseDatetime0220 = DateTime.Now;

                            #endregion Receive data message 0230 from host

                            #region Unpack message 0230 to get response code

                            Thread.Sleep(100);
                            var messageResponse0230 = new AS2805Message();

                            receiveMessage0220 = DataHelper.ByteArrayToHexString(receiveBuffer0220, 0, bytesReceived);
                            Console.WriteLine($"Receive message 0220: {receiveMessage0220} ");
                            responseData0220 = receiveMessage0220;
                            //unpack message
                            messageResponse0230 = (AS2805Message)packer.Unpack(receiveBuffer0220, 0, bytesReceived);

                            string responseCode0230 = null;
                            if (!string.IsNullOrEmpty(receiveMessage0220))
                            {
                                if (messageResponse0230.IsValidMessage)
                                {
                                    responseCode0230 = messageResponse0230.ResponseCode;
                                }
                                else
                                {
                                    responseCode0230 = "Error"; //In the case: receiveBuffer0220 = {0, 0, 0, 0} => receiveMessage0220 = "0000";
                                }
                            }
                            else
                            {
                                responseCode0230 = "N/A"; //timeout
                            }

                            #endregion Unpack message 0230 to get response code

                            #region Save

                            //save
                            UpdateDataResponse(connection, requestDatetime0220, responseData0220, responseDatetime0220, responseCode0230, messageDetail0220.ID);

                            #endregion Save
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"Error 0220: {e.Message}");

                            UpdateDataResponse(connection, DateTime.Now, e.Message, DateTime.Now, "Error", messageDetail0220.ID);

                            continue;
                        }
                        finally
                        {
                            if (conn.Status == ConnectionStatus.Connected)
                                conn.Disconnect();
                        }
                    }

                    #endregion Message 0220

                    #region Message 0500

                    conn = new TcpConnection(IpAddress, Port);
                    protocol = new BcdProtocol(conn);

                    var messageDetail0500 = batchDataToSendMessages.FirstOrDefault(x => x.MessageTypeID == "0500");

                    try
                    {
                        if (conn.Status != ConnectionStatus.Connected)
                            conn.Connect();



                        #region Send data message 0500 to host

                        //send
                        Console.WriteLine("Send message 0500");
                        protocol.SendFrame(messageDetail0500.DataToSendMessage, 0, messageDetail0500.DataToSendMessage.Length);
                        var requestDatetime0500 = DateTime.Now;

                        #endregion Send data message 0500 to host

                        #region Receive data message 0510 from host

                        //receive
                        byte[] receiveBuffer0500 = new byte[4096];
                        protocol.OnFrameReceived += new ProtocolHandler.FrameReceived(protocol_OnFrameReceived0500);
                        protocol.WaitFrameAsync(receiveBuffer0500);
                        dataReceivedEvent0500.WaitOne(TimeoutResponse);

                        string receiveMessage0500 = DataHelper.ByteArrayToHexString(receiveBuffer0500, 0, bytesReceived);
                        Console.WriteLine($"Receive message 0500: {receiveMessage0500}");

                        var responseData0500 = receiveMessage0500;
                        var responseDatetime0500 = DateTime.Now;

                        #endregion Receive data message 0510 from host

                        #region Unpack message 0510 to get response code

                        //unpack message
                        var messageResponse0510 = (AS2805Message)packer.Unpack(receiveBuffer0500, 0, bytesReceived);

                        string responseCode0500 = null;
                        if (!string.IsNullOrEmpty(receiveMessage0500))
                        {
                            if (messageResponse0510.IsValidMessage)
                            {
                                responseCode0500 = messageResponse0510.ResponseCode;
                            }
                            else
                            {
                                responseCode0500 = "Error"; //In the case: receiveBuffer0500 = {0, 0, 0, 0} => receiveMessage0500 = "0000";
                            }
                        }
                        else
                        {
                            responseCode0500 = "N/A"; //timeout
                        }

                        #endregion Unpack message 0510 to get response code

                        #region Save

                        //save
                        UpdateDataResponse(connection, requestDatetime0500, responseData0500, responseDatetime0500, responseCode0500, messageDetail0500.ID);

                        #endregion Save

                        if (conn.Status == ConnectionStatus.Connected)
                            conn.Disconnect();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Error 0500: {e.Message}");

                        //save

                        UpdateDataResponse(connection, DateTime.Now, e.Message, DateTime.Now, "Error", messageDetail0500.ID);
                    }
                    finally
                    {
                        if (conn.Status == ConnectionStatus.Connected)
                            conn.Disconnect();
                    }

                    #endregion Message 0500
                }

                if (conn.Status == ConnectionStatus.Connected)
                    conn.Disconnect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RunTaskWithSingleMessage()
        {
            try
            {
                TcpConnection conn = new TcpConnection(IpAddress, Port);
                BcdProtocol protocol = new BcdProtocol(conn);

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    conn = new TcpConnection(IpAddress, Port);
                    protocol = new BcdProtocol(conn);

                    UpdateStatusBatchWhenSentMessage(connection, batchDataToSendMessages.FirstOrDefault().TerminalID, batchDataToSendMessages.FirstOrDefault().BatchNumber);

                    foreach (var messageDetail in batchDataToSendMessages)
                    {
                        conn = new TcpConnection(IpAddress, Port);
                        protocol = new BcdProtocol(conn);

                        try
                        {
                            if (conn.Status != ConnectionStatus.Connected)
                                conn.Connect();

                            #region Send data to host

                            //send
                            protocol.SendFrame(messageDetail.DataToSendMessage, 0, messageDetail.DataToSendMessage.Length);
                            var requestDatetime = DateTime.Now;
                            Console.WriteLine("Send message");

                            #endregion Send data to host

                            #region Receive data from host

                            //receive
                            byte[] receiveBuffer = new byte[8192];

                            protocol.OnFrameReceived += new ProtocolHandler.FrameReceived(protocol_OnFrameReceived);
                            protocol.WaitFrameAsync(receiveBuffer);
                            dataReceivedEvent.WaitOne(TimeoutResponse);

                            var responseData = string.Empty;
                            var receiveMessage = string.Empty;
                            var responseDatetime = DateTime.Now;

                            #endregion Receive data from host

                            #region Unpack message to get response code

                            Thread.Sleep(100);
                            var messageResponse = new AS2805Message();

                            receiveMessage = DataHelper.ByteArrayToHexString(receiveBuffer, 0, bytesReceived);
                            Console.WriteLine($"Receive message: {receiveMessage} ");
                            responseData = receiveMessage;
                            //unpack message
                            messageResponse = (AS2805Message)packer.Unpack(receiveBuffer, 0, bytesReceived);

                            string responseCode = null;
                            if (!string.IsNullOrEmpty(receiveMessage))
                            {
                                if (messageResponse.IsValidMessage)
                                {
                                    responseCode = messageResponse.ResponseCode;
                                }
                                else
                                {
                                    responseCode = "Error"; //In the case: receiveBuffer = {0, 0, 0, 0} => receiveMessage = "0000";
                                }
                            }
                            else
                            {
                                responseCode = "N/A"; //timeout
                            }

                            #endregion Unpack message to get response code

                            #region Save

                            //save
                            UpdateDataResponse(connection, requestDatetime, responseData, responseDatetime, responseCode, messageDetail.ID);

                            #endregion Save
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"Error: {e.Message}");

                            UpdateDataResponse(connection, DateTime.Now, e.Message, DateTime.Now, "Error", messageDetail.ID);

                            continue;
                        }
                        finally
                        {
                            if (conn.Status == ConnectionStatus.Connected)
                                conn.Disconnect();
                        }
                    }
                }

                if (conn.Status == ConnectionStatus.Connected)
                    conn.Disconnect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void UpdateStatusBatchWhenSentMessage(SqlConnection connection, string terminalId, string batchNumber)
        {
            SqlCommand cmdUpdateStatus = new SqlCommand(SqlCommandUpdateStatus, connection);
            cmdUpdateStatus.Parameters.AddWithValue("@TerminalID", terminalId);
            cmdUpdateStatus.Parameters.AddWithValue("@BatchNumber", batchNumber);

            cmdUpdateStatus.ExecuteNonQuery();
        }

        private void UpdateDataResponse(SqlConnection connection, DateTime requestDateTime, string responseData, DateTime responseDatetime, string responseCode, int id)
        {
            SqlCommand cmd = new SqlCommand(SqlCommandUpdateSocketTesterDetail, connection);
            cmd.Parameters.AddWithValue("@RequestDatetime", requestDateTime);
            cmd.Parameters.AddWithValue("@ResponseData", responseData);
            cmd.Parameters.AddWithValue("@ResponseDatetime", responseDatetime);
            cmd.Parameters.AddWithValue("@ResponseCode", responseCode);
            cmd.Parameters.AddWithValue("@ID", id);

            cmd.ExecuteNonQuery();
        }
    }
}