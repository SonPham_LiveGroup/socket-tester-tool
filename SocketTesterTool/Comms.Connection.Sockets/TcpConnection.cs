﻿using Comms.Connection.Common;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Comms.Connection.Sockets
{
    /// <summary>
    /// Represents a TcpConnection
    /// </summary>
    public class TcpConnection : AsyncConnectionBase
    {
        private TcpClient _client = null;
        private Stream _clientStream = null;
        private string _ipAddress = "";
        private int _port = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpConnection" /> class using an already connected TcpClient object.
        /// Used by TcpConnectionLister
        /// </summary>
        /// <param name="connectedClient">The connected client.</param>
        public TcpConnection(TcpClient connectedClient)
        {
            _client = connectedClient;
            _clientStream = connectedClient.GetStream();
            _clientStream.ReadTimeout = 60000;

            // extract ip address and port from already connected client
            _ipAddress = ((IPEndPoint)_client.Client.RemoteEndPoint).Address.ToString();
            _port = ((IPEndPoint)_client.Client.RemoteEndPoint).Port;
            Status = ConnectionStatus.Connected;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpConnection" /> class.
        /// </summary>
        /// <param name="ip">The ip.</param>
        /// <param name="port">The port.</param>
        public TcpConnection(string ip, int port)
        {
            _ipAddress = ip;
            _port = port;
            _client = new TcpClient();
            Status = ConnectionStatus.Disconnected;
        }

        /// <summary>
        /// Connects the underlying connection to ip/port specified in constructor.
        /// </summary>
        public override void Connect()
        {
            _client.Connect(_ipAddress, _port);
            _clientStream = _client.GetStream();
            _clientStream.ReadTimeout = 60000;
            Status = ConnectionStatus.Connected;
        }

        /// <summary>
        /// Disconnected the underlying connection.
        /// </summary>
        public override void Disconnect()
        {
            CleanUpAndDisconnectClient();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("TCP ");
            sb.Append(this._ipAddress);
            sb.Append(":");
            sb.Append(this.Port);
            return sb.ToString();
        }

        /// <summary>
        /// Gets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public int Port
        {
            get { return _port; }
        }

        /// <summary>
        /// Gets the ip.
        /// </summary>
        /// <value>
        /// The ip.
        /// </value>
        public string Ip
        {
            get { return _ipAddress; }
        }

        /// <summary>
        /// Reads from the connection into the a byte array.
        /// </summary>
        /// <param name="into">Read into array.</param>
        /// <param name="pos">At pos.</param>
        /// <param name="length">How may bytes to read.</param>
        public override void Read(byte[] into, int pos, int length)
        {
            try
            {
                _clientStream.Read(into, pos, length);
            }
            catch (Exception e)
            {
                CleanUpAndDisconnectClient();
                throw e;
            }
        }

        /// <summary>
        /// Cleans up and disconnects underlying client object.
        /// </summary>
        private void CleanUpAndDisconnectClient()
        {
            try
            {
                _clientStream.Close();
            }
            catch { }

            try
            {
                _client.Close();
            }
            catch { }

            _client = new TcpClient();
            Status = ConnectionStatus.Disconnected;
        }

        /// <summary>
        /// Writes to the connection from a byte array.
        /// </summary>
        /// <param name="from">From array.</param>
        /// <param name="pos">At pos.</param>
        /// <param name="length">How many bytes to write.</param>
        public override void Write(byte[] from, int pos, int length)
        {
            try
            {
                _clientStream.Write(from, pos, length);
            }
            catch (Exception e)
            {
                CleanUpAndDisconnectClient();
                throw e;
            }
        }

        public override async Task WriteAsync(byte[] from, int pos, int length)
        {
            try
            {
                await _clientStream.WriteAsync(from, pos, length);
            }
            catch (Exception e)
            {
                CleanUpAndDisconnectClient();
                throw e;
            }
        }

        /// <summary>
        /// Begins an async read.
        /// OnDataReceived event should be set before this to
        /// ensure a result is received back to the caller.
        /// </summary>
        /// <param name="intoBuffer">Read into buffer.</param>
        /// <param name="atOffset">At offset.</param>
        /// <param name="length">For length.</param>
        public override void BeginRead(byte[] intoBuffer, int atOffset, int length)
        {
            try
            {
                _clientStream.BeginRead(intoBuffer, atOffset, length, new AsyncCallback(this.ProcessAsyncReceivedData), this);
            }
            catch (Exception e)
            {
                CleanUpAndDisconnectClient();
                throw e;
            }
        }

        /// <summary>
        /// Internal processing of the async received data.
        /// Calls ProcessDataReceived
        /// </summary>
        /// <param name="ar">The async result.</param>
        private void ProcessAsyncReceivedData(IAsyncResult ar)
        {
            int bytesAwating = 0;
            try
            {
                bytesAwating = _clientStream.EndRead(ar);
            }
            catch { }

            if (bytesAwating < 1)
            {
                // we have a problem
                CleanUpAndDisconnectClient();
            }

            ProcessDataReceived(bytesAwating);
        }
    }
}