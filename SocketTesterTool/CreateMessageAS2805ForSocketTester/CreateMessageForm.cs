﻿using Comms.Message.AS2805;
using Comms.Message.Common;
using CreateMessageAS2805ForSocketTester.Entities;
using CreateMessageAS2805ForSocketTester.MessageAS2805;
using CreateMessageAS2805ForSocketTester.Models;
using Host.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Util.Common;
using Util.Data;

namespace CreateMessageAS2805ForSocketTester
{
    public partial class CreateMessageForm : Form
    {
        public CreateMessageForm()
        {
            InitializeComponent();
            btnSendMessage.Enabled = false;
            txtGlideboxAmount.Text = "0.00";
            radioButtonFormatNonDebugLog.Checked = true;
            lblOtherFileName.Visible = false;
            txtOtherFileName.Visible = false;
            txtCardName.Text = "VISA";
            dateTimePickerTransactionDate.Format = DateTimePickerFormat.Custom;
            dateTimePickerTransactionDate.CustomFormat = "dd/MM/yyyy HH:mm:ss";

            //dataGridViewGlidebox.Visible = false;
        }

        private bool IsQuantityValid = false;

        private void btnCreateMessage_Click(object sender, EventArgs e)
        {
            var transactionHostEntities = new TransactionHostEntities();

            try
            {
                //64000066
                //txtMerchantID.Text = "03023166200";
                //txtSerialNumber.Text = "327-164-336";
                //txtModel.Text = "vx675";

                //64000072
                //txtMerchantID.Text = "03023166226";
                //txtSerialNumber.Text = "331-467-267";
                //txtModel.Text = "vx675";

                #region Validation

                if (!ValidateTerminalID())
                {
                    return;
                }

                if (string.IsNullOrEmpty(txtBatchNumber.Text) || !int.TryParse(txtBatchNumber.Text, out int serialBatchNumber))
                {
                    MessageBox.Show("Please input batch number", "Error");
                    return;
                }

                DateTime.TryParse(dateTimePickerTransactionDate.Text, out DateTime transactionDate);

                if (string.IsNullOrEmpty(txtNumberTransactionPerBatch.Text) || !int.TryParse(txtNumberTransactionPerBatch.Text, out int numberTransactionPerBatch) || numberTransactionPerBatch <= 0)
                {
                    MessageBox.Show("Please input number of transaction per batch", "Error");
                    return;
                }

                if (string.IsNullOrEmpty(txtTransactionAmount.Text))
                {
                    MessageBox.Show("Please input transaction amount with format: xxx;yyy", "Error");
                    return;
                }
                
                var transactionAmountList = new List<decimal>();
                 
                foreach (var amount in txtTransactionAmount.Text.Split(';'))
                {
                    if (!decimal.TryParse(amount, out decimal transactionAmount))
                    {
                        MessageBox.Show("Transaction Amount invalid", "Error");
                        return;
                    }
                    else
                    {
                        transactionAmountList.Add(transactionAmount);
                    }
                }

                if (transactionAmountList.Count() != numberTransactionPerBatch)
                {
                    MessageBox.Show("Please input transaction amount match with number transaction per batch", "Error");
                    return;
                }

                if (string.IsNullOrEmpty(txtCardName.Text))
                {
                    MessageBox.Show("Please input card name", "Error");
                    return;
                }

                if (string.IsNullOrEmpty(txtAutoTipAmount.Text) || !decimal.TryParse(txtAutoTipAmount.Text, out decimal autoTipAmount) || autoTipAmount < 0)
                {
                    MessageBox.Show("Please input auto tip amount", "Error");
                    return;
                }

                //if (!IsQuantityValid)
                //{
                //    MessageBox.Show("Quantity invalid", "Error");
                //    return;
                //}

                #endregion Validation

                #region Create batch number

                var batchNumber = $"{transactionDate.ToString("ddMMyyyy")}" + serialBatchNumber.ToString("00");

                #endregion Create batch number

                #region Get glidebox items

                string glideboxItems = GetGlideboxItems();
                decimal glideboxAmount = Convert.ToDecimal(txtGlideboxAmount.Text);

                if (string.IsNullOrEmpty(glideboxItems))
                {
                    glideboxAmount = 0;
                }

                #endregion Get glidebox items

                var logonRequest = BuildContentMessageAS2085.CreateMessageLogonReuqest0800(txtTerminalID.Text, txtMerchantID.Text, txtSerialNumber.Text, txtModel.Text, batchNumber, transactionDate);

                var messageRequest0800 = new SocketTesterDetail()
                {
                    BatchNumber = batchNumber,
                    TerminalID = txtTerminalID.Text,
                    MessageTypeID = "0800",
                    RequestData = logonRequest
                };
                transactionHostEntities.SocketTesterDetails.Add(messageRequest0800);

                for (int i = 1; i <= numberTransactionPerBatch; i++)
                {
                    #region Create transaction number

                    var randomTransactionNumber = new Random().Next(1, 9999);
                    var transactionNumber = $"{batchNumber}" + randomTransactionNumber.ToString("0000");

                    #endregion Create transaction number

                    if (CheckForExistingTransaction(txtTerminalID.Text, txtMerchantID.Text, 1, "123456", ref transactionNumber))
                    {
                        MessageBox.Show("Invalid transaction number when creating message", "Error");
                        return;
                    }

                    var financialAdviceRequest = BuildContentMessageAS2085.CreateMessageFinancialAdvice0220(txtTerminalID.Text, txtMerchantID.Text, transactionAmountList[i - 1], glideboxItems, glideboxAmount, transactionNumber, transactionDate.AddMinutes(i), txtCardName.Text, autoTipAmount);

                    var messageRequest0220 = new SocketTesterDetail()
                    {
                        BatchNumber = batchNumber,
                        TerminalID = txtTerminalID.Text,
                        MessageTypeID = "0220",
                        RequestData = financialAdviceRequest,
                        TransactionAmount = transactionAmountList[i - 1],
                        TransactionNumber = transactionNumber,
                        GlideboxAmount = glideboxAmount,
                        GlideboxItems = glideboxItems,
                        AutoTipAmount = autoTipAmount
                    };
                    transactionHostEntities.SocketTesterDetails.Add(messageRequest0220);
                }

                var settlementRequest = BuildContentMessageAS2085.CreateMessageSettlementRequest0500(txtTerminalID.Text, txtMerchantID.Text, batchNumber, numberTransactionPerBatch, transactionAmountList.Sum(), (decimal)(numberTransactionPerBatch * glideboxAmount), transactionDate.AddMinutes(numberTransactionPerBatch + 1), txtSerialNumber.Text);

                var messageRequest0500 = new SocketTesterDetail()
                {
                    BatchNumber = batchNumber,
                    TerminalID = txtTerminalID.Text,
                    MessageTypeID = "0500",
                    RequestData = settlementRequest
                };
                transactionHostEntities.SocketTesterDetails.Add(messageRequest0500);

                var socketTesterSummary = new SocketTesterSummary()
                {
                    TerminalID = txtTerminalID.Text,
                    MerchantID = txtMerchantID.Text,
                    SerialNumber = txtSerialNumber.Text,
                    TerminalModel = txtSerialNumber.Text,
                    BatchNumber = batchNumber,
                    TotalAmount = transactionAmountList.Sum(),
                    TotalGlideboxAmount = (decimal)(numberTransactionPerBatch * glideboxAmount),
                    CreatedDatetime = DateTime.Now,
                    SentDate = DateTime.Now.Date
                };

                transactionHostEntities.SocketTesterSummaries.Add(socketTesterSummary);

                transactionHostEntities.SaveChanges();

                btnSendMessage.Enabled = true;
                txtSearchTerminalID.Text = txtTerminalID.Text;
                txtSearchBatchNumber.Text = batchNumber;
                dataGridViewResult.DataSource = null;

                MessageBox.Show("Done!", "Success");
            }
            catch (InvalidBatchNumberException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            finally
            {
                transactionHostEntities.Dispose();
            }
        }

        private void btnValidateTerminalID_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTerminalID.Text))
            {
                MessageBox.Show("Please input TerminalID", "Error");
                return;
            }

            txtMerchantID.Clear();
            txtSerialNumber.Clear();
            txtModel.Clear();
            txtBatchNumber.Clear();
            txtTransactionAmount.Clear();
            txtNumberTransactionPerBatch.Clear();
            txtGlideboxAmount.Text = "0.00";
            btnSendMessage.Enabled = false;
            txtSearchBatchNumber.Clear();
            txtSearchTerminalID.Clear();
            dataGridViewResult.DataSource = null;
            cbEnableGldiebox.Checked = false;
            try
            {
                using (var taxiepayEntities = new TaxiepayEntities())
                {
                    var terminalAllocated = taxiepayEntities.usp_SocketTesterTool_GetTerminalAllocated_v1().FirstOrDefault(x => x.TerminalId == txtTerminalID.Text);

                    if (terminalAllocated != null)
                    {
                        txtMerchantID.Text = terminalAllocated.MerchantID;
                        txtSerialNumber.Text = terminalAllocated.SerialNumber;
                        txtModel.Text = terminalAllocated.Model;
                        cbEnableGldiebox.Checked = terminalAllocated.EnableGlidebox;
                        
                        using (var transactionHostEntities1 = new TransactionHostEntities())
                        {
 
                            var terminalCategories = transactionHostEntities1.TerminalCategories.Where(x => x.TerminalID == txtTerminalID.Text && x.IsActive == true && terminalAllocated.EnableGlidebox == true)
                                                                                                .Join(transactionHostEntities1.GlideBoxCategories, tc => tc.CategoryID, c => c.CategoryID, (tc, c) =>
                                                                                                new
                                                                                                {
                                                                                                    CategoryID = c.CategoryID,
                                                                                                    CategoryName = c.CategoryName,
                                                                                                    Price = c.Price
                                                                                                }).ToList();

                            //if (terminalCategories != null)
                            //{
                            //    dataGridViewGlidebox.DataSource = terminalCategories;
                            //}

                            if (dataGridViewGlidebox.Rows.Count == 0)
                            {
                                IsQuantityValid = true;
                            }
                        }
                        MessageBox.Show("TerminalID valid", "Sucesss");
                    }
                    else
                    {
                        MessageBox.Show("TerminalID invalid", "Error");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("System Error", "Error");
            }
        }

        private string GetGlideboxItems()
        {
            //List<string> glideboxItems = new List<string>();

            //foreach (DataGridViewRow row in dataGridViewGlidebox.Rows)
            //{
            //    var categoryID = row.Cells["CategoryID"].Value;
            //    var categoryName = row.Cells["CategoryName"].Value;
            //    var price = row.Cells["Price"].Value;

            //    if (int.TryParse(row.Cells["Quantity"].Value.ToString(), out int quantity) && quantity > 0)
            //    {
            //        string glideboxItem = $"{categoryID},{quantity},{(Convert.ToInt32(quantity) * Convert.ToDecimal(price)).ToString("#.##")}";
            //        glideboxItems.Add(glideboxItem);
            //    }
            //}

            //if (glideboxItems.Count > 0)
            //{
            //    return string.Join("|", glideboxItems);
            //}

            return "";
        }

        private void dataGridViewGlidebox_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            decimal total = 0;

            foreach (DataGridViewRow row in dataGridViewGlidebox.Rows)
            {
                var price = row.Cells["Price"].Value;

                if (row.Cells["Quantity"].Value != null)
                {
                    if (!int.TryParse(row.Cells["Quantity"].Value.ToString(), out int quantity) || quantity < 0)
                    {
                        MessageBox.Show("Quantity invalid", "Error");
                        IsQuantityValid = false;
                        total = 0;
                    }
                    else
                    {
                        total += quantity * Convert.ToDecimal(price);
                        IsQuantityValid = true;
                    }
                }
            }

            txtGlideboxAmount.Text = total.ToString();
        }

        private bool CheckForExistingTransaction(string _terminalid, string _merchantID, int _transactionType, string _tranauth, ref string _transactionNumber)
        {
            using (TransactionHostEntities entities = new TransactionHostEntities())
            {
                int? check = entities.CheckTransactionExistence(_merchantID, _terminalid, _tranauth, _transactionType, _transactionNumber).FirstOrDefault();
                if (check == 0)
                {
                    return false;
                }
                else
                {
                    string batchserie = _transactionNumber.Substring(8, 4); //get the last four digits
                    int batchserieno = 0;
                    if (int.TryParse(batchserie, out batchserieno))
                    {
                        if (batchserieno == 0)
                        {
                            throw new InvalidBatchNumberException(string.Format("TerminalID: {0}. Transaction Number: {1}. MerchantID: {2}", _terminalid, _transactionNumber, _merchantID));
                        }
                        else
                        {
                            batchserieno++; //increase batch number by 1
                            _transactionNumber = _transactionNumber.Substring(0, 8) + batchserieno.ToString().PadLeft(4, '0');

                            return CheckForExistingTransaction(_terminalid, _merchantID, _transactionType, _tranauth, ref _transactionNumber);
                        }
                    }
                    else
                    {
                        throw new InvalidBatchNumberException(string.Format("TerminalID: {0}. Transaction Number: {1}. MerchantID: {2}", _terminalid, _transactionNumber, _merchantID));
                    }
                }
            }
        }

        private void btnSendMessage_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(Application.StartupPath + "\\SocketTester.exe");
            }
            catch (Exception ex)
            {
                MessageBox.Show("System Error", "Error");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            using (var transactionHostEntities = new TransactionHostEntities())
            {
                var socketTesterDetails = transactionHostEntities.SocketTesterDetails.Where(x => x.TerminalID == txtSearchTerminalID.Text && x.BatchNumber == txtSearchBatchNumber.Text).OrderByDescending(x => x.ID).ToList();
                dataGridViewResult.DataSource = socketTesterDetails;

                MessageBox.Show("Done!", "Sucesss");
            }
        }

        private void CreateMessageForm_Load(object sender, EventArgs e)
        {
            var filenames = Enum.GetNames(typeof(FileName));
            var dataSource = new List<SelectListItem>();

            foreach (var item in filenames)
            {
                Enum.TryParse(item.ToString(), out FileName fileNameValue);

                dataSource.Add(new SelectListItem() { Name = item.ToString(), Value = StringEnum.GetStringValue(fileNameValue) });
            }

            comboBoxFileNameRequest.DataSource = dataSource;
            comboBoxFileNameRequest.DisplayMember = "Name";
            comboBoxFileNameRequest.ValueMember = "Value";
        }

        private void btnCreateMessage0300_Click(object sender, EventArgs e)
        {
            if (!ValidateTerminalID())
            {
                return;
            }
            string fileName = string.Empty;

            if (string.IsNullOrEmpty(comboBoxFileNameRequest.SelectedValue.ToString()))
            {
                MessageBox.Show("Please select file name", "Error");
                return;
            }
            else
            {
                fileName = comboBoxFileNameRequest.SelectedValue.ToString();

                if (comboBoxFileNameRequest.SelectedValue.ToString() == "OTHER")
                {
                    if (string.IsNullOrEmpty(txtOtherFileName.Text))
                    {
                        MessageBox.Show("Please input file name", "Error");
                        return;
                    }
                    else
                    {
                        fileName = txtOtherFileName.Text;
                    }
                }
            }

            var transactionHostEntities = new TransactionHostEntities();

            try
            {
                var tableDownloadRequest = BuildContentMessageAS2085.CreateMessageTableDownloadRequest0300(txtTerminalID.Text, txtMerchantID.Text, fileName);

                var messageRequest0300 = new SocketTesterDetail()
                {
                    BatchNumber = "",
                    TerminalID = txtTerminalID.Text,
                    MessageTypeID = "0300",
                    RequestData = tableDownloadRequest
                };
                transactionHostEntities.SocketTesterDetails.Add(messageRequest0300);

                var socketTesterSummary = new SocketTesterSummary()
                {
                    TerminalID = txtTerminalID.Text,
                    MerchantID = txtMerchantID.Text,
                    SerialNumber = txtSerialNumber.Text,
                    TerminalModel = txtSerialNumber.Text,
                    BatchNumber = "",
                    CreatedDatetime = DateTime.Now,
                    SentDate = DateTime.Now.Date
                };

                transactionHostEntities.SocketTesterSummaries.Add(socketTesterSummary);
                transactionHostEntities.SaveChanges();

                txtSearchTerminalID.Text = txtTerminalID.Text;
                txtSearchBatchNumber.Text = "";

                btnSendMessage.Enabled = true;
                MessageBox.Show("Done!", "Sucesss");
            }
            catch (Exception)
            {
                MessageBox.Show("System Error", "Error");
            }
            finally
            {
                transactionHostEntities.Dispose();
            }
        }

        private bool ValidateTerminalID()
        {
            if (string.IsNullOrEmpty(txtTerminalID.Text))
            {
                MessageBox.Show("Please input TerminalID", "Error");
                return false;
            }

            if (string.IsNullOrEmpty(txtMerchantID.Text) || string.IsNullOrEmpty(txtSerialNumber.Text) || string.IsNullOrEmpty(txtModel.Text))
            {
                MessageBox.Show("TerminalID invalid. Please input new TerminalID and validate again.", "Error");
                return false;
            }

            return true;
        }

        private void btnParser_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBoxMessageParsser.Clear();

                if (string.IsNullOrEmpty(richTextBoxMessageAS2805.Text))
                {
                    MessageBox.Show("Please input message as2805", "Error");
                    return;
                }

                LiveMessageFactory converter = new LiveMessageFactory();
                AS805Packer packer = new AS805Packer();

                byte[] dataToSend = null;
                if (radioButtonFormatDebugLog.Checked)
                {
                    dataToSend = DataHelper.DebugDataToByteArray(richTextBoxMessageAS2805.Text);
                }

                if (radioButtonFormatNonDebugLog.Checked)
                {
                    dataToSend = DataHelper.HexStringToByteArray(richTextBoxMessageAS2805.Text.Trim());
                }
                
                AS2805Message as2805Message = (AS2805Message)packer.Unpack(dataToSend, 0, dataToSend.Length);

                LiveMessage liveRequest = converter.ToLiveMessage(as2805Message);

                string message = JsonConvert.SerializeObject(liveRequest, Formatting.Indented);
                richTextBoxMessageParsser.Text = message;
                MessageBox.Show("Done!", "Sucesss");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not parser message as2805 to live object. Please contact with Son to support.", "Error");
            }
        }

        private void comboBoxFileNameRequest_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxFileNameRequest.SelectedValue.ToString() == "OTHER")
            {
                lblOtherFileName.Visible = true;
                txtOtherFileName.Visible = true;
            }
            else
            {
                lblOtherFileName.Visible = false;
                txtOtherFileName.Visible = false;
            }
        }
    }
}