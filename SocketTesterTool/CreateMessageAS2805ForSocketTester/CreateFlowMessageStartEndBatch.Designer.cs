﻿namespace CreateMessageAS2805ForSocketTester
{
    partial class CreateFlowMessageStartEndBatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNumberBatchPerDay = new System.Windows.Forms.TextBox();
            this.lblNumberTransactionPerBatch = new System.Windows.Forms.Label();
            this.lblNumberBatchPerDay = new System.Windows.Forms.Label();
            this.txtNumberTransactionPerBatch = new System.Windows.Forms.TextBox();
            this.btnCreateMessage = new System.Windows.Forms.Button();
            this.groupBoxConfiguration = new System.Windows.Forms.GroupBox();
            this.txtSerialBatchNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerToDate = new System.Windows.Forms.DateTimePicker();
            this.lblToDate = new System.Windows.Forms.Label();
            this.dateTimePickerFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.txtNumberTerminal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxConfiguration.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNumberBatchPerDay
            // 
            this.txtNumberBatchPerDay.Location = new System.Drawing.Point(236, 68);
            this.txtNumberBatchPerDay.Name = "txtNumberBatchPerDay";
            this.txtNumberBatchPerDay.Size = new System.Drawing.Size(319, 22);
            this.txtNumberBatchPerDay.TabIndex = 0;
            // 
            // lblNumberTransactionPerBatch
            // 
            this.lblNumberTransactionPerBatch.AutoSize = true;
            this.lblNumberTransactionPerBatch.Location = new System.Drawing.Point(18, 146);
            this.lblNumberTransactionPerBatch.Name = "lblNumberTransactionPerBatch";
            this.lblNumberTransactionPerBatch.Size = new System.Drawing.Size(212, 17);
            this.lblNumberTransactionPerBatch.TabIndex = 1;
            this.lblNumberTransactionPerBatch.Text = "Number of transaction per batch";
            // 
            // lblNumberBatchPerDay
            // 
            this.lblNumberBatchPerDay.AutoSize = true;
            this.lblNumberBatchPerDay.Location = new System.Drawing.Point(18, 71);
            this.lblNumberBatchPerDay.Name = "lblNumberBatchPerDay";
            this.lblNumberBatchPerDay.Size = new System.Drawing.Size(165, 17);
            this.lblNumberBatchPerDay.TabIndex = 12;
            this.lblNumberBatchPerDay.Text = "Number of batch per day";
            // 
            // txtNumberTransactionPerBatch
            // 
            this.txtNumberTransactionPerBatch.Location = new System.Drawing.Point(236, 146);
            this.txtNumberTransactionPerBatch.Name = "txtNumberTransactionPerBatch";
            this.txtNumberTransactionPerBatch.Size = new System.Drawing.Size(319, 22);
            this.txtNumberTransactionPerBatch.TabIndex = 13;
            // 
            // btnCreateMessage
            // 
            this.btnCreateMessage.Location = new System.Drawing.Point(223, 305);
            this.btnCreateMessage.Name = "btnCreateMessage";
            this.btnCreateMessage.Size = new System.Drawing.Size(132, 33);
            this.btnCreateMessage.TabIndex = 14;
            this.btnCreateMessage.Text = "Create Message";
            this.btnCreateMessage.UseVisualStyleBackColor = true;
            this.btnCreateMessage.Click += new System.EventHandler(this.btnCreateMessage_Click);
            // 
            // groupBoxConfiguration
            // 
            this.groupBoxConfiguration.Controls.Add(this.txtSerialBatchNumber);
            this.groupBoxConfiguration.Controls.Add(this.label2);
            this.groupBoxConfiguration.Controls.Add(this.dateTimePickerToDate);
            this.groupBoxConfiguration.Controls.Add(this.lblToDate);
            this.groupBoxConfiguration.Controls.Add(this.dateTimePickerFromDate);
            this.groupBoxConfiguration.Controls.Add(this.lblFromDate);
            this.groupBoxConfiguration.Controls.Add(this.txtNumberTerminal);
            this.groupBoxConfiguration.Controls.Add(this.label1);
            this.groupBoxConfiguration.Controls.Add(this.lblNumberBatchPerDay);
            this.groupBoxConfiguration.Controls.Add(this.txtNumberBatchPerDay);
            this.groupBoxConfiguration.Controls.Add(this.txtNumberTransactionPerBatch);
            this.groupBoxConfiguration.Controls.Add(this.lblNumberTransactionPerBatch);
            this.groupBoxConfiguration.Location = new System.Drawing.Point(12, 12);
            this.groupBoxConfiguration.Name = "groupBoxConfiguration";
            this.groupBoxConfiguration.Size = new System.Drawing.Size(566, 275);
            this.groupBoxConfiguration.TabIndex = 15;
            this.groupBoxConfiguration.TabStop = false;
            this.groupBoxConfiguration.Text = "Configuration";
            // 
            // txtSerialBatchNumber
            // 
            this.txtSerialBatchNumber.Location = new System.Drawing.Point(236, 107);
            this.txtSerialBatchNumber.Name = "txtSerialBatchNumber";
            this.txtSerialBatchNumber.Size = new System.Drawing.Size(319, 22);
            this.txtSerialBatchNumber.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Serial batch number";
            // 
            // dateTimePickerToDate
            // 
            this.dateTimePickerToDate.Location = new System.Drawing.Point(236, 229);
            this.dateTimePickerToDate.Name = "dateTimePickerToDate";
            this.dateTimePickerToDate.Size = new System.Drawing.Size(319, 22);
            this.dateTimePickerToDate.TabIndex = 19;
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.Location = new System.Drawing.Point(18, 224);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(59, 17);
            this.lblToDate.TabIndex = 18;
            this.lblToDate.Text = "To Date";
            // 
            // dateTimePickerFromDate
            // 
            this.dateTimePickerFromDate.Location = new System.Drawing.Point(236, 189);
            this.dateTimePickerFromDate.Name = "dateTimePickerFromDate";
            this.dateTimePickerFromDate.Size = new System.Drawing.Size(319, 22);
            this.dateTimePickerFromDate.TabIndex = 17;
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Location = new System.Drawing.Point(18, 189);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(74, 17);
            this.lblFromDate.TabIndex = 16;
            this.lblFromDate.Text = "From Date";
            // 
            // txtNumberTerminal
            // 
            this.txtNumberTerminal.Location = new System.Drawing.Point(236, 30);
            this.txtNumberTerminal.Name = "txtNumberTerminal";
            this.txtNumberTerminal.Size = new System.Drawing.Size(319, 22);
            this.txtNumberTerminal.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Number of terminal per day";
            // 
            // CreateFlowMessageStartEndBatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 360);
            this.Controls.Add(this.groupBoxConfiguration);
            this.Controls.Add(this.btnCreateMessage);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateFlowMessageStartEndBatch";
            this.Text = "Create Flow Message Start End Batch";
            this.groupBoxConfiguration.ResumeLayout(false);
            this.groupBoxConfiguration.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtNumberBatchPerDay;
        private System.Windows.Forms.Label lblNumberTransactionPerBatch;
        private System.Windows.Forms.Label lblNumberBatchPerDay;
        private System.Windows.Forms.TextBox txtNumberTransactionPerBatch;
        private System.Windows.Forms.Button btnCreateMessage;
        private System.Windows.Forms.GroupBox groupBoxConfiguration;
        private System.Windows.Forms.TextBox txtNumberTerminal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerFromDate;
        private System.Windows.Forms.Label lblToDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerToDate;
        private System.Windows.Forms.TextBox txtSerialBatchNumber;
        private System.Windows.Forms.Label label2;
    }
}