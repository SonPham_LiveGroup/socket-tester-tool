﻿using Comms.Message.AS2805;
using Comms.Message.Common;
using CreateMessageAS2805ForSocketTester.Entities;
using CreateMessageAS2805ForSocketTester.Models;
using Host.Shared;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Util.Data;
using Util.Tlv;
using Z.Dapper.Plus;

namespace CreateMessageAS2805ForSocketTester
{
    public partial class CreateFlowMessageStartEndBatch : Form
    {
        public CreateFlowMessageStartEndBatch()
        {
            InitializeComponent();
        }

        private void btnCreateMessage_Click(object sender, EventArgs e)
        {
            try
            {
                #region Validation

                DateTime.TryParse(dateTimePickerFromDate.Text, out DateTime fromDateTransaction);
                DateTime.TryParse(dateTimePickerToDate.Text, out DateTime toDateTransaction);

                if (fromDateTransaction > toDateTransaction)
                {
                    MessageBox.Show("Please choose From Date <= To Date", "Error");
                    return;
                }

                if (string.IsNullOrEmpty(txtNumberTerminal.Text) || !int.TryParse(txtNumberTerminal.Text, out int numberTerminal) || numberTerminal <= 0)
                {
                    MessageBox.Show("Please input number of transaction per batch", "Error");
                    return;
                }

                if (string.IsNullOrEmpty(txtNumberBatchPerDay.Text) || !int.TryParse(txtNumberBatchPerDay.Text, out int numberBatchPerDay) || numberBatchPerDay <= 0)
                {
                    MessageBox.Show("Please input number of batch per day", "Error");
                    return;
                }

                if (string.IsNullOrEmpty(txtNumberTransactionPerBatch.Text) || !int.TryParse(txtNumberTransactionPerBatch.Text, out int numberTransactionPerBatch) || numberTransactionPerBatch <= 0)
                {
                    MessageBox.Show("Please input number of transaction per batch", "Error");
                    return;
                }

                if (string.IsNullOrEmpty(txtSerialBatchNumber.Text) || !int.TryParse(txtSerialBatchNumber.Text, out int serialBatchNumber))
                {
                    MessageBox.Show("Please input serial batch number", "Error");
                    return;
                }

                if (serialBatchNumber <= 0 || serialBatchNumber > 99)
                {
                    MessageBox.Show("Serial batch number between 1 and 99", "Error");
                    return;
                }

                if (numberBatchPerDay + serialBatchNumber > 99)
                {
                    MessageBox.Show("Number of batch per day + serial batch number must be less than or equal 99", "Error");
                    return;
                }

                #endregion Validation

                #region Get list terminal allocated

                var listTerminalAllocated = new List<usp_SocketTesterTool_GetTerminalAllocated_v1_Result>();

                using (var taxiepayEntities = new TaxiepayEntities())
                {
                    listTerminalAllocated = taxiepayEntities.usp_SocketTesterTool_GetTerminalAllocated_v1().Where(n => n != null).ToList();
                }

                #endregion Get list terminal allocated

                #region Get random list terminal allocated

                listTerminalAllocated = RandomList(listTerminalAllocated, numberTerminal);

                #endregion Get random list terminal allocated

                var transactionHostEntities = new TransactionHostEntities();
                var lstSocketTesterSummary = new List<SocketTesterSummary>();
                var lst = new List<SocketTesterDetail>();
                var loopFromDateTransaction = fromDateTransaction;
                string sqlConnectStr = transactionHostEntities.Database.Connection.ConnectionString;

                //Start count timer
                var watcher = new Stopwatch();
                watcher.Start();

                SqlConnection connection = new SqlConnection(sqlConnectStr);
                //Start build data for everyday.
                //We will loop for every day and base on the list of terminal and total of batch and total of transaction per batch. After perform build data for 1 day, perform save to database via Dapper Plus.
                while (loopFromDateTransaction <= toDateTransaction)
                {
                    //Build data for Summary and Detail
                    RunWithTerminalList(lstSocketTesterSummary, lst, listTerminalAllocated, loopFromDateTransaction,
                        numberBatchPerDay, numberTransactionPerBatch, serialBatchNumber);

                    //Bulk insert to database for everyday to avoid large data will flush to database at the same time.
                    connection.BulkInsert(lstSocketTesterSummary);
                    connection.BulkInsert(lst);
                    //Renew the data list
                    lstSocketTesterSummary.Clear();
                    lst.Clear();
                    loopFromDateTransaction = loopFromDateTransaction.AddDays(1);
                }
                //End count timer
                watcher.Stop();
                MessageBox.Show($"Done! {numberTerminal * numberBatchPerDay} batchs created (Total {numberTerminal * numberBatchPerDay * numberTransactionPerBatch} transactions) in {watcher.ElapsedMilliseconds / 1000} secs", "Success");
            }
            catch (InvalidBatchNumberException ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void RunWithTerminalList(List<SocketTesterSummary> lstSocketTesterSummary, List<SocketTesterDetail> lst, List<usp_SocketTesterTool_GetTerminalAllocated_v1_Result> listTerminalAllocated, DateTime sendDate, int numberBatchPerDay, int numberTransactionPerBatch, int serialBatchNumber)
        {
            foreach (var item in listTerminalAllocated)
            {
                #region Create batch
                for (int i = 0; i < numberBatchPerDay; i++)
                {
                    #region Create batch number
                    
                    var batchNumber = $"{sendDate.ToString("ddMMyyyy")}" + (i + serialBatchNumber).ToString("00");

                    //if (CheckLogonExistence(terminal.TerminalID, terminal.MerchantID, terminal.SerialNumber, ref batchNumber))
                    //{
                    //    MessageBox.Show("Invalid batch number when creating message", "Error");
                    //    return;
                    //}

                    #endregion Create batch number

                    #region Create message logon request 0800

                    string logonRequest = CreateMessageLogonReuqest0800(item.TerminalId, item.MerchantID, item.SerialNumber, item.Model, batchNumber);

                    var messageRequest0800 = new SocketTesterDetail()
                    {
                        BatchNumber = batchNumber,
                        TerminalID = item.TerminalId,
                        MessageTypeID = "0800",
                        RequestData = logonRequest
                    };
                    lst.Add(messageRequest0800);

                    #endregion Create message logon request 0800

                    #region Create message financial advice 0220

                    var listTransactionAmount = new List<decimal>();
                    for (int j = 1; j <= numberTransactionPerBatch; j++)
                    {
                        var transactionNumber = $"{batchNumber}" + j.ToString("0000");
                        decimal randomTransactionAmount = new Random().Next(1, 50);
                        //TODO: Check availabled.
                        //if (CheckForExistingTransaction(terminal.TerminalID, terminal.MerchantID, 1, "123456", ref transactionNumber))
                        //{
                        //    MessageBox.Show("Invalid transaction number when creating message", "Error");
                        //    return;
                        //}

                        listTransactionAmount.Add(randomTransactionAmount);

                        string financialAdviceRequest = CreateMessageFinancialAdvice0220(item.TerminalId, item.MerchantID, randomTransactionAmount, transactionNumber, sendDate);
                        var messageRequest0220 = new SocketTesterDetail()
                        {
                            BatchNumber = batchNumber,
                            TerminalID = item.TerminalId,
                            MessageTypeID = "0220",
                            RequestData = financialAdviceRequest,
                            TransactionAmount = randomTransactionAmount,
                            TransactionNumber = transactionNumber
                        };
                        lst.Add(messageRequest0220);
                    }

                    #endregion Create message financial advice 0220

                    #region Create message settlement request 0500

                    string settlementRequest = CreateMessageSettlementRequest0500(item.TerminalId, item.MerchantID, batchNumber, listTransactionAmount.Count, listTransactionAmount.Sum(), sendDate, item.SerialNumber);
                    var messageRequest0500 = new SocketTesterDetail()
                    {
                        BatchNumber = batchNumber,
                        TerminalID = item.TerminalId,
                        MessageTypeID = "0500",
                        RequestData = settlementRequest
                    };
                    lst.Add(messageRequest0500);

                    #endregion Create message settlement request 0500

                    #region Build batch information before insert into database

                    try
                    {
                        var socketTesterSummary = new SocketTesterSummary()
                        {
                            TerminalID = item.TerminalId,
                            MerchantID = item.MerchantID,
                            SerialNumber = item.SerialNumber,
                            TerminalModel = item.Model,
                            BatchNumber = batchNumber,
                            TotalAmount = listTransactionAmount.Sum(),
                            CreatedDatetime = DateTime.Now,
                            SentDate = sendDate
                        };

                        lstSocketTesterSummary.Add(socketTesterSummary);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }

                    #endregion Build batch information before insert into database
                }
                #endregion Create batch
            }
        }

        //private void Run(List<SocketTesterSummary> lstSocketTesterSummary, List<SocketTesterDetail> lst, usp_SocketTesterTool_GetTerminalAllocated_v1_Result item, DateTime fromDateTransaction, DateTime toDateTransaction, int numberBatchPerDay, int numberTransactionPerBatch)
        //{
        //    #region Get terminal allocated detail

        //    if (item == null)
        //    {
        //        MessageBox.Show("Invalid terminal id when get terminal allocated", "Error");
        //        return;
        //    }
        //    #endregion Get terminal allocated detail

        //    var loopfromDateTransaction = fromDateTransaction;

        //    #region Create batch

        //    while (loopfromDateTransaction <= toDateTransaction)
        //    {
        //        string batchNumberFormat = $"{fromDateTransaction.ToString("ddMMyyyy")}";
        //        for (int i = 1; i <= numberBatchPerDay; i++)
        //        {
        //            var batchMessageRequests = new List<BatchMessageRequestModel>();

        //            #region Create batch number

        //            var batchNumber = batchNumberFormat + i.ToString("00");

        //            //if (CheckLogonExistence(terminal.TerminalID, terminal.MerchantID, terminal.SerialNumber, ref batchNumber))
        //            //{
        //            //    MessageBox.Show("Invalid batch number when creating message", "Error");
        //            //    return;
        //            //}

        //            #endregion Create batch number

        //            #region Create message logon request 0800

        //            string logonRequest = CreateMessageLogonReuqest0800(item.TerminalId, item.MerchantID, item.SerialNumber, item.Model, batchNumber);

        //            var messageRequest0800 = new SocketTesterDetail()
        //            {
        //                BatchNumber = batchNumber,
        //                TerminalID = item.TerminalId,
        //                MessageTypeID = "0800",
        //                RequestData = logonRequest
        //            };
        //            lst.Add(messageRequest0800);

        //            #endregion Create message logon request 0800

        //            #region Create message financial advice 0220

        //            var listTransactionNumber = new List<string>();
        //            var listTransactionAmount = new List<decimal>();
        //            string transactionNumberFormat = $"{batchNumber}";
        //            for (int j = 1; j <= numberTransactionPerBatch; j++)
        //            {
        //                var transactionNumber = transactionNumberFormat + j.ToString("0000");
        //                decimal randomTransactionAmount = new Random().Next(1, 50);
        //                //TODO: Check availabled.
        //                //if (CheckForExistingTransaction(terminal.TerminalID, terminal.MerchantID, 1, "123456", ref transactionNumber))
        //                //{
        //                //    MessageBox.Show("Invalid transaction number when creating message", "Error");
        //                //    return;
        //                //}

        //                listTransactionNumber.Add(transactionNumber);
        //                listTransactionAmount.Add(randomTransactionAmount);

        //                string financialAdviceRequest = CreateMessageFinancialAdvice0220(item.TerminalId, item.MerchantID, randomTransactionAmount, transactionNumber);
        //                var messageRequest0220 = new SocketTesterDetail()
        //                {
        //                    BatchNumber = batchNumber,
        //                    TerminalID = item.TerminalId,
        //                    MessageTypeID = "0220",
        //                    RequestData = financialAdviceRequest,
        //                    TransactionAmount = randomTransactionAmount,
        //                    TransactionNumber = transactionNumber
        //                };
        //                lst.Add(messageRequest0220);
        //            }

        //            #endregion Create message financial advice 0220

        //            #region Create message settlement request 0500

        //            string settlementRequest = CreateMessageSettlementRequest0500(item.TerminalId, item.MerchantID, batchNumber, listTransactionAmount.Count, listTransactionAmount.Sum());
        //            var messageRequest0500 = new SocketTesterDetail()
        //            {
        //                BatchNumber = batchNumber,
        //                TerminalID = item.TerminalId,
        //                MessageTypeID = "0500",
        //                RequestData = settlementRequest
        //            };
        //            lst.Add(messageRequest0500);

        //            #endregion Create message settlement request 0500

        //            #region Insert batch information into database

        //            try
        //            {
        //                var socketTesterSummary = new SocketTesterSummary()
        //                {
        //                    TerminalID = item.TerminalId,
        //                    MerchantID = item.MerchantID,
        //                    SerialNumber = item.SerialNumber,
        //                    TerminalModel = item.Model,
        //                    BatchNumber = batchNumber,
        //                    TotalAmount = listTransactionAmount.Sum(),
        //                    CreatedDatetime = DateTime.Now,
        //                    SentDate = loopfromDateTransaction
        //                };

        //                lstSocketTesterSummary.Add(socketTesterSummary);
        //            }
        //            catch (Exception ex)
        //            {
        //                MessageBox.Show(ex.Message);
        //                return;
        //            }

        //            #endregion Insert batch information into database
        //        }
        //        loopfromDateTransaction = loopfromDateTransaction.AddDays(1);
        //    }

        //    #endregion Create batch


        //}

        private bool CheckLogonExistence(string _terminalID, string _merchantID, string _serialnumber, ref string _batchnumber)
        {
            if (!string.IsNullOrEmpty(_batchnumber) && _batchnumber.Length == 10)
            {
                DateTime dt = new DateTime(Convert.ToInt32(_batchnumber.Substring(4, 4)), Convert.ToInt32(_batchnumber.Substring(2, 2)), Convert.ToInt32(_batchnumber.Substring(0, 2)));
                if ((dt > DateTime.Now.AddDays(1)) || (dt < DateTime.Now.AddMonths(-1)))
                {
                    throw new InvalidBatchNumberException(string.Format("TerminalID: {0}. Batch Number: {1}. MerchantID: {2}", _terminalID, _batchnumber, _merchantID));
                }
                else
                {
                    int count = 0;
                    int openbatch; //open batch, different serial number
                    string batchnumber = _batchnumber;
                    using (TransactionHostEntities entities = new TransactionHostEntities())
                    {
                        count = entities.SettlementHistories.Count(r => r.TerminalID == _terminalID && r.MerchantID == _merchantID && r.BatchNumber == batchnumber); //(r => r.TerminalID == _terminalID && r.MerchantID == _merchantID && r.BatchNumber == _batchnumber).ToList<SettlementHistory>();
                        if (!string.IsNullOrEmpty(_serialnumber))
                            openbatch = entities.SettlementLogs.Count(r => r.TerminalID == _terminalID && r.MerchantID == _merchantID && r.Batchnumber == batchnumber && r.TerminalSerialNo != _serialnumber);
                        else
                        {
                            openbatch = 0;
                        }
                    }

                    if (count == 0 && openbatch == 0)
                    {
                        return false; //no existing batch
                    }
                    else
                    {
                        string batchserie = _batchnumber.Substring(8, 2); //get the last two digits
                        int batchserieno = 0;
                        if (int.TryParse(batchserie, out batchserieno))
                        {
                            if (batchserieno == 0)
                            {
                                throw new InvalidBatchNumberException(string.Format("TerminalID: {0}. Batch Number: {1}. MerchantID: {2}", _terminalID, _batchnumber, _merchantID));
                            }
                            else
                            {
                                batchserieno++; //increase batch number by 1
                                _batchnumber = _batchnumber.Substring(0, 8) + batchserieno.ToString().PadLeft(2, '0');
                                return CheckLogonExistence(_terminalID, _merchantID, _serialnumber, ref _batchnumber);
                            }
                        }
                        else
                        {
                            throw new InvalidBatchNumberException(string.Format("TerminalID: {0}. Batch Number: {1}. MerchantID: {2}", _terminalID, _batchnumber, _merchantID));
                        }
                    }
                }
            }
            else
            {
                throw new InvalidBatchNumberException(string.Format("TerminalID: {0}. Batch Number: {1}. MerchantID: {2}", _terminalID, _batchnumber, _merchantID));
            }
        }

        private bool CheckForExistingTransaction(string _terminalid, string _merchantID, int _transactionType, string _tranauth, ref string _transactionNumber)
        {
            using (TransactionHostEntities entities = new TransactionHostEntities())
            {
                int? check = entities.CheckTransactionExistence(_merchantID, _terminalid, _tranauth, _transactionType, _transactionNumber).FirstOrDefault();
                if (check == 0)
                {
                    return false;
                }
                else
                {
                    string batchserie = _transactionNumber.Substring(8, 4); //get the last four digits
                    int batchserieno = 0;
                    if (int.TryParse(batchserie, out batchserieno))
                    {
                        if (batchserieno == 0)
                        {
                            throw new InvalidBatchNumberException(string.Format("TerminalID: {0}. Transaction Number: {1}. MerchantID: {2}", _terminalid, _transactionNumber, _merchantID));
                        }
                        else
                        {
                            batchserieno++; //increase batch number by 1
                            _transactionNumber = _transactionNumber.Substring(0, 8) + batchserieno.ToString().PadLeft(4, '0');

                            return CheckForExistingTransaction(_terminalid, _merchantID, _transactionType, _tranauth, ref _transactionNumber);
                        }
                    }
                    else
                    {
                        throw new InvalidBatchNumberException(string.Format("TerminalID: {0}. Transaction Number: {1}. MerchantID: {2}", _terminalid, _transactionNumber, _merchantID));
                    }
                }
            }
        }

        private string CreateMessageLogonReuqest0800(string terminalID, string merchantID, string serialNumber, string terminalModel, string batchNumber)
        {
            var request = new AS2805Message();
            var tags = new TagList();

            //Common field
            request.TerminalID = terminalID;
            request.MerchantID = merchantID;
            request.STAN = 1;
            request.MessageTypeIdentifier = MessageTypeIdentifiers.NetworkManagementRequest;
            request.ProcessingCode = 910000; //Logon request

            //Private field 63
            tags.AddValueTagString(TlvTagIds.LastTableUpdateTime, DateTime.Now.ToString("yyyyMMddhhmmss"));
            tags.AddValueTagString(TlvTagIds.UserId, "12345");
            tags.AddValueTagString(TlvTagIds.BusinessId, "12345");
            tags.AddValueTagString(TlvTagIds.ABN, "1234543423");
            tags.AddValueTagString(TlvTagIds.NetworkId, "1234543423");
            tags.AddValueTagString(TlvTagIds.SerialNumber, serialNumber);
            tags.AddValueTagString(TlvTagIds.TerminalModel, terminalModel);
            tags.AddValueTagString(TlvTagIds.IMEI, "gsm2423432");
            tags.AddValueTagString(TlvTagIds.SimSerialNo, "simserial312");
            tags.AddValueTagString(TlvTagIds.SimMobileNumber, "0423123456");
            tags.AddValueTagString(TlvTagIds.PTID, "98765432");
            tags.AddValueTagString(TlvTagIds.BatchNumber, batchNumber);
            tags.AddValueTagString(TlvTagIds.BatteryStatus, "2000");
            tags.AddValueTagString(TlvTagIds.SignalStrength, "20001");
            tags.AddValueTagString(TlvTagIds.TerminalOSSoftwareVer, "OS123123");
            tags.AddValueTagString(TlvTagIds.VAASoftwareVer, "VAA123123");
            tags.AddValueTagString(TlvTagIds.ApplicationType, "FFFFFF");
            tags.AddValueTagString(TlvTagIds.TerminalConfigVersion, "000001");
            tags.AddValueTagString(TlvTagIds.ReceiptVerNo, "000001");
            tags.AddValueTagString(TlvTagIds.PrinterLogoVer, "000001");
            tags.AddValueTagString(TlvTagIds.CardTableVer, "000001");
            tags.AddValueTagString(TlvTagIds.PromptTableVer, "000001");
            tags.AddValueTagString(TlvTagIds.NetworkTableVer, "000001");
            tags.AddValueTagString(TlvTagIds.TransactionDateTime, DateTime.Now.ToString("yyyyMMddhhmmss"));
            tags.AddValueTagString(TlvTagIds.ScreenLogoVer, "");
            tags.AddValueTagString(TlvTagIds.TerminalFSVer, "");
            tags.AddValueTagString(TlvTagIds.TipTrickTableVer, "");
            tags.AddValueTagString(TlvTagIds.LocationTableVer, "");
            tags.AddValueTagString(TlvTagIds.CellTowerTableVer, "");
            tags.AddValueTagString(TlvTagIds.OnlineOrderConfigVer, "");
            tags.AddValueTagString(TlvTagIds.OnlineOrderLogoVer, "");
            tags.AddValueTagString(TlvTagIds.DeliveryLogoVer, "");
            tags.AddValueTagString(TlvTagIds.PickupLogoVer, "");
            tags.AddValueTagString(TlvTagIds.CellTowerMCC, "");
            tags.AddValueTagString(TlvTagIds.CellTowerMNC, "");
            tags.AddValueTagString(TlvTagIds.CellTowerLAC, "");
            tags.AddValueTagString(TlvTagIds.CellTowerCID, "");

            request.PrivateData63 = DataHelper.ByteArrayToString(tags.ToByteArray());

            AS805Packer packer = new AS805Packer();
            byte[] sendBuffer = packer.Pack((CommsMessage)request);

            var messageRequest = DataHelper.ByteArrayToHexString(sendBuffer);

            return messageRequest;
        }

        private string CreateMessageFinancialAdvice0220(string terminalID, string merchantID, decimal transactionAmount, string transactionNumber, DateTime sendDate)
        {
            var request = new AS2805Message();
            var tags = new TagList();

            //Common field
            request.TerminalID = terminalID;
            request.MerchantID = merchantID;
            request.STAN = 1;

            request.MessageTypeIdentifier = MessageTypeIdentifiers.FinancialTransactionAdvice;
            request.ProcessingCode = 0;
            request.TransactionAmount = transactionAmount;
            request.POSEntryMode = 0;
            request.RRN = "122423423423";

            //Private field 63
            tags.AddValueTagString(TlvTagIds.UserId, "ABC");
            tags.AddValueTagString(TlvTagIds.BusinessId, "M-55555");
            tags.AddValueTagString(TlvTagIds.ABN, "698555555");
            tags.AddValueTagString(TlvTagIds.NetworkId, "");
            tags.AddValueTagString(TlvTagIds.PickupLocationID, "");
            tags.AddValueTagString(TlvTagIds.DropoffLocationID, "0");
            tags.AddValueTagString(TlvTagIds.GPSPosition, "5555");
            tags.AddValueTagString(TlvTagIds.AmountsEntered, "");
            tags.AddValueTagString(TlvTagIds.TransactionResponseCode, "00");
            tags.AddValueTagString(TlvTagIds.TransactionResponseTime, "0");
            tags.AddValueTagString(TlvTagIds.TransactionNumber, transactionNumber);
            tags.AddValueTagString(TlvTagIds.CardDataMasked, "374245 1004");
            tags.AddValueTagString(TlvTagIds.CardExpiryDate, "0321");
            tags.AddValueTagString(TlvTagIds.CustomerMobileNumber, "");
            tags.AddValueTagString(TlvTagIds.TransactionDateTime, sendDate.ToString("yyyyMMddhhmmss"));
            tags.AddValueTagString(TlvTagIds.CardServiceFee, "0");
            tags.AddValueTagString(TlvTagIds.GSTOnServiceFee, "0");
            tags.AddValueTagString(TlvTagIds.TotalAmount, transactionAmount.ToString());
            tags.AddValueTagString(TlvTagIds.AccountType, "CRD");
            tags.AddValueTagString(TlvTagIds.CardName, "AMEX");
            tags.AddValueTagString(TlvTagIds.TransactionType, "Purchase");
            tags.AddValueTagString(TlvTagIds.TransactionAuthCode, "123456");
            tags.AddValueTagString(TlvTagIds.ProcessingTime, "");
            tags.AddValueTagString(TlvTagIds.TranSTAN, "522");
            tags.AddValueTagString(TlvTagIds.TranEntryMode, "T");
            tags.AddValueTagString(TlvTagIds.TranReceiptResponseCode, "00");
            tags.AddValueTagString(TlvTagIds.CellTowerMCC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerMNC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerLAC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerCID, "0");
            tags.AddValueTagString(TlvTagIds.CardHolderName, "");
            tags.AddValueTagString(TlvTagIds.FunctionIndicator, "");
            tags.AddValueTagString(TlvTagIds.AutoTipAmount, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipFee, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipSelection, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipFeeAppliedCents, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipFeeAppliedPercentage, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoAmount, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoFee, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoSelection, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoFeeAppliedCents, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoFeeAppliedPercentage, "0");
            tags.AddValueTagString(TlvTagIds.ReceiptReference, "");
            tags.AddValueTagString(TlvTagIds.LevyCharge, "0");
            tags.AddValueTagString(TlvTagIds.TransactionGUID, Guid.NewGuid().ToString());
            tags.AddValueTagString(TlvTagIds.RewardMembershipNumber, "");
            tags.AddValueTagString(TlvTagIds.RewardPointsEarned, "");
            tags.AddValueTagString(TlvTagIds.TransactionReceiptText, "");
            tags.AddValueTagString(TlvTagIds.PaymentAppMerchantID, "");
            tags.AddValueTagString(TlvTagIds.TransactionReceiptNumber, "");
            tags.AddValueTagString(TlvTagIds.MembershipNumberEntryMethod, "");

            request.PrivateData63 = DataHelper.ByteArrayToString(tags.ToByteArray());

            AS805Packer packer = new AS805Packer();
            byte[] sendBuffer = packer.Pack((CommsMessage)request);

            var messageRequest = DataHelper.ByteArrayToHexString(sendBuffer);

            return messageRequest;
        }

        private string CreateMessageSettlementRequest0500(string terminalID, string merchantID, string batchNumber, int totalTransaction, decimal totalTransactionAmount, DateTime sendDate, string serialNumber)
        {
            var request = new AS2805Message();
            var tags = new TagList();

            //Common field
            request.TerminalID = terminalID;
            request.MerchantID = merchantID;
            request.STAN = 1;

            request.MessageTypeIdentifier = MessageTypeIdentifiers.SettlementRequest;
            request.ProcessingCode = 7807;
            request.CreditsNumber = totalTransaction;
            request.CreditsReversalNumber = 0;
            request.DebitsNumber = 0;
            request.DebitsReversalNumber = 0;
            request.CreditsAmount = 0;
            request.CreditsReversalAmount = 0;
            request.DebitsAmount = 0;
            request.DebitsReversalAmount = 0;
            request.SettlementAmount = totalTransactionAmount;

            //Private field 63
            tags.AddValueTagString(TlvTagIds.BatchNumber, batchNumber);
            tags.AddValueTagString(TlvTagIds.TransactionDateTime, sendDate.ToString("yyyyMMddhhmmss"));
            tags.AddValueTagString(TlvTagIds.BatteryStatus, "100");
            tags.AddValueTagString(TlvTagIds.SignalStrength, "");
            tags.AddValueTagString(TlvTagIds.TerminalConfigVersion, "");
            tags.AddValueTagString(TlvTagIds.ReceiptVerNo, "");
            tags.AddValueTagString(TlvTagIds.PrinterLogoVer, "");
            tags.AddValueTagString(TlvTagIds.ScreenLogoVer, "");
            tags.AddValueTagString(TlvTagIds.CardTableVer, "");
            tags.AddValueTagString(TlvTagIds.PromptTableVer, "");
            tags.AddValueTagString(TlvTagIds.NetworkTableVer, "");
            tags.AddValueTagString(TlvTagIds.TipTrickTableVer, "");
            tags.AddValueTagString(TlvTagIds.LocationTableVer, "");
            tags.AddValueTagString(TlvTagIds.CellTowerTableVer, "");
            tags.AddValueTagString(TlvTagIds.OnlineOrderConfigVer, "");
            tags.AddValueTagString(TlvTagIds.OnlineOrderLogoVer, "");
            tags.AddValueTagString(TlvTagIds.DeliveryLogoVer, "");
            tags.AddValueTagString(TlvTagIds.PickupLogoVer, "");
            tags.AddValueTagString(TlvTagIds.VAASoftwareVer, "");
            tags.AddValueTagString(TlvTagIds.SerialNumber, serialNumber);
            tags.AddValueTagString(TlvTagIds.ScreenBrightness, "");
            tags.AddValueTagString(TlvTagIds.CellTowerMCC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerMNC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerLAC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerCID, "0");

            request.PrivateData63 = DataHelper.ByteArrayToString(tags.ToByteArray());

            AS805Packer packer = new AS805Packer();
            byte[] sendBuffer = packer.Pack((CommsMessage)request);

            var messageRequest = DataHelper.ByteArrayToHexString(sendBuffer);

            return messageRequest;
        }

        private List<usp_SocketTesterTool_GetTerminalAllocated_v1_Result> RandomList(List<usp_SocketTesterTool_GetTerminalAllocated_v1_Result> inputList, int totalCount)
        {
            var randomList = new List<usp_SocketTesterTool_GetTerminalAllocated_v1_Result>();

            Random r = new Random();
            int randomIndex = 0;

            while (randomList.Count != totalCount)
            {
                randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
                randomList.Add(inputList[randomIndex]); //add it to the new, random list
                inputList.RemoveAt(randomIndex); //remove to avoid duplicates
            }

            return randomList; //return the new random list
        }
    }
}