﻿using Comms.Message.AS2805;
using Comms.Message.Common;
using Host.Shared;
using System;
using Util.Data;
using Util.Tlv;

namespace CreateMessageAS2805ForSocketTester.MessageAS2805
{
    public static class BuildContentMessageAS2085
    {
        public static string CreateMessageLogonReuqest0800(string terminalID, string merchantID, string serialNumber, string terminalModel, string batchNumber, DateTime transactionDatetime)
        {
            var request = new AS2805Message();
            var tags = new TagList();

            //Common field
            request.TerminalID = terminalID;
            request.MerchantID = merchantID;
            request.STAN = 1;
            request.MessageTypeIdentifier = MessageTypeIdentifiers.NetworkManagementRequest;
            request.ProcessingCode = 910000; //Logon request

            //Private field 63
            tags.AddValueTagString(TlvTagIds.LastTableUpdateTime, transactionDatetime.ToString("yyyyMMddHHmmss"));
            tags.AddValueTagString(TlvTagIds.UserId, "12345");
            tags.AddValueTagString(TlvTagIds.BusinessId, "12345");
            tags.AddValueTagString(TlvTagIds.ABN, "1234543423");
            tags.AddValueTagString(TlvTagIds.NetworkId, "1234543423");
            tags.AddValueTagString(TlvTagIds.SerialNumber, serialNumber);
            tags.AddValueTagString(TlvTagIds.TerminalModel, terminalModel);
            tags.AddValueTagString(TlvTagIds.IMEI, "gsm2423432");
            tags.AddValueTagString(TlvTagIds.SimSerialNo, "simserial312");
            tags.AddValueTagString(TlvTagIds.SimMobileNumber, "0423123456");
            tags.AddValueTagString(TlvTagIds.PTID, "98765432");
            tags.AddValueTagString(TlvTagIds.BatchNumber, batchNumber);
            tags.AddValueTagString(TlvTagIds.BatteryStatus, "2000");
            tags.AddValueTagString(TlvTagIds.SignalStrength, "20001");
            tags.AddValueTagString(TlvTagIds.TerminalOSSoftwareVer, "OS123123");
            tags.AddValueTagString(TlvTagIds.VAASoftwareVer, "VAA123123");
            tags.AddValueTagString(TlvTagIds.ApplicationType, "FFFFFF");
            tags.AddValueTagString(TlvTagIds.TerminalConfigVersion, "000001");
            tags.AddValueTagString(TlvTagIds.ReceiptVerNo, "000001");
            tags.AddValueTagString(TlvTagIds.PrinterLogoVer, "000001");
            tags.AddValueTagString(TlvTagIds.CardTableVer, "000001");
            tags.AddValueTagString(TlvTagIds.PromptTableVer, "000001");
            tags.AddValueTagString(TlvTagIds.NetworkTableVer, "000001");
            tags.AddValueTagString(TlvTagIds.TransactionDateTime, transactionDatetime.ToString("yyyyMMddHHmmss"));
            tags.AddValueTagString(TlvTagIds.ScreenLogoVer, "");
            tags.AddValueTagString(TlvTagIds.TerminalFSVer, "");
            tags.AddValueTagString(TlvTagIds.TipTrickTableVer, "");
            tags.AddValueTagString(TlvTagIds.LocationTableVer, "");
            tags.AddValueTagString(TlvTagIds.CellTowerTableVer, "");
            tags.AddValueTagString(TlvTagIds.OnlineOrderConfigVer, "");
            tags.AddValueTagString(TlvTagIds.OnlineOrderLogoVer, "");
            tags.AddValueTagString(TlvTagIds.DeliveryLogoVer, "");
            tags.AddValueTagString(TlvTagIds.PickupLogoVer, "");
            tags.AddValueTagString(TlvTagIds.CellTowerMCC, "");
            tags.AddValueTagString(TlvTagIds.CellTowerMNC, "");
            tags.AddValueTagString(TlvTagIds.CellTowerLAC, "");
            tags.AddValueTagString(TlvTagIds.CellTowerCID, "");
            tags.AddValueTagString(TlvTagIds.GlideboxTableVer, "A000000000");

            request.PrivateData63 = DataHelper.ByteArrayToString(tags.ToByteArray());

            AS805Packer packer = new AS805Packer();
            byte[] sendBuffer = packer.Pack((CommsMessage)request);

            var messageRequest = DataHelper.ByteArrayToHexString(sendBuffer);

            return messageRequest;
        }

        public static string CreateMessageFinancialAdvice0220(string terminalID, string merchantID, decimal transactionAmount, string glideboxItems, decimal glideboxAmount, string transactionNumber, DateTime sendDate, string cardName, decimal autoTipAmount)
        {
            var request = new AS2805Message();
            var tags = new TagList();

            //Common field
            request.TerminalID = terminalID;
            request.MerchantID = merchantID;
            request.STAN = 1;

            request.MessageTypeIdentifier = MessageTypeIdentifiers.FinancialTransactionAdvice;
            request.ProcessingCode = 0;
            request.TransactionAmount = transactionAmount;
            request.POSEntryMode = 0;
            request.RRN = "122423423423";

            //Private field 63
            tags.AddValueTagString(TlvTagIds.UserId, "ABC");
            tags.AddValueTagString(TlvTagIds.BusinessId, "M-55555");
            tags.AddValueTagString(TlvTagIds.ABN, "698555555");
            tags.AddValueTagString(TlvTagIds.NetworkId, "");
            tags.AddValueTagString(TlvTagIds.PickupLocationID, "");
            tags.AddValueTagString(TlvTagIds.DropoffLocationID, "0");
            tags.AddValueTagString(TlvTagIds.GPSPosition, "5555");
            tags.AddValueTagString(TlvTagIds.TransactionResponseCode, "00");
            tags.AddValueTagString(TlvTagIds.TransactionResponseTime, "0");
            tags.AddValueTagString(TlvTagIds.TransactionNumber, transactionNumber);
            tags.AddValueTagString(TlvTagIds.CardDataMasked, "374245 1004");
            tags.AddValueTagString(TlvTagIds.CardExpiryDate, "0321");
            tags.AddValueTagString(TlvTagIds.CustomerMobileNumber, "");
            tags.AddValueTagString(TlvTagIds.TransactionDateTime, sendDate.ToString("yyyyMMddHHmmss"));
            tags.AddValueTagString(TlvTagIds.CardServiceFee, "0");
            tags.AddValueTagString(TlvTagIds.GSTOnServiceFee, "0");
            tags.AddValueTagString(TlvTagIds.TotalAmount, (transactionAmount * 100).ToString());
            tags.AddValueTagString(TlvTagIds.AccountType, "CRD");
            tags.AddValueTagString(TlvTagIds.CardName, cardName);
            tags.AddValueTagString(TlvTagIds.TransactionType, "Purchase");
            tags.AddValueTagString(TlvTagIds.TransactionAuthCode, "123456");
            tags.AddValueTagString(TlvTagIds.ProcessingTime, "");
            tags.AddValueTagString(TlvTagIds.TranSTAN, "522");
            tags.AddValueTagString(TlvTagIds.TranEntryMode, "T");
            tags.AddValueTagString(TlvTagIds.TranReceiptResponseCode, "00");
            tags.AddValueTagString(TlvTagIds.CellTowerMCC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerMNC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerLAC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerCID, "0");
            tags.AddValueTagString(TlvTagIds.CardHolderName, "");
            tags.AddValueTagString(TlvTagIds.FunctionIndicator, "");
            tags.AddValueTagString(TlvTagIds.AutoTipAmount, (autoTipAmount*100).ToString());
            tags.AddValueTagString(TlvTagIds.AutoTipFee, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipSelection, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipFeeAppliedCents, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipFeeAppliedPercentage, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoAmount, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoFee, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoSelection, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoFeeAppliedCents, "0");
            tags.AddValueTagString(TlvTagIds.AutoTipTwoFeeAppliedPercentage, "0");
            tags.AddValueTagString(TlvTagIds.ReceiptReference, "");
            tags.AddValueTagString(TlvTagIds.LevyCharge, "0");
            tags.AddValueTagString(TlvTagIds.TransactionGUID, Guid.NewGuid().ToString());
            tags.AddValueTagString(TlvTagIds.RewardMembershipNumber, "");
            tags.AddValueTagString(TlvTagIds.RewardPointsEarned, "");
            tags.AddValueTagString(TlvTagIds.TransactionReceiptText, "");
            tags.AddValueTagString(TlvTagIds.PaymentAppMerchantID, "");
            tags.AddValueTagString(TlvTagIds.TransactionReceiptNumber, "");
            tags.AddValueTagString(TlvTagIds.MembershipNumberEntryMethod, "");

            if (glideboxAmount > 0)
            {
                tags.AddValueTagString(TlvTagIds.GlideboxItemSold, glideboxItems);
                tags.AddValueTagString(TlvTagIds.GlideboxSaleAmount, (glideboxAmount * 100).ToString());
            }

            tags.AddValueTagString(TlvTagIds.AmountsEntered, (transactionAmount * 100).ToString());

            request.PrivateData63 = DataHelper.ByteArrayToString(tags.ToByteArray());

            AS805Packer packer = new AS805Packer();
            byte[] sendBuffer = packer.Pack((CommsMessage)request);

            var messageRequest = DataHelper.ByteArrayToHexString(sendBuffer);

            return messageRequest;
        }

        public static string CreateMessageSettlementRequest0500(string terminalID, string merchantID, string batchNumber, int totalTransaction, decimal totalTransactionAmount, decimal totalGlideboxAmount, DateTime sendDate, string serialNumber)
        {
            var request = new AS2805Message();
            var tags = new TagList();

            //Common field
            request.TerminalID = terminalID;
            request.MerchantID = merchantID;
            request.STAN = 1;

            request.MessageTypeIdentifier = MessageTypeIdentifiers.SettlementRequest;
            request.ProcessingCode = 7807;
            request.CreditsNumber = 0;
            request.CreditsReversalNumber = 0;
            request.DebitsNumber = totalTransaction;
            request.DebitsReversalNumber = 0;
            request.CreditsAmount = 0;
            request.CreditsReversalAmount = 0;
            request.DebitsAmount = totalTransactionAmount;
            request.DebitsReversalAmount = 0;
            request.SettlementAmount = totalTransactionAmount;

            //Private field 63
            tags.AddValueTagString(TlvTagIds.BatchNumber, batchNumber);
            tags.AddValueTagString(TlvTagIds.TransactionDateTime, sendDate.ToString("yyyyMMddHHmmss"));
            tags.AddValueTagString(TlvTagIds.BatteryStatus, "100");
            tags.AddValueTagString(TlvTagIds.SignalStrength, "");
            tags.AddValueTagString(TlvTagIds.TerminalConfigVersion, "");
            tags.AddValueTagString(TlvTagIds.ReceiptVerNo, "");
            tags.AddValueTagString(TlvTagIds.PrinterLogoVer, "");
            tags.AddValueTagString(TlvTagIds.ScreenLogoVer, "");
            tags.AddValueTagString(TlvTagIds.CardTableVer, "");
            tags.AddValueTagString(TlvTagIds.PromptTableVer, "");
            tags.AddValueTagString(TlvTagIds.NetworkTableVer, "");
            tags.AddValueTagString(TlvTagIds.TipTrickTableVer, "");
            tags.AddValueTagString(TlvTagIds.LocationTableVer, "");
            tags.AddValueTagString(TlvTagIds.CellTowerTableVer, "");
            tags.AddValueTagString(TlvTagIds.OnlineOrderConfigVer, "");
            tags.AddValueTagString(TlvTagIds.OnlineOrderLogoVer, "");
            tags.AddValueTagString(TlvTagIds.DeliveryLogoVer, "");
            tags.AddValueTagString(TlvTagIds.PickupLogoVer, "");
            tags.AddValueTagString(TlvTagIds.VAASoftwareVer, "");
            tags.AddValueTagString(TlvTagIds.SerialNumber, serialNumber);
            tags.AddValueTagString(TlvTagIds.ScreenBrightness, "");
            tags.AddValueTagString(TlvTagIds.CellTowerMCC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerMNC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerLAC, "0");
            tags.AddValueTagString(TlvTagIds.CellTowerCID, "0");
            tags.AddValueTagString(TlvTagIds.GlideboxTableVer, "A000000000");

            if (totalGlideboxAmount > 0)
            {
                tags.AddValueTagString(TlvTagIds.DebitGlideboxAmount, (totalGlideboxAmount * 100).ToString());
                tags.AddValueTagString(TlvTagIds.ReverseGlideboxAmount, "0");
                tags.AddValueTagString(TlvTagIds.NetGlideboxAmount, (totalGlideboxAmount * 100).ToString());
                tags.AddValueTagString(TlvTagIds.DebitGlideboxNumber, totalTransaction.ToString());
                tags.AddValueTagString(TlvTagIds.ReverseGlideboxNumber, "0");
            }

            request.PrivateData63 = DataHelper.ByteArrayToString(tags.ToByteArray());

            AS805Packer packer = new AS805Packer();
            byte[] sendBuffer = packer.Pack((CommsMessage)request);

            var messageRequest = DataHelper.ByteArrayToHexString(sendBuffer);

            return messageRequest;
        }

        public static string CreateMessageTableDownloadRequest0300(string terminalID, string merchantID, string fileName)
        {
            var request = new AS2805Message();
            var tags = new TagList();

            //Common field         
            request.MessageTypeIdentifier = MessageTypeIdentifiers.TableRequest;
            request.STAN = 109;
            request.TerminalID = terminalID;
            request.MerchantID = merchantID;
            request.FileName = fileName;
            request.MessageNumber = 1;
            request.PrivateData60 = "";

            AS805Packer packer = new AS805Packer();
            byte[] sendBuffer = packer.Pack((CommsMessage)request);

            var messageRequest = DataHelper.ByteArrayToHexString(sendBuffer);

            return messageRequest;
        }
    }
}