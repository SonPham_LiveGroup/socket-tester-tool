﻿namespace CreateMessageAS2805ForSocketTester
{
    partial class CreateMessageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblTerminalID = new System.Windows.Forms.Label();
            this.txtTerminalID = new System.Windows.Forms.TextBox();
            this.lblMerchantID = new System.Windows.Forms.Label();
            this.txtMerchantID = new System.Windows.Forms.TextBox();
            this.txtTransactionAmount = new System.Windows.Forms.TextBox();
            this.lblAmount = new System.Windows.Forms.Label();
            this.groupBoxTransaction = new System.Windows.Forms.GroupBox();
            this.txtAutoTipAmount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCardName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtGlideboxAmount = new System.Windows.Forms.TextBox();
            this.lblGlideboxAmount = new System.Windows.Forms.Label();
            this.txtNumberTransactionPerBatch = new System.Windows.Forms.TextBox();
            this.dataGridViewGlidebox = new System.Windows.Forms.DataGridView();
            this.CategoryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CategoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBatchNumber = new System.Windows.Forms.TextBox();
            this.dateTimePickerTransactionDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNumberTransactionPerBatch = new System.Windows.Forms.Label();
            this.lblBatchNumber = new System.Windows.Forms.Label();
            this.groupBoxTerminal = new System.Windows.Forms.GroupBox();
            this.cbEnableGldiebox = new System.Windows.Forms.CheckBox();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblSerialNumber = new System.Windows.Forms.Label();
            this.btnValidateTerminalID = new System.Windows.Forms.Button();
            this.btnCreateMessage = new System.Windows.Forms.Button();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.dataGridViewResult = new System.Windows.Forms.DataGridView();
            this.txtSearchTerminalID = new System.Windows.Forms.TextBox();
            this.txtSearchBatchNumber = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxResult = new System.Windows.Forms.GroupBox();
            this.groupBoxTableDownload = new System.Windows.Forms.GroupBox();
            this.lblOtherFileName = new System.Windows.Forms.Label();
            this.txtOtherFileName = new System.Windows.Forms.TextBox();
            this.btnCreateMessage0300 = new System.Windows.Forms.Button();
            this.comboBoxFileNameRequest = new System.Windows.Forms.ComboBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.btnParser = new System.Windows.Forms.Button();
            this.groupBoxMessageParser = new System.Windows.Forms.GroupBox();
            this.radioButtonFormatDebugLog = new System.Windows.Forms.RadioButton();
            this.radioButtonFormatNonDebugLog = new System.Windows.Forms.RadioButton();
            this.richTextBoxMessageParsser = new System.Windows.Forms.RichTextBox();
            this.richTextBoxMessageAS2805 = new System.Windows.Forms.RichTextBox();
            this.lblMessageAS2805 = new System.Windows.Forms.Label();
            this.groupBoxTransaction.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGlidebox)).BeginInit();
            this.groupBoxTerminal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            this.groupBoxResult.SuspendLayout();
            this.groupBoxTableDownload.SuspendLayout();
            this.groupBoxMessageParser.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTerminalID
            // 
            this.lblTerminalID.AutoSize = true;
            this.lblTerminalID.Location = new System.Drawing.Point(21, 26);
            this.lblTerminalID.Name = "lblTerminalID";
            this.lblTerminalID.Size = new System.Drawing.Size(80, 17);
            this.lblTerminalID.TabIndex = 0;
            this.lblTerminalID.Text = "Terminal ID";
            // 
            // txtTerminalID
            // 
            this.txtTerminalID.Location = new System.Drawing.Point(139, 24);
            this.txtTerminalID.Name = "txtTerminalID";
            this.txtTerminalID.Size = new System.Drawing.Size(226, 22);
            this.txtTerminalID.TabIndex = 1;
            // 
            // lblMerchantID
            // 
            this.lblMerchantID.AutoSize = true;
            this.lblMerchantID.Location = new System.Drawing.Point(21, 63);
            this.lblMerchantID.Name = "lblMerchantID";
            this.lblMerchantID.Size = new System.Drawing.Size(84, 17);
            this.lblMerchantID.TabIndex = 2;
            this.lblMerchantID.Text = "Merchant ID";
            // 
            // txtMerchantID
            // 
            this.txtMerchantID.Location = new System.Drawing.Point(139, 58);
            this.txtMerchantID.Name = "txtMerchantID";
            this.txtMerchantID.ReadOnly = true;
            this.txtMerchantID.Size = new System.Drawing.Size(226, 22);
            this.txtMerchantID.TabIndex = 2;
            // 
            // txtTransactionAmount
            // 
            this.txtTransactionAmount.Location = new System.Drawing.Point(249, 124);
            this.txtTransactionAmount.Name = "txtTransactionAmount";
            this.txtTransactionAmount.Size = new System.Drawing.Size(261, 22);
            this.txtTransactionAmount.TabIndex = 9;
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(18, 129);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(135, 17);
            this.lblAmount.TabIndex = 5;
            this.lblAmount.Text = "Transaction Amount";
            // 
            // groupBoxTransaction
            // 
            this.groupBoxTransaction.Controls.Add(this.txtAutoTipAmount);
            this.groupBoxTransaction.Controls.Add(this.label5);
            this.groupBoxTransaction.Controls.Add(this.txtCardName);
            this.groupBoxTransaction.Controls.Add(this.label4);
            this.groupBoxTransaction.Controls.Add(this.txtGlideboxAmount);
            this.groupBoxTransaction.Controls.Add(this.lblGlideboxAmount);
            this.groupBoxTransaction.Controls.Add(this.txtNumberTransactionPerBatch);
            this.groupBoxTransaction.Controls.Add(this.dataGridViewGlidebox);
            this.groupBoxTransaction.Controls.Add(this.txtBatchNumber);
            this.groupBoxTransaction.Controls.Add(this.dateTimePickerTransactionDate);
            this.groupBoxTransaction.Controls.Add(this.label3);
            this.groupBoxTransaction.Controls.Add(this.lblNumberTransactionPerBatch);
            this.groupBoxTransaction.Controls.Add(this.lblBatchNumber);
            this.groupBoxTransaction.Controls.Add(this.lblAmount);
            this.groupBoxTransaction.Controls.Add(this.txtTransactionAmount);
            this.groupBoxTransaction.Location = new System.Drawing.Point(399, 19);
            this.groupBoxTransaction.Name = "groupBoxTransaction";
            this.groupBoxTransaction.Size = new System.Drawing.Size(517, 369);
            this.groupBoxTransaction.TabIndex = 9;
            this.groupBoxTransaction.TabStop = false;
            this.groupBoxTransaction.Text = "Transaction";
            // 
            // txtAutoTipAmount
            // 
            this.txtAutoTipAmount.Location = new System.Drawing.Point(249, 189);
            this.txtAutoTipAmount.Name = "txtAutoTipAmount";
            this.txtAutoTipAmount.Size = new System.Drawing.Size(261, 22);
            this.txtAutoTipAmount.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 17);
            this.label5.TabIndex = 24;
            this.label5.Text = "Auto Tip Amount";
            // 
            // txtCardName
            // 
            this.txtCardName.Location = new System.Drawing.Point(249, 157);
            this.txtCardName.Name = "txtCardName";
            this.txtCardName.Size = new System.Drawing.Size(261, 22);
            this.txtCardName.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "Card name";
            // 
            // txtGlideboxAmount
            // 
            this.txtGlideboxAmount.Location = new System.Drawing.Point(249, 224);
            this.txtGlideboxAmount.Name = "txtGlideboxAmount";
            this.txtGlideboxAmount.ReadOnly = true;
            this.txtGlideboxAmount.Size = new System.Drawing.Size(261, 22);
            this.txtGlideboxAmount.TabIndex = 10;
            // 
            // lblGlideboxAmount
            // 
            this.lblGlideboxAmount.AutoSize = true;
            this.lblGlideboxAmount.Location = new System.Drawing.Point(20, 225);
            this.lblGlideboxAmount.Name = "lblGlideboxAmount";
            this.lblGlideboxAmount.Size = new System.Drawing.Size(115, 17);
            this.lblGlideboxAmount.TabIndex = 21;
            this.lblGlideboxAmount.Text = "Glidebox Amount";
            // 
            // txtNumberTransactionPerBatch
            // 
            this.txtNumberTransactionPerBatch.Location = new System.Drawing.Point(249, 92);
            this.txtNumberTransactionPerBatch.Name = "txtNumberTransactionPerBatch";
            this.txtNumberTransactionPerBatch.Size = new System.Drawing.Size(261, 22);
            this.txtNumberTransactionPerBatch.TabIndex = 8;
            // 
            // dataGridViewGlidebox
            // 
            this.dataGridViewGlidebox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGlidebox.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CategoryID,
            this.CategoryName,
            this.Price,
            this.Quantity});
            this.dataGridViewGlidebox.Location = new System.Drawing.Point(23, 274);
            this.dataGridViewGlidebox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewGlidebox.Name = "dataGridViewGlidebox";
            this.dataGridViewGlidebox.RowTemplate.Height = 28;
            this.dataGridViewGlidebox.Size = new System.Drawing.Size(488, 90);
            this.dataGridViewGlidebox.TabIndex = 11;
            this.dataGridViewGlidebox.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewGlidebox_CellEndEdit);
            // 
            // CategoryID
            // 
            this.CategoryID.DataPropertyName = "CategoryID";
            this.CategoryID.HeaderText = "ID";
            this.CategoryID.Name = "CategoryID";
            this.CategoryID.ReadOnly = true;
            this.CategoryID.Width = 30;
            // 
            // CategoryName
            // 
            this.CategoryName.DataPropertyName = "CategoryName";
            this.CategoryName.FillWeight = 120F;
            this.CategoryName.HeaderText = "Category Name";
            this.CategoryName.Name = "CategoryName";
            this.CategoryName.ReadOnly = true;
            this.CategoryName.Width = 170;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.HeaderText = "Price";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 50;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle1.NullValue = null;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle1;
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Quantity.Width = 70;
            // 
            // txtBatchNumber
            // 
            this.txtBatchNumber.Location = new System.Drawing.Point(249, 58);
            this.txtBatchNumber.Name = "txtBatchNumber";
            this.txtBatchNumber.Size = new System.Drawing.Size(261, 22);
            this.txtBatchNumber.TabIndex = 7;
            // 
            // dateTimePickerTransactionDate
            // 
            this.dateTimePickerTransactionDate.Location = new System.Drawing.Point(249, 22);
            this.dateTimePickerTransactionDate.Name = "dateTimePickerTransactionDate";
            this.dateTimePickerTransactionDate.Size = new System.Drawing.Size(261, 22);
            this.dateTimePickerTransactionDate.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Transaction Date";
            // 
            // lblNumberTransactionPerBatch
            // 
            this.lblNumberTransactionPerBatch.AutoSize = true;
            this.lblNumberTransactionPerBatch.Location = new System.Drawing.Point(18, 97);
            this.lblNumberTransactionPerBatch.Name = "lblNumberTransactionPerBatch";
            this.lblNumberTransactionPerBatch.Size = new System.Drawing.Size(212, 17);
            this.lblNumberTransactionPerBatch.TabIndex = 13;
            this.lblNumberTransactionPerBatch.Text = "Number of transaction per batch";
            // 
            // lblBatchNumber
            // 
            this.lblBatchNumber.AutoSize = true;
            this.lblBatchNumber.Location = new System.Drawing.Point(18, 63);
            this.lblBatchNumber.Name = "lblBatchNumber";
            this.lblBatchNumber.Size = new System.Drawing.Size(149, 17);
            this.lblBatchNumber.TabIndex = 12;
            this.lblBatchNumber.Text = "Batch Number (1 - 99)";
            // 
            // groupBoxTerminal
            // 
            this.groupBoxTerminal.Controls.Add(this.cbEnableGldiebox);
            this.groupBoxTerminal.Controls.Add(this.txtModel);
            this.groupBoxTerminal.Controls.Add(this.txtSerialNumber);
            this.groupBoxTerminal.Controls.Add(this.lblModel);
            this.groupBoxTerminal.Controls.Add(this.lblSerialNumber);
            this.groupBoxTerminal.Controls.Add(this.btnValidateTerminalID);
            this.groupBoxTerminal.Controls.Add(this.lblTerminalID);
            this.groupBoxTerminal.Controls.Add(this.txtTerminalID);
            this.groupBoxTerminal.Controls.Add(this.txtMerchantID);
            this.groupBoxTerminal.Controls.Add(this.lblMerchantID);
            this.groupBoxTerminal.Location = new System.Drawing.Point(12, 19);
            this.groupBoxTerminal.Name = "groupBoxTerminal";
            this.groupBoxTerminal.Size = new System.Drawing.Size(381, 342);
            this.groupBoxTerminal.TabIndex = 10;
            this.groupBoxTerminal.TabStop = false;
            this.groupBoxTerminal.Text = "Terminal";
            // 
            // cbEnableGldiebox
            // 
            this.cbEnableGldiebox.AutoSize = true;
            this.cbEnableGldiebox.Enabled = false;
            this.cbEnableGldiebox.Location = new System.Drawing.Point(135, 166);
            this.cbEnableGldiebox.Name = "cbEnableGldiebox";
            this.cbEnableGldiebox.Size = new System.Drawing.Size(133, 21);
            this.cbEnableGldiebox.TabIndex = 16;
            this.cbEnableGldiebox.Text = "Enable Glidebox";
            this.cbEnableGldiebox.UseVisualStyleBackColor = true;
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(139, 129);
            this.txtModel.Name = "txtModel";
            this.txtModel.ReadOnly = true;
            this.txtModel.Size = new System.Drawing.Size(226, 22);
            this.txtModel.TabIndex = 4;
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.Location = new System.Drawing.Point(139, 92);
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.ReadOnly = true;
            this.txtSerialNumber.Size = new System.Drawing.Size(226, 22);
            this.txtSerialNumber.TabIndex = 3;
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(21, 134);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(46, 17);
            this.lblModel.TabIndex = 14;
            this.lblModel.Text = "Model";
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.AutoSize = true;
            this.lblSerialNumber.Location = new System.Drawing.Point(21, 97);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(98, 17);
            this.lblSerialNumber.TabIndex = 13;
            this.lblSerialNumber.Text = "Serial Number";
            // 
            // btnValidateTerminalID
            // 
            this.btnValidateTerminalID.Location = new System.Drawing.Point(23, 203);
            this.btnValidateTerminalID.Name = "btnValidateTerminalID";
            this.btnValidateTerminalID.Size = new System.Drawing.Size(342, 32);
            this.btnValidateTerminalID.TabIndex = 5;
            this.btnValidateTerminalID.Text = "Validate TerminalID";
            this.btnValidateTerminalID.UseVisualStyleBackColor = true;
            this.btnValidateTerminalID.Click += new System.EventHandler(this.btnValidateTerminalID_Click);
            // 
            // btnCreateMessage
            // 
            this.btnCreateMessage.Location = new System.Drawing.Point(449, 392);
            this.btnCreateMessage.Name = "btnCreateMessage";
            this.btnCreateMessage.Size = new System.Drawing.Size(180, 34);
            this.btnCreateMessage.TabIndex = 12;
            this.btnCreateMessage.Text = "Create Message";
            this.btnCreateMessage.UseVisualStyleBackColor = true;
            this.btnCreateMessage.Click += new System.EventHandler(this.btnCreateMessage_Click);
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Location = new System.Drawing.Point(671, 393);
            this.btnSendMessage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(193, 34);
            this.btnSendMessage.TabIndex = 13;
            this.btnSendMessage.Text = "Send Message";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewResult.Location = new System.Drawing.Point(13, 73);
            this.dataGridViewResult.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridViewResult.Name = "dataGridViewResult";
            this.dataGridViewResult.RowTemplate.Height = 28;
            this.dataGridViewResult.Size = new System.Drawing.Size(1262, 183);
            this.dataGridViewResult.TabIndex = 14;
            // 
            // txtSearchTerminalID
            // 
            this.txtSearchTerminalID.Location = new System.Drawing.Point(114, 35);
            this.txtSearchTerminalID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSearchTerminalID.Name = "txtSearchTerminalID";
            this.txtSearchTerminalID.Size = new System.Drawing.Size(139, 22);
            this.txtSearchTerminalID.TabIndex = 15;
            // 
            // txtSearchBatchNumber
            // 
            this.txtSearchBatchNumber.Location = new System.Drawing.Point(384, 35);
            this.txtSearchBatchNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSearchBatchNumber.Name = "txtSearchBatchNumber";
            this.txtSearchBatchNumber.Size = new System.Drawing.Size(152, 22);
            this.txtSearchBatchNumber.TabIndex = 16;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(1156, 28);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(112, 30);
            this.btnSearch.TabIndex = 17;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Terminal ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(277, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "Batch Number";
            // 
            // groupBoxResult
            // 
            this.groupBoxResult.Controls.Add(this.txtSearchTerminalID);
            this.groupBoxResult.Controls.Add(this.dataGridViewResult);
            this.groupBoxResult.Controls.Add(this.label2);
            this.groupBoxResult.Controls.Add(this.txtSearchBatchNumber);
            this.groupBoxResult.Controls.Add(this.label1);
            this.groupBoxResult.Controls.Add(this.btnSearch);
            this.groupBoxResult.Location = new System.Drawing.Point(12, 431);
            this.groupBoxResult.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxResult.Name = "groupBoxResult";
            this.groupBoxResult.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBoxResult.Size = new System.Drawing.Size(1281, 271);
            this.groupBoxResult.TabIndex = 20;
            this.groupBoxResult.TabStop = false;
            this.groupBoxResult.Text = "Results";
            // 
            // groupBoxTableDownload
            // 
            this.groupBoxTableDownload.Controls.Add(this.lblOtherFileName);
            this.groupBoxTableDownload.Controls.Add(this.txtOtherFileName);
            this.groupBoxTableDownload.Controls.Add(this.btnCreateMessage0300);
            this.groupBoxTableDownload.Controls.Add(this.comboBoxFileNameRequest);
            this.groupBoxTableDownload.Controls.Add(this.lblFileName);
            this.groupBoxTableDownload.Location = new System.Drawing.Point(922, 19);
            this.groupBoxTableDownload.Name = "groupBoxTableDownload";
            this.groupBoxTableDownload.Size = new System.Drawing.Size(371, 342);
            this.groupBoxTableDownload.TabIndex = 21;
            this.groupBoxTableDownload.TabStop = false;
            this.groupBoxTableDownload.Text = "Table Download (0300 Request)";
            // 
            // lblOtherFileName
            // 
            this.lblOtherFileName.AutoSize = true;
            this.lblOtherFileName.Location = new System.Drawing.Point(102, 61);
            this.lblOtherFileName.Name = "lblOtherFileName";
            this.lblOtherFileName.Size = new System.Drawing.Size(151, 17);
            this.lblOtherFileName.TabIndex = 4;
            this.lblOtherFileName.Text = "Please input file name ";
            // 
            // txtOtherFileName
            // 
            this.txtOtherFileName.Location = new System.Drawing.Point(105, 83);
            this.txtOtherFileName.Name = "txtOtherFileName";
            this.txtOtherFileName.Size = new System.Drawing.Size(253, 22);
            this.txtOtherFileName.TabIndex = 3;
            // 
            // btnCreateMessage0300
            // 
            this.btnCreateMessage0300.Location = new System.Drawing.Point(94, 290);
            this.btnCreateMessage0300.Name = "btnCreateMessage0300";
            this.btnCreateMessage0300.Size = new System.Drawing.Size(170, 34);
            this.btnCreateMessage0300.TabIndex = 2;
            this.btnCreateMessage0300.Text = "Create Message 0300";
            this.btnCreateMessage0300.UseVisualStyleBackColor = true;
            this.btnCreateMessage0300.Click += new System.EventHandler(this.btnCreateMessage0300_Click);
            // 
            // comboBoxFileNameRequest
            // 
            this.comboBoxFileNameRequest.FormattingEnabled = true;
            this.comboBoxFileNameRequest.Location = new System.Drawing.Point(105, 22);
            this.comboBoxFileNameRequest.Name = "comboBoxFileNameRequest";
            this.comboBoxFileNameRequest.Size = new System.Drawing.Size(258, 24);
            this.comboBoxFileNameRequest.TabIndex = 1;
            this.comboBoxFileNameRequest.SelectedIndexChanged += new System.EventHandler(this.comboBoxFileNameRequest_SelectedIndexChanged);
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(17, 26);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(71, 17);
            this.lblFileName.TabIndex = 0;
            this.lblFileName.Text = "File Name";
            // 
            // btnParser
            // 
            this.btnParser.Location = new System.Drawing.Point(475, 188);
            this.btnParser.Name = "btnParser";
            this.btnParser.Size = new System.Drawing.Size(171, 35);
            this.btnParser.TabIndex = 3;
            this.btnParser.Text = "Parser =>";
            this.btnParser.UseVisualStyleBackColor = true;
            this.btnParser.Click += new System.EventHandler(this.btnParser_Click);
            // 
            // groupBoxMessageParser
            // 
            this.groupBoxMessageParser.Controls.Add(this.radioButtonFormatDebugLog);
            this.groupBoxMessageParser.Controls.Add(this.radioButtonFormatNonDebugLog);
            this.groupBoxMessageParser.Controls.Add(this.richTextBoxMessageParsser);
            this.groupBoxMessageParser.Controls.Add(this.richTextBoxMessageAS2805);
            this.groupBoxMessageParser.Controls.Add(this.lblMessageAS2805);
            this.groupBoxMessageParser.Controls.Add(this.btnParser);
            this.groupBoxMessageParser.Location = new System.Drawing.Point(12, 707);
            this.groupBoxMessageParser.Name = "groupBoxMessageParser";
            this.groupBoxMessageParser.Size = new System.Drawing.Size(1281, 260);
            this.groupBoxMessageParser.TabIndex = 22;
            this.groupBoxMessageParser.TabStop = false;
            this.groupBoxMessageParser.Text = "Message Parser";
            // 
            // radioButtonFormatDebugLog
            // 
            this.radioButtonFormatDebugLog.AutoSize = true;
            this.radioButtonFormatDebugLog.Location = new System.Drawing.Point(135, 222);
            this.radioButtonFormatDebugLog.Name = "radioButtonFormatDebugLog";
            this.radioButtonFormatDebugLog.Size = new System.Drawing.Size(212, 21);
            this.radioButtonFormatDebugLog.TabIndex = 8;
            this.radioButtonFormatDebugLog.TabStop = true;
            this.radioButtonFormatDebugLog.Text = "Message Format: Debug Log";
            this.radioButtonFormatDebugLog.UseVisualStyleBackColor = true;
            // 
            // radioButtonFormatNonDebugLog
            // 
            this.radioButtonFormatNonDebugLog.AutoSize = true;
            this.radioButtonFormatNonDebugLog.Location = new System.Drawing.Point(135, 195);
            this.radioButtonFormatNonDebugLog.Name = "radioButtonFormatNonDebugLog";
            this.radioButtonFormatNonDebugLog.Size = new System.Drawing.Size(254, 21);
            this.radioButtonFormatNonDebugLog.TabIndex = 7;
            this.radioButtonFormatNonDebugLog.TabStop = true;
            this.radioButtonFormatNonDebugLog.Text = "Message Format: None Debug Log ";
            this.radioButtonFormatNonDebugLog.UseVisualStyleBackColor = true;
            // 
            // richTextBoxMessageParsser
            // 
            this.richTextBoxMessageParsser.Location = new System.Drawing.Point(697, 21);
            this.richTextBoxMessageParsser.Name = "richTextBoxMessageParsser";
            this.richTextBoxMessageParsser.Size = new System.Drawing.Size(571, 152);
            this.richTextBoxMessageParsser.TabIndex = 2;
            this.richTextBoxMessageParsser.Text = "";
            // 
            // richTextBoxMessageAS2805
            // 
            this.richTextBoxMessageAS2805.Location = new System.Drawing.Point(135, 21);
            this.richTextBoxMessageAS2805.Name = "richTextBoxMessageAS2805";
            this.richTextBoxMessageAS2805.Size = new System.Drawing.Size(556, 152);
            this.richTextBoxMessageAS2805.TabIndex = 6;
            this.richTextBoxMessageAS2805.Text = "";
            // 
            // lblMessageAS2805
            // 
            this.lblMessageAS2805.AutoSize = true;
            this.lblMessageAS2805.Location = new System.Drawing.Point(6, 132);
            this.lblMessageAS2805.Name = "lblMessageAS2805";
            this.lblMessageAS2805.Size = new System.Drawing.Size(119, 17);
            this.lblMessageAS2805.TabIndex = 4;
            this.lblMessageAS2805.Text = "Message AS2805";
            // 
            // CreateMessageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1301, 979);
            this.Controls.Add(this.groupBoxMessageParser);
            this.Controls.Add(this.groupBoxTableDownload);
            this.Controls.Add(this.groupBoxResult);
            this.Controls.Add(this.btnSendMessage);
            this.Controls.Add(this.btnCreateMessage);
            this.Controls.Add(this.groupBoxTerminal);
            this.Controls.Add(this.groupBoxTransaction);
            this.Name = "CreateMessageForm";
            this.Text = "Create Message Form";
            this.Load += new System.EventHandler(this.CreateMessageForm_Load);
            this.groupBoxTransaction.ResumeLayout(false);
            this.groupBoxTransaction.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGlidebox)).EndInit();
            this.groupBoxTerminal.ResumeLayout(false);
            this.groupBoxTerminal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            this.groupBoxResult.ResumeLayout(false);
            this.groupBoxResult.PerformLayout();
            this.groupBoxTableDownload.ResumeLayout(false);
            this.groupBoxTableDownload.PerformLayout();
            this.groupBoxMessageParser.ResumeLayout(false);
            this.groupBoxMessageParser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTerminalID;
        private System.Windows.Forms.TextBox txtTerminalID;
        private System.Windows.Forms.Label lblMerchantID;
        private System.Windows.Forms.TextBox txtMerchantID;
        private System.Windows.Forms.TextBox txtTransactionAmount;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.GroupBox groupBoxTransaction;
        private System.Windows.Forms.GroupBox groupBoxTerminal;
        private System.Windows.Forms.Button btnCreateMessage;
        private System.Windows.Forms.Button btnValidateTerminalID;
        private System.Windows.Forms.Label lblSerialNumber;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblBatchNumber;
        private System.Windows.Forms.Label lblNumberTransactionPerBatch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBatchNumber;
        private System.Windows.Forms.DateTimePicker dateTimePickerTransactionDate;
        private System.Windows.Forms.TextBox txtNumberTransactionPerBatch;
        private System.Windows.Forms.DataGridView dataGridViewGlidebox;
        private System.Windows.Forms.TextBox txtGlideboxAmount;
        private System.Windows.Forms.Label lblGlideboxAmount;
        private System.Windows.Forms.Button btnSendMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridView dataGridViewResult;
        private System.Windows.Forms.TextBox txtSearchTerminalID;
        private System.Windows.Forms.TextBox txtSearchBatchNumber;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxResult;
        private System.Windows.Forms.GroupBox groupBoxTableDownload;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.ComboBox comboBoxFileNameRequest;
        private System.Windows.Forms.Button btnCreateMessage0300;
        private System.Windows.Forms.Button btnParser;
        private System.Windows.Forms.GroupBox groupBoxMessageParser;
        private System.Windows.Forms.RichTextBox richTextBoxMessageAS2805;
        private System.Windows.Forms.Label lblMessageAS2805;
        private System.Windows.Forms.RichTextBox richTextBoxMessageParsser;
        private System.Windows.Forms.CheckBox cbEnableGldiebox;
        private System.Windows.Forms.RadioButton radioButtonFormatDebugLog;
        private System.Windows.Forms.RadioButton radioButtonFormatNonDebugLog;
        private System.Windows.Forms.Label lblOtherFileName;
        private System.Windows.Forms.TextBox txtOtherFileName;
        private System.Windows.Forms.TextBox txtCardName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAutoTipAmount;
        private System.Windows.Forms.Label label5;
    }
}