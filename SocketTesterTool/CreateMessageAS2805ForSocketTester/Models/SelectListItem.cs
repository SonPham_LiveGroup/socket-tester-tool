﻿namespace CreateMessageAS2805ForSocketTester.Models
{
    public class SelectListItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}