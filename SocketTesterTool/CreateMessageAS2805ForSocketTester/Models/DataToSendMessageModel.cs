﻿namespace CreateMessageAS2805ForSocketTester.Models
{
    public class DataToSendMessageModel
    {
        public int SocketTesterDetailID { get; set; }
        public byte[] DataToSendMessage { get; set; }
    }
}