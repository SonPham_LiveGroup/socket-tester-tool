﻿namespace CreateMessageAS2805ForSocketTester.Models
{
    public class BatchMessageRequestModel
    {
        public string MessageTypeID { get; set; }
        public string MessageRequestData { get; set; }
        public string transactionNumber { get; set; }
        public decimal Amount { get; set; }
    }
}