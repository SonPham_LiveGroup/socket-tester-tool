﻿namespace Comms.Message.Common
{
    /// <summary>
    /// Packs/Unpacks CommsMessages
    /// </summary>
    public abstract class CommsMessagePacker
    {
        /// <summary>
        /// Packs the specified message into a byte array to
        /// be sent as a response etc.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>An array with binary representation of the message</returns>
        public abstract byte[] Pack(CommsMessage message);

        /// <summary>
        /// Unpacks the specified buffer into a CommsMessage object
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="pos">The pos.</param>
        /// <param name="length">The length.</param>
        /// <returns>An unpacked message. Caller should cast to relevant child class.</returns>
        public abstract CommsMessage Unpack(byte[] buffer, int pos, int length);
    }
}