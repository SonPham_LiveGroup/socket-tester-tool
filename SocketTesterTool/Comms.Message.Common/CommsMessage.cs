﻿namespace Comms.Message.Common
{
    /// <summary>
    /// Represents the minimal information needed
    /// to represent a message for the framework.
    /// </summary>
    public abstract class CommsMessage
    {
        /// <summary>
        /// Gets the routing id.
        /// This routing id is used when matching a request to a response message
        /// for routing and connection sharing purposes.
        /// </summary>
        /// <value>
        /// The routing id.
        /// </value>
        public abstract string RoutingId { get; }

        /// <summary>
        /// Gets the message type description. EG: AS2805Message
        /// </summary>
        /// <value>
        /// The message type description.
        /// </value>
        public abstract string MessageTypeDescription { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is a response.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is a response; otherwise, <c>false</c>.
        /// </value>
        public abstract bool IsResponse { get; }
    }
}