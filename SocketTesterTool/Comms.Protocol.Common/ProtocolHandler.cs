﻿using Comms.Connection.Common;
using System.Threading.Tasks;

namespace Comms.Protocol.Common
{
    /// <summary>
    /// Base class which all protocol handlers should inherit from.
    /// Handles the framing of data when sending and receiving from a connection.
    ///
    /// ProtocolHandlers should remove the protocol information when receiving
    /// and add protocol information on the data when sending.
    /// </summary>
    public abstract class ProtocolHandler
    {
        /// <summary>
        /// The underlying connection used by the protocol handler.
        /// </summary>
        protected AsyncConnectionBase _connection;

        /// <summary>
        /// Internal buffer for reading frame data from a connection.
        /// </summary>
        protected byte[] _buffer = new byte[4096];

        /// <summary>
        /// Current receive position (for multiple chunk handling)
        /// </summary>
        protected int _position = 0;

        /// <summary>
        /// The final receive buffer with protocol information removed.
        /// </summary>
        protected byte[] _outBuffer;

        /// <summary>
        /// The length of the frame
        /// </summary>
        protected int _frameLength = 0;

        /// <summary>
        /// Delegate for receiving the frame data from an aync read.
        /// </summary>
        /// <param name="bytesReceived">The number of bytes received for the frame.</param>
        public delegate void FrameReceived(int bytesReceived);

        /// <summary>
        /// Occurs when a protocol frame is received.
        /// </summary>
        public event FrameReceived OnFrameReceived;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolHandler" /> class.
        /// </summary>
        /// <param name="connectionToUse">The connection to use.</param>
        public ProtocolHandler(AsyncConnectionBase connectionToUse)
        {
            _connection = connectionToUse;
            _connection.OnDataReceived += new AsyncConnectionBase.DataReceived(HandleChunk);
        }

        /// <summary>
        /// Handles a chunk of data received from the connection.
        /// This may or may not be the full frame and the protocol should handle multiple
        /// chunks per frame.
        /// </summary>
        /// <param name="bytesReceived">The bytes received.</param>
        protected abstract void HandleChunk(int bytesReceived);

        /// <summary>
        /// Sends the frame. This should add protocol information onto the send data.
        /// </summary>
        /// <param name="sendBuffer">The send buffer.</param>
        /// <param name="sourcePosition">The source position to send from.</param>
        /// <param name="length">The length.</param>
        public abstract void SendFrame(byte[] sendBuffer, int sourcePosition, int length);

        public abstract Task SendFrameAsync(byte[] sendBuffer, int sourcePosition, int length);

        /// <summary>
        /// Gets the connection being used by this protocol handler.
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        public AsyncConnectionBase Connection
        {
            get { return _connection; }
        }

        /// <summary>
        /// Waits for data and puts the result in intBuffer.
        /// </summary>
        /// <param name="intoBuffer">The into buffer.</param>
        public void WaitFrameAsync(byte[] intoBuffer)
        {
            _position = 0;
            _frameLength = 0;
            _outBuffer = intoBuffer;
            _connection.BeginRead(_buffer, 0, _buffer.Length);
        }

        /// <summary>
        /// Processes the received frame.
        /// </summary>
        /// <param name="bytesReceived">The bytes received.</param>
        protected void ProcessReceivedFrame(int bytesReceived)
        {
            _position = 0;
            _frameLength = 0;
            if (OnFrameReceived != null)
                OnFrameReceived(bytesReceived);
        }
    }
}