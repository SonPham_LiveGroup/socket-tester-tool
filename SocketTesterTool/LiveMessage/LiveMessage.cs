﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Util.Common;

namespace Host.Shared
{
    public abstract class LiveMessage
    {
        public LiveMessage()
        {
            //this.PrivateDataTLV = new TagList();
        }

        public LiveMessage(MessageTypes type)
        {
            this.MessageType = type;
            //this.PrivateDataTLV = new TagList();
        }

        /* common fields */

        /// <summary>
        ///
        /// </summary>
        public MessageTypes MessageType { get; set; }

        public string MessageTypeID { get; set; }
        /// <summary>
        /// Current Terminal ID in the Terminal
        /// </summary>
        public string TerminalID { get; set; }

        /// <summary>
        /// Current Merchant ID set in the terminal
        /// </summary>
        public string MerchantID { get; set; }

        /// <summary>
        /// Systems trace audit number (unique per message request sent from terminal)
        /// </summary>
        public string STAN { get; set; }                   // Systems trace audit number (unique per message request sent from terminal)

        /// <summary>
        /// Reference retrieval number (the reference number for a transaction)
        /// </summary>
        public string RRN { get; set; }                 // Reference retrieval number (the reference number for a transaction)

        /// <summary>
        /// Network international identifier (routing id)
        /// </summary>
        public string NII { get; set; }                    // Network international identifier (routing id)

        public string ProcessingCode { get; set; }

        /// <summary>
        /// Battery level percentage value Tag ID 0x3D
        /// </summary>
        public string BatteryStatus
        {
            get;
            set;
        }

        public string SignalStrength
        {
            get;
            set;
        }

        public string IPAddress { get; set; }

        public string CellTowerMCC { get; set; }
        public string CellTowerMNC { get; set; }
        public string CellTowerID { get; set; }
        public string CellTowerLAC { get; set; }

        public string PushNotificationMesage { get; set; }
       
        public void ConvertToLiveMessage(dynamic message)
        {
            try
            {
                object o = message;
                var type = o.GetType();
                PropertyInfo[] properties = type.GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    try
                    {
                        if (this.GetType().GetProperty(property.Name) != null)
                        {
                            if (this.GetType().GetProperty(property.Name).GetValue(message, null) != null)
                            {
                                this.GetType().GetProperty(property.Name).SetValue(this, property.GetValue(message, null), null);
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
            catch
            {
            }
        }

        public LiveMessage TranslateToLiveMessage(dynamic message)
        {
            this.ConvertToLiveMessage(message);
            return this;
        }

        public DateTime GetDateTime(string dateTime)
        {
            string dtvalue = "";
            dtvalue = dateTime;
            DateTime dt = new DateTime();
            if (dtvalue != null && dtvalue != "" && dtvalue.Length == 14)
            {
                try
                {
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    dt = DateTime.ParseExact(dtvalue, "yyyyMMddHHmmss", provider);
                }
                catch
                {
                    throw;// new FormatErrorException("Invalid Last Table Update Date Time Format. LastTableUpdateDateTime must follow format CCYYMMDDhhmmss. Value: " + LastTableUpdateDateTime);
                }
            }
            else
                dt = new DateTime(2000, 1, 1);
            return dt;
        }

        public string PaymentAppMerchantID { get; set; }
    }

    //public class HostResponseMessage : LiveMessage
    //{
    //    public HostResponseMessage():base()
    //    {
    //    }
    //    public HostResponseMessage(MessageTypes type)
    //        : base(type)
    //    {
    //    }
    //    public HostResponseMessage(HostRequestMessage message):this()
    //    {
    //        this.STAN = message.STAN;
    //        this.TerminalID = message.TerminalID;
    //        this.NII = message.NII;
    //        this.MerchantID = message.MerchantID;
    //        this.RRN = message.RRN;
    //        this.MessageType = message.MessageType;

    //    }
    //    public new LiveMessage TranslateToLiveMessage(dynamic message)
    //    {
    //        this.ConvertToLiveMessage(message);
    //        return this;
    //    }
    //    public CustomResponseCode ResponseCode { get; set; }
    //    public string ResponseCodeString
    //    {
    //        get
    //        {
    //            return StringEnum.GetStringValue(this.ResponseCode);
    //        }
    //    }
    //}

    //public  class HostRequestMessage : LiveMessage
    //{
    //    public HostRequestMessage():base()
    //    {
    //    }
    //    public HostRequestMessage(MessageTypes type)
    //        : base(type)
    //    {
    //    }
    //    public HostRequestMessage(HostRequestMessage message)
    //        : this()
    //    {
    //    }
    //    public LiveMessage TranslateToLiveMessage(dynamic message)
    //    {
    //        this.ConvertToLiveMessage(message);
    //        return this;
    //    }
    //}

    public class LogonMessage : LiveMessage
    {
        public LogonMessage() : base()
        {
        }

        public LogonMessage(MessageTypes type)
            : base(type)
        {
        }

        public LogonMessage(LiveMessage message)
        {
            if (message != null)
            {
                this.ConvertToLiveMessage(message);
            }
        }

        public bool IsLogonRequest { get; set; }

        public bool IsInitialisation { get; set; }

        public string ApplicationType
        {
            get;
            set;
        }

        public string TerminalConfigVersion
        {
            get;
            set;
        }

        public string ReceiptDataVer
        {
            get;
            set;
        }

        public string TerminalOSSoftwareVer
        {
            get;
            set;
        }

        public string VAASoftwareVer
        {
            get;
            set;
        }

        public string TerminalFSVer
        {
            get;
            set;
        }

        public string ScreenLogoVer
        {
            get;
            set;
        }

        public string PrinterLogoVer
        {
            get;
            set;
        }

        public string CardTableVer
        {
            get;
            set;
        }

        public string PromptTableVer
        {
            get;
            set;
        }

        public string NetworkTableVer
        {
            get;
            set;
        }

        public string TransactionDateTime { get; set; }

        public string BatchNumber
        {
            get;
            set;
        }

        public string UserID
        {
            get;
            set;
        }

        public string ABN
        {
            get;
            set;
        }

        /// <summary>
        /// Business iD Tag ID 0x06
        /// </summary>
        public string BusinessID
        {
            get;
            set;
        }

        public string TipsTricksVer
        {
            get;
            set;
        }

        public string LocationTableVer
        {
            get;
            set;
        }

        public string CellTowerVer
        {
            get;
            set;
        }

        public string LastTableUpdateDateTime
        {
            get;
            set;
        }

        public DateTime GetLastTableUpdateTime()
        {
            try
            {
                return GetDateTime(LastTableUpdateDateTime);
            }
            catch
            {
                throw new FormatErrorException("Invalid Last Table Update Date Time Format. LastTableUpdateDateTime must follow format CCYYMMDDhhmmss. Value: " + LastTableUpdateDateTime);
            }
            //string dtvalue = "";
            //dtvalue = LastTableUpdateDateTime;
            //DateTime dt = new DateTime();
            //if (dtvalue != null && dtvalue != "" && dtvalue.Length == 14)
            //{
            //    try
            //    {
            //        CultureInfo provider = CultureInfo.InvariantCulture;
            //        dt = DateTime.ParseExact(dtvalue, "yyyyMMddHHmmss", provider);
            //    }
            //    catch
            //    {
            //        throw new FormatErrorException("Invalid Last Table Update Date Time Format. LastTableUpdateDateTime must follow format CCYYMMDDhhmmss. Value: " + LastTableUpdateDateTime);
            //    }

            //}
            //else
            //    dt = new DateTime(2000, 1, 1);
            //return dt;
        }

        public DateTime GetTransactionDateTime()
        {
            try
            {
                return GetDateTime(TransactionDateTime);
            }
            catch
            {
                throw new FormatErrorException("Invalid Transaction Date Time Format. TransactionDateTime must follow format CCYYMMDDhhmmss. Value: " + TransactionDateTime);
            }

            //string dtvalue = "";
            //dtvalue = TransactionDateTime;
            //DateTime dt = new DateTime();
            //if (dtvalue != null)
            //{
            //    try
            //    {
            //        CultureInfo provider = CultureInfo.InvariantCulture;
            //        dt = DateTime.ParseExact(dtvalue, "yyyyMMddHHmmss", provider);
            //        //dt = new DateTime(Convert.ToInt32(dtvalue.Substring(0, 4)), Convert.ToInt32(dtvalue.Substring(4, 2)), Convert.ToInt32(dtvalue.Substring(6, 2)),
            //        //    Convert.ToInt32(dtvalue.Substring(8, 2)), Convert.ToInt32(dtvalue.Substring(10, 2)), Convert.ToInt32(dtvalue.Substring(12, 2)));
            //    }
            //    catch
            //    {
            //        throw new FormatErrorException("Invalid Settlement Date Time Format. TransactionDateTime must follow format CCYYMMDDhhmmss. Value: " + TransactionDateTime);
            //    }

            //}
            //else
            //    dt = DateTime.MinValue;
            //return dt;
        }

        public string NetworkID
        {
            get;
            set;
        }

        public string SerialNumber
        {
            get;
            set;
        }

        public string TerminalModel
        {
            get;
            set;
        }

        public string IMEI
        {
            get;
            set;
        }

        public string SimSerialNo
        {
            get;
            set;
        }

        public string SimMobileNumber
        {
            get;
            set;
        }

        public string PTID
        {
            get;
            set;
        }

        public string OnlineOrderConfigVer
        {
            get;
            set;
        }

        public string DeliveryLogoVer
        {
            get;
            set;
        }

        public string PickupLogoVer
        {
            get;
            set;
        }

        public string OnlineOrderLogoVer
        {
            get;
            set;
        }
        public string GlideboxTableVer { get; set; }
        //public string CellTowerMCC { get; set; }
        //public string CellTowerMNC { get; set; }
        //public string CellTowerID { get; set; }
        //public string CellTowerLAC { get; set; }
    }

    public class LogonRequestMessage : LogonMessage
    {
        public LogonRequestMessage(MessageTypes type)
            : base(type)
        {
        }

        public LogonRequestMessage()
            : base(MessageTypes.LogonRequest)
        {
        }

        public LogonRequestMessage(LiveMessage message) : base(message)
        {
        }
    }

    public class LogonResponseMessage : LogonMessage
    {
        public LogonResponseMessage()
            : base(MessageTypes.LogonResponse)
        {
        }

        public LogonResponseMessage(LogonRequestMessage message)
            : this()
        {
            this.STAN = message.STAN;
            this.TerminalID = message.TerminalID;
            this.NII = message.NII;
            this.MerchantID = message.MerchantID;
            this.RRN = message.RRN;

            this.TerminalConfigVersion = message.TerminalConfigVersion;
            this.ReceiptDataVer = message.ReceiptDataVer;
            this.PromptTableVer = message.PromptTableVer;
            this.PrinterLogoVer = message.PrinterLogoVer;
            this.ScreenLogoVer = message.ScreenLogoVer;
            this.CardTableVer = message.CardTableVer;
            this.NetworkTableVer = message.NetworkTableVer;
            this.ConnectToTMS = "false";
            this.BusinessID = message.BusinessID;
            this.ABN = message.ABN;
            this.UserID = message.UserID;
            this.BatchNumber = "";
            this.BatchNumberToResend = "";
            this.OnlineOrderLogoVer = message.OnlineOrderLogoVer;
            this.OnlineOrderConfigVer = message.OnlineOrderConfigVer;
            this.PickupLogoVer = message.PickupLogoVer;
            this.DeliveryLogoVer = message.DeliveryLogoVer;
            this.GlideboxTableVer = message.GlideboxTableVer;
        }

        public LogonResponseMessage(LiveMessage message)
            : base(message)
        {
        }

        public CustomResponseCode ResponseCode { get; set; }
        public string ResponseCodeString
        {
            get
            {
                return $"Field No: 39 - Value: {StringEnum.GetStringValue(this.ResponseCode)}";
            }
        }

        public string VAATerminalID
        {
            get;
            set;
        }

        public string VAAMerchantID
        {
            get;
            set;
        }

        public string ConnectToTMS
        { get; set; }

        public string DownloadTMSConfig
        { get; set; }

        public string BusinessCardInfo
        {
            get;
            set;
        }

        public string BatchNumberToResend { get; set; }
    }

    public abstract class FinancialAdvice : LiveMessage
    {
        public FinancialAdvice(MessageTypes type)
            : base(type)
        {
        }

        public FinancialAdvice() : base()
        {
        }

        /// <summary>
        /// Business iD Tag ID 0x06
        /// </summary>
        public string BusinessID
        {
            get;
            set;
        }

        public string UserID
        {
            get;
            set;
        }

        public string ABN
        {
            get;
            set;
        }

        public string NetworkID
        {
            get;
            set;
        }

        /* transaction fields */

        //public string TruncatedPan { get; set; }
        /// <summary>
        /// Total Transaction amount of the transaction including service fee and gst on service fee. Field no 4
        /// </summary>
        public string Amount { get; set; }

        //public DateTime TransactionLocalTime { get; set; }
        public short PosEntryMode { get; set; }

        public TransactionTypes TransactionType { get; set; }

        public string TransactionTypeTag { get; set; }

        /// <summary>
        /// Transaction DateTime TagID 0x4C
        /// </summary>
        public string TransactionDateTime
        {
            get;
            set;
        }

        public string TransactionGUID { get; set; }

        public DateTime GetTransactionDateTime()
        {
            string dtvalue = "";
            dtvalue = TransactionDateTime;
            DateTime dt = new DateTime();
            if (dtvalue != null)
            {
                try
                {
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    dt = DateTime.ParseExact(dtvalue, "yyyyMMddHHmmss", provider);
                    //dt = new DateTime(Convert.ToInt32(dtvalue.Substring(0, 4)), Convert.ToInt32(dtvalue.Substring(4, 2)), Convert.ToInt32(dtvalue.Substring(6, 2)),
                    //    Convert.ToInt32(dtvalue.Substring(8, 2)), Convert.ToInt32(dtvalue.Substring(10, 2)), Convert.ToInt32(dtvalue.Substring(12, 2)));
                }
                catch
                {
                    throw new FormatErrorException("Invalid Financial Date Time Format. TransactionDateTime must follow format CCYYMMDDhhmmss. Value: " + TransactionDateTime);
                }
            }
            else
                dt = DateTime.MinValue;
            return dt;
        }

        public string TransactionNumber
        {
            get;
            set;
        }

        public string TransactionReceiptText
        {
            get; set;
        }

        public string MembershipNumberEntryMethod { get; set; }
        public string TransactionReceiptNumber { get; set; }
        public string GlideboxSaleAmount { get; set; }
        public string GlideboxItem { get; set; }
    }

    public abstract class FinancialRequestAbstract : FinancialAdvice
    {
        public FinancialRequestAbstract(MessageTypes types) : base(types)
        {
        }

        public FinancialRequestAbstract() : base()
        {
        }

        public int PaymentProviderNumber { get; set; }
        public string MembershipNumber { get; set; }
        public string ReceiptNumber { get; set; }
        public string OriginalTransactionGUID { get; set; }
    }

    public class FinancialRequest : FinancialRequestAbstract
    {
        public FinancialRequest() : base(MessageTypes.FinancialRequest)
        {
        }

        public string PIN { get; set; }
        public string TranEntryMode { get; set; }

        public string ZipStoreCode { get; set; }
    }

    public class FinancialRequestResponse : FinancialRequestAbstract
    {
        public FinancialRequestResponse() : base(MessageTypes.FinancialRequestResponse)
        { }

        public FinancialRequestResponse(FinancialRequest request) : this()
        {
            this.ProcessingCode = request.ProcessingCode;
            this.Amount = request.Amount;
            this.STAN = request.STAN;
            this.TerminalID = request.TerminalID;
            this.MerchantID = request.MerchantID;
            this.TransactionType = request.TransactionType;
            this.TransactionGUID = request.TransactionGUID;
            this.PaymentProviderNumber = request.PaymentProviderNumber;
            this.MembershipNumber = request.MembershipNumber;
        }

        public CustomResponseCode ResponseCode { get; set; }

        public string ResponseCodeString
        {
            get
            {
                return StringEnum.GetStringValue(this.ResponseCode);
            }
        }

        public string AliPayQRCode { get; set; }
        public string AcquirerTransactionNumber { get; set; }

        public string RewardResponseMessage
        {
            get
            {
                return StringEnum.GetStringDescription(this.ResponseCode);
            }
        }

        public int TotalPoinstRedeemed { get; set; }

        public string PaymentProviderResponseMessage { get; set; }
        public string Status { get; set; }
    }

    public class FinancialTranAdvice : FinancialAdvice
    {
        public FinancialTranAdvice()
            : base(MessageTypes.FinancialTransactionAdvice)
        {
        }

        public string PickupLocationID
        {
            get;
            set;
        }

        public string DropoffLocationID
        {
            get;
            set;
        }

        public string GPSPosition
        {
            get;
            set;
        }

        public string AmountsEntered
        {
            get;
            set;
        }

        public string AmountsEnteredTag
        {
            get;
            set;
        }

        public string TransactionResponseCode
        {
            get;
            set;
        }

        public string CardDataMasked
        {
            get;
            set;
        }

        public string CardExpiryDate
        {
            get;
            set;
        }

        public string CustomerMobileNumber
        {
            get;
            set;
        }

        public string ServiceFee
        {
            get;
            set;
        }

        public string GSTOnServiceFee
        {
            get;
            set;
        }

        public string TotalAmount
        {
            get;
            set;
        }

        public decimal Amount1
        {
            get
            {
                return AmountList.ElementAtOrDefault(0);
            }
        }

        public decimal Amount2
        {
            get
            {
                return AmountList.ElementAtOrDefault(1);
            }
        }

        public decimal Amount3
        {
            get
            {
                return AmountList.ElementAtOrDefault(2);
            }
        }

        public decimal Amount4
        {
            get
            {
                return AmountList.ElementAtOrDefault(3);
            }
        }

        public decimal Amount5
        {
            get
            {
                return AmountList.ElementAtOrDefault(4);
            }
        }

        public decimal Amount6
        {
            get
            {
                return AmountList.ElementAtOrDefault(5);
            }
        }

        public decimal Amount7
        {
            get
            {
                return AmountList.ElementAtOrDefault(6);
            }
        }

        public decimal Amount8
        {
            get
            {
                return AmountList.ElementAtOrDefault(7);
            }
        }

        public decimal Amount9
        {
            get
            {
                return AmountList.ElementAtOrDefault(8);
            }
        }

        public List<decimal> AmountList
        {
            get
            {
                List<decimal> amounts = new List<decimal>();
                if (!string.IsNullOrEmpty(AmountsEntered))
                {
                    List<string> amountstring = new List<string>();
                    amountstring = this.AmountsEntered.Split('|').ToList();
                    foreach (string amount in amountstring)
                    {
                        amounts.Add(Convert.ToDecimal(amount) / 100);
                    }
                }
                return amounts;
            }
        }

        public string AccountType { get; set; }
        public string CardName { get; set; }
        public string PaymentTransactionType { get; set; }
        public string TransactionAuthCode { get; set; }
        public string TransactionResponseTime { get; set; }
        public short ProcessingTIme { get; set; }
        public string TranSTAN { get; set; }
        public string TranReceiptResponseCode { get; set; }
        public string TranEntryMode { get; set; }

        //public string CellTowerMCC { get; set; }
        //public string CellTowerMNC { get; set; }
        //public string CellTowerID { get; set; }
        //public string CellTowerLAC { get; set; }
        public string CardHolderName { get; set; }

        public string FunctionIndicator { get; set; }

        //public decimal AutoTipAmount { get; set; }
        //public decimal AutoTipFee { get; set; }
        //public string AutoTipSelection { get; set; }
        //public decimal AutoTipFeeAppliedCents { get; set; }
        //public float AutoTipFeeAppliedPercentage { get; set; }

        public string AutoTipAmount { get; set; }
        public string AutoTipFee { get; set; }
        public string AutoTipSelection { get; set; }
        public string AutoTipFeeAppliedCents { get; set; }
        public string AutoTipFeeAppliedPercentage { get; set; }

        public string AutoTipTwoAmount { get; set; }
        public string AutoTipTwoFee { get; set; }
        public string AutoTipTwoSelection { get; set; }
        public string AutoTipTwoFeeAppliedCents { get; set; }
        public string AutoTipTwoFeeAppliedPercentage { get; set; }

        public string ReceiptReference { get; set; }

        public string LevyCharge { get; set; }

        public string RewardMembershipNumber { get; set; }
        public string RewardPointsEarned { get; set; }
    }

    public class FinancialTranAdviceResponse : FinancialAdvice
    {
        public FinancialTranAdviceResponse()
            : base(MessageTypes.FinancialTransactionResponse)
        {
        }

        public FinancialTranAdviceResponse(FinancialTranAdvice request) : this()
        {
            this.ProcessingCode = request.ProcessingCode;
            this.Amount = request.Amount;
            this.STAN = request.STAN;
            this.TerminalID = request.TerminalID;
            this.MerchantID = request.MerchantID;
            this.TransactionType = request.TransactionType;
            this.TransactionGUID = request.TransactionGUID;
        }

        public CustomResponseCode ResponseCode { get; set; }

        public string ResponseCodeString
        {
            get
            {
                return $"Field No: 39 - Value: {StringEnum.GetStringValue(this.ResponseCode)}";
            }
        }
    }

    public abstract class SettlementMessage : LogonMessage
    {
        public SettlementMessage(MessageTypes type)
            : base(type)
        {
        }

        public SettlementMessage()
            : base()
        {
        }

        /*
        public int SettlementCreditsCount { get; set; }
        public int SettlementDebitsCount { get; set; }
        public decimal SettlementCreditTotal { get; set; }
        public decimal SettlementDebitTotal { get; set; }
        public int SettlementCashCount { get; set; }
        public decimal SettlementCashTotal { get; set; }
        */
    }

    public class SettlementAdvice : SettlementMessage
    {
        public SettlementAdvice()
            : base(MessageTypes.SettlementAdvice)
        {
        }

        //public string BatchNumber
        //{
        //    get;
        //    set;

        //}
        /* Settlement specific fields */
        public string SettlementCreditsNumber { get; set; }
        public string SettlementDebitsNumber { get; set; }
        public string SettlementCreditAmount { get; set; }
        public string SettlementDebitAmount { get; set; }
        public string SettlementCreditReversalNumber { get; set; }
        public string SettlementDebitReversalNumber { get; set; }

        public string SettlementCreditReversalAmount
        {
            get;
            set;
        }

        public string SettlementDebitReversalAmount
        {
            get;
            set;
        }

        public string SettlementNetAmount { get; set; }
        public string ScreenBrightness { get; set; }

        public string DebitGlideboxAmount { get; set; }
        public string ReverseGlideboxAmount { get; set; }
        public string NetGlideboxAmount { get; set; }

        public string SettlementDebitGlideboxNumber { get; set; }
        public string SettlementReverseGlideboxNumber { get; set; }

        //public string TransactionDateTime { get; set; }
        //public DateTime GetTransactionDateTime()
        //{
        //    string dtvalue = "";
        //    dtvalue = TransactionDateTime;
        //    DateTime dt = new DateTime();
        //    if (dtvalue != null)
        //    {
        //        try
        //        {
        //            CultureInfo provider = CultureInfo.InvariantCulture;
        //            dt = DateTime.ParseExact(dtvalue, "yyyyMMddHHmmss", provider);
        //            //dt = new DateTime(Convert.ToInt32(dtvalue.Substring(0, 4)), Convert.ToInt32(dtvalue.Substring(4, 2)), Convert.ToInt32(dtvalue.Substring(6, 2)),
        //            //    Convert.ToInt32(dtvalue.Substring(8, 2)), Convert.ToInt32(dtvalue.Substring(10, 2)), Convert.ToInt32(dtvalue.Substring(12, 2)));
        //        }
        //        catch
        //        {
        //            throw new FormatErrorException("Invalid Settlement Date Time Format. TransactionDateTime must follow format CCYYMMDDhhmmss. Value: " + TransactionDateTime);
        //        }

        //    }
        //    else
        //        dt = DateTime.MinValue;
        //    return dt;
        //}
    }

    public class SettlementResponse : SettlementMessage
    {
        public SettlementResponse()
            : base(MessageTypes.SettlementResponse)
        {
        }

        public SettlementResponse(SettlementAdvice request)
            : this()
        {
            this.MerchantID = request.MerchantID;
            this.TerminalID = request.TerminalID;
            this.BatchNumber = request.BatchNumber;
            this.STAN = request.STAN;
            this.ResponseCode = CustomResponseCode.UnBalanceReconciled;
        }

        public CustomResponseCode ResponseCode { get; set; }

        public string ResponseCodeString
        {
            get
            {
                return $"Field No: 39 - Value: {StringEnum.GetStringValue(this.ResponseCode)}";
            }
        }
    }

    public abstract class FileDownload : LiveMessage
    {
        public FileDownload(MessageTypes type) : base(type)
        {
        }

        public FileName FileNameCode
        {
            get
            {
                try { return (FileName)StringEnum.Parse(typeof(FileName), this.Filename); }
                catch
                {
                    return FileName.Unknown;
                }
            }
        }

        public FileDownload()
        {
        }

        /* table download fields */

        /// <summary>
        /// current message number for downloading
        /// </summary>
        public string MessageNumber { get; set; }        // current message number for downloading

        /// <summary>
        /// last message number for downloading
        /// </summary>
        public string MessageNumberLast { get; set; }    // last message number for downloading

        /// <summary>
        /// the id of the download
        /// </summary>
        public string AuthIdResponse { get; set; }      // the id of the download

        /// <summary>
        /// what table/file we are downloading
        /// </summary>
        public string Filename { get; set; }            // what table/file we are downloading

        public const int MaxFileContentSize = 900;
    }

    public class FileDownloadRequest : FileDownload
    {
        public FileDownloadRequest()
            : base(MessageTypes.TableDownload)
        {
        }
    }

    public class FileDownloadResponse : FileDownload
    {
        public FileDownloadResponse()
            : base(MessageTypes.TableDownloadResponse)
        {
        }

        public FileDownloadResponse(FileDownloadRequest request)
            : this()
        {
            this.TerminalID = request.TerminalID;
            this.MerchantID = request.MerchantID;
            this.STAN = request.STAN;
            this.MessageNumber = request.MessageNumber;
            PromptTableContent = new PromptTableContent();
            GlideboxItemTableContent = new GlideboxItemTableContent();
            FooterLogoTableContent = new FooterLogoTableContent();
            TerminalNetworkTable = new TerminalNetworkTable();
            TerminalConfigTable = new TerminalConfigTable();
            ReceiptTableContent = new ReceiptTableContent();
        }

        #region Prompts Table
        public PromptTableContent PromptTableContent { get; set; }
        #endregion

        #region Terminal Network Table
        public TerminalNetworkTable TerminalNetworkTable { get; set; }
        #endregion

        #region Terminal Config Table
        public TerminalConfigTable TerminalConfigTable { get; set; }
        #endregion

        #region Glidebox Table
        public GlideboxItemTableContent GlideboxItemTableContent { get; set; }
        #endregion

        #region FooterLogo Table
        public FooterLogoTableContent FooterLogoTableContent { get; set; }
        #endregion

        #region Receipt Table
        public ReceiptTableContent ReceiptTableContent { get; set; }
        #endregion

        public string MessageContent
        {
            get;
            set;
        }

        public CustomResponseCode ResponseCode { get; set; }

        public string ResponseCodeString
        {
            get
            {
                return StringEnum.GetStringValue(this.ResponseCode);
            }
        }
    }

    public class PrivateAdministrativeAdvice : LiveMessage
    {
        public PrivateAdministrativeAdvice() :
            base(MessageTypes.PrivateAdministrativeAdvice)
        {
        }

        public AdminMessageTypes AdminMessageType { get; set; }

        public string OldTerminalSerialNumber
        {
            get;
            set;
        }

        public string CustomerMobileNumber
        {
            get;
            set;
        }

        public string DriverLicenseTrack1
        {
            get;
            set;
        }

        public string OldTerminalID { get; set; }
        public string TransactionDateTime { get; set; }

        public DateTime GetTransactionDateTime()
        {
            try
            {
                return GetDateTime(TransactionDateTime);
            }
            catch
            {
                throw new FormatErrorException("Invalid Transaction Date Time Format. TransactionDateTime must follow format CCYYMMDDhhmmss. Value: " + TransactionDateTime);
            }
        }

        public int PaperRollNumber { get; set; }
    }

    public class PrivateAdministrativeResponse : LiveMessage
    {
        public PrivateAdministrativeResponse()
            : base(MessageTypes.PrivateAdministrativeResponse)
        {
        }

        public PrivateAdministrativeResponse(PrivateAdministrativeAdvice advice)
            : this()
        {
            this.TerminalID = advice.TerminalID;
            this.MerchantID = advice.MerchantID;
            this.STAN = advice.STAN;
            this.ProcessingCode = advice.ProcessingCode;
        }

        public CustomResponseCode ResponseCode { get; set; }

        public string ResponseCodeString
        {
            get
            {
                return StringEnum.GetStringValue(this.ResponseCode);
            }
        }
    }

    public class SettlementGetList : LiveMessage
    {
        public SettlementGetList() : base(MessageTypes.SettlementGetListRequest)
        { }

        public string TransactionNumber { get; set; }
    }

    public class SettlementGetResponse : LiveMessage
    {
        public SettlementGetResponse() : base(MessageTypes.SettlementGetListRequest)
        { }

        public SettlementGetResponse(SettlementGetList request)
        {
            this.TerminalID = request.TerminalID;
            this.MerchantID = request.MerchantID;
            this.STAN = request.STAN;
            //this.ProcessingCode = request.ProcessingCode;
        }

        public CustomResponseCode ResponseCode { get; set; }

        public string ResponseCodeString
        {
            get
            {
                return StringEnum.GetStringValue(this.ResponseCode);
            }
        }

        public string TransactionList { get; set; }
    }

    public abstract class OnlineOrder : LiveMessage
    {
        public OnlineOrder()
        { }

        public OnlineOrder(MessageTypes type) : base(type)
        {
        }

        public string OrderNumber { get; set; }
        public CustomResponseCode ResponseCode { get; set; }

        public string ResponseCodeString
        {
            get
            {
                return StringEnum.GetStringValue(this.ResponseCode);
            }
        }
    }

    public class OnlineOrderRequest : OnlineOrder
    {
        public OnlineOrderRequest() : base(MessageTypes.OnlineOrderRequest)
        {
        }

        public OnlineOrderRequest(MessageTypes type) : base(type)
        { }
    }

    public class OnlineOrderStatusResponse : OnlineOrder
    {
        public OnlineOrderStatusResponse() : base(MessageTypes.OnlineOrderResponse)
        { }

        public OnlineOrderStatusResponse(MessageTypes type) : base(type)
        { }

        public OnlineOrderStatusResponse(OnlineOrderStatusAdvice advice) : this()
        {
            this.STAN = advice.STAN;
            this.TerminalID = advice.TerminalID;
            this.MerchantID = advice.MerchantID;
            this.OrderNumber = advice.OrderNumber;
            this.ProcessingCode = advice.ProcessingCode;
        }

        public short OrderCount { get; set; }
    }

    public class OnlineOrderStatusAdvice : OnlineOrderRequest
    {
        public OnlineOrderStatusAdvice() : base(MessageTypes.OnlineOrderStatusAdvice)
        { }

        public string OrderResponse { get; set; }
        public int OrderWaitingTime { get; set; }

        public string DeclineReason { get; set; }
    }

    public class OnlineOrderResponse : OnlineOrderStatusResponse
    {
        public OnlineOrderResponse() : base(MessageTypes.OnlineOrderResponse)
        { }

        public OnlineOrderResponse(OnlineOrderRequest request) : this()
        {
            this.TerminalID = request.TerminalID;
            this.MerchantID = request.MerchantID;
            this.STAN = request.STAN;
            this.ProcessingCode = request.ProcessingCode;
        }

        public string CustomerName { get; set; }
        public string CustomerContactNumber { get; set; }
        public decimal OrderTotalAmount { get; set; }
        public string DeliveryAddress { get; set; }
        public DateTime? OrderDateTime { get; set; }
        public string OrderContent { get; set; }
    }

    public class RewardPointsRequest : FinancialAdvice
    {
        public RewardPointsRequest() : base(MessageTypes.LoyaltyPointsRequest)
        {
        }

        public RewardPointsRequest(MessageTypes types) : base(types)
        { }

        public string MembershipNumber { get; set; }

        public int RewardProgram { get; set; }

        public string PIN { get; set; }
    }

    public class RewardPointsResponse : RewardPointsRequest
    {
        public RewardPointsResponse()
            : base(MessageTypes.LoyaltyPointsResponse)
        {
        }

        public RewardPointsResponse(RewardPointsRequest request)
        : this()
        {
            this.TransactionGUID = request.TransactionGUID;
            this.ProcessingCode = request.ProcessingCode;
            this.Amount = request.Amount;
            this.STAN = request.STAN;
            this.TerminalID = request.TerminalID;
            this.MerchantID = request.MerchantID;
            this.TransactionType = request.TransactionType;
            this.MembershipNumber = request.MembershipNumber;
        }

        public CustomResponseCode ResponseCode { get; set; }

        public string ResponseCodeString
        {
            get
            {
                return StringEnum.GetStringValue(this.ResponseCode);
            }
        }

        public string RewardResponseMessage { get; set; }
        public string RewardTransactionNumber { get; set; }
        public int TotalPointsRewarded { get; set; }
        public int TotalPointsRedeemed { get; set; }
    }

    public class PaperRollOrderRequest : PrivateAdministrativeAdvice
    {
    }

    public class SupportCallBackRequest : PrivateAdministrativeAdvice
    {
    }

    public class TransactionStatusRequest : LiveMessage
    {
        public TransactionStatusRequest() : base(MessageTypes.TransactionStatusRequest)
        {
        }

        public TransactionStatusRequest(MessageTypes types) : base(types)
        { }

        public string TransactionGUID { get; set; }
        public int PaymentProviderNumber { get; set; }
    }

    public class TransactionStatusResponse : LiveMessage
    {
        public TransactionStatusResponse() : base(MessageTypes.TransactionStatusResponse)
        {
        }

        public TransactionStatusResponse(MessageTypes types) : base(types)
        { }

        public TransactionStatusResponse(TransactionStatusRequest request)
            : this()
        {
            this.MerchantID = request.MerchantID;
            this.TerminalID = request.TerminalID;
            this.ProcessingCode = request.ProcessingCode;
            this.TransactionGUID = request.TransactionGUID;
            //this.PaymentProviderNumber = request.PaymentProviderNumber;
            this.ResponseCode = CustomResponseCode.PaymentProviderNotAvailable;
            this.STAN = request.STAN;
        }

        public int PaymentProviderNumber { get; set; }
        public CustomResponseCode ResponseCode { get; set; }

        public string ResponseCodeString
        {
            get
            {
                return StringEnum.GetStringValue(this.ResponseCode);
            }
        }

        public string TransactionGUID { get; set; }

        public string TransactionReceiptNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string CustomerAccountNumber { get; set; }
        public string ResponseText { get; set; }
    }

    public class NotificationMessageRequest : LiveMessage
    {
        public NotificationMessageRequest() : base(MessageTypes.NotificationMessageRequest)
        {
        }

        public string RecordingFromVAA { get; set; }
    }

    public class NotificationMessageResponse : LiveMessage
    {
        public NotificationMessageResponse() : base(MessageTypes.NotificationMessageResponse)
        {
        }

        public NotificationMessageResponse(NotificationMessageRequest request) : this()
        {
            this.ProcessingCode = request.ProcessingCode;
            this.MerchantID = request.MerchantID;
            this.TerminalID = request.TerminalID;
            this.STAN = request.STAN;
        }

        public CustomResponseCode ResponseCode { get; set; }
    }
}