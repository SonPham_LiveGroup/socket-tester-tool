﻿using System;
using System.Runtime.Serialization;
using Util.Common;

namespace Host.Shared
{
    public abstract class LiveMessageException : System.Exception, ISerializable
    {
        public LiveMessageException()
        {
        }

        public LiveMessageException(string message)
            : base(message)
        {
        }

        public LiveMessageException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public LiveMessageException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        protected CustomResponseCode _ResponseCode;

        public CustomResponseCode ResponseCode
        {
            get
            {
                return _ResponseCode;
            }
        }

        public string ResponseCodeString
        {
            get
            {
                return StringEnum.GetStringValue(this.ResponseCode);
            }
        }

        public string ResponseText
        {
            get
            {
                return StringEnum.GetStringDescription(this.ResponseCode);
            }
        }
    }

    public class InvalidTerminalIDException : LiveMessageException
    {
        public InvalidTerminalIDException()
            : base("Invalid Terminal ID. ")
        {
            this._ResponseCode = CustomResponseCode.InvalidTerminalID;
        }

        public InvalidTerminalIDException(string message)
           : base("Invalid Terminal ID. " + message)
        {
            this._ResponseCode = CustomResponseCode.InvalidTerminalID;
        }

        public InvalidTerminalIDException(string message, Exception innerException)
             : base("Invalid Terminal ID. " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.InvalidTerminalID;
        }

        public InvalidTerminalIDException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.InvalidTerminalID;
        }
    }

    public class InvalidMerchantIDException : LiveMessageException
    {
        public InvalidMerchantIDException()
            : base("Invalid Merchant ID")
        {
            this._ResponseCode = CustomResponseCode.InvalidMerchant;
        }

        public InvalidMerchantIDException(string message)
            : base("Invalid Merchant ID. " + message)
        {
            this._ResponseCode = CustomResponseCode.InvalidMerchant;
        }

        public InvalidMerchantIDException(string message, Exception innerException)
            : base("Invalid Merchant ID. " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.InvalidMerchant;
        }

        public InvalidMerchantIDException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.InvalidMerchant;
        }
    }

    public class InvalidUserIDException : LiveMessageException
    {
        public InvalidUserIDException()
            : base("Invalid User ID")
        {
            this._ResponseCode = CustomResponseCode.InvalidUserID;
        }

        public InvalidUserIDException(string message)
            : base("Invalid User ID. " + message)
        {
            this._ResponseCode = CustomResponseCode.InvalidUserID;
        }

        public InvalidUserIDException(string message, Exception innerException)
            : base("Invalid User ID. " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.InvalidUserID;
        }

        public InvalidUserIDException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.InvalidUserID;
        }
    }

    public class InvalidBusinessIDException : LiveMessageException
    {
        public InvalidBusinessIDException()
            : base("Invalid Business ID")
        {
            this._ResponseCode = CustomResponseCode.InvalidBusinessNO;
        }

        public InvalidBusinessIDException(string message)
            : base("Invalid Business ID. " + message)
        {
            this._ResponseCode = CustomResponseCode.InvalidBusinessNO;
        }

        public InvalidBusinessIDException(string message, Exception innerException)
            : base("Invalid Business ID. " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.InvalidBusinessNO;
        }

        public InvalidBusinessIDException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.InvalidBusinessNO;
        }
    }

    public class InvalidNetworkIDException : LiveMessageException
    {
        public InvalidNetworkIDException()
            : base("Invalid Network ID")
        {
            this._ResponseCode = CustomResponseCode.InvalidNetworkID;
        }

        public InvalidNetworkIDException(string message)
            : base("Invalid Network ID. " + message)
        {
            this._ResponseCode = CustomResponseCode.InvalidNetworkID;
        }

        public InvalidNetworkIDException(string message, Exception innerException)
            : base("Invalid Network ID. " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.InvalidNetworkID;
        }

        public InvalidNetworkIDException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.InvalidNetworkID;
        }
    }

    public class InvalidSerialNumberOrModelException : LiveMessageException
    {
        public InvalidSerialNumberOrModelException()
            : base("Invalid SerialNumber or Model")
        {
            this._ResponseCode = CustomResponseCode.InvalidSerialNoOrModel;
        }

        public InvalidSerialNumberOrModelException(string message)
            : base("Invalid SerialNumber or Model. " + message)
        {
            this._ResponseCode = CustomResponseCode.InvalidSerialNoOrModel;
        }

        public InvalidSerialNumberOrModelException(string message, Exception innerException)
            : base("Invalid SerialNumber or Model. " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.InvalidSerialNoOrModel;
        }

        public InvalidSerialNumberOrModelException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.InvalidSerialNoOrModel;
        }
    }

    public class InvalidBatchNumberException : LiveMessageException
    {
        public InvalidBatchNumberException()
            : base("Invalid Batch Number")
        {
            this._ResponseCode = CustomResponseCode.InvalidBatchNumber;
        }

        public InvalidBatchNumberException(string message)
            : base("Invalid Batch Number. " + message)
        {
            this._ResponseCode = CustomResponseCode.InvalidBatchNumber;
        }

        public InvalidBatchNumberException(string message, Exception innerException)
            : base(message, innerException)
        {
            this._ResponseCode = CustomResponseCode.InvalidBatchNumber;
        }

        public InvalidBatchNumberException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.InvalidBatchNumber;
        }
    }

    public class UnbalancedReconcileException : LiveMessageException
    {
        public UnbalancedReconcileException()
            : base("Unbalanced Reconciled")
        {
            this._ResponseCode = CustomResponseCode.UnBalanceReconciled;
        }

        public UnbalancedReconcileException(string message)
            : base("Unbalanced Reconciled. " + message)
        {
            this._ResponseCode = CustomResponseCode.UnBalanceReconciled;
        }

        public UnbalancedReconcileException(string message, Exception innerException)
            : base("Unbalanced Reconciled. " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.UnBalanceReconciled;
        }

        public UnbalancedReconcileException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.UnBalanceReconciled;
        }
    }

    public class TransactionExistsException : LiveMessageException
    {
        public TransactionExistsException()
            : base("Transaction Exists")
        {
            this._ResponseCode = CustomResponseCode.TransactionExists;
        }

        public TransactionExistsException(string message)
            : base("Transaction Exists. " + message)
        {
            this._ResponseCode = CustomResponseCode.TransactionExists;
        }

        public TransactionExistsException(string message, Exception innerException)
            : base("Transaction Exists. " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.TransactionExists;
        }

        public TransactionExistsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.TransactionExists;
        }
    }

    public class LogonBatchExistsException : LiveMessageException
    {
        public LogonBatchExistsException()
            : base("Logon Batch Exists")
        {
            this._ResponseCode = CustomResponseCode.LogonBatchExists;
        }

        public LogonBatchExistsException(string message)
            : base("Logon Batch Exists. " + message)
        {
            this._ResponseCode = CustomResponseCode.LogonBatchExists;
        }

        public LogonBatchExistsException(string message, Exception innerException)
            : base(message, innerException)
        {
            this._ResponseCode = CustomResponseCode.LogonBatchExists;
        }

        public LogonBatchExistsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.LogonBatchExists;
        }
    }

    public class InvalidMessageIDException : LiveMessageException
    {
        public InvalidMessageIDException()
            : base("Invalid Message ID")
        {
            this._ResponseCode = CustomResponseCode.InvalidMessageID;
        }

        public InvalidMessageIDException(string message)
            : base("Invalid Message ID" + message)
        {
            this._ResponseCode = CustomResponseCode.InvalidMessageID;
        }

        public InvalidMessageIDException(string message, Exception innerException)
            : base("Invalid Message ID" + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.InvalidMessageID;
        }

        public InvalidMessageIDException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.InvalidMessageID;
        }
    }

    public class FormatErrorException : LiveMessageException
    {
        public FormatErrorException()
            : base("Format Error")
        {
            this._ResponseCode = CustomResponseCode.FormatError;
        }

        public FormatErrorException(string message)
            : base("Format Error. " + message)
        {
            this._ResponseCode = CustomResponseCode.FormatError;
        }

        public FormatErrorException(string message, Exception innerException)
            : base("Format Error" + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.FormatError;
        }

        public FormatErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.FormatError;
        }
    }

    public class UnableToLocateFileException : LiveMessageException
    {
        public UnableToLocateFileException()
            : base("Unalbe to Locate File")
        {
            this._ResponseCode = CustomResponseCode.UnableToLocateRecordOnFile;
        }

        public UnableToLocateFileException(string message)
            : base("Unalbe to Locate File" + message)
        {
            this._ResponseCode = CustomResponseCode.UnableToLocateRecordOnFile;
        }

        public UnableToLocateFileException(string message, Exception innerException)
            : base("Unalbe to Locate File" + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.UnableToLocateRecordOnFile;
        }

        public UnableToLocateFileException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.UnableToLocateRecordOnFile;
        }
    }

    public class InvalidResponseException : LiveMessageException
    {
        public InvalidResponseException()
            : base("Invalid Response")
        {
            this._ResponseCode = CustomResponseCode.InvalidResponse;
        }

        public InvalidResponseException(string message)
            : base("Invalid Response" + message)
        {
            this._ResponseCode = CustomResponseCode.InvalidResponse;
        }

        public InvalidResponseException(string message, Exception innerException)
            : base("Invalid Response" + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.InvalidResponse;
        }

        public InvalidResponseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.InvalidResponse;
        }
    }

    public class ReconcileErrorException : LiveMessageException
    {
        public ReconcileErrorException()
            : base("Reconcile Error")
        {
            this._ResponseCode = CustomResponseCode.ReconcileError;
        }

        public ReconcileErrorException(string message)
            : base("Reconcile Error" + message)
        {
            this._ResponseCode = CustomResponseCode.ReconcileError;
        }

        public ReconcileErrorException(string message, Exception innerException)
            : base("Reconcile Error" + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.ReconcileError;
        }

        public ReconcileErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.ReconcileError;
        }
    }

    public class SystemErrorException : LiveMessageException
    {
        public SystemErrorException()
            : base("System Error")
        {
            this._ResponseCode = CustomResponseCode.SystemError;
        }

        public SystemErrorException(string message)
            : base("System Error" + message)
        {
            this._ResponseCode = CustomResponseCode.SystemError;
        }

        public SystemErrorException(string message, Exception innerException)
            : base("System Error" + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.SystemError;
        }

        public SystemErrorException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.SystemError;
        }
    }

    public class UnclosedBatchException : LiveMessageException
    {
        public UnclosedBatchException()
            : base("Unclosed Batch")
        {
            this._ResponseCode = CustomResponseCode.UnclosedBatch;
        }

        public UnclosedBatchException(string message)
            : base("Unclosed Batch" + message)
        {
            this._ResponseCode = CustomResponseCode.UnclosedBatch;
        }

        public UnclosedBatchException(string message, Exception innerException)
            : base("Unclosed Batch" + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.UnclosedBatch;
        }

        public UnclosedBatchException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.UnclosedBatch;
        }
    }

    public class NotImplementException : LiveMessageException
    {
        public NotImplementException()
            : base("Feature not Implemented.")
        {
            this._ResponseCode = CustomResponseCode.FormatError;
        }

        public NotImplementException(string message)
            : base("Feature not Implemented. " + message)
        {
            this._ResponseCode = CustomResponseCode.FormatError;
        }

        public NotImplementException(string message, Exception innerException)
            : base("Feature not Implemented " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.FormatError;
        }

        public NotImplementException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.FormatError;
        }
    }

    public class OldTerminalNotFoundException : LiveMessageException
    {
        public OldTerminalNotFoundException()
            : base("Old Terminal Not Found.")
        {
            this._ResponseCode = CustomResponseCode.OldTerminalNotFound;
        }

        public OldTerminalNotFoundException(string message)
            : base("Old Terminal Not Found." + message)
        {
            this._ResponseCode = CustomResponseCode.OldTerminalNotFound;
        }

        public OldTerminalNotFoundException(string message, Exception innerException)
            : base("Old Terminal Not Found: " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.OldTerminalNotFound;
        }

        public OldTerminalNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.OldTerminalNotFound;
        }
    }

    public class NewTerminalNotFoundException : LiveMessageException
    {
        public NewTerminalNotFoundException()
            : base("New Terminal Not Found.")
        {
            this._ResponseCode = CustomResponseCode.NewTerminalNotFound;
        }

        public NewTerminalNotFoundException(string message)
            : base("New Terminal Not Found." + message)
        {
            this._ResponseCode = CustomResponseCode.NewTerminalNotFound;
        }

        public NewTerminalNotFoundException(string message, Exception innerException)
            : base("New Terminal Not Found." + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.NewTerminalNotFound;
        }

        public NewTerminalNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.NewTerminalNotFound;
        }
    }

    public class TerminalNotAllocatedException : LiveMessageException
    {
        public TerminalNotAllocatedException()
            : base("Terminal Not Allocated")
        {
            this._ResponseCode = CustomResponseCode.TerminalNotAllocated;
        }

        public TerminalNotAllocatedException(string message)
            : base("Terminal Not Allocated." + message)
        {
            this._ResponseCode = CustomResponseCode.TerminalNotAllocated;
        }

        public TerminalNotAllocatedException(string message, Exception innerException)
            : base("Terminal Not Allocated: " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.TerminalNotAllocated;
        }

        public TerminalNotAllocatedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.TerminalNotAllocated;
        }
    }

    public class IncorrectAPITokenException : LiveMessageException
    {
        public IncorrectAPITokenException()
            : base("Incorrect API Token Or Password")
        {
            this._ResponseCode = CustomResponseCode.IncorrectAPIToken;
        }

        public IncorrectAPITokenException(string message)
            : base("Incorrect API Token Or Password." + message)
        {
            this._ResponseCode = CustomResponseCode.IncorrectAPIToken;
        }

        public IncorrectAPITokenException(string message, Exception innerException)
            : base("Incorrect API Token Or Password: " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.IncorrectAPIToken;
        }

        public IncorrectAPITokenException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.IncorrectAPIToken;
        }
    }

    public class TerminalUnreachableException : LiveMessageException
    {
        public TerminalUnreachableException()
            : base("Incorrect API Token Or Password")
        {
            this._ResponseCode = CustomResponseCode.TerminalNotReachable;
        }

        public TerminalUnreachableException(string message)
            : base("Incorrect API Token Or Password." + message)
        {
            this._ResponseCode = CustomResponseCode.TerminalNotReachable;
        }

        public TerminalUnreachableException(string message, Exception innerException)
            : base("Incorrect API Token Or Password: " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.TerminalNotReachable;
        }

        public TerminalUnreachableException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.TerminalNotReachable;
        }
    }

    public class TransactionNotExists : LiveMessageException
    {
        public TransactionNotExists()
            : base("Transaction Not Found")
        {
            this._ResponseCode = CustomResponseCode.TransactionNotExists;
        }

        public TransactionNotExists(string message)
            : base("Transaction Not Found." + message)
        {
            this._ResponseCode = CustomResponseCode.TransactionNotExists;
        }

        public TransactionNotExists(string message, Exception innerException)
            : base("Transaction Not Found: " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.TransactionNotExists;
        }

        public TransactionNotExists(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.TransactionNotExists;
        }
    }

    public class OrderStatusException : LiveMessageException
    {
        public OrderStatusException()
            : base("Bad Order Status")
        {
            this._ResponseCode = CustomResponseCode.BadOrderStatus;
        }

        public OrderStatusException(string message)
            : base("Bad Order Status: " + message)
        {
            this._ResponseCode = CustomResponseCode.BadOrderStatus;
        }

        public OrderStatusException(string message, Exception innerException)
            : base("Bad Order Status: " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.BadOrderStatus;
        }

        public OrderStatusException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.BadOrderStatus;
        }
    }

    public class RewarPointsFail : LiveMessageException
    {
        public RewarPointsFail()
            : base("Reward Points Fail")
        {
            this._ResponseCode = CustomResponseCode.RewardPointsFail;
        }

        public RewarPointsFail(string message)
            : base("Reward Points Fail: " + message)
        {
            this._ResponseCode = CustomResponseCode.RewardPointsFail;
        }

        public RewarPointsFail(string message, Exception innerException)
            : base("Reward Points Fail: " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.RewardPointsFail;
        }

        public RewarPointsFail(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.RewardPointsFail;
        }
    }

    public class PaymentProvideNotAvailable : LiveMessageException
    {
        public PaymentProvideNotAvailable()
            : base("Reward Points Fail")
        {
            this._ResponseCode = CustomResponseCode.PaymentProviderNotAvailable;
        }

        public PaymentProvideNotAvailable(string message)
            : base("Reward Points Fail: " + message)
        {
            this._ResponseCode = CustomResponseCode.PaymentProviderNotAvailable;
        }

        public PaymentProvideNotAvailable(string message, Exception innerException)
            : base("Reward Points Fail: " + message, innerException)
        {
            this._ResponseCode = CustomResponseCode.PaymentProviderNotAvailable;
        }

        public PaymentProvideNotAvailable(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._ResponseCode = CustomResponseCode.PaymentProviderNotAvailable;
        }
    }
}