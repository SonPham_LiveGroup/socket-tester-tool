﻿using Util.Common;

namespace Host.Shared
{
    public enum FileName
    {
        [StringValue("TBL_PROMPTS")]
        DisplayText,

        [StringValue("TBL_NETWORK")]
        TaxiNetwork,

        [StringValue("TBL_PRNLOGO")]
        PrinterLogo,

        [StringValue("TBL_RECEIPT")]
        Receipt,

        [StringValue("TBL_TERMCFG")]
        TerminalConfig,

        [StringValue("TBL_SCRLOGO")]
        ScreenLogo,

        [StringValue("TBL_CARD")]
        CardsTable,

        [StringValue("TBL_TIPS_TRICKS")] //new table for 675
        TipsAndTricks,

        [StringValue("TBL_LOCATION")]//new table for 675
        Location,

        [StringValue("TBL_CELL_TOWERS")]//new table for 675
        CellTower,

        [StringValue("TBL_LOVETAKEAWAY")]//new table for 675
        OnlineOrderConfig,

        [StringValue("TBL_DELIVERYLOGO")]//new table for 675
        DeliveryLogo,

        [StringValue("TBL_PICKUPLOGO")]//new table for 675
        PickupLogo,

        [StringValue("TBL_LOVETAKEAWAYLOGO")]//new table for 675
        OnlineOrderLogo,

        [StringValue("TBL_GLIDEBOX")]
        Glidebox,

        [StringValue("TBL_FOOTERLOGO")]
        FooterLogo,

        [StringValue("OTHER")]
        Other,

        Unknown
    }
}