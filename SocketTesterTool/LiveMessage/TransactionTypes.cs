﻿using Util.Common;

namespace Host.Shared
{
    public enum TransactionTypes
    {
        [StringValue("Purchase")]
        Purchase = 1,

        [StringValue("Void")]
        Void = 2,

        [StringValue("Refund")]
        Refund = 3,

        [StringValue("Purchase With Cash Back")]
        PurchaseWithCashback = 4,

        [StringValue("Cash Advance")]
        CashAdvance = 5
    }

    public enum AdminMessageTypes
    {
        NewTerminal = 93,
        Swap = 94,
        Return = 95,
        UpdateInformation = 96,
        CellTowerInfo = 97,
        ChangeSettlementTime = 87,
        PaperRollOrder = 98,
        SupportCallbackRequest = 99
    }

    public enum OnlineOrderType
    {
        OrderRequest = 45,
        OrderStatusUpdate = 46
    }
}