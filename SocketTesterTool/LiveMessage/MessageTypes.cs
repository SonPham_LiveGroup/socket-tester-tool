﻿using Util.Common;

namespace Host.Shared
{
    /// <summary>
    /// Host message types
    /// </summary>
    public enum MessageTypes
    {
        /// <summary>
        /// 0x0220
        /// </summary>
        ///
        [StringValue("FinancialTransactionAdvice")]
        FinancialTransactionAdvice, //0x0220

        /// <summary>
        /// 0x0230
        /// </summary>
        ///
        [StringValue("FinancialTransactionResponse")]
        FinancialTransactionResponse, //0x0230

        /// <summary>
        /// 0x0800
        /// </summary>
        ///
        [StringValue("LogonRequest")]
        LogonRequest, //0x0800

        /// <summary>
        /// 0x0810
        /// </summary>
        ///
        [StringValue("LogonResponse")]
        LogonResponse, //0x0810

        /// <summary>
        /// 0x0520
        /// </summary>
        ///

        [StringValue("SettlementAdvice")]
        SettlementAdvice, //0x0520

        /// <summary>
        /// 0530
        /// </summary>
        ///
        [StringValue("SettlementResponse")]
        SettlementResponse, //0x0530

        /// <summary>
        /// 0300
        /// </summary>
        ///
        [StringValue("TableDownload")]
        TableDownload,

        /// <summary>
        /// 0310
        /// </summary>
        ///
        [StringValue("TableDownloadResponse")]
        TableDownloadResponse,

        /// <summary>
        /// 0x0800
        /// </summary>
        ///
        [StringValue("Initialisation")]
        Initialisation,

        /// <summary>
        /// 0x0810
        /// </summary>
        ///
        [StringValue("InitialisationResponse")]
        InitialisationResponse,

        /// <summary>
        /// 0x0800
        /// </summary>
        ///
        [StringValue("Logon")]
        Logon,

        /// <summary>
        /// 0x0220
        /// </summary>
        ///
        [StringValue("FinancialAdvice")]
        FinancialAdvice,

        /// <summary>
        /// 0x9620
        /// </summary>
        ///
        [StringValue("PrivateAdministrativeAdvice")]
        PrivateAdministrativeAdvice,

        /// <summary>
        /// 0x9630
        /// </summary>
        ///
        [StringValue("PrivateAdministrativeResponse")]
        PrivateAdministrativeResponse,

        /// <summary>
        /// 0x0540
        /// </summary>
        ///
        [StringValue("SettlementGetListRequest")]
        SettlementGetListRequest,

        /// <summary>
        /// 0x0550
        /// </summary>
        ///
        [StringValue("SettlementGetListResponse")]
        SettlementGetListResponse,

        /// <summary>
        /// 0x9625
        /// </summary>
        ///
        [StringValue("OnlineOrderRequest")]
        OnlineOrderRequest,

        /// <summary>
        /// 0x9635
        /// </summary>
        ///
        [StringValue("OnlineOrderResponse")]
        OnlineOrderResponse,

        /// <summary>
        /// 0x9625
        /// </summary>
        ///
        [StringValue("OnlineOrderStatusAdvice")]
        OnlineOrderStatusAdvice,

        /// <summary>
        /// 0x9635
        /// </summary>
        ///
        [StringValue("OnlineOrderStatusResponse")]
        OnlineOrderStatusResponse,

        /// <summary>
        /// 0x0240
        /// </summary>
        ///
        [StringValue("LoyaltyPointsRequest")]
        LoyaltyPointsRequest,

        /// <summary>
        /// 0x0250
        /// </summary>
        ///
        [StringValue("LoyaltyPointsResponse")]
        LoyaltyPointsResponse,

        /// <summary>
        /// 0x0200
        /// </summary>
        ///
        [StringValue("FinancialRequest")]
        FinancialRequest,

        /// <summary>
        /// 0210
        /// </summary>
        ///
        [StringValue("FinancialRequestResponse")]
        FinancialRequestResponse,

        /// <summary>
        /// 0x0260
        /// </summary>
        ///
        [StringValue("TransactionStatusRequest")]
        TransactionStatusRequest,

        /// <summary>
        /// 0270
        /// </summary>
        ///
        [StringValue("TransactionStatusResponse")]
        TransactionStatusResponse,

        /// <summary>
        /// 9640
        /// </summary>
        [StringValue("NotificationMessageRequest")]
        NotificationMessageRequest,

        /// <summary>
        /// 9650
        /// </summary>
        [StringValue("NotificationMessageResponse")]
        NotificationMessageResponse
    }
}