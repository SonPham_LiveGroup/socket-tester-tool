﻿using Util.Common;

namespace Host.Shared
{
    public enum PaymentProviders
    {
        [StringValue("QANTAS")]
        QANTAS = 1,

        [StringValue("ALIPAY")]
        ALIPAY = 2,

        [StringValue("ZIPPAY")]
        ZIPPAY = 3
    }
}