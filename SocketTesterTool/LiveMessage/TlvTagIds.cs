﻿namespace Host.Shared
{
    public static class TlvTagIds
    {
        #region Private data field tag for 0800 message

        //public const string DisplayProfileVersionNo = "DF01";    // string (AN 4 chars)
        public const string UserId = "DF05";                     // string (ANS? 40 characters)

        public const string BusinessId = "DF06";                // string (ANS? 40 characters)
        public const string BatchNumber = "DF3C";                 // string (ANS 10 characters)
        public const string ABN = "DF07";                         // string (N 11 characters)
        #region Extra Tags for both Initialisation and Logon Responses
        public const string NetworkId = "DF08";                  // string (ANS? 40 characters)
        public const string TerminalId = "DF09";                 // string (ANS 8 characters)
        public const string MerchantID = "DF0A";
        #endregion Extra Tags for both Initialisation and Logon Responses

        #region Prompt/DisplayText
        public const string UserIdPrompt = "DF02";               // string (ANS 256 chars)
        public const string BusinessIdPrompt = "DF03";           // string (ANS 256 chars)
        public const string AmountIdPrompt = "DF04";             // string (ANS 256 chars)
        public const string PromptTableVer = "DF01";
        public const string LocationOnePrompt = "DF89";
        public const string LocationTwoPrompt = "DF8A";
        public const string DisableLocationOne = "DF87";
        public const string DisableLocationTwo = "DF88";

        //version1.2.2
        public const string EOSButtonText = "DF9C";             // string (ANS 256 chars)

        public const string EOSApprovedText = "DF9D";
        public const string EOSConfirmText = "DF9E";
        public const string EOSPriorText = "DF9F";
        public const string EOSNotFoundText = "DFA0";
        public const string EOSBatchOpenText = "DFA1";

        //version1.2.3
        public const string AutoTipNumOne = "DFAD";

        public const string AutoTipNumTwo = "DFAE";
        public const string AutoTipNumThree = "DFAF";
        public const string AutoTipNumFour = "DFB0";
        public const string AutoTipIsPercentOne = "DFB1";
        public const string AutoTipIsPercentTwo = "DFB2";
        public const string AutoTipIsPercentThree = "DFB3";
        public const string AutoTipIsPercentFour = "DFB4";

        public const string AutoTipDisplayOption = "DFB5";
        public const string EnableAutoTipAmountDisplay = "DFB6";
        public const string EnableAutoTipTotalDisplay = "DFB7";
        public const string AutoTipTitle = "DFB8";
        public const string AutoTipPriorAmountLabel = "DFB9";
        public const string AutoTipEnterConfirmation = "DFBA";

        //version 1.2.4
        public const string AutoTipEOSReportOption = "DFC0";

        public const string AutoTipButtonOneFeeCents = "DFC1";
        public const string AutoTipButtonOneFeePercentage = "DFC2";
        public const string AutoTipButtonTwoFeeCents = "DFC3";
        public const string AutoTipButtonTwoFeePercentage = "DFC4";
        public const string AutoTipButtonThreeFeeCents = "DFC5";
        public const string AutoTipButtonThreeFeePercentage = "DFC6";
        public const string AutoTipButtonFourFeeCents = "DFC7";
        public const string AutoTipButtonFourFeePercentage = "DFC8";

        public const string AutoTipManualEntryFeeCents = "DFC9";
        public const string AutoTipManualEntryFeePercentage = "DFCA";
        public const string AutoTipAmountLabel = "DFCB";
        public const string AutoTipEOSReportMessage = "DFD1";
        public const string AutoTipFeeIncluded = "DFD2";

        public const string AutoTipButtonOneID = "DFD3";
        public const string AutoTipButtonTwoID = "DFD4";
        public const string AutoTipButtonThreeID = "DFD5";
        public const string AutoTipButtonFourID = "DFD6";

        //version 1.2.6
        public const string ClearOverrideSettings = "DFD7";

        //version 1.3.0
        public const string AutoTipDefaultButtonID = "DFD8";

        public const string AutoTipScreenText = "DFD9";

        //version 1.4.0
        public const string AutoTipTwoNumOne = "1F41";

        public const string AutoTipTwoNumTwo = "1F42";
        public const string AutoTipTwoNumThree = "1F43";
        public const string AutoTipTwoNumFour = "1F44";
        public const string AutoTipTwoIsPercentOne = "1F45";
        public const string AutoTipTwoIsPercentTwo = "1F46";
        public const string AutoTipTwoIsPercentThree = "1F47";
        public const string AutoTipTwoIsPercentFour = "1F48";

        public const string AutoTipTwoDisplayOption = "1F49";
        public const string EnableAutoTipTwoAmountDisplay = "1F4A";
        public const string EnableAutoTipTwoTotalDisplay = "1F4B";
        public const string AutoTipTwoTitle = "1F4C";
        public const string AutoTipTwoPriorAmountLabel = "1F4D";
        public const string AutoTipTwoEnterConfirmation = "1F4E";

        public const string AutoTipTwoEOSReportOption = "1F4F";
        public const string AutoTipTwoButtonOneFeeCents = "1F50";
        public const string AutoTipTwoButtonOneFeePercentage = "1F51";
        public const string AutoTipTwoButtonTwoFeeCents = "1F52";
        public const string AutoTipTwoButtonTwoFeePercentage = "1F53";
        public const string AutoTipTwoButtonThreeFeeCents = "1F54";
        public const string AutoTipTwoButtonThreeFeePercentage = "1F55";
        public const string AutoTipTwoButtonFourFeeCents = "1F56";
        public const string AutoTipTwoButtonFourFeePercentage = "1F57";

        public const string AutoTipTwoManualEntryFeeCents = "1F58";
        public const string AutoTipTwoManualEntryFeePercentage = "1F59";
        public const string AutoTipTwoAmountLabel = "1F5A";
        public const string AutoTipTwoEOSReportMessage = "1F5B";
        public const string AutoTipTwoFeeIncluded = "1F5C";

        public const string AutoTipTwoButtonOneID = "1F5D";
        public const string AutoTipTwoButtonTwoID = "1F5E";
        public const string AutoTipTwoButtonThreeID = "1F5F";
        public const string AutoTipTwoButtonFourID = "1F60";

        public const string AutoTipTwoDefaultButtonID = "1F61";
        public const string AutoTipTwoScreenText = "1F62";

        public const string ReceiptReferenceLabel = "DFE5";
        public const string ReceiptReferenceText = "DFE6";
        public const string ReceiptReference = "DFE4";

        //version 1.5.7
        public const string LevyCharge = "1F71";

        public const string LevyLabel = "1F93";
        public const string IncludeGSTText = "1F97";

        #endregion Prompt/DisplayText

        public const string SerialNumber = "DF35";               // string (ANS 11 characters)
        public const string TerminalModel = "DF36";              // string (ANS 16 characters)

        public const string LastTableUpdateTime = "DF11";
        public const string IMEI = "DF38";                      // string (ANS 16 characters)
        public const string SimSerialNo = "DF39";
        public const string SimMobileNumber = "DF3A";
        public const string PTID = "DF3B";
        public const string BatteryStatus = "DF3D";
        public const string SignalStrength = "DF3E";
        public const string TerminalOSSoftwareVer = "DF3F";
        public const string VAASoftwareVer = "DF40";
        public const string TerminalFSVer = "DF58";
        public const string ApplicationType = "DF12";

        //public const string ReceiptFooterVer = "DF15";

        public const string CardTableVer = "DF17";
        public const string DiscountTable = "DF1B";
        public const string ServiceFeeTable = "DF1C";
        public const string ConnectToTMS = "DF5E";
        public const string DownloadTMSConfig = "1F92";
        #endregion Private data field tag for 0800 message

        #region Extra tags for 0220 sale Advice message
        public const string PickupLocationID = "DF0B";
        public const string DropoffLocationID = "DF0C";
        public const string GPSPosition = "DF0D";
        public const string AmountsEntered = "DF0E";
        public const string TransactionResponseCode = "DF0F";
        public const string TransactionResponseTime = "DF10";
        public const string TransactionNumber = "DF42";
        public const string CardDataMasked = "DF49";
        public const string CardExpiryDate = "DF4A";
        public const string CustomerMobileNumber = "DF4B";
        public const string TransactionDateTime = "DF4C";
        public const string CardServiceFee = "DF4D";
        public const string GSTOnServiceFee = "DF4E";
        public const string TotalAmount = "DF4F";
        public const string AccountType = "DF59";
        public const string CardName = "DF5A";
        public const string TransactionType = "DF5B";
        public const string TransactionAuthCode = "DF5C";
        public const string ProcessingTime = "DF62";
        public const string TranSTAN = "DF63";
        public const string TranReceiptResponseCode = "DF6D";
        public const string TranEntryMode = "DF6C";
        public const string CellTowerMCC = "DFA4";
        public const string CellTowerMNC = "DFA5";
        public const string CellTowerLAC = "DFA6";
        public const string CellTowerCID = "DFA7";
        public const string CardHolderName = "DFA8";

        public const string FunctionIndicator = "DFA9";
        public const string AutoTipAmount = "DFCC";
        public const string AutoTipFee = "DFCD";
        public const string AutoTipSelection = "DFCE";
        public const string AutoTipFeeAppliedCents = "DFCF";
        public const string AutoTipFeeAppliedPercentage = "DFD0";

        public const string AutoTipTwoAmount = "1F64";
        public const string AutoTipTwoFee = "1F65";
        public const string AutoTipTwoSelection = "1F66";
        public const string AutoTipTwoFeeAppliedCents = "1F67";
        public const string AutoTipTwoFeeAppliedPercentage = "1F68";

        public const string TransactionGUID = "1F99";
        public const string RewardMembershipNumber = "1FA0";
        public const string RewardPointsEarned = "1FA1";

        public const string TransactionReceiptNumber = "1FB7";
        #endregion Extra tags for 0220 sale Advice message

        #region Tag for 0300 File Action Operations

        public const string MessageContent = "DF43";
        #endregion Tag for 0300 File Action Operations

        #region Login Parameters Table
        public const string LoginParameterTableVerNo = "DF1E";
        public const string LoginMode = "DF1F";
        public const string LoginParameterOptions = "DF20";
        #endregion Login Parameters Table

        #region Receipt Table
        public const string ReceiptVerNo = "DF14";

        public const string ReceiptHeaderData = "DF21";          // string (ANS 512 characters)
        public const string ReceiptFooterData = "DF22";          // string (ANS 512 characters)

        public const string ReceiptHeader1 = "DF50";
        public const string ReceiptHeader2 = "DF51";
        public const string ReceiptHeader3 = "DF52";
        public const string ReceiptHeader4 = "DF53";
        public const string ReceiptFooter1 = "DF54";
        public const string ReceiptFooter2 = "DF55";
        public const string ReceiptFooter3 = "DF56";
        public const string ReceiptFooter4 = "DF57";
        public const string UserIDReceiptLabel = "DF83";
        public const string BusinessIDReceiptLable = "DF84";
        public const string LocationOneReceiptLabel = "DF85";
        public const string LocationTwoReceiptLabel = "DF86";
        public const string ReceiptAmountDiscriptor = "DF8B";
        public const string IncludeServiceFeeInTotal = "DF8C";
        public const string EOSTranAmountItemisation = "DF8D";
        public const string EOSReceiptSummary = "DF97";
        public const string AutoEOSNotificationMode = "DF99";
        public const string AutoEOSOutputMode = "DF9A";
        public const string EOSReportText = "DFA2";
        public const string BusinessCardInfo = "1F6C";

        public const string AutotipEOStext = "1F6D";
        public const string Autotip2EOSText = "1F6E";
        public const string AutotipSummaryEOSText = "1F6F";
        public const string AutoTipsInclude = "1F70";

        public const string CustomFooter = "1F98";

        #endregion Receipt Table
        #region Printer Logo Table
        public const string PrinterLogoVer = "DF16";
        public const string PrinterLogoData = "DF23";
        #endregion Printer Logo Table
        #region Screen Logo Table
        public const string ScreenLogoVer = "DF25";
        public const string ScreenLogoData = "DF24";
        #endregion Screen Logo Table

        #region Taxi Network Table
        public const string NetworkTableVer = "DF1A";
        public const string AvailableNetworks = "DF27";          // string (ANS 400 characters)

        #endregion Taxi Network Table

        #region Terminal Configuration Table
        public const string TerminalConfigVersion = "DF13";      // string (AN 4 characters)
        public const string UserInputTimeout = "DF28";
        public const string BacklightOffPeriod = "DF29";
        public const string SleepModeTime = "DF2A";
        public const string ScreenBrightness = "DF2B";
        public const string SoundOnOff = "DF2C";
        public const string RememberMeSettings = "DF2D";
        public const string ABNSetting = "DF2E";
        public const string LiveBusinessUnit = "DF2F";
        public const string MinimumCharge = "DF30";
        public const string MaximumCharge = "DF31";
        public const string AutoEOSTime = "DF32";
        public const string Prefix = "DF5D";
        public const string GPRSReconnectTimer = "DF5F";
        public const string ApprovedResponseCode = "DF60";
        public const string ApprovedSignatureResponseCode = "DF61";
        public const string IsPreTranPing = "DF64";
        public const string TranAdviceSendMode = "DF65";
        public const string DefaultServiceFee = "DF66";
        public const string ServiceFeeMode = "DF67";
        public const string CustomerPaymentType = "DF68";
        public const string EnableNetworkReceiptPrint = "DF7B";
        public const string EnableMerchantLogoPrint = "DF7C";
        public const string NetworkTableStatus = "DF26";
        public const string SignatureVerificationTimeOut = "DF37";
        public const string AutoLogonHour = "DF8E";
        public const string AutoLogonMode = "DF8F";

        //version 1.2.2
        public const string EnableRefundTran = "DF90";

        public const string EnableAuthTran = "DF91";
        public const string EnableTipTran = "DF92";
        public const string EnableCashOutTran = "DF93";
        public const string MerchantPassword = "DF94";
        public const string BankLogonRespCode = "DF95";

        public const string DefaultBackgroundColour = "DF96";
        public const string EnableMOTO = "DF98";
        public const string EnableMOTORefund = "DFA3";
        public const string AutoEOSMode = "DF9B";

        //version 1.2.3
        public const string RebootHours = "DFAA";

        public const string EnableAutoTip = "DFAC";
        public const string EnableDualAPNMode = "DFBB"; // 1- Dual APN , 0-Single APN

        //version 1.2.4
        public const string CellTowerUpdateInterval = "DFBC"; //in minutes

        public const string UpdateCellTowerMode = "DFBD"; //"1" enables. If this is enabled, and DFBC is set to "0", then a message is sent on every change. If this is enabled and DFBC is set non-zero, then the interval message will only be sent if it is different from the last one.
        public const string VAAIPAddress = "DFBE";
        public const string VAAPHostPort = "DFBF";

        //version 1.5.0
        public const string EnableReceiptReference = "DFE2";

        public const string BusinessTypeIndicator = "DFE3";
        public const string EnableAutoTipTwo = "1F63";

        public const string SkinSelection = "1F6B";

        public const string EnableOnlineOrder = "1F73";

        public const string CardPresentTransactionLimit = "1FC8";
        public const string CardNotPresentTransactionLimit = "1FC9";

        #endregion Terminal Configuration Table

        #region Card Table
        public const string CardTableEntry = "FF44";
        public const string CardStartRange = "DF45";
        public const string CardServiceFeeMode = "DF47";
        public const string CardEndRange = "DF46";
        public const string CardServiceFeeValue = "DF48";

        #endregion Card Table

        #region Tips Tricks Table
        public const string TipTrickTableVer = "DF71";
        public const string TipTrickEntry = "FF72";
        public const string TipTrickTitle = "DF73";
        public const string TipTrickBody = "DF74";
        #endregion Tips Tricks Table

        #region Location Table
        public const string LocationTableVer = "DF75";
        public const string LocationbEntry = "FF76";
        public const string LocationID = "DF77";
        public const string LocationName = "DF78";
        public const string LocationType = "DF7A";
        #endregion Location Table

        #region Cell Tower Table
        public const string CellTowerTableVer = "DF79";
        public const string CellTowerEntry = "FF80";
        public const string CellTowerID = "DF81";

        public const string CellTowerLocationList = "FF82";
        #endregion Cell Tower Table

        #region Private Administrative Advice

        public const string DriverLicenseTrack1 = "DFAB";
        #endregion Private Administrative Advice

        #region Online Order Config Table
        public const string OnlineOrderConfigVer = "1F72";
        public const string IdleTime = "1F74";
        public const string PollTime = "1F75";
        public const string ETATimeOne = "1F76";
        public const string ETATimeTwo = "1F77";
        public const string ETATimeThree = "1F78";
        public const string ETATimeFour = "1F79";
        public const string DeclinedReasonOne = "1F7A";
        public const string DeclinedReasonTwo = "1F7B";
        public const string DeclinedReasonThree = "1F7C";
        public const string DeclinedReasonFour = "1F7D";
        public const string DeclinedReasonFive = "1F7E";
        public const string AlertTime = "1F7F";
        public const string OrderReceiptStoreTime = "1F80";
        #endregion Online Order Config Table

        #region Online Order Request/Response Message

        public const string OrderCount = "1F83";
        public const string OrderNumber = "1F84";
        public const string CustomerName = "1F85";
        public const string CustomerContactNumber = "1F86";
        public const string OrderTotalAmount = "1F87";
        public const string DeliveryAddress = "1F88";
        public const string OrderResponse = "1F89";
        public const string DeclineReason = "1F8A";
        public const string OrderDateTime = "1F8B";
        public const string OrderContent = "1F8C";
        public const string OrderWaitingTime = "1F91";

        #endregion Online Order Request/Response Message

        #region Online Order - Delivery Logo
        public const string DeliveryLogoVer = "1F8E";
        public const string DeliveryLogoData = "1F82";
        #endregion Online Order - Delivery Logo

        #region Online Order - Pickup Logo
        public const string PickupLogoVer = "1F8D";
        public const string PickupLogoData = "1F81";
        #endregion Online Order - Pickup Logo

        #region Online Order - Online Order Logo
        public const string OnlineOrderLogoVer = "1F90";
        public const string OnlineOrderLogoData = "1F8F";
        #endregion Online Order - Online Order Logo
        #region Settlement Get List Response
        public const string TransactionList = "DFE8";
        #endregion Settlement Get List Response

        //version 1.4.9
        public const string BatchNumberToResend = "1F6A";

        #region QantasPointsRequest
        public const string EnableQantasReward = "1FA2";
        public const string MembershipTransactionNumber = "1FA4";
        public const string PushNotificationMessage = "1FA9";

        public const string RewardProgram = "1FA3";

        #endregion QantasPointsRequest

        #region Alipay
        public const string AlipayQRCode = "1F9A";
        public const string InvoiceNumber = "1F9B";
        public const string CustomerAccountNumber = "1F9C";

        public const string ResponseText = "1F9D";
        public const string AlipayStatusErrorCode = "1F9E";
        public const string EnableAlipay = "1F9F";

        #endregion Alipay

        #region Business Logo
        public const string EnableBusinessLogo = "1FA6";
        public const string BusinesLogoTableVersion = "1FA7";
        public const string BusinessLogo = "1FA8";
        #endregion Business Logo

        #region Qantas
        public const string PaymentProvider = "1FAA";
        public const string TotalPointsRedeemed = "1FAB";
        public const string EnableQantasPointsRedemption = "1FAC";
        public const string PIN = "1FAD";
        public const string RewardResponseMessage = "1FA5";
        #endregion Qantas
        public const string AddLevyChargeToAmount = "1FAE";

        public const string EnablePaperRollOrder = "1F94";
        public const string EnableSupportCallBack = "1F95";
        public const string RefundTransMax = "1FB1";
        public const string RefundCumulativeMax = "1FB2";

        public const string ZipStoreCode = "1FB3";
        public const string PaymentAppMerchantID = "1FB4";
        public const string PaymentProviderResponseMessage = "1FB5";
        public const string PaperRollNumber = "1FB6";
        public const string EnableZip = "1FB8";
        public const string EnableGPS = "1FB9";
        public const string TransactionReceiptText = "1FAF";

        public const string ReceiptNumber = "1FB7";

        public const string MembershipNumberEntryMethod = "1FBB";

        public const string IdleTimer = "1FBC";
        public const string IdleTransactions = "1FBD";
        public const string DefaultTaxiNetworkID = "1FBE";

        public const string OriginalTransactionGUID = "1FBA";

        # region NotificationMessage - 9640/9650
        public const string RecordingMode = "1FC0";
        public const string UploadTimer = "1FC1";
        public const string RecordingFromVAA = "1FC2";
        #endregion

        #region Glidebox tags for message 0800
        public const string EnableGlidebox = "1FC3";
        #endregion

        #region Glidebox tags for message 0220
        public const string GlideboxSaleAmount = "1FC6";
        public const string GlideboxItemSold = "1FC5";
        #endregion

        #region Glidebox table for message 0300
        public const string GlideboxItems = "1FC4";
        public const string GlideboxTableVer = "1FF9";
        #endregion

        #region Glidebox tags for message 0500
        public const string DebitGlideboxAmount = "1FCD";
        public const string DebitGlideboxNumber = "1FCC";
        public const string ReverseGlideboxAmount = "1FCF";
        public const string ReverseGlideboxNumber = "1FCE";
        public const string NetGlideboxAmount = "1FD0";
        #endregion

        #region Receipt Overhaul
        public const string PrintReceiptMode = "1FCB";
        public const string EnablePrintCustomerReceiptFirst = "1FD2";
        public const string EnablePrintSettlement = "1FD1";
        #endregion

        #region Footer Logo Table
        public const string FooterLogoVer = "1FA7";
        public const string FooterLogoData = "1FA8";
        #endregion

        public const string BrandName_PhoneNumber = "1FD3";
    }
}