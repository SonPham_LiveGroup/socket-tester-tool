﻿using Comms.Message.AS2805;
using System;
using Util.Tlv;

namespace Host.Shared
{
    public abstract class MessageFactory
    {
    }

    public enum ProcessingCodes
    {
        // for 800 messages
        Initialise = 900000,

        Logon = 910000,

        // for 220 messages.. TODO - Do we need account types too?
        NormalSale = 000000,

        Void = 200000,
        PurchaseWithCash = 090000,
        CashAdvance = 010000,
        Refund = 220000,

        //for 9620 message.
        NewTerminal = 930000,

        Swap = 940000,
        Return = 950000,
        UpdateInformation = 960000,
        CellTowerInfo = 970000,
        OnlineOrderRequest = 450000,
        OnlineOrderStatusUpdate = 460000,
        QantasPointsBurningQuote = 470000,
        ChangeSettlementTime = 870000,
        PaperRollRequest = 980000,
        SupportCallbackRequest = 990000
    }

    public class LiveMessageFactory : MessageFactory
    {
        /// <summary>
        /// Converts as AS2805 message in the Live host message format
        /// into a LiveMessage object to be used by the host
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>A LiveMessage object</returns>
        /// <exception cref="System.Exception"></exception>
        public LiveMessage ToLiveMessage(AS2805Message message)
        {
            LiveMessageBuilder builder = null;

            switch (message.MessageTypeIdentifier)
            {
                //0800 request
                case MessageTypeIdentifiers.NetworkManagementRequest:
                    if (message.ProcessingCode == (int)ProcessingCodes.Logon)
                        builder = new LogonRequestMessageBuilder(message);
                    else if (message.ProcessingCode == (int)ProcessingCodes.Initialise)
                        builder = new InitialisationRequestMessageBuilder(message);
                    break;
                //0810 response
                case MessageTypeIdentifiers.NetworkManagementResponse:
                    builder = new LogonResponseMessageBuilder(message);
                    break;

                //0300 request
                case MessageTypeIdentifiers.TableRequest:
                    builder = new FileDownloadRequestBuilder(message);
                    break;
                //0310 reponse
                case MessageTypeIdentifiers.TableRequestResponse:
                    builder = new FileDownloadResponseBuilder(message);
                    break;

                //0220 request
                case MessageTypeIdentifiers.FinancialTransactionAdvice:
                    builder = new FinancialAdviceMessageBuilder(message);
                    break;
                //0230 response
                case MessageTypeIdentifiers.FinancialTransactionAdviceResponse:
                    builder = new FinancialAdviceResponseMessageBuilder(message);
                    break;

                //0500 request
                case MessageTypeIdentifiers.SettlementRequest:
                    builder = new SettlementAdviceBuilder(message);
                    break;
                //0510 response
                case MessageTypeIdentifiers.SettlementRequestResponse:
                    builder = new SettlementAdviceResponseBuilder(message);
                    break;

                case MessageTypeIdentifiers.PrivateAdministrativeAdvice:
                    builder = new PrivateAdministrativeAdviceBuilder(message);
                    break;

                case MessageTypeIdentifiers.SettlementGetListRequest:
                    builder = new SettlementListRequestBuilder(message);
                    break;

                case MessageTypeIdentifiers.OnlineOrderRequest:
                    if (message.ProcessingCode == (int)ProcessingCodes.OnlineOrderRequest)
                        builder = new OnlineOrderRequestBuilder(message);
                    else if (message.ProcessingCode == (int)ProcessingCodes.OnlineOrderStatusUpdate)
                        builder = new OnlineOrderStatusAdviceBuilder(message);
                    break;

                case MessageTypeIdentifiers.RewardPointsRequest:
                    builder = new RewardPointsRequestBuilder(message);
                    break;

                case MessageTypeIdentifiers.FinancialTransactionRequest:
                    builder = new FinancialRequestMessageBuilder(message);
                    break;

                case MessageTypeIdentifiers.TransactionStatusRequest:
                    builder = new TransactionStatusRequestBuilder(message);
                    break;

                case MessageTypeIdentifiers.NotificationMessageRequest:
                    builder = new NotificationMessageRequestBuilder(message);
                    break;

                default:
                    builder = null;
                    break;
            }
            if (builder != null)
                return builder.BuildMessage();
            else
                throw new FormatErrorException();
        }
    }

    public class AS2805MessageBuilder
    {
        public AS2805MessageBuilder(LiveMessage message)
        {
            this.message = message;
            this.messageReturn = new AS2805Message();
        }

        protected LiveMessage message;
        protected AS2805Message messageReturn;

        public AS2805Message AS2805Message
        {
            get
            {
                return messageReturn;
            }
        }
    }

    public abstract class LiveMessageBuilder
    {
        protected LiveMessage messageReturn;
        protected AS2805Message message;
        private TagList tags63;

        protected TagList Tags63
        {
            get
            {
                if (tags63 == null && !String.IsNullOrEmpty(this.message.PrivateData63))
                    tags63 = new TagList(this.message.PrivateData63);
                return tags63;
            }
        }

        private TagList tags60;

        protected TagList Tags60
        {
            get
            {
                if (tags60 == null && !String.IsNullOrEmpty(this.message.PrivateData60))
                    tags60 = new TagList(this.message.PrivateData60);
                return tags60;
            }
        }

        private TagList tags62;

        protected TagList Tags62
        {
            get
            {
                if (tags62 == null && !String.IsNullOrEmpty(this.message.PrivateData62))
                    tags62 = new TagList(this.message.PrivateData62);
                return tags62;
            }
        }

        public LiveMessageBuilder(AS2805Message message)
        {
            this.message = message;
        }

        public LiveMessage LiveMessage
        {
            get
            {
                return messageReturn;
            }
        }

        public LiveMessage BuildMessage()
        {
            BuildStandardFields();
            BuildPrivateFields();
            return messageReturn;
        }

        private void BuildStandardFields()
        {
            if (message.HasField(11))
            {
                messageReturn.STAN = $"Field No: 11 - Value: {message.STAN}";
            }

            if (message.HasField(42))
            {
                messageReturn.MerchantID = $"Field No: 42 - Value: {message.MerchantID.Trim()}";
            }

            if (message.HasField(41))
            {
                messageReturn.TerminalID = $"Field No: 41 - Value: {message.TerminalID}";
            }

            if (message.HasField(24))
            {
                messageReturn.NII = $"Field No: 24 - Value: {message.NII}";
            }

            if (message.HasField(37))
            {
                messageReturn.RRN = $"Field No: 37 - Value: {message.RRN}";
            }

            /*

             * put code to copy data from AS2805 message to Live message
             * */
        }

        protected abstract void BuildPrivateFields();

        public string ConvertHexStringToTLVTag(string tagId)
        {
            return $"TagId: {tagId} - Value: {Tags63.GetTagValueString(tagId)}";
        }
    }

    public class InitialisationRequestMessageBuilder : LiveMessageBuilder
    {
        public InitialisationRequestMessageBuilder(AS2805Message message)
            : base(message)
        {
            if (message.MessageTypeIdentifier == MessageTypeIdentifiers.NetworkManagementRequest)
            {
                this.messageReturn = new LogonRequestMessage();

                if (message.ProcessingCode == 900000)
                {
                    ((LogonRequestMessage)this.messageReturn).IsLogonRequest = false;
                    ((LogonRequestMessage)this.messageReturn).IsInitialisation = true;
                }
                else if (message.ProcessingCode == 910000)
                {
                    ((LogonRequestMessage)this.messageReturn).IsLogonRequest = true;
                    ((LogonRequestMessage)this.messageReturn).IsInitialisation = false;
                }
            }
            else
            {
                this.messageReturn = new LogonResponseMessage();
            }
        }

        protected override void BuildPrivateFields()
        {
            /*
             * Code to copy data from TLV tags in AS2805 message to Live Message
             * */
            LogonRequestMessage lmessage = (LogonRequestMessage)this.messageReturn;
            BuildCommonFields();
        }

        protected void BuildCommonFields()
        {
            LogonMessage lmessage = (LogonMessage)this.messageReturn;
            lmessage.BatchNumber = $"TagId: {TlvTagIds.BatchNumber} - Value: {Tags63.GetTagValueString(TlvTagIds.BatchNumber)}";

            lmessage.SerialNumber = $"TagId: {TlvTagIds.SerialNumber} - Value: {Tags63.GetTagValueString(TlvTagIds.SerialNumber)}";
            lmessage.TerminalModel = $"TagId: {TlvTagIds.TerminalModel} - Value: {Tags63.GetTagValueString(TlvTagIds.TerminalModel)}";
            lmessage.IMEI = $"TagId: {TlvTagIds.IMEI} - Value: {Tags63.GetTagValueString(TlvTagIds.IMEI)}";
            lmessage.SimSerialNo = $"TagId: {TlvTagIds.SimSerialNo} - Value: {Tags63.GetTagValueString(TlvTagIds.SimSerialNo)}";
            lmessage.SimMobileNumber = $"TagId: {TlvTagIds.SimMobileNumber} - Value: {Tags63.GetTagValueString(TlvTagIds.SimMobileNumber)}";
            lmessage.PTID = $"TagId: {TlvTagIds.PTID} - Value: {Tags63.GetTagValueString(TlvTagIds.PTID)}";

            lmessage.BatteryStatus = $"TagId: {TlvTagIds.BatteryStatus} - Value: {TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.BatteryStatus)}";
            lmessage.SignalStrength = $"TagId: {TlvTagIds.SignalStrength} - Value: {TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.SignalStrength)}";
            lmessage.TerminalOSSoftwareVer = $"TagId: {TlvTagIds.TerminalOSSoftwareVer} - Value: {Tags63.GetTagValueString(TlvTagIds.TerminalOSSoftwareVer)}";
            lmessage.VAASoftwareVer = $"TagId: {TlvTagIds.VAASoftwareVer} - Value: {Tags63.GetTagValueString(TlvTagIds.VAASoftwareVer)}";
            lmessage.ApplicationType = $"TagId: {TlvTagIds.ApplicationType} - Value: {Tags63.GetTagValueString(TlvTagIds.ApplicationType)}";
            lmessage.TerminalConfigVersion = $"TagId: {TlvTagIds.TerminalConfigVersion} - Value: {Tags63.GetTagValueString(TlvTagIds.TerminalConfigVersion)}";
            lmessage.ReceiptDataVer = $"TagId: {TlvTagIds.ReceiptVerNo} - Value: {Tags63.GetTagValueString(TlvTagIds.ReceiptVerNo)}";
            lmessage.PrinterLogoVer = $"TagId: {TlvTagIds.PrinterLogoVer} - Value: {Tags63.GetTagValueString(TlvTagIds.PrinterLogoVer)}";
            lmessage.CardTableVer = $"TagId: {TlvTagIds.CardTableVer} - Value: {Tags63.GetTagValueString(TlvTagIds.CardTableVer)}";
            lmessage.PromptTableVer = $"TagId: {TlvTagIds.PromptTableVer} - Value: {Tags63.GetTagValueString(TlvTagIds.PromptTableVer)}";
            lmessage.NetworkTableVer = $"TagId: {TlvTagIds.NetworkTableVer} - Value: {Tags63.GetTagValueString(TlvTagIds.NetworkTableVer)}";
            lmessage.TransactionDateTime = $"TagId: {TlvTagIds.TransactionDateTime} - Value: {Tags63.GetTagValueString(TlvTagIds.TransactionDateTime)}";
            lmessage.ScreenLogoVer = $"TagId: {TlvTagIds.ScreenLogoVer} - Value: {Tags63.GetTagValueString(TlvTagIds.ScreenLogoVer)}";
            lmessage.TerminalOSSoftwareVer = $"TagId: {TlvTagIds.TerminalOSSoftwareVer} - Value: {Tags63.GetTagValueString(TlvTagIds.TerminalOSSoftwareVer)}";
            lmessage.VAASoftwareVer = $"TagId: {TlvTagIds.VAASoftwareVer} - Value: {Tags63.GetTagValueString(TlvTagIds.VAASoftwareVer)}";
            lmessage.TerminalFSVer = $"TagId: {TlvTagIds.TerminalFSVer} - Value: {Tags63.GetTagValueString(TlvTagIds.TerminalFSVer)}";
            lmessage.TipsTricksVer = $"TagId: {TlvTagIds.TipTrickTableVer} - Value: {Tags63.GetTagValueString(TlvTagIds.TipTrickTableVer)}";
            lmessage.LocationTableVer = $"TagId: {TlvTagIds.LocationTableVer} - Value: {Tags63.GetTagValueString(TlvTagIds.LocationTableVer)}";
            lmessage.CellTowerVer = $"TagId: {TlvTagIds.CellTowerTableVer} - Value: {Tags63.GetTagValueString(TlvTagIds.CellTowerTableVer)}";
            lmessage.CellTowerID = $"TagId: {TlvTagIds.CellTowerID} - Value: {Tags63.GetTagValueString(TlvTagIds.CellTowerID)}";
            lmessage.CellTowerLAC = $"TagId: {TlvTagIds.CellTowerLAC} - Value: {Tags63.GetTagValueString(TlvTagIds.CellTowerLAC)}";
            lmessage.CellTowerMCC = $"TagId: {TlvTagIds.CellTowerMCC} - Value: {Tags63.GetTagValueString(TlvTagIds.CellTowerMCC)}";
            lmessage.CellTowerMNC = $"TagId: {TlvTagIds.CellTowerMNC} - Value: {Tags63.GetTagValueString(TlvTagIds.CellTowerMNC)}";
            lmessage.DeliveryLogoVer = $"TagId: {TlvTagIds.DeliveryLogoVer} - Value: {Tags63.GetTagValueString(TlvTagIds.DeliveryLogoVer)}";
            lmessage.OnlineOrderLogoVer = $"TagId: {TlvTagIds.OnlineOrderLogoVer} - Value: {Tags63.GetTagValueString(TlvTagIds.OnlineOrderLogoVer)}";
            lmessage.OnlineOrderConfigVer = $"TagId: {TlvTagIds.OnlineOrderConfigVer} - Value: {Tags63.GetTagValueString(TlvTagIds.OnlineOrderConfigVer)}";
            lmessage.PickupLogoVer = $"TagId: {TlvTagIds.PickupLogoVer} - Value: {Tags63.GetTagValueString(TlvTagIds.PickupLogoVer)}";
            lmessage.GlideboxTableVer = $"TagId: {TlvTagIds.GlideboxTableVer} - Value: {Tags63.GetTagValueString(TlvTagIds.GlideboxTableVer)}";
        }
    }

    public class LogonRequestMessageBuilder : InitialisationRequestMessageBuilder
    {
        //Log
        public LogonRequestMessageBuilder(AS2805Message message)
            : base(message)
        {
        }

        protected override void BuildPrivateFields()
        {
            /*
             * Code to copy data from TLV tags in AS2805 message to Live Message
             * */
            LogonRequestMessage lmessage = (LogonRequestMessage)this.messageReturn;
            BuildCommonFields();
            lmessage.MessageTypeID = $"{message.MessageTypeIdentifier.ToString()}";
            lmessage.ProcessingCode = $"Field No: 3 - Value: {message.ProcessingCode}";
            lmessage.BusinessID = ConvertHexStringToTLVTag(TlvTagIds.BusinessId);
            lmessage.LastTableUpdateDateTime = ConvertHexStringToTLVTag(TlvTagIds.LastTableUpdateTime);
            lmessage.UserID = ConvertHexStringToTLVTag(TlvTagIds.UserId);
            lmessage.ABN = ConvertHexStringToTLVTag(TlvTagIds.ABN);
            lmessage.NetworkID = ConvertHexStringToTLVTag(TlvTagIds.NetworkId);
            lmessage.PaymentAppMerchantID = ConvertHexStringToTLVTag(TlvTagIds.PaymentAppMerchantID);
        }
    }

    public class LogonResponseMessageBuilder : InitialisationRequestMessageBuilder
    {
        //Log
        public LogonResponseMessageBuilder(AS2805Message message)
            : base(message)
        {
        }

        protected override void BuildPrivateFields()
        {
            /*
             * Code to copy data from TLV tags in AS2805 message to Live Message
             * */
            LogonResponseMessage lmessage = (LogonResponseMessage)this.messageReturn;
            BuildCommonFields();
            lmessage.MessageTypeID = $"{message.MessageTypeIdentifier.ToString()}";
            lmessage.ProcessingCode = $"Field No: 3 - Value: {message.ProcessingCode}";

            lmessage.BusinessID = ConvertHexStringToTLVTag(TlvTagIds.BusinessId);
            lmessage.LastTableUpdateDateTime = ConvertHexStringToTLVTag(TlvTagIds.LastTableUpdateTime);
            lmessage.UserID = ConvertHexStringToTLVTag(TlvTagIds.UserId);
            lmessage.ABN = ConvertHexStringToTLVTag(TlvTagIds.ABN);
            lmessage.PaymentAppMerchantID = ConvertHexStringToTLVTag(TlvTagIds.PaymentAppMerchantID);

            lmessage.ConnectToTMS = ConvertHexStringToTLVTag(TlvTagIds.ConnectToTMS);
            lmessage.DownloadTMSConfig = ConvertHexStringToTLVTag(TlvTagIds.DownloadTMSConfig);
        }
    }

    public class FinancialAdviceMessageBuilder : LiveMessageBuilder
    {
        public FinancialAdviceMessageBuilder(AS2805Message message)
            : base(message)
        {
            this.messageReturn = new FinancialTranAdvice();
            this.messageReturn.ProcessingCode = message.ProcessingCode.ToString();

            switch (message.ProcessingCode)
            {
                case (int)ProcessingCodes.NormalSale:
                    ((FinancialTranAdvice)this.messageReturn).TransactionType = TransactionTypes.Purchase;
                    ((FinancialTranAdvice)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.Purchase.ToString()}";
                    break;

                case (int)ProcessingCodes.Void:
                    ((FinancialTranAdvice)this.messageReturn).TransactionType = TransactionTypes.Void;
                    ((FinancialTranAdvice)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.Void.ToString()}";
                    break;

                case (int)ProcessingCodes.PurchaseWithCash:
                    ((FinancialTranAdvice)this.messageReturn).TransactionType = TransactionTypes.PurchaseWithCashback;
                    ((FinancialTranAdvice)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.PurchaseWithCashback.ToString()}";
                    break;

                case (int)ProcessingCodes.CashAdvance:
                    ((FinancialTranAdvice)this.messageReturn).TransactionType = TransactionTypes.CashAdvance;
                    ((FinancialTranAdvice)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.CashAdvance.ToString()}";
                    break;

                case (int)ProcessingCodes.Refund:
                    ((FinancialTranAdvice)this.messageReturn).TransactionType = TransactionTypes.Refund;
                    ((FinancialTranAdvice)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.Refund.ToString()}";
                    break;

                default:
                    throw new Exception(string.Format("Processing code not supported for financial advice {0}", message.ProcessingCode));
            }
        }

        protected override void BuildPrivateFields()
        {
            FinancialTranAdvice fmessage = (FinancialTranAdvice)this.LiveMessage;
            fmessage.MessageTypeID = $"{message.MessageTypeIdentifier.ToString()}";
            fmessage.ProcessingCode = $"Field No: 3 - Value: {message.ProcessingCode}";
            fmessage.UserID = $"TagId: {TlvTagIds.UserId} - Value: {Tags63.GetTagValueString(TlvTagIds.UserId)}";
            fmessage.BusinessID = $"TagId: {TlvTagIds.BusinessId} - Value: {Tags63.GetTagValueString(TlvTagIds.BusinessId)}";
            fmessage.ABN = $"TagId: {TlvTagIds.ABN} - Value: {Tags63.GetTagValueString(TlvTagIds.ABN)}";
            fmessage.NetworkID = $"TagId: {TlvTagIds.NetworkId} - Value: {Tags63.GetTagValueString(TlvTagIds.NetworkId)}";
            fmessage.PickupLocationID = $"TagId: {TlvTagIds.PickupLocationID} - Value: {TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.PickupLocationID)}";
            fmessage.DropoffLocationID = $"TagId: {TlvTagIds.DropoffLocationID} - Value: {TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.DropoffLocationID)}";
            fmessage.GPSPosition = $"TagId: {TlvTagIds.GPSPosition} - Value: {Tags63.GetTagValueString(TlvTagIds.GPSPosition)}";
            fmessage.AmountsEntered = Tags63.GetTagValueString(TlvTagIds.AmountsEntered);
            fmessage.AmountsEnteredTag = $"TagId: {TlvTagIds.AmountsEntered} - Value: {Tags63.GetTagValueString(TlvTagIds.AmountsEntered)}";
            fmessage.TransactionResponseCode = $"TagId: {TlvTagIds.TransactionResponseCode} - Value: {Tags63.GetTagValueString(TlvTagIds.TransactionResponseCode)}";
            fmessage.TransactionNumber = $"TagId: {TlvTagIds.TransactionNumber} - Value: {Tags63.GetTagValueString(TlvTagIds.TransactionNumber)}";
            fmessage.CardDataMasked = $"TagId: {TlvTagIds.CardDataMasked} - Value: {Tags63.GetTagValueString(TlvTagIds.CardDataMasked)}";
            fmessage.CardExpiryDate = $"TagId: {TlvTagIds.CardExpiryDate} - Value: {Tags63.GetTagValueString(TlvTagIds.CardExpiryDate)}";
            fmessage.CustomerMobileNumber = $"TagId: {TlvTagIds.CustomerMobileNumber} - Value: {Tags63.GetTagValueString(TlvTagIds.CustomerMobileNumber)}";
            fmessage.TransactionDateTime = $"TagId: {TlvTagIds.TransactionDateTime} - Value: {Tags63.GetTagValueString(TlvTagIds.TransactionDateTime)}";
            fmessage.ServiceFee = $"TagId: {TlvTagIds.CardServiceFee} - Value: {Tags63.GetTagValueString(TlvTagIds.CardServiceFee)}";
            fmessage.GSTOnServiceFee = $"TagId: {TlvTagIds.GSTOnServiceFee} - Value: {Tags63.GetTagValueString(TlvTagIds.GSTOnServiceFee)}";
            fmessage.TotalAmount = $"TagId: {TlvTagIds.TotalAmount} - Value: {Tags63.GetTagValueString(TlvTagIds.TotalAmount)}";
            fmessage.AccountType = $"TagId: {TlvTagIds.AccountType} - Value: {Tags63.GetTagValueString(TlvTagIds.AccountType)}";
            fmessage.CardName = $"TagId: {TlvTagIds.CardName} - Value: {Tags63.GetTagValueString(TlvTagIds.CardName)}";
            fmessage.PaymentTransactionType = $"TagId: {TlvTagIds.TransactionType} - Value: {Tags63.GetTagValueString(TlvTagIds.TransactionType)}";
            fmessage.TransactionAuthCode = $"TagId: {TlvTagIds.TransactionAuthCode} - Value: {Tags63.GetTagValueString(TlvTagIds.TransactionAuthCode)}";
            fmessage.Amount = $"Field No: 4 - Value: { message.TransactionAmount}";
            fmessage.TransactionResponseTime = $"TagId: {TlvTagIds.TransactionResponseTime} - Value: {Convert.ToInt16(TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.TransactionResponseTime) / 10)}";
            fmessage.BatteryStatus = $"TagId: {TlvTagIds.BatteryStatus} - Value: {TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.BatteryStatus)}";
            fmessage.SignalStrength = $"TagId: {TlvTagIds.SignalStrength} - Value: {TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.SignalStrength)}";
            fmessage.ProcessingTIme = Convert.ToInt16(TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.ProcessingTime) / 10);
            fmessage.TranSTAN = $"TagId: {TlvTagIds.TranSTAN} - Value: {Tags63.GetTagValueString(TlvTagIds.TranSTAN)}";
            fmessage.TranReceiptResponseCode = $"TagId: {TlvTagIds.TranReceiptResponseCode} - Value: {Tags63.GetTagValueString(TlvTagIds.TranReceiptResponseCode)}";
            fmessage.TranEntryMode = $"TagId: {TlvTagIds.TranEntryMode} - Value: {Tags63.GetTagValueString(TlvTagIds.TranEntryMode)}";

            fmessage.CellTowerMNC = $"TagId: {TlvTagIds.CellTowerMNC} - Value: {Tags63.GetTagValueString(TlvTagIds.CellTowerMNC)}";
            fmessage.CellTowerMCC = $"TagId: {TlvTagIds.CellTowerMCC} - Value: {Tags63.GetTagValueString(TlvTagIds.CellTowerMCC)}";
            fmessage.CellTowerLAC = $"TagId: {TlvTagIds.CellTowerLAC} - Value: {Tags63.GetTagValueString(TlvTagIds.CellTowerLAC)}";
            fmessage.CellTowerID = $"TagId: {TlvTagIds.CellTowerID} - Value: {Tags63.GetTagValueString(TlvTagIds.CellTowerID)}";
            fmessage.CardHolderName = $"TagId: {TlvTagIds.CardHolderName} - Value: {Tags63.GetTagValueString(TlvTagIds.CardHolderName)}";

            fmessage.FunctionIndicator = $"TagId: {TlvTagIds.FunctionIndicator} - Value: {Tags63.GetTagValueString(TlvTagIds.FunctionIndicator)}";

            fmessage.AutoTipAmount = $"TagId: {TlvTagIds.AutoTipAmount} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipAmount)}";
            fmessage.AutoTipFee = $"TagId: {TlvTagIds.AutoTipFee} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipFee)}";
            fmessage.AutoTipSelection = $"TagId: {TlvTagIds.AutoTipSelection} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipSelection)}";
            fmessage.AutoTipFeeAppliedCents = $"TagId: {TlvTagIds.AutoTipFeeAppliedCents} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipFeeAppliedCents)}";
            fmessage.AutoTipFeeAppliedPercentage = $"TagId: {TlvTagIds.AutoTipFeeAppliedPercentage} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipFeeAppliedPercentage)}";

            fmessage.AutoTipTwoAmount = $"TagId: {TlvTagIds.AutoTipTwoAmount} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipTwoAmount)}";
            fmessage.AutoTipTwoFee = $"TagId: {TlvTagIds.AutoTipTwoFee} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipTwoFee)}";
            fmessage.AutoTipTwoSelection = $"TagId: {TlvTagIds.AutoTipTwoSelection} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipTwoSelection)}";
            fmessage.AutoTipTwoFeeAppliedCents = $"TagId: {TlvTagIds.AutoTipTwoFeeAppliedCents} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipTwoFeeAppliedCents)}";
            fmessage.AutoTipTwoFeeAppliedPercentage = $"TagId: {TlvTagIds.AutoTipTwoFeeAppliedPercentage} - Value: {Tags63.GetTagValueString(TlvTagIds.AutoTipTwoFeeAppliedPercentage)}";
            fmessage.ReceiptReference = $"TagId: {TlvTagIds.ReceiptReference} - Value: {Tags63.GetTagValueString(TlvTagIds.ReceiptReference)}";
            fmessage.LevyCharge = $"TagId: {TlvTagIds.LevyCharge} - Value: {Tags63.GetTagValueString(TlvTagIds.LevyCharge)}";
            fmessage.TransactionGUID = $"TagId: {TlvTagIds.TransactionGUID} - Value: {Tags63.GetTagValueString(TlvTagIds.TransactionGUID)}";
            fmessage.RewardMembershipNumber = $"TagId: {TlvTagIds.RewardMembershipNumber} - Value: {Tags63.GetTagValueString(TlvTagIds.RewardMembershipNumber)}";
            fmessage.RewardPointsEarned = $"TagId: {TlvTagIds.RewardPointsEarned} - Value: {Tags63.GetTagValueString(TlvTagIds.RewardPointsEarned)}";
            fmessage.TransactionReceiptText = $"TagId: {TlvTagIds.TransactionReceiptText} - Value: {Tags63.GetTagValueString(TlvTagIds.TransactionReceiptText)}";
            fmessage.PaymentAppMerchantID = $"TagId: {TlvTagIds.PaymentAppMerchantID} - Value: {Tags63.GetTagValueString(TlvTagIds.PaymentAppMerchantID)}";
            fmessage.TransactionReceiptNumber = $"TagId: {TlvTagIds.TransactionReceiptNumber} - Value: {Tags63.GetTagValueString(TlvTagIds.TransactionReceiptNumber)}";
            fmessage.MembershipNumberEntryMethod = $"TagId: {TlvTagIds.MembershipNumberEntryMethod} - Value: {Tags63.GetTagValueString(TlvTagIds.MembershipNumberEntryMethod)}";
            fmessage.GlideboxSaleAmount = $"TagId: {TlvTagIds.GlideboxSaleAmount} - Value: {Tags63.GetTagValueString(TlvTagIds.GlideboxSaleAmount)}";
            fmessage.GlideboxItem = $"TagId: {TlvTagIds.GlideboxItemSold} - Value: {Tags63.GetTagValueString(TlvTagIds.GlideboxItemSold)}";
        }
    }

    public class FinancialAdviceResponseMessageBuilder : LiveMessageBuilder
    {
        public FinancialAdviceResponseMessageBuilder(AS2805Message message)
            : base(message)
        {
            this.messageReturn = new FinancialTranAdviceResponse();
            this.messageReturn.ProcessingCode = message.ProcessingCode.ToString();

            switch (message.ProcessingCode)
            {
                case (int)ProcessingCodes.NormalSale:
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionType = TransactionTypes.Purchase;
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.Purchase.ToString()}";
                    break;

                case (int)ProcessingCodes.Void:
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionType = TransactionTypes.Void;
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.Void.ToString()}";
                    break;

                case (int)ProcessingCodes.PurchaseWithCash:
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionType = TransactionTypes.PurchaseWithCashback;
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.PurchaseWithCashback.ToString()}";
                    break;

                case (int)ProcessingCodes.CashAdvance:
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionType = TransactionTypes.CashAdvance;
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.CashAdvance.ToString()}";
                    break;

                case (int)ProcessingCodes.Refund:
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionType = TransactionTypes.Refund;
                    ((FinancialTranAdviceResponse)this.messageReturn).TransactionTypeTag = $"TagId: {TlvTagIds.TransactionType} - Value: {TransactionTypes.Refund.ToString()}";
                    break;

                default:
                    throw new Exception(string.Format("Processing code not supported for financial advice {0}", message.ProcessingCode));
            }
        }

        protected override void BuildPrivateFields()
        {
            FinancialTranAdviceResponse fmessage = (FinancialTranAdviceResponse)this.LiveMessage;
            fmessage.MessageTypeID = $"{message.MessageTypeIdentifier.ToString()}";
            fmessage.ProcessingCode = $"Field No: 3 - Value: {message.ProcessingCode}";
            fmessage.Amount = $"Field No: 4 - Value: {message.TransactionAmount}";

            fmessage.TransactionGUID = ConvertHexStringToTLVTag(TlvTagIds.TransactionGUID);
        }
    }

    public class FileDownloadRequestBuilder : LiveMessageBuilder
    {
        public FileDownloadRequestBuilder(AS2805Message message)
            : base(message)
        {
            this.messageReturn = new FileDownloadRequest();
        }

        protected override void BuildPrivateFields()
        {
            FileDownloadRequest fmessage = (FileDownloadRequest)this.messageReturn;
            fmessage.MessageTypeID = $"{message.MessageTypeIdentifier.ToString()}";
            fmessage.MessageNumber = $"Field No: 71 - Value: {message.MessageNumber}";
            if (message.HasField(72))
            {
                fmessage.MessageNumberLast = $"Field No: 72 - Value: {message.MessageNumberLast}";
            }

            fmessage.Filename = $"Field No: 101 - Value: {message.FileName}";
            fmessage.AuthIdResponse = $"Field No: 60 - Value: {message.PrivateData60}";
        }
    }

    public class FileDownloadResponseBuilder : LiveMessageBuilder
    {
        public FileDownloadResponseBuilder(AS2805Message message)
            : base(message)
        {
            this.messageReturn = new FileDownloadResponse();
        }

        protected override void BuildPrivateFields()
        {
            FileDownloadResponse fmessage = (FileDownloadResponse)this.messageReturn;
            fmessage.MessageTypeID = $"{message.MessageTypeIdentifier.ToString()}";
            fmessage.MessageNumber = $"Field No: 71 - Value: {message.MessageNumber}";
            if (message.HasField(72))
            {
                fmessage.MessageNumberLast = $"Field No: 72 - Value: {message.MessageNumberLast}";
            }
            fmessage.AuthIdResponse = $"Field No: 60 - Value: {message.PrivateData60}";

            #region Prompts Table

            var promptTableContent = new PromptTableContent
            {
                PromptTableVer = ConvertHexStringToTLVTag(TlvTagIds.PromptTableVer),
                UserIdPrompt = ConvertHexStringToTLVTag(TlvTagIds.UserIdPrompt),
                BusinessId = ConvertHexStringToTLVTag(TlvTagIds.BusinessId),
                AmountIdPrompt = ConvertHexStringToTLVTag(TlvTagIds.AmountIdPrompt),

                LocationOnePrompt = ConvertHexStringToTLVTag(TlvTagIds.LocationOnePrompt),
                LocationTwoPrompt = ConvertHexStringToTLVTag(TlvTagIds.LocationTwoPrompt),
                DisableLocationOne = ConvertHexStringToTLVTag(TlvTagIds.DisableLocationOne),
                DisableLocationTwo = ConvertHexStringToTLVTag(TlvTagIds.DisableLocationTwo),
                EOSApprovedText = ConvertHexStringToTLVTag(TlvTagIds.EOSApprovedText),
                EOSBatchOpenText = ConvertHexStringToTLVTag(TlvTagIds.EOSBatchOpenText),
                EOSButtonText = ConvertHexStringToTLVTag(TlvTagIds.EOSButtonText),
                EOSConfirmText = ConvertHexStringToTLVTag(TlvTagIds.EOSConfirmText),
                EOSNotFoundText = ConvertHexStringToTLVTag(TlvTagIds.EOSNotFoundText),
                EOSPriorText = ConvertHexStringToTLVTag(TlvTagIds.EOSPriorText),

                AutoTipNumOne = ConvertHexStringToTLVTag(TlvTagIds.AutoTipNumOne),
                AutoTipNumTwo = ConvertHexStringToTLVTag(TlvTagIds.AutoTipNumTwo),
                AutoTipNumThree = ConvertHexStringToTLVTag(TlvTagIds.AutoTipNumThree),
                AutoTipNumFour = ConvertHexStringToTLVTag(TlvTagIds.AutoTipNumFour),

                AutoTipIsPercentOne = ConvertHexStringToTLVTag(TlvTagIds.AutoTipIsPercentOne),
                AutoTipIsPercentTwo = ConvertHexStringToTLVTag(TlvTagIds.AutoTipIsPercentTwo),
                AutoTipIsPercentThree = ConvertHexStringToTLVTag(TlvTagIds.AutoTipIsPercentThree),
                AutoTipIsPercentFour = ConvertHexStringToTLVTag(TlvTagIds.AutoTipIsPercentFour),
                AutoTipDisplayOption = ConvertHexStringToTLVTag(TlvTagIds.AutoTipDisplayOption),

                EnableAutoTipAmountDisplay = ConvertHexStringToTLVTag(TlvTagIds.EnableAutoTipAmountDisplay),
                EnableAutoTipTotalDisplay = ConvertHexStringToTLVTag(TlvTagIds.EnableAutoTipTotalDisplay),
                AutoTipTitle = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTitle),
                AutoTipPriorAmountLabel = ConvertHexStringToTLVTag(TlvTagIds.AutoTipPriorAmountLabel),

                AutoTipEnterConfirmation = ConvertHexStringToTLVTag(TlvTagIds.AutoTipEnterConfirmation),
                AutoTipEOSReportOption = ConvertHexStringToTLVTag(TlvTagIds.AutoTipEOSReportOption),
                AutoTipEOSReportMessage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipEOSReportMessage),

                AutoTipButtonOneFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonOneFeeCents),
                AutoTipButtonOneFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonOneFeePercentage),

                AutoTipButtonTwoFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonTwoFeeCents),
                AutoTipButtonTwoFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonTwoFeePercentage),

                AutoTipButtonThreeFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonThreeFeeCents),
                AutoTipButtonThreeFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonThreeFeePercentage),

                AutoTipButtonFourFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonFourFeeCents),
                AutoTipButtonFourFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonFourFeePercentage),

                AutoTipManualEntryFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipManualEntryFeeCents),
                AutoTipManualEntryFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipManualEntryFeePercentage),

                AutoTipAmountLabel = ConvertHexStringToTLVTag(TlvTagIds.AutoTipAmountLabel),
                AutoTipFeeIncluded = ConvertHexStringToTLVTag(TlvTagIds.AutoTipFeeIncluded),

                AutoTipButtonOneID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonOneID),
                AutoTipButtonTwoID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonTwoID),
                AutoTipButtonThreeID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonThreeID),
                AutoTipButtonFourID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipButtonFourID),

                AutoTipDefaultButtonID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipDefaultButtonID),
                AutoTipScreenText = ConvertHexStringToTLVTag(TlvTagIds.AutoTipScreenText),

                //Change for version 1.4.0

                AutoTipTwoNumOne = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoNumOne),
                AutoTipTwoNumTwo = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoNumTwo),
                AutoTipTwoNumThree = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoNumThree),
                AutoTipTwoNumFour = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoNumFour),

                AutoTipTwoIsPercentOne = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoIsPercentOne),
                AutoTipTwoIsPercentTwo = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoIsPercentTwo),
                AutoTipTwoIsPercentThree = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoIsPercentThree),
                AutoTipTwoIsPercentFour = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoIsPercentFour),
                AutoTipTwoDisplayOption = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoDisplayOption),
                EnableAutoTipTwoAmountDisplay = ConvertHexStringToTLVTag(TlvTagIds.EnableAutoTipTwoAmountDisplay),
                EnableAutoTipTwoTotalDisplay = ConvertHexStringToTLVTag(TlvTagIds.EnableAutoTipTwoTotalDisplay),
                AutoTipTwoTitle = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoTitle),
                AutoTipTwoPriorAmountLabel = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoPriorAmountLabel),
                AutoTipTwoEnterConfirmation = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoEnterConfirmation),
                AutoTipTwoEOSReportOption = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoEOSReportOption),
                AutoTipTwoEOSReportMessage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoEOSReportMessage),

                AutoTipTwoButtonOneFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonOneFeeCents),
                AutoTipTwoButtonOneFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonOneFeePercentage),

                AutoTipTwoButtonTwoFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonTwoFeeCents),
                AutoTipTwoButtonTwoFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonTwoFeePercentage),

                AutoTipTwoButtonThreeFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonThreeFeeCents),
                AutoTipTwoButtonThreeFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonThreeFeePercentage),

                AutoTipTwoButtonFourFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonFourFeeCents),
                AutoTipTwoButtonFourFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonFourFeePercentage),

                AutoTipTwoManualEntryFeeCents = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoManualEntryFeeCents),
                AutoTipTwoManualEntryFeePercentage = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoManualEntryFeePercentage),

                AutoTipTwoAmountLabel = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoAmountLabel),
                AutoTipTwoFeeIncluded = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoFeeIncluded),

                AutoTipTwoButtonOneID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonOneID),
                AutoTipTwoButtonTwoID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonTwoID),
                AutoTipTwoButtonThreeID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonThreeID),
                AutoTipTwoButtonFourID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoButtonFourID),

                AutoTipTwoDefaultButtonID = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoDefaultButtonID),
                AutoTipTwoScreenText = ConvertHexStringToTLVTag(TlvTagIds.AutoTipTwoScreenText),

                ReceiptReferenceLabel = ConvertHexStringToTLVTag(TlvTagIds.ReceiptReferenceLabel),
                ReceiptReferenceText = ConvertHexStringToTLVTag(TlvTagIds.ReceiptReferenceText),
                LevyCharge = ConvertHexStringToTLVTag(TlvTagIds.LevyCharge),
                LevyLabel = ConvertHexStringToTLVTag(TlvTagIds.LevyLabel)
            };

            fmessage.PromptTableContent = promptTableContent;

            #endregion Prompts Table

            #region Terminal Network Table

            var terminalNetworkTable = new TerminalNetworkTable()
            {
                TerminalNetworkVersion = ConvertHexStringToTLVTag(TlvTagIds.NetworkTableVer),
                AvailableNetworks = ConvertHexStringToTLVTag(TlvTagIds.AvailableNetworks),
            };

            fmessage.TerminalNetworkTable = terminalNetworkTable;

            #endregion Terminal Network Table

            #region Terminal Config Table
            var terminalConfigTable = new TerminalConfigTable()
            {
                TerminalConfigVersion = ConvertHexStringToTLVTag(TlvTagIds.TerminalConfigVersion),
                UserInputTimeout = ConvertHexStringToTLVTag(TlvTagIds.UserInputTimeout),
                BacklightOffPeriod = ConvertHexStringToTLVTag(TlvTagIds.BacklightOffPeriod),
                SleepModeTime = ConvertHexStringToTLVTag(TlvTagIds.SleepModeTime),
                ScreenBrightness = ConvertHexStringToTLVTag(TlvTagIds.ScreenBrightness),
                SoundOnOff = ConvertHexStringToTLVTag(TlvTagIds.SoundOnOff),
                RememberMeSettings = ConvertHexStringToTLVTag(TlvTagIds.ABNSetting),
                ABNSetting = ConvertHexStringToTLVTag(TlvTagIds.ABNSetting),
                MinimumCharge = ConvertHexStringToTLVTag(TlvTagIds.MinimumCharge),
                MaximumCharge = ConvertHexStringToTLVTag(TlvTagIds.MaximumCharge),

                NetworkTableStatus = ConvertHexStringToTLVTag(TlvTagIds.NetworkTableStatus),
                AutoEOSTime = ConvertHexStringToTLVTag(TlvTagIds.AutoEOSTime),
                LoginMode = ConvertHexStringToTLVTag(TlvTagIds.LoginMode),
                LoginParameterOptions = ConvertHexStringToTLVTag(TlvTagIds.LoginParameterOptions),
                Prefix = ConvertHexStringToTLVTag(TlvTagIds.Prefix),
                GPRSReconnectTimer = ConvertHexStringToTLVTag(TlvTagIds.GPRSReconnectTimer),
                ApprovedResponseCode = ConvertHexStringToTLVTag(TlvTagIds.ApprovedResponseCode),
                ApprovedSignatureResponseCode = ConvertHexStringToTLVTag(TlvTagIds.ApprovedSignatureResponseCode),
                IsPreTranPing = ConvertHexStringToTLVTag(TlvTagIds.IsPreTranPing),
                TranAdviceSendMode = ConvertHexStringToTLVTag(TlvTagIds.TranAdviceSendMode),
                DefaultServiceFee = ConvertHexStringToTLVTag(TlvTagIds.DefaultServiceFee),
                ServiceFeeMode = ConvertHexStringToTLVTag(TlvTagIds.ServiceFeeMode),
                CustomerPaymentType = ConvertHexStringToTLVTag(TlvTagIds.CustomerPaymentType),
                EnableMerchantLogoPrint = ConvertHexStringToTLVTag(TlvTagIds.EnableMerchantLogoPrint),
                EnableNetworkReceiptPrint = ConvertHexStringToTLVTag(TlvTagIds.EnableNetworkReceiptPrint),
                SignatureVerificationTimeOut = ConvertHexStringToTLVTag(TlvTagIds.SignatureVerificationTimeOut),
                AutoLogonHour = ConvertHexStringToTLVTag(TlvTagIds.AutoLogonHour),
                AutoLogonMode = ConvertHexStringToTLVTag(TlvTagIds.AutoLogonMode),
                EnableRefundTran = ConvertHexStringToTLVTag(TlvTagIds.EnableRefundTran),
                EnableAuthTran = ConvertHexStringToTLVTag(TlvTagIds.EnableAuthTran),
                EnableTipTran = ConvertHexStringToTLVTag(TlvTagIds.EnableTipTran),
                EnableCashOutTran = ConvertHexStringToTLVTag(TlvTagIds.EnableCashOutTran),
                MerchantPassword = ConvertHexStringToTLVTag(TlvTagIds.MerchantPassword),
                BankLogonRespCode = ConvertHexStringToTLVTag(TlvTagIds.BankLogonRespCode),
                DefaultBackgroundColour = ConvertHexStringToTLVTag(TlvTagIds.DefaultBackgroundColour),
                EnableMOTO = ConvertHexStringToTLVTag(TlvTagIds.EnableMOTO),
                EnableMOTORefund = ConvertHexStringToTLVTag(TlvTagIds.EnableMOTORefund),
                AutoEOSMode = ConvertHexStringToTLVTag(TlvTagIds.AutoEOSMode),
                RebootHours = ConvertHexStringToTLVTag(TlvTagIds.RebootHours),
                EnableAutoTip = ConvertHexStringToTLVTag(TlvTagIds.EnableAutoTip),
                EnableDualAPNMode = ConvertHexStringToTLVTag(TlvTagIds.EnableDualAPNMode),
                CellTowerUpdateInterval = ConvertHexStringToTLVTag(TlvTagIds.CellTowerUpdateInterval),
                UpdateCellTowerMode = ConvertHexStringToTLVTag(TlvTagIds.UpdateCellTowerMode),
                ClearOverrideSettings = ConvertHexStringToTLVTag(TlvTagIds.ClearOverrideSettings),

                EnableAutoTipTwo = ConvertHexStringToTLVTag(TlvTagIds.EnableAutoTipTwo),
                EnableReceiptReference = ConvertHexStringToTLVTag(TlvTagIds.EnableReceiptReference),
                BusinessTypeIndicator = ConvertHexStringToTLVTag(TlvTagIds.BusinessTypeIndicator),
                SkinSelection = ConvertHexStringToTLVTag(TlvTagIds.SkinSelection),
                EnableOnlineOrder = ConvertHexStringToTLVTag(TlvTagIds.EnableOnlineOrder),
                LevyCharge = ConvertHexStringToTLVTag(TlvTagIds.LevyCharge),
                LevyLabel = ConvertHexStringToTLVTag(TlvTagIds.LevyLabel),
                EnableAlipay = ConvertHexStringToTLVTag(TlvTagIds.EnableAlipay),
                EnableQantasReward = ConvertHexStringToTLVTag(TlvTagIds.EnableQantasReward),
                EnableBusinessLogo = ConvertHexStringToTLVTag(TlvTagIds.EnableBusinessLogo),

                EnableQantasPointsRedemption = ConvertHexStringToTLVTag(TlvTagIds.EnableQantasPointsRedemption),

                CustomFooter = ConvertHexStringToTLVTag(TlvTagIds.CustomFooter),
                AddLevyChargeToAmount = ConvertHexStringToTLVTag(TlvTagIds.AddLevyChargeToAmount),
                EnablePaperRollOrder = ConvertHexStringToTLVTag(TlvTagIds.EnablePaperRollOrder),
                EnableSupportCallBack = ConvertHexStringToTLVTag(TlvTagIds.EnableSupportCallBack),
                RefundTransMax = ConvertHexStringToTLVTag(TlvTagIds.RefundTransMax),
                RefundCumulativeMax = ConvertHexStringToTLVTag(TlvTagIds.RefundCumulativeMax),
                EnableZip = ConvertHexStringToTLVTag(TlvTagIds.EnableZip),

                IdleTimer = ConvertHexStringToTLVTag(TlvTagIds.IdleTimer),
                IdleTransactions = ConvertHexStringToTLVTag(TlvTagIds.IdleTransactions),

                DefaultTaxiNetworkID = ConvertHexStringToTLVTag(TlvTagIds.DefaultTaxiNetworkID),

                #region NotificationMessage 9640/9650

                RecordingMode = ConvertHexStringToTLVTag(TlvTagIds.RecordingMode),
                UploadTimer = ConvertHexStringToTLVTag(TlvTagIds.UploadTimer),

                #endregion NotificationMessage 9640/9650

                #region Glidebox

                EnableGlidebox = ConvertHexStringToTLVTag(TlvTagIds.EnableGlidebox),

                #endregion Glidebox

                CardPresentTransactionLimit = ConvertHexStringToTLVTag(TlvTagIds.CardPresentTransactionLimit),
                CardNotPresentTransactionLimit = ConvertHexStringToTLVTag(TlvTagIds.CardNotPresentTransactionLimit),
                BrandName_PhoneNumber = ConvertHexStringToTLVTag(TlvTagIds.BrandName_PhoneNumber),
            };
            fmessage.TerminalConfigTable = terminalConfigTable;
            #endregion Terminal Config Table

            #region Glidebox Item Table

            var glideboxItemTableContent = new GlideboxItemTableContent()
            {
                GlideboxTableVer = ConvertHexStringToTLVTag(TlvTagIds.GlideboxTableVer),
                GlideboxItems = ConvertHexStringToTLVTag(TlvTagIds.GlideboxItems),
            };

            fmessage.GlideboxItemTableContent = glideboxItemTableContent;

            #endregion Glidebox Item Table

            #region FooterLogo Table

            var footerLogoTableContent = new FooterLogoTableContent()
            {
                FooterLogoVer = ConvertHexStringToTLVTag(TlvTagIds.FooterLogoVer),
                FooterLogoData = ConvertHexStringToTLVTag(TlvTagIds.FooterLogoData),
            };

            fmessage.FooterLogoTableContent = footerLogoTableContent;

            #endregion FooterLogo Table

            #region Receipt Table
            var receiptTableContent = new ReceiptTableContent()
            {
                ReceiptVerNo = ConvertHexStringToTLVTag(TlvTagIds.ReceiptVerNo),
                ReceiptHeader1 = ConvertHexStringToTLVTag(TlvTagIds.ReceiptHeader1),
                ReceiptHeader2 = ConvertHexStringToTLVTag(TlvTagIds.ReceiptHeader2),
                ReceiptHeader3 = ConvertHexStringToTLVTag(TlvTagIds.ReceiptHeader3),
                ReceiptHeader4 = ConvertHexStringToTLVTag(TlvTagIds.ReceiptHeader4),
                ReceiptFooter1 = ConvertHexStringToTLVTag(TlvTagIds.ReceiptFooter1),
                ReceiptFooter2 = ConvertHexStringToTLVTag(TlvTagIds.ReceiptFooter2),
                ReceiptFooter3 = ConvertHexStringToTLVTag(TlvTagIds.ReceiptFooter3),
                ReceiptFooter4 = ConvertHexStringToTLVTag(TlvTagIds.ReceiptFooter4),
                BusinessIDReceiptTable = ConvertHexStringToTLVTag(TlvTagIds.BusinessIDReceiptLable),
                UserIDReceiptTable = ConvertHexStringToTLVTag(TlvTagIds.UserIDReceiptLabel),
                LocationOneReceiptLabel = ConvertHexStringToTLVTag(TlvTagIds.LocationOneReceiptLabel),
                LocationTwoReceiptLabel = ConvertHexStringToTLVTag(TlvTagIds.LocationTwoReceiptLabel),
                ReceiptAmountDiscriptor = ConvertHexStringToTLVTag(TlvTagIds.ReceiptAmountDiscriptor),
                IncludeServiceFeeInTotal = ConvertHexStringToTLVTag(TlvTagIds.IncludeServiceFeeInTotal),
                EOSTranAmountItemisation = ConvertHexStringToTLVTag(TlvTagIds.EOSTranAmountItemisation),
                EOSReceiptSummary = ConvertHexStringToTLVTag(TlvTagIds.EOSReceiptSummary),
                AutoEOSNotificationMode = ConvertHexStringToTLVTag(TlvTagIds.AutoEOSNotificationMode),
                AutoEOSOutputMode = ConvertHexStringToTLVTag(TlvTagIds.AutoEOSOutputMode),
                EOSReportText = ConvertHexStringToTLVTag(TlvTagIds.EOSReportText),
                BusinessCardInfo = ConvertHexStringToTLVTag(TlvTagIds.BusinessCardInfo),

                AutotipEOStext = ConvertHexStringToTLVTag(TlvTagIds.AutotipEOStext),
                Autotip2EOSText = ConvertHexStringToTLVTag(TlvTagIds.Autotip2EOSText),
                AutotipSummaryEOSText = ConvertHexStringToTLVTag(TlvTagIds.AutotipSummaryEOSText),
                AutoTipsInclude = ConvertHexStringToTLVTag(TlvTagIds.AutoTipsInclude),
                IncludeGSTText = ConvertHexStringToTLVTag(TlvTagIds.IncludeGSTText),
                CustomFooter = ConvertHexStringToTLVTag(TlvTagIds.CustomFooter),

                PrintReceiptMode = ConvertHexStringToTLVTag(TlvTagIds.PrintReceiptMode),
                EnablePrintCustomerReceiptFirst = ConvertHexStringToTLVTag(TlvTagIds.EnablePrintCustomerReceiptFirst),
                EnablePrintSettlement = ConvertHexStringToTLVTag(TlvTagIds.EnablePrintSettlement),
            };

            fmessage.ReceiptTableContent = receiptTableContent;
            #endregion
        }
    }

    public class SettlementAdviceBuilder : InitialisationRequestMessageBuilder// LiveMessageBuilder
    {
        public SettlementAdviceBuilder(AS2805Message messaage) :
            base(messaage)
        {
            this.messageReturn = new SettlementAdvice();
        }

        protected override void BuildPrivateFields()
        {
            SettlementAdvice smessage = (SettlementAdvice)this.LiveMessage;
            BuildCommonFields();

            smessage.MessageTypeID = $"{message.MessageTypeIdentifier.ToString()}";
            smessage.ProcessingCode = $"Field No: 3 - Value: {message.ProcessingCode}";

            smessage.SettlementCreditsNumber = $"Field No: 74 - Value: {message.CreditsNumber}";
            smessage.SettlementCreditAmount = $"Field No: 86 - Value: {message.CreditsAmount}";
            smessage.SettlementCreditReversalNumber = $"Field No: 75 - Value: {message.CreditsReversalNumber}";
            smessage.SettlementCreditReversalAmount = $"Field No: 87 - Value: {message.CreditsReversalAmount}";
            smessage.SettlementDebitsNumber = $"Field No: 76 - Value: {message.DebitsNumber}";
            smessage.SettlementDebitAmount = $"Field No: 88 - Value: {message.DebitsAmount}";
            smessage.SettlementDebitReversalNumber = $"Field No: 77 - Value: {message.DebitsReversalNumber}";
            smessage.SettlementDebitReversalAmount = $"Field No: 89 - Value: {message.DebitsReversalAmount}";
            smessage.SettlementNetAmount = $"Field No: 97 - Value: {message.SettlementAmount}";

            #region Glidebox tags

            smessage.DebitGlideboxAmount = $"TagId: {TlvTagIds.DebitGlideboxAmount} - Value: {TLVHelper.GetDecimalValueFromStringTag(Tags63, TlvTagIds.DebitGlideboxAmount, 3)}";
            smessage.ReverseGlideboxAmount = $"TagId: {TlvTagIds.ReverseGlideboxAmount}- Value: {TLVHelper.GetDecimalValueFromStringTag(Tags63, TlvTagIds.ReverseGlideboxAmount, 3)}";
            smessage.NetGlideboxAmount = $"TagId: {TlvTagIds.NetGlideboxAmount} - Value: {TLVHelper.GetDecimalValueFromStringTag(Tags63, TlvTagIds.NetGlideboxAmount, 3)}";
            smessage.SettlementDebitGlideboxNumber = $"TagId: {TlvTagIds.DebitGlideboxNumber} - Value: {TLVHelper.GetIntValueFromStringTag(Tags63, TlvTagIds.DebitGlideboxNumber)}";
            smessage.SettlementReverseGlideboxNumber = $"TagId: {TlvTagIds.ReverseGlideboxNumber} - Value: {TLVHelper.GetIntValueFromStringTag(Tags63, TlvTagIds.ReverseGlideboxNumber)}";

            #endregion Glidebox tags

            smessage.ScreenBrightness = $"TagId: {TlvTagIds.ScreenBrightness} - Value: {TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.ScreenBrightness)}";
            smessage.PaymentAppMerchantID = $"TagId: {TlvTagIds.PaymentAppMerchantID} - Value: {Tags63.GetTagValueString(TlvTagIds.PaymentAppMerchantID)}";
        }
    }

    public class SettlementAdviceResponseBuilder : LiveMessageBuilder// LiveMessageBuilder
    {
        public SettlementAdviceResponseBuilder(AS2805Message messaage) :
            base(messaage)
        {
            this.messageReturn = new SettlementResponse();
        }

        protected override void BuildPrivateFields()
        {
            SettlementResponse smessage = (SettlementResponse)this.LiveMessage;

            smessage.MessageTypeID = $"{message.MessageTypeIdentifier.ToString()}";
        }
    }

    public class PrivateAdministrativeAdviceBuilder : LiveMessageBuilder
    {
        public PrivateAdministrativeAdviceBuilder(AS2805Message message) : base(message)
        {
            this.messageReturn = new PrivateAdministrativeAdvice();
            this.messageReturn.ProcessingCode = message.ProcessingCode.ToString();
            switch (message.ProcessingCode)
            {
                case (int)ProcessingCodes.NewTerminal:
                    ((PrivateAdministrativeAdvice)this.messageReturn).AdminMessageType = AdminMessageTypes.NewTerminal;
                    break;

                case (int)ProcessingCodes.Swap:
                    ((PrivateAdministrativeAdvice)this.messageReturn).AdminMessageType = AdminMessageTypes.Swap;
                    break;

                case (int)ProcessingCodes.Return:
                    ((PrivateAdministrativeAdvice)this.messageReturn).AdminMessageType = AdminMessageTypes.Return;
                    break;

                case (int)ProcessingCodes.UpdateInformation:
                    ((PrivateAdministrativeAdvice)this.messageReturn).AdminMessageType = AdminMessageTypes.UpdateInformation;
                    break;

                case (int)ProcessingCodes.CellTowerInfo:
                    ((PrivateAdministrativeAdvice)this.messageReturn).AdminMessageType = AdminMessageTypes.CellTowerInfo;
                    break;

                case (int)ProcessingCodes.ChangeSettlementTime:
                    ((PrivateAdministrativeAdvice)this.messageReturn).AdminMessageType = AdminMessageTypes.ChangeSettlementTime;
                    break;

                case (int)ProcessingCodes.PaperRollRequest:
                    ((PrivateAdministrativeAdvice)this.messageReturn).AdminMessageType = AdminMessageTypes.PaperRollOrder;
                    break;

                case (int)ProcessingCodes.SupportCallbackRequest:
                    ((PrivateAdministrativeAdvice)this.messageReturn).AdminMessageType = AdminMessageTypes.SupportCallbackRequest;
                    break;

                default:
                    throw new Exception(string.Format("Processing code not supported for Private Admin Advice {0}", message.ProcessingCode));
            }
        }

        protected override void BuildPrivateFields()
        {
            PrivateAdministrativeAdvice pmessage = (PrivateAdministrativeAdvice)this.LiveMessage;
            pmessage.OldTerminalSerialNumber = Tags63.GetTagValueString(TlvTagIds.SerialNumber);
            ////pmessage.BatteryStatus = TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.BatteryStatus);
            ////pmessage.SignalStrength = TLVHelper.GetUInt16ValueFromStringTag(Tags63, TlvTagIds.SignalStrength);
            pmessage.CustomerMobileNumber = Tags63.GetTagValueString(TlvTagIds.CustomerMobileNumber);
            pmessage.DriverLicenseTrack1 = Tags63.GetTagValueString(TlvTagIds.DriverLicenseTrack1);
            pmessage.OldTerminalID = Tags63.GetTagValueString(TlvTagIds.TerminalId);
            pmessage.TransactionDateTime = Tags63.GetTagValueString(TlvTagIds.TransactionDateTime);
            pmessage.ProcessingCode = this.LiveMessage.ProcessingCode;
            pmessage.CellTowerMNC = Tags63.GetTagValueString(TlvTagIds.CellTowerMNC);
            pmessage.CellTowerMCC = Tags63.GetTagValueString(TlvTagIds.CellTowerMCC);
            pmessage.CellTowerLAC = Tags63.GetTagValueString(TlvTagIds.CellTowerLAC);
            pmessage.CellTowerID = Tags63.GetTagValueString(TlvTagIds.CellTowerCID);
            int rollnumber = 0;
            Int32.TryParse(Tags63.GetTagValueString(TlvTagIds.PaperRollNumber), out rollnumber);
            pmessage.PaperRollNumber = rollnumber;
        }
    }

    public class SettlementListRequestBuilder : LiveMessageBuilder
    {
        public SettlementListRequestBuilder(AS2805Message message) : base(message)
        {
            this.messageReturn = new SettlementGetList();
        }

        protected override void BuildPrivateFields()
        {
            SettlementGetList message = (SettlementGetList)this.LiveMessage;
            message.TransactionNumber = Tags63.GetTagValueString(TlvTagIds.TransactionNumber);
        }
    }

    public class OnlineOrderRequestBuilder : LiveMessageBuilder
    {
        public OnlineOrderRequestBuilder(AS2805Message message) : base(message)
        {
            this.messageReturn = new OnlineOrderRequest();
        }

        protected override void BuildPrivateFields()
        {
            //OnlineOrderRequest message = (OnlineOrderRequest)this.LiveMessage;
            //message.OrderNumber = Tags63.GetTagValueString(TlvTagIds.OrderNumber);
        }
    }

    public class OnlineOrderStatusAdviceBuilder : LiveMessageBuilder
    {
        public OnlineOrderStatusAdviceBuilder(AS2805Message message) : base(message)
        {
            this.messageReturn = new OnlineOrderStatusAdvice();
        }

        protected override void BuildPrivateFields()
        {
            OnlineOrderStatusAdvice message = (OnlineOrderStatusAdvice)this.LiveMessage;
            message.OrderNumber = Tags63.GetTagValueString(TlvTagIds.OrderNumber);
            message.OrderResponse = Tags63.GetTagValueString(TlvTagIds.OrderResponse);
            message.DeclineReason = Tags63.GetTagValueString(TlvTagIds.DeclineReason);
            message.OrderWaitingTime = TLVHelper.GetIntValueFromStringTag(Tags63, TlvTagIds.OrderWaitingTime);// Convert.ToInt32(Tags63.GetTagValue16Big(TlvTagIds.OrderWaitingTime));
                                                                                                              //message.OrderWaitingTime = Tags63.GetTagValueString()
        }
    }

    public class RewardPointsRequestBuilder : LiveMessageBuilder
    {
        public RewardPointsRequestBuilder(AS2805Message message) : base(message)
        {
            this.messageReturn = new RewardPointsRequest();

            this.messageReturn.ProcessingCode = message.ProcessingCode.ToString();

            switch (message.ProcessingCode)
            {
                case (int)ProcessingCodes.NormalSale:
                    ((RewardPointsRequest)this.messageReturn).TransactionType = TransactionTypes.Purchase;
                    break;

                case (int)ProcessingCodes.Void:
                    ((RewardPointsRequest)this.messageReturn).TransactionType = TransactionTypes.Void;
                    break;

                case (int)ProcessingCodes.PurchaseWithCash:
                    ((RewardPointsRequest)this.messageReturn).TransactionType = TransactionTypes.PurchaseWithCashback;
                    break;

                case (int)ProcessingCodes.CashAdvance:
                    ((RewardPointsRequest)this.messageReturn).TransactionType = TransactionTypes.CashAdvance;
                    break;

                case (int)ProcessingCodes.Refund:
                    ((RewardPointsRequest)this.messageReturn).TransactionType = TransactionTypes.Refund;
                    break;

                case (int)ProcessingCodes.QantasPointsBurningQuote:
                    ((RewardPointsRequest)this.messageReturn).TransactionType = TransactionTypes.Purchase;
                    break;

                default:
                    throw new Exception(string.Format("Processing code not supported for reward points request {0}", message.ProcessingCode));
            }
        }

        protected override void BuildPrivateFields()
        {
            RewardPointsRequest rmessage = (RewardPointsRequest)this.LiveMessage;
            rmessage.MembershipNumber = Tags63.GetTagValueString(TlvTagIds.RewardMembershipNumber);
            PaymentProviders programstring;
            Enum.TryParse<PaymentProviders>(Tags63.GetTagValueString(TlvTagIds.RewardProgram), out programstring);
            int program = 0;
            //Int32.TryParse(Tags63.GetTagValueString(TlvTagIds.RewardProgram), out program);
            rmessage.RewardProgram = (int)programstring;
            rmessage.TransactionGUID = Tags63.GetTagValueString(TlvTagIds.TransactionGUID);
            rmessage.TransactionDateTime = Tags63.GetTagValueString(TlvTagIds.TransactionDateTime);
            rmessage.TransactionType = TransactionTypes.Purchase; //hard code the transaction type for now because we only accept points transaction for sale
                                                                  //rmessage.ProcessingCode = message.ProcessingCode; // (int)ProcessingCodes.NormalSale;
            rmessage.Amount = this.message.TransactionAmount.ToString();
            rmessage.PIN = Tags63.GetTagValueString(TlvTagIds.PIN);
            rmessage.MembershipNumberEntryMethod = Tags63.GetTagValueString(TlvTagIds.MembershipNumberEntryMethod);
        }
    }

    public class FinancialRequestMessageBuilder : LiveMessageBuilder
    {
        public FinancialRequestMessageBuilder(AS2805Message message)
            : base(message)
        {
            this.messageReturn = new FinancialRequest();
            this.messageReturn.ProcessingCode = message.ProcessingCode.ToString();

            switch (message.ProcessingCode)
            {
                case (int)ProcessingCodes.NormalSale:
                    ((FinancialRequest)this.messageReturn).TransactionType = TransactionTypes.Purchase;
                    break;

                case (int)ProcessingCodes.Void:
                    ((FinancialRequest)this.messageReturn).TransactionType = TransactionTypes.Void;
                    break;

                case (int)ProcessingCodes.PurchaseWithCash:
                    ((FinancialRequest)this.messageReturn).TransactionType = TransactionTypes.PurchaseWithCashback;
                    break;

                case (int)ProcessingCodes.CashAdvance:
                    ((FinancialRequest)this.messageReturn).TransactionType = TransactionTypes.CashAdvance;
                    break;

                case (int)ProcessingCodes.Refund:
                    ((FinancialRequest)this.messageReturn).TransactionType = TransactionTypes.Refund;
                    break;

                default:
                    throw new Exception(string.Format("Processing code not supported for financial request {0}", message.ProcessingCode));
            }
        }

        protected override void BuildPrivateFields()
        {
            FinancialRequest fmessage = (FinancialRequest)this.LiveMessage;
            fmessage.UserID = Tags63.GetTagValueString(TlvTagIds.UserId);
            fmessage.BusinessID = Tags63.GetTagValueString(TlvTagIds.BusinessId);
            fmessage.ABN = Tags63.GetTagValueString(TlvTagIds.ABN);
            fmessage.NetworkID = Tags63.GetTagValueString(TlvTagIds.NetworkId);
            fmessage.TransactionDateTime = Tags63.GetTagValueString(TlvTagIds.TransactionDateTime);
            fmessage.Amount = message.TransactionAmount.ToString();
            fmessage.TransactionGUID = Tags63.GetTagValueString(TlvTagIds.TransactionGUID);
            fmessage.TranEntryMode = Tags63.GetTagValueString(TlvTagIds.TranEntryMode);
            fmessage.TransactionNumber = Tags63.GetTagValueString(TlvTagIds.TransactionNumber);
            fmessage.MembershipNumber = Tags63.GetTagValueString(TlvTagIds.RewardMembershipNumber);
            fmessage.PIN = Tags63.GetTagValueString(TlvTagIds.PIN);
            fmessage.ZipStoreCode = Tags63.GetTagValueString(TlvTagIds.ZipStoreCode);
            fmessage.OriginalTransactionGUID = Tags63.GetTagValueString(TlvTagIds.OriginalTransactionGUID);
            int paymentprovider = 0;
            Int32.TryParse(Tags63.GetTagValueString(TlvTagIds.PaymentProvider), out paymentprovider);
            fmessage.PaymentProviderNumber = paymentprovider;
        }
    }

    public class TransactionStatusRequestBuilder : FinancialAdviceMessageBuilder
    {
        public TransactionStatusRequestBuilder(AS2805Message message)
            : base(message)
        {
            this.messageReturn = new TransactionStatusRequest();
            this.messageReturn.ProcessingCode = message.ProcessingCode.ToString();

            //switch (message.ProcessingCode)
            //{
            //    case (int)ProcessingCodes.NormalSale:
            //        ((TransactionStatusRequest)this.messageReturn).TransactionType = TransactionTypes.Purchase;
            //        break;

            //    case (int)ProcessingCodes.Void:
            //        ((TransactionStatusRequest)this.messageReturn).TransactionType = TransactionTypes.Void;
            //        break;

            //    case (int)ProcessingCodes.PurchaseWithCash:
            //        ((TransactionStatusRequest)this.messageReturn).TransactionType = TransactionTypes.PurchaseWithCashback;
            //        break;

            //    case (int)ProcessingCodes.CashAdvance:
            //        ((TransactionStatusRequest)this.messageReturn).TransactionType = TransactionTypes.CashAdvance;
            //        break;
            //    case (int)ProcessingCodes.Refund:
            //        ((TransactionStatusRequest)this.messageReturn).TransactionType = TransactionTypes.Refund;
            //        break;
            //    default:
            //        throw new Exception(string.Format("Processing code not supported for transaction status request {0}", message.ProcessingCode));
            //}
        }

        protected override void BuildPrivateFields()
        {
            TransactionStatusRequest fmessage = (TransactionStatusRequest)this.LiveMessage;
            fmessage.TransactionGUID = Tags63.GetTagValueString(TlvTagIds.TransactionGUID);
            int paymentprovider = 0;
            Int32.TryParse(Tags63.GetTagValueString(TlvTagIds.PaymentProvider), out paymentprovider);
            fmessage.PaymentProviderNumber = paymentprovider;
        }
    }

    public class NotificationMessageRequestBuilder : LiveMessageBuilder
    {
        public NotificationMessageRequestBuilder(AS2805Message message) : base(message)
        {
            this.messageReturn = new NotificationMessageRequest
            {
                ProcessingCode = message.ProcessingCode.ToString()
            };
        }

        protected override void BuildPrivateFields()
        {
            NotificationMessageRequest fmessage = (NotificationMessageRequest)this.LiveMessage;
            fmessage.RecordingFromVAA = Tags63.GetTagValueString(TlvTagIds.RecordingFromVAA);
        }
    }
}