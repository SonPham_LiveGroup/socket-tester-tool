﻿using Util.Common;

namespace Host.Shared
{
    public enum CustomResponseCode
    {
        [StringValue("00")]
        [StringDescription("Approved")]
        Approved,

        [StringValue("03")]
        [StringDescription("Invalid Merchant")]
        InvalidMerchant,

        [StringValue("20")]
        [StringDescription("Invalid Response")]
        InvalidResponse,

        [StringValue("25")]
        [StringDescription("Unable To Locate Record On File")]
        UnableToLocateRecordOnFile,

        [StringValue("95")]
        [StringDescription("Reconcile Error")]
        ReconcileError,

        [StringValue("96")]
        [StringDescription("System Error")]
        SystemError,

        [StringValue("TM")]
        [StringDescription("Operator Cancelled")]
        OperatorCancelled,

        [StringValue("N0")]
        [StringDescription("Invalid Terminal ID")]
        InvalidTerminalID,

        [StringValue("N1")]
        [StringDescription("Invalid User ID")]
        InvalidUserID,

        [StringValue("N2")]
        [StringDescription("Invalid Business ID")]
        InvalidBusinessNO,

        [StringValue("N3")]
        [StringDescription("Invalid Taxi ID")]
        InvalidTaxiID,

        [StringValue("N4")]
        InvalidNetworkID,

        [StringValue("N5")]
        [StringDescription("Invalid Serial Number Or Model")]
        InvalidSerialNoOrModel,

        [StringValue("N6")]
        [StringDescription("Invalid Batch Number")]
        InvalidBatchNumber,

        [StringValue("N7")]
        [StringDescription("Unbalance Reconciled")]
        UnBalanceReconciled,

        [StringValue("N8")]
        [StringDescription("Transaction Exists")]
        TransactionExists,

        [StringValue("N9")]
        [StringDescription("Logon Batch Exists")]
        LogonBatchExists,

        [StringValue("NA")]
        UnclosedBatch,

        [StringValue("NB")]
        InvalidMessageID,

        [StringValue("NC")]
        [StringDescription("Format Error")]
        FormatError,

        [StringValue("ND")]
        OldTerminalNotFound,

        [StringValue("NE")]
        NewTerminalNotFound,

        [StringValue("NF")]
        TerminalNotAllocated,

        [StringValue("NG")]
        [StringDescription("Terminal Not Reachable")]
        TerminalNotReachable,

        [StringValue("NH")]
        [StringDescription("Incorrect API Token Or Password")]
        IncorrectAPIToken,

        [StringValue("NI")]
        [StringDescription("Transaction Not Exists")]
        TransactionNotExists,

        [StringValue("NJ")]
        [StringDescription("Poll Again")]
        PollAgain,

        [StringValue("NK")]
        [StringDescription("Reward Points Fails")]
        RewardPointsFail,

        [StringValue("NL")]
        [StringDescription("Transaction Declined By Provider")]
        ProviderDeclined,

        [StringValue("NM")]
        [StringDescription("Payment Provider Not Available")]
        PaymentProviderNotAvailable,

        [StringValue("NN")]
        [StringDescription("Bad Order Status")]
        BadOrderStatus,

        [StringValue("NO")]
        [StringDescription("Reward Provider Not Available")]
        RewardProviderNotAvailable,

        [StringValue("NP")]
        [StringDescription("Invalid Request")]
        InvalidRequest,

        [StringValue("NQ")]
        [StringDescription("Invalid Zip Store Code")]
        InvalidZipStoreCode,

        [StringValue("NR")]
        [StringDescription("Invalid Zip Merchant")]
        InvalidZipMerchant,

        [StringValue("NS")]
        [StringDescription("Duplicate Request")]
        DuplicateRequest,

        [StringValue("NT")]
        [StringDescription("Zip Request Expired")]
        ZipRequestExpired,

        [StringValue("NU")]
        [StringDescription("Zip Request Cancelled")]
        ZipRequestCancelled,

        [StringValue("NV")]
        [StringDescription("Zip Request Declined")]
        ZipRequestDeclined,

        [StringValue("NX")]
        [StringDescription("Request Time Out")]
        TimeOut
    }
}