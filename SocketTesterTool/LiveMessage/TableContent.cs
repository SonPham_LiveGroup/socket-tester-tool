﻿using System;
using System.Collections.Generic;
using Util.Data;
using Util.Tlv;

namespace Host.Shared
{
    public abstract class TableContent
    {
        //public FileName FileName
        //{ get; set; }
        //public string TableVersion { get; set; }
        [NonSerialized]
        private TagList _tableTagList;

        public TagList TableTagList
        {
            get
            {
                return _tableTagList;
            }
            set
            {
                this._tableTagList = value;
            }
        }

        public virtual string GetTableContentText()
        {
            try
            {
                byte[] tlvData = TableTagList.ToByteArray();
                return DataHelper.ByteArrayToString(tlvData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public string TableContentText
        //{
        //    get
        //    {
        //        byte[] tlvData = TableTagList.ToByteArray();
        //        return DataHelper.ByteArrayToString(tlvData);
        //    }
        //}

        public TableContent()
        {
            if (TableTagList == null)
                this.TableTagList = new TagList();
        }
    }

    public class PromptTableContent : TableContent
    {
        public PromptTableContent()
            : base()
        {
        }

        public string PromptTableVer
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.PromptTableVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.PromptTableVer))
                    TableTagList.AddValueTagString(TlvTagIds.PromptTableVer, value);
            }
        }

        public string UserIdPrompt
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.UserIdPrompt);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.UserIdPrompt))
                    TableTagList.AddValueTagString(TlvTagIds.UserIdPrompt, value);
            }
        }

        public string BusinessId
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.BusinessIdPrompt);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.BusinessIdPrompt))
                    TableTagList.AddValueTagString(TlvTagIds.BusinessIdPrompt, value);
            }
        }

        public string AmountIdPrompt
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AmountIdPrompt);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AmountIdPrompt))
                    TableTagList.AddValueTagString(TlvTagIds.AmountIdPrompt, value);
            }
        }

        public string LocationOnePrompt
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LocationOnePrompt);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LocationOnePrompt))
                    TableTagList.AddValueTagString(TlvTagIds.LocationOnePrompt, value);
            }
        }

        public string LocationTwoPrompt
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LocationTwoPrompt);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LocationTwoPrompt))
                    TableTagList.AddValueTagString(TlvTagIds.LocationTwoPrompt, value);
            }
        }

        public string DisableLocationOne
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DisableLocationOne);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DisableLocationOne))
                    TableTagList.AddValueTagString(TlvTagIds.DisableLocationOne, value);
            }
        }

        public string DisableLocationTwo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DisableLocationTwo);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DisableLocationTwo))
                    TableTagList.AddValueTagString(TlvTagIds.DisableLocationTwo, value);
            }
        }

        public string EOSButtonText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EOSButtonText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EOSButtonText))
                    TableTagList.AddValueTagString(TlvTagIds.EOSButtonText, value);
            }
        }

        public string EOSApprovedText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EOSApprovedText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EOSApprovedText))
                    TableTagList.AddValueTagString(TlvTagIds.EOSApprovedText, value);
            }
        }

        public string EOSConfirmText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EOSConfirmText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EOSConfirmText))
                    TableTagList.AddValueTagString(TlvTagIds.EOSConfirmText, value);
            }
        }

        public string EOSPriorText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EOSPriorText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EOSPriorText))
                    TableTagList.AddValueTagString(TlvTagIds.EOSPriorText, value);
            }
        }

        public string EOSNotFoundText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EOSNotFoundText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EOSNotFoundText))
                    TableTagList.AddValueTagString(TlvTagIds.EOSNotFoundText, value);
            }
        }

        public string EOSBatchOpenText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EOSBatchOpenText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EOSBatchOpenText))
                    TableTagList.AddValueTagString(TlvTagIds.EOSBatchOpenText, value);
            }
        }

        public string AutoTipNumOne
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipNumOne);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipNumOne))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipNumOne, value);
            }
        }

        public string AutoTipNumTwo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipNumTwo);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipNumTwo))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipNumTwo, value);
            }
        }

        public string AutoTipNumThree
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipNumThree);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipNumThree))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipNumThree, value);
            }
        }

        public string AutoTipNumFour
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipNumFour);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipNumFour))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipNumFour, value);
            }
        }

        public string AutoTipIsPercentOne
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipIsPercentOne
                    );
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipIsPercentOne))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipIsPercentOne, value);
            }
        }

        public string AutoTipIsPercentTwo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipIsPercentTwo);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipIsPercentTwo))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipIsPercentTwo, value);
            }
        }

        public string AutoTipIsPercentThree
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipIsPercentThree);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipIsPercentThree))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipIsPercentThree, value);
            }
        }

        public string AutoTipIsPercentFour
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipIsPercentFour);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipIsPercentFour))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipIsPercentFour, value);
            }
        }

        public string AutoTipDisplayOption
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipDisplayOption);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipDisplayOption))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipDisplayOption, value);
            }
        }

        public string EnableAutoTipAmountDisplay
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableAutoTipAmountDisplay);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableAutoTipAmountDisplay))
                    TableTagList.AddValueTagString(TlvTagIds.EnableAutoTipAmountDisplay, value);
            }
        }

        public string EnableAutoTipTotalDisplay
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableAutoTipTotalDisplay);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableAutoTipTotalDisplay))
                    TableTagList.AddValueTagString(TlvTagIds.EnableAutoTipTotalDisplay, value);
            }
        }

        public string AutoTipTitle
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTitle);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTitle))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTitle, value);
            }
        }

        public string AutoTipPriorAmountLabel
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipPriorAmountLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipPriorAmountLabel))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipPriorAmountLabel, value);
            }
        }

        public string AutoTipEnterConfirmation
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipEnterConfirmation);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipEnterConfirmation))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipEnterConfirmation, value);
            }
        }

        //
        public string AutoTipEOSReportOption
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipEOSReportOption);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipEOSReportOption))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipEOSReportOption, value);
            }
        }

        public string AutoTipButtonOneFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonOneFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonOneFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonOneFeeCents, value);
            }
        }

        public string AutoTipButtonOneFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonOneFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonOneFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonOneFeePercentage, value);
            }
        }

        public string AutoTipButtonTwoFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonTwoFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonTwoFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonTwoFeeCents, value);
            }
        }

        public string AutoTipButtonTwoFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonTwoFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonTwoFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonTwoFeePercentage, value);
            }
        }

        public string AutoTipButtonThreeFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonThreeFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonThreeFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonThreeFeeCents, value);
            }
        }

        //
        public string AutoTipButtonThreeFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonThreeFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonThreeFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonThreeFeePercentage, value);
            }
        }

        public string AutoTipButtonFourFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonFourFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonFourFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonFourFeeCents, value);
            }
        }

        public string AutoTipButtonFourFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonFourFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonFourFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonFourFeePercentage, value);
            }
        }

        public string AutoTipManualEntryFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipManualEntryFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipManualEntryFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipManualEntryFeeCents, value);
            }
        }

        public string AutoTipManualEntryFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipManualEntryFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipManualEntryFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipManualEntryFeePercentage, value);
            }
        }

        public string AutoTipAmountLabel
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipAmountLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipAmountLabel))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipAmountLabel, value);
            }
        }

        //
        public string AutoTipEOSReportMessage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipEOSReportMessage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipEOSReportMessage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipEOSReportMessage, value);
            }
        }

        public string AutoTipFeeIncluded
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipFeeIncluded);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipFeeIncluded))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipFeeIncluded, value);
            }
        }

        public string AutoTipButtonOneID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonOneID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonOneID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonOneID, value);
            }
        }

        public string AutoTipButtonTwoID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonTwoID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonTwoID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonTwoID, value);
            }
        }

        public string AutoTipButtonThreeID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonThreeID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonThreeID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonThreeID, value);
            }
        }

        public string AutoTipButtonFourID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipButtonFourID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipButtonFourID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipButtonFourID, value);
            }
        }

        public string AutoTipDefaultButtonID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipDefaultButtonID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipDefaultButtonID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipDefaultButtonID, value);
            }
        }

        public string AutoTipScreenText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipScreenText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipScreenText))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipScreenText, value);
            }
        }

        //change for Version 1.4.0
        public string AutoTipTwoNumOne
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoNumOne);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoNumOne))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoNumOne, value);
            }
        }

        public string AutoTipTwoNumTwo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoNumTwo);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoNumTwo))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoNumTwo, value);
            }
        }

        public string AutoTipTwoNumThree
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoNumThree);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoNumThree))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoNumThree, value);
            }
        }

        public string AutoTipTwoNumFour
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoNumFour);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoNumFour))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoNumFour, value);
            }
        }

        public string AutoTipTwoIsPercentOne
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoIsPercentOne
                    );
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoIsPercentOne))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoIsPercentOne, value);
            }
        }

        public string AutoTipTwoIsPercentTwo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoIsPercentTwo);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoIsPercentTwo))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoIsPercentTwo, value);
            }
        }

        public string AutoTipTwoIsPercentThree
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoIsPercentThree);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoIsPercentThree))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoIsPercentThree, value);
            }
        }

        public string AutoTipTwoIsPercentFour
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoIsPercentFour);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoIsPercentFour))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoIsPercentFour, value);
            }
        }

        public string AutoTipTwoDisplayOption
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoDisplayOption);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoDisplayOption))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoDisplayOption, value);
            }
        }

        public string EnableAutoTipTwoAmountDisplay
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableAutoTipTwoAmountDisplay);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableAutoTipTwoAmountDisplay))
                    TableTagList.AddValueTagString(TlvTagIds.EnableAutoTipTwoAmountDisplay, value);
            }
        }

        public string EnableAutoTipTwoTotalDisplay
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableAutoTipTwoTotalDisplay);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableAutoTipTwoTotalDisplay))
                    TableTagList.AddValueTagString(TlvTagIds.EnableAutoTipTwoTotalDisplay, value);
            }
        }

        public string AutoTipTwoTitle
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoTitle);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoTitle))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoTitle, value);
            }
        }

        public string AutoTipTwoPriorAmountLabel
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoPriorAmountLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoPriorAmountLabel))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoPriorAmountLabel, value);
            }
        }

        public string AutoTipTwoEnterConfirmation
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoEnterConfirmation);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoEnterConfirmation))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoEnterConfirmation, value);
            }
        }

        //
        public string AutoTipTwoEOSReportOption
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoEOSReportOption);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoEOSReportOption))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoEOSReportOption, value);
            }
        }

        public string AutoTipTwoButtonOneFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonOneFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonOneFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonOneFeeCents, value);
            }
        }

        public string AutoTipTwoButtonOneFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonOneFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonOneFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonOneFeePercentage, value);
            }
        }

        public string AutoTipTwoButtonTwoFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonTwoFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonTwoFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonTwoFeeCents, value);
            }
        }

        public string AutoTipTwoButtonTwoFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonTwoFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonTwoFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonTwoFeePercentage, value);
            }
        }

        public string AutoTipTwoButtonThreeFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonThreeFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonThreeFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonThreeFeeCents, value);
            }
        }

        //
        public string AutoTipTwoButtonThreeFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonThreeFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonThreeFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonThreeFeePercentage, value);
            }
        }

        public string AutoTipTwoButtonFourFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonFourFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonFourFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonFourFeeCents, value);
            }
        }

        public string AutoTipTwoButtonFourFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonFourFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonFourFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonFourFeePercentage, value);
            }
        }

        public string AutoTipTwoManualEntryFeeCents
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoManualEntryFeeCents);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoManualEntryFeeCents))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoManualEntryFeeCents, value);
            }
        }

        public string AutoTipTwoManualEntryFeePercentage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoManualEntryFeePercentage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoManualEntryFeePercentage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoManualEntryFeePercentage, value);
            }
        }

        public string AutoTipTwoAmountLabel
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoAmountLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoAmountLabel))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoAmountLabel, value);
            }
        }

        //
        public string AutoTipTwoEOSReportMessage
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoEOSReportMessage);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoEOSReportMessage))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoEOSReportMessage, value);
            }
        }

        public string AutoTipTwoFeeIncluded
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoFeeIncluded);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoFeeIncluded))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoFeeIncluded, value);
            }
        }

        public string AutoTipTwoButtonOneID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonOneID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonOneID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonOneID, value);
            }
        }

        public string AutoTipTwoButtonTwoID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonTwoID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonTwoID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonTwoID, value);
            }
        }

        public string AutoTipTwoButtonThreeID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonThreeID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonThreeID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonThreeID, value);
            }
        }

        public string AutoTipTwoButtonFourID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoButtonFourID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoButtonFourID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoButtonFourID, value);
            }
        }

        public string AutoTipTwoDefaultButtonID
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoDefaultButtonID);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoDefaultButtonID))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoDefaultButtonID, value);
            }
        }

        public string AutoTipTwoScreenText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipTwoScreenText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipTwoScreenText))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipTwoScreenText, value);
            }
        }

        public string ReceiptReferenceLabel
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptReferenceLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptReferenceLabel))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptReferenceLabel, value);
            }
        }

        public string ReceiptReferenceText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptReferenceText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptReferenceText))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptReferenceText, value);
            }
        }

        public string LevyLabel
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LevyLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LevyLabel))
                    TableTagList.AddValueTagString(TlvTagIds.LevyLabel, value);
            }
        }

        public string LevyCharge
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LevyCharge);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LevyCharge))
                    TableTagList.AddValueTagString(TlvTagIds.LevyCharge, value);
            }
        }
    }

    public class ReceiptTableContent : TableContent
    {
        public ReceiptTableContent()
            : base()
        {
        }

        public string ReceiptVerNo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptVerNo);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptVerNo))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptVerNo, value);
            }
        }

        public string ReceiptHeaderData
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptHeaderData);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptHeaderData))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptHeaderData, value);
            }
        }

        public string ReceiptFooterData
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptFooterData);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptFooterData))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptFooterData, value);
            }
        }

        public string ReceiptHeader1
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptHeader1);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptHeader1))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptHeader1, value);
            }
        }

        public string ReceiptHeader2
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptHeader2);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptHeader2))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptHeader2, value);
            }
        }

        public string ReceiptHeader3
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptHeader3);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptHeader3))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptHeader3, value);
            }
        }

        public string ReceiptHeader4
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptHeader4);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptHeader4))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptHeader4, value);
            }
        }

        public string ReceiptFooter1
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptFooter1);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptFooter1))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptFooter1, value);
            }
        }

        public string ReceiptFooter2
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptFooter2);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptFooter2))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptFooter2, value);
            }
        }

        public string ReceiptFooter3
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptFooter3);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptFooter3))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptFooter3, value);
            }
        }

        public string ReceiptFooter4
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptFooter4);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptFooter4))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptFooter4, value);
            }
        }

        public string UserIDReceiptTable
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.UserIDReceiptLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.UserIDReceiptLabel))
                    TableTagList.AddValueTagString(TlvTagIds.UserIDReceiptLabel, value);
            }
        }

        public string BusinessIDReceiptTable
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.BusinessIDReceiptLable);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.BusinessIDReceiptLable))
                    TableTagList.AddValueTagString(TlvTagIds.BusinessIDReceiptLable, value);
            }
        }

        public string LocationOneReceiptLabel
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LocationOneReceiptLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LocationOneReceiptLabel))
                    TableTagList.AddValueTagString(TlvTagIds.LocationOneReceiptLabel, value);
            }
        }

        public string LocationTwoReceiptLabel
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LocationTwoReceiptLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LocationTwoReceiptLabel))
                    TableTagList.AddValueTagString(TlvTagIds.LocationTwoReceiptLabel, value);
            }
        }

        public string ReceiptAmountDiscriptor
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ReceiptAmountDiscriptor);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ReceiptAmountDiscriptor))
                    TableTagList.AddValueTagString(TlvTagIds.ReceiptAmountDiscriptor, value);
            }
        }

        public string IncludeServiceFeeInTotal
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.IncludeServiceFeeInTotal);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.IncludeServiceFeeInTotal))
                    TableTagList.AddValueTagString(TlvTagIds.IncludeServiceFeeInTotal, value);
            }
        }

        public string EOSTranAmountItemisation
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EOSTranAmountItemisation);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EOSTranAmountItemisation))
                    TableTagList.AddValueTagString(TlvTagIds.EOSTranAmountItemisation, value);
            }
        }

        public string EOSReceiptSummary
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EOSReceiptSummary);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EOSReceiptSummary))
                    TableTagList.AddValueTagString(TlvTagIds.EOSReceiptSummary, value);
            }
        }

        public string AutoEOSNotificationMode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoEOSNotificationMode);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoEOSNotificationMode))
                    TableTagList.AddValueTagString(TlvTagIds.AutoEOSNotificationMode, value);
            }
        }

        public string AutoEOSOutputMode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoEOSOutputMode);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoEOSOutputMode))
                    TableTagList.AddValueTagString(TlvTagIds.AutoEOSOutputMode, value);
            }
        }

        public string EOSReportText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EOSReportText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EOSReportText))
                    TableTagList.AddValueTagString(TlvTagIds.EOSReportText, value);
            }
        }

        public string BusinessCardInfo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.BusinessCardInfo);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.BusinessCardInfo))
                    TableTagList.AddValueTagString(TlvTagIds.BusinessCardInfo, value);
            }
        }

        public string AutotipEOStext
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutotipEOStext);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutotipEOStext))
                    TableTagList.AddValueTagString(TlvTagIds.AutotipEOStext, value);
            }
        }

        public string Autotip2EOSText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.Autotip2EOSText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.Autotip2EOSText))
                    TableTagList.AddValueTagString(TlvTagIds.Autotip2EOSText, value);
            }
        }

        public string AutotipSummaryEOSText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutotipSummaryEOSText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutotipSummaryEOSText))
                    TableTagList.AddValueTagString(TlvTagIds.AutotipSummaryEOSText, value);
            }
        }

        public string AutoTipsInclude
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoTipsInclude);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoTipsInclude))
                    TableTagList.AddValueTagString(TlvTagIds.AutoTipsInclude, value);
            }
        }

        public string IncludeGSTText
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.IncludeGSTText);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.IncludeGSTText))
                    TableTagList.AddValueTagString(TlvTagIds.IncludeGSTText, value);
            }
        }

        public string CustomFooter
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.CustomFooter);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CustomFooter))
                    TableTagList.AddValueTagString(TlvTagIds.CustomFooter, value);
            }
        }

        public string PrintReceiptMode
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.PrintReceiptMode); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.PrintReceiptMode))
                {
                    TableTagList.AddValueTagString(TlvTagIds.PrintReceiptMode, value);
                }
            }
        }

        public string EnablePrintCustomerReceiptFirst
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.EnablePrintCustomerReceiptFirst); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnablePrintCustomerReceiptFirst))
                {
                    TableTagList.AddValueTagString(TlvTagIds.EnablePrintCustomerReceiptFirst, value);
                }
            }
        }

        public string EnablePrintSettlement
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.EnablePrintSettlement); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnablePrintSettlement))
                {
                    TableTagList.AddValueTagString(TlvTagIds.EnablePrintSettlement, value);
                }
            }
        }

    }

    public class PrinterLogoTableContent : TableContent
    {
        public PrinterLogoTableContent()
            : base()
        {
        }

        public string PrinterLogoVer
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.PrinterLogoVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.PrinterLogoVer))
                    TableTagList.AddValueTagString(TlvTagIds.PrinterLogoVer, value);
            }
        }

        //public string PrinterLogoData
        //{
        //    //get;
        //    //set;
        //    get
        //    {
        //        return TableTagList.GetTagValueString(TlvTagIds.PrinterLogoData);
        //    }
        //    set
        //    {
        //        if (!TableTagList.IsTagSet(TlvTagIds.PrinterLogoData))
        //            TableTagList.AddValueTagString(TlvTagIds.PrinterLogoData, value);
        //    }
        //}

        public byte[] PrinterLogoData
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueBytes(TlvTagIds.PrinterLogoData);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.PrinterLogoData))
                    TableTagList.AddValueTagBytes(TlvTagIds.PrinterLogoData, value);
            }
        }
    }

    public class ScrrenLogoTableContent : TableContent
    {
        public ScrrenLogoTableContent()
            : base()
        {
        }

        public string ScreenLogoVer
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ScreenLogoVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ScreenLogoVer))
                    TableTagList.AddValueTagString(TlvTagIds.ScreenLogoVer, value);
            }
        }

        public byte[] ScreenLogoData
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueBytes(TlvTagIds.ScreenLogoData);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ScreenLogoData))
                    TableTagList.AddValueTagBytes(TlvTagIds.ScreenLogoData, value);
            }
        }
    }

    public class TerminalNetworkTable : TableContent
    {
        public TerminalNetworkTable()
            : base()
        {
        }

        public string TerminalNetworkVersion
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.NetworkTableVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.NetworkTableVer))
                    TableTagList.AddValueTagString(TlvTagIds.NetworkTableVer, value);
            }
        }

        public string AvailableNetworks
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AvailableNetworks);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AvailableNetworks))
                    TableTagList.AddValueTagString(TlvTagIds.AvailableNetworks, value);
            }
        }
    }

    public class TerminalConfigTable : TableContent
    {
        public TerminalConfigTable()
            : base()
        {
        }
        public string TerminalConfigVersion
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.TerminalConfigVersion);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.TerminalConfigVersion))
                    TableTagList.AddValueTagString(TlvTagIds.TerminalConfigVersion, value);
            }
        }


        public string UserInputTimeout
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.UserInputTimeout);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.UserInputTimeout))
                    TableTagList.AddValueTagString(TlvTagIds.UserInputTimeout, value);
            }
        }
        public string BacklightOffPeriod
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.BacklightOffPeriod);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.BacklightOffPeriod))
                    TableTagList.AddValueTagString(TlvTagIds.BacklightOffPeriod, value);
            }
        }


        public string SleepModeTime
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.SleepModeTime);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.SleepModeTime))
                    TableTagList.AddValueTagString(TlvTagIds.SleepModeTime, value);
            }
        }
        public string ScreenBrightness
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ScreenBrightness);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ScreenBrightness))
                    TableTagList.AddValueTagString(TlvTagIds.ScreenBrightness, value);
            }
        }


        public string SoundOnOff
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.SoundOnOff);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.SoundOnOff))
                    TableTagList.AddValueTagString(TlvTagIds.SoundOnOff, value);
            }
        }
        public string RememberMeSettings
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.RememberMeSettings);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.RememberMeSettings))
                    TableTagList.AddValueTagString(TlvTagIds.RememberMeSettings, value);
            }
        }


        public string ABNSetting
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ABNSetting);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ABNSetting))
                    TableTagList.AddValueTagString(TlvTagIds.ABNSetting, value);
            }
        }
        public string LiveBusinessUnit
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LiveBusinessUnit);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LiveBusinessUnit))
                    TableTagList.AddValueTagString(TlvTagIds.LiveBusinessUnit, value);
            }
        }


        public string MinimumCharge
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.MinimumCharge);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.MinimumCharge))
                    TableTagList.AddValueTagString(TlvTagIds.MinimumCharge, value);
            }
        }
        public string MaximumCharge
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.MaximumCharge);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.MaximumCharge))
                    TableTagList.AddValueTagString(TlvTagIds.MaximumCharge, value);
            }
        }


        public string AutoEOSTime
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoEOSTime);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoEOSTime))
                    TableTagList.AddValueTagString(TlvTagIds.AutoEOSTime, value);
            }
        }
        public string LoginParameterOptions
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LoginParameterOptions);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LoginParameterOptions))
                    TableTagList.AddValueTagString(TlvTagIds.LoginParameterOptions, value);
            }
        }

        public string LoginMode
        {

            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LoginMode);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LoginMode))
                    TableTagList.AddValueTagString(TlvTagIds.LoginMode, value);
            }
        }

        public string NetworkTableStatus
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.NetworkTableStatus);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.NetworkTableStatus))
                    TableTagList.AddValueTagString(TlvTagIds.NetworkTableStatus, value);
            }
        }
        public string Prefix
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.Prefix);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.Prefix))
                    TableTagList.AddValueTagString(TlvTagIds.Prefix, value);
            }
        }
        public string GPRSReconnectTimer
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.GPRSReconnectTimer);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.GPRSReconnectTimer))
                    TableTagList.AddValueTagString(TlvTagIds.GPRSReconnectTimer, value);
            }
        }
        public string ApprovedResponseCode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ApprovedResponseCode);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ApprovedResponseCode))
                    TableTagList.AddValueTagString(TlvTagIds.ApprovedResponseCode, value);
            }
        }
        public string ApprovedSignatureResponseCode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ApprovedSignatureResponseCode);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ApprovedSignatureResponseCode))
                    TableTagList.AddValueTagString(TlvTagIds.ApprovedSignatureResponseCode, value);
            }
        }
        public string IsPreTranPing
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.IsPreTranPing);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.IsPreTranPing))
                    TableTagList.AddValueTagString(TlvTagIds.IsPreTranPing, value);
            }
        }
        public string TranAdviceSendMode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.TranAdviceSendMode);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.TranAdviceSendMode))
                    TableTagList.AddValueTagString(TlvTagIds.TranAdviceSendMode, value);
            }
        }
        public string DefaultServiceFee
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DefaultServiceFee);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DefaultServiceFee))
                    TableTagList.AddValueTagString(TlvTagIds.DefaultServiceFee, value);
            }
        }
        public string ServiceFeeMode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ServiceFeeMode);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ServiceFeeMode))
                    TableTagList.AddValueTagString(TlvTagIds.ServiceFeeMode, value);
            }
        }
        public string CustomerPaymentType
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.CustomerPaymentType);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CustomerPaymentType))
                    TableTagList.AddValueTagString(TlvTagIds.CustomerPaymentType, value);
            }
        }

        public string EnableMerchantLogoPrint
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableMerchantLogoPrint);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableMerchantLogoPrint))
                    TableTagList.AddValueTagString(TlvTagIds.EnableMerchantLogoPrint, value);
            }
        }

        public string EnableNetworkReceiptPrint
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableNetworkReceiptPrint);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableNetworkReceiptPrint))
                    TableTagList.AddValueTagString(TlvTagIds.EnableNetworkReceiptPrint, value);
            }
        }
        public string SignatureVerificationTimeOut
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.SignatureVerificationTimeOut);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.SignatureVerificationTimeOut))
                    TableTagList.AddValueTagString(TlvTagIds.SignatureVerificationTimeOut, value);
            }
        }
        public string AutoLogonHour
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoLogonHour);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoLogonHour))
                    TableTagList.AddValueTagString(TlvTagIds.AutoLogonHour, value);
            }
        }
        public string AutoLogonMode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoLogonMode);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoLogonMode))
                    TableTagList.AddValueTagString(TlvTagIds.AutoLogonMode, value);
            }
        }

        public string EnableRefundTran
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableRefundTran);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableRefundTran))
                    TableTagList.AddValueTagString(TlvTagIds.EnableRefundTran, value);
            }
        }
        public string EnableAuthTran
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableAuthTran);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableAuthTran))
                    TableTagList.AddValueTagString(TlvTagIds.EnableAuthTran, value);
            }
        }
        public string EnableTipTran
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableTipTran);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableTipTran))
                    TableTagList.AddValueTagString(TlvTagIds.EnableTipTran, value);
            }
        }
        public string EnableCashOutTran
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableCashOutTran);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableCashOutTran))
                    TableTagList.AddValueTagString(TlvTagIds.EnableCashOutTran, value);
            }
        }

        public string MerchantPassword
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.MerchantPassword);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.MerchantPassword))
                    TableTagList.AddValueTagString(TlvTagIds.MerchantPassword, value);
            }
        }
        public string BankLogonRespCode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.BankLogonRespCode);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.BankLogonRespCode))
                    TableTagList.AddValueTagString(TlvTagIds.BankLogonRespCode, value);
            }
        }
        public string EnableMOTO
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableMOTO);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableMOTO))
                    TableTagList.AddValueTagString(TlvTagIds.EnableMOTO, value);
            }
        }
        public string DefaultBackgroundColour
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DefaultBackgroundColour);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DefaultBackgroundColour))
                    TableTagList.AddValueTagString(TlvTagIds.DefaultBackgroundColour, value);
            }
        }



        public string EnableMOTORefund
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableMOTORefund);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableMOTORefund))
                    TableTagList.AddValueTagString(TlvTagIds.EnableMOTORefund, value);
            }
        }

        public string AutoEOSMode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AutoEOSMode);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AutoEOSMode))
                    TableTagList.AddValueTagString(TlvTagIds.AutoEOSMode, value);
            }
        }

        public string RebootHours
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.RebootHours);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.RebootHours))
                    TableTagList.AddValueTagString(TlvTagIds.RebootHours, value);
            }
        }

        public string EnableAutoTip
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableAutoTip);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableAutoTip))
                    TableTagList.AddValueTagString(TlvTagIds.EnableAutoTip, value);
            }
        }

        public string EnableDualAPNMode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableDualAPNMode);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableDualAPNMode))
                    TableTagList.AddValueTagString(TlvTagIds.EnableDualAPNMode, value);
            }
        }


        public string CellTowerUpdateInterval
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.CellTowerUpdateInterval);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CellTowerUpdateInterval))
                    TableTagList.AddValueTagString(TlvTagIds.CellTowerUpdateInterval, value);
            }
        }

        public string UpdateCellTowerMode
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.UpdateCellTowerMode);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.UpdateCellTowerMode))
                    TableTagList.AddValueTagString(TlvTagIds.UpdateCellTowerMode, value);
            }
        }

        public string VAAIPAddress
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.VAAIPAddress);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.VAAIPAddress))
                    TableTagList.AddValueTagString(TlvTagIds.VAAIPAddress, value);
            }
        }

        public string VAAPHostPort
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.VAAPHostPort);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.VAAPHostPort))
                    TableTagList.AddValueTagString(TlvTagIds.VAAPHostPort, value);
            }
        }

        public string ClearOverrideSettings
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ClearOverrideSettings);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ClearOverrideSettings))
                    TableTagList.AddValueTagString(TlvTagIds.ClearOverrideSettings, value);
            }
        }

        //change for version 1.4.0

        public string EnableAutoTipTwo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableAutoTipTwo);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableAutoTipTwo))
                    TableTagList.AddValueTagString(TlvTagIds.EnableAutoTipTwo, value);
            }
        }

        public string EnableReceiptReference
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableReceiptReference);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableReceiptReference))
                    TableTagList.AddValueTagString(TlvTagIds.EnableReceiptReference, value);
            }
        }

        public string BusinessTypeIndicator
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.BusinessTypeIndicator);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.BusinessTypeIndicator))
                    TableTagList.AddValueTagString(TlvTagIds.BusinessTypeIndicator, value);
            }


        }


        public string SkinSelection
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.SkinSelection);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.SkinSelection))
                    TableTagList.AddValueTagString(TlvTagIds.SkinSelection, value);
            }


        }

        public string EnableOnlineOrder
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableOnlineOrder);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableOnlineOrder))
                    TableTagList.AddValueTagString(TlvTagIds.EnableOnlineOrder, value);
            }


        }
        public string LevyLabel
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LevyLabel);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LevyLabel))
                    TableTagList.AddValueTagString(TlvTagIds.LevyLabel, value);
            }
        }

        public string LevyCharge
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LevyCharge);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LevyCharge))
                    TableTagList.AddValueTagString(TlvTagIds.LevyCharge, value);
            }
        }

        public string EnableAlipay
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableAlipay);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableAlipay))
                    TableTagList.AddValueTagString(TlvTagIds.EnableAlipay, value);
            }


        }

        public string EnableQantasReward
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableQantasReward);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableQantasReward))
                    TableTagList.AddValueTagString(TlvTagIds.EnableQantasReward, value);
            }


        }

        public string EnableBusinessLogo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableBusinessLogo);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableBusinessLogo))
                    TableTagList.AddValueTagString(TlvTagIds.EnableBusinessLogo, value);
            }


        }

        public string EnableQantasPointsRedemption
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableQantasPointsRedemption);

            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableQantasPointsRedemption))
                    TableTagList.AddValueTagString(TlvTagIds.EnableQantasPointsRedemption, value);
            }


        }

        public string CustomFooter
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.CustomFooter);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CustomFooter))
                    TableTagList.AddValueTagString(TlvTagIds.CustomFooter, value);
            }
        }

        public string AddLevyChargeToAmount
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AddLevyChargeToAmount);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AddLevyChargeToAmount))
                    TableTagList.AddValueTagString(TlvTagIds.AddLevyChargeToAmount, value);
            }
        }

        public string EnablePaperRollOrder
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnablePaperRollOrder);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnablePaperRollOrder))
                    TableTagList.AddValueTagString(TlvTagIds.EnablePaperRollOrder, value);
            }
        }

        public string EnableSupportCallBack
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableSupportCallBack);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableSupportCallBack))
                    TableTagList.AddValueTagString(TlvTagIds.EnableSupportCallBack, value);
            }
        }

        public string RefundTransMax
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.RefundTransMax);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.RefundTransMax))
                    TableTagList.AddValueTagString(TlvTagIds.RefundTransMax, value);
            }
        }

        public string RefundCumulativeMax
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.RefundCumulativeMax);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.RefundCumulativeMax))
                    TableTagList.AddValueTagString(TlvTagIds.RefundCumulativeMax, value);
            }
        }

        public string EnableZip
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.EnableZip);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableZip))
                    TableTagList.AddValueTagString(TlvTagIds.EnableZip, value);
            }
        }

        public string IdleTimer
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.IdleTimer); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.IdleTimer))
                {
                    TableTagList.AddValueTagString(TlvTagIds.IdleTimer, value);
                }
            }
        }
        public string IdleTransactions
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.IdleTransactions); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.IdleTransactions))
                {
                    TableTagList.AddValueTagString(TlvTagIds.IdleTransactions, value);
                }
            }
        }

        public string DefaultTaxiNetworkID
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.DefaultTaxiNetworkID); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DefaultTaxiNetworkID))
                {
                    TableTagList.AddValueTagString(TlvTagIds.DefaultTaxiNetworkID, value);
                }
            }
        }

        public string RecordingMode
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.RecordingMode); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.RecordingMode))
                {
                    TableTagList.AddValueTagString(TlvTagIds.RecordingMode, value);
                }
            }
        }

        public string UploadTimer
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.UploadTimer); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.UploadTimer))
                {
                    TableTagList.AddValueTagString(TlvTagIds.UploadTimer, value);
                }
            }
        }

        public string EnableGlidebox
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.EnableGlidebox); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.EnableGlidebox))
                {
                    TableTagList.AddValueTagString(TlvTagIds.EnableGlidebox, value);
                }
            }
        }

        public string CardPresentTransactionLimit
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.CardPresentTransactionLimit); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CardPresentTransactionLimit))
                {
                    TableTagList.AddValueTagString(TlvTagIds.CardPresentTransactionLimit, value);
                }
            }
        }

        public string CardNotPresentTransactionLimit
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.CardNotPresentTransactionLimit); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CardNotPresentTransactionLimit))
                {
                    TableTagList.AddValueTagString(TlvTagIds.CardNotPresentTransactionLimit, value);
                }
            }
        }

        public string BrandName_PhoneNumber
        {
            get { return TableTagList.GetTagValueString(TlvTagIds.BrandName_PhoneNumber); }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.BrandName_PhoneNumber))
                {
                    TableTagList.AddValueTagString(TlvTagIds.BrandName_PhoneNumber, value);
                }
            }
        }
    }

    public class CardTableContent : TableContent
    {
        public CardTableContent()
            : base()
        {
            if (CardEntries == null)
                CardEntries = new List<CardEntryCollection>();
        }

        public string CardTableVer
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.CardTableVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CardTableVer))
                    TableTagList.AddValueTagString(TlvTagIds.CardTableVer, value);
            }
        }

        public List<TagList> CardTableEntry
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetSubTags(TlvTagIds.CardTableEntry);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CardTableEntry))
                    //TableTagList.AddValueTagString(TlvTagIds.CardTableEntry, value);
                    foreach (TagList item in value)
                        TableTagList.AddTagList(TlvTagIds.CardTableEntry, item);
            }
        }

        private string CardEntriesTag
        {
            get
            {
                string st = "";
                foreach (CardEntryCollection item in CardEntries)
                {
                    st += item.GetTableContentText();
                }
                return st;
            }
        }

        //[NonSerialized]

        //public List<CardEntry> Cardentries { get; set; }

        public List<CardEntryCollection> CardEntries { get; set; }

        //public override string  GetTableContentText()
        //{
        //     return base.GetTableContentText()+CardEntriesTag;
        //}
    }

    public class CardEntryCollection : TableContent
    {
        public CardEntryCollection() : base()
        {
        }

        public string CardTableEntry
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.CardTableEntry);
            }
            //set
            //{
            //    if (!TableTagList.IsTagSet(TlvTagIds.CardTableEntry))
            //        TableTagList.AddValueTagString(TlvTagIds.CardTableEntry, value);
            //}
        }

        private CardEntry entries;

        public CardEntry cardentry
        {
            get
            {
                return entries;
            }
            set
            {
                entries = value;
                if (!TableTagList.IsTagSet(TlvTagIds.CardTableEntry))
                    TableTagList.AddValueTagString(TlvTagIds.CardTableEntry, value.GetCardEntryTagString());
            }
        }
    }

    public class CardEntry
    {
        public CardEntry()
        {
            if (CardTaglist == null)
                CardTaglist = new TagList();
        }

        public TagList CardTaglist { get; set; }

        public string CardStartRange
        {
            //get;
            //set;
            get
            {
                return CardTaglist.GetTagValueString(TlvTagIds.CardStartRange);
            }
            set
            {
                if (!CardTaglist.IsTagSet(TlvTagIds.CardStartRange))
                    CardTaglist.AddValueTagString(TlvTagIds.CardStartRange, value);
            }
        }

        public string CardEndRange
        {
            //get;
            //set;
            get
            {
                return CardTaglist.GetTagValueString(TlvTagIds.CardEndRange);
            }
            set
            {
                if (!CardTaglist.IsTagSet(TlvTagIds.CardEndRange))
                    CardTaglist.AddValueTagString(TlvTagIds.CardEndRange, value);
            }
        }

        public string CardServiceFeeMode
        {
            //get;
            //set;
            get
            {
                return CardTaglist.GetTagValueString(TlvTagIds.CardServiceFeeMode);
            }
            set
            {
                if (!CardTaglist.IsTagSet(TlvTagIds.CardServiceFeeMode))
                    CardTaglist.AddValueTagString(TlvTagIds.CardServiceFeeMode, value);
            }
        }

        public string CardServiceFeeValue
        {
            //get;
            //set;
            get
            {
                return CardTaglist.GetTagValueString(TlvTagIds.CardServiceFeeValue);
            }
            set
            {
                if (!CardTaglist.IsTagSet(TlvTagIds.CardServiceFeeValue))
                    CardTaglist.AddValueTagString(TlvTagIds.CardServiceFeeValue, value);
            }
        }

        public string CardName
        {
            //get;
            //set;
            get
            {
                return CardTaglist.GetTagValueString(TlvTagIds.CardName);
            }
            set
            {
                if (!CardTaglist.IsTagSet(TlvTagIds.CardName))
                    CardTaglist.AddValueTagString(TlvTagIds.CardName, value);
            }
        }

        public string GetCardEntryTagString()
        {
            try
            {
                byte[] tlvData = CardTaglist.ToByteArray();
                return DataHelper.ByteArrayToString(tlvData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class TipTricksTableContent : TableContent
    {
        public TipTricksTableContent()
            : base()
        {
            if (TipTrickEntries == null)
                TipTrickEntries = new List<TipsTrickEntryCollection>();
        }

        public string TipTrickTableVer
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.TipTrickTableVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.TipTrickTableVer))
                    TableTagList.AddValueTagString(TlvTagIds.TipTrickTableVer, value);
            }
        }

        public List<TagList> TipTrickTableEntry
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetSubTags(TlvTagIds.TipTrickEntry);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.TipTrickEntry))
                    //TableTagList.AddValueTagString(TlvTagIds.CardTableEntry, value);
                    foreach (TagList item in value)
                        TableTagList.AddTagList(TlvTagIds.TipTrickEntry, item);
            }
        }

        private string TipTrickEntriesTag
        {
            get
            {
                string st = "";
                foreach (TipsTrickEntryCollection item in TipTrickEntries)
                {
                    st += item.GetTableContentText();
                }
                return st;
            }
        }

        //[NonSerialized]

        //public List<CardEntry> Cardentries { get; set; }

        public List<TipsTrickEntryCollection> TipTrickEntries { get; set; }

        //public override string  GetTableContentText()
        //{
        //     return base.GetTableContentText()+CardEntriesTag;
        //}
    }

    public class TipsTrickEntryCollection : TableContent
    {
        public TipsTrickEntryCollection()
            : base()
        {
        }

        public string TipsTrickEntry
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.TipTrickEntry);
            }
        }

        private TipsTricksEntry entries;

        public TipsTricksEntry tiptrickentry
        {
            get
            {
                return entries;
            }
            set
            {
                entries = value;
                if (!TableTagList.IsTagSet(TlvTagIds.TipTrickEntry))
                    TableTagList.AddValueTagString(TlvTagIds.CardTableEntry, value.GetTipTricksTagString());
            }
        }
    }

    public class TipsTricksEntry
    {
        public TipsTricksEntry()
        {
            if (TipsTricksTaglist == null)
                TipsTricksTaglist = new TagList();
        }

        public TagList TipsTricksTaglist { get; set; }

        public string TipTrickTitle
        {
            get
            {
                return TipsTricksTaglist.GetTagValueString(TlvTagIds.TipTrickTitle);
            }
            set
            {
                if (!TipsTricksTaglist.IsTagSet(TlvTagIds.TipTrickTitle))
                    TipsTricksTaglist.AddValueTagString(TlvTagIds.TipTrickTitle, value);
            }
        }

        public string TipTrickBody
        {
            get
            {
                return TipsTricksTaglist.GetTagValueString(TlvTagIds.TipTrickBody);
            }
            set
            {
                if (!TipsTricksTaglist.IsTagSet(TlvTagIds.TipTrickBody))
                    TipsTricksTaglist.AddValueTagString(TlvTagIds.TipTrickBody, value);
            }
        }

        public string GetTipTricksTagString()
        {
            try
            {
                byte[] tlvData = TipsTricksTaglist.ToByteArray();
                return DataHelper.ByteArrayToString(tlvData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class LocationTableContent : TableContent
    {
        public LocationTableContent()
            : base()
        {
            if (LocationEntries == null)
                LocationEntries = new List<LocationEntryCollection>();
        }

        public string LocationTableVer
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LocationTableVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LocationTableVer))
                    TableTagList.AddValueTagString(TlvTagIds.LocationTableVer, value);
            }
        }

        public List<TagList> LocationTableEntry
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetSubTags(TlvTagIds.LocationbEntry);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LocationbEntry))
                    foreach (TagList item in value)
                        TableTagList.AddTagList(TlvTagIds.LocationbEntry, item);
            }
        }

        private string LocationEntriesTag
        {
            get
            {
                string st = "";
                foreach (LocationEntryCollection item in LocationEntries)
                {
                    st += item.GetTableContentText();
                }
                return st;
            }
        }

        public List<LocationEntryCollection> LocationEntries { get; set; }
    }

    public class LocationEntryCollection : TableContent
    {
        public LocationEntryCollection()
            : base()
        {
        }

        public string LocationEntry
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.LocationbEntry);
            }
        }

        private LocationEntry entries;

        public LocationEntry LocationEntries
        {
            get
            {
                return entries;
            }
            set
            {
                entries = value;
                if (!TableTagList.IsTagSet(TlvTagIds.LocationbEntry))
                    TableTagList.AddValueTagString(TlvTagIds.LocationbEntry, value.GetLocationTagString());
            }
        }
    }

    public class LocationEntry
    {
        public LocationEntry()
        {
            if (LocationTaglist == null)
                LocationTaglist = new TagList();
        }

        public TagList LocationTaglist { get; set; }

        public int LocationID
        {
            get
            {
                return Convert.ToInt32(LocationTaglist.GetTagValue16Big(TlvTagIds.LocationID));// .GetTagValueString(TlvTagIds.LocationID);
            }
            set
            {
                if (!LocationTaglist.IsTagSet(TlvTagIds.LocationID))
                    LocationTaglist.AddValueTag16Big(TlvTagIds.LocationID, Convert.ToUInt16(value));
                //LocationTaglist.AddValueTagString(TlvTagIds.LocationID, value);
            }
        }

        public string LocationName
        {
            get
            {
                return LocationTaglist.GetTagValueString(TlvTagIds.LocationName);
            }
            set
            {
                if (!LocationTaglist.IsTagSet(TlvTagIds.LocationName))
                    LocationTaglist.AddValueTagString(TlvTagIds.LocationName, value);
            }
        }

        public string GetLocationTagString()
        {
            try
            {
                byte[] tlvData = LocationTaglist.ToByteArray();
                return DataHelper.ByteArrayToString(tlvData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public byte LocationType
        {
            get
            {
                return Convert.ToByte(LocationTaglist.GetTagValueByte(TlvTagIds.LocationType));// .GetTagValueString(TlvTagIds.LocationID);
            }
            set
            {
                if (!LocationTaglist.IsTagSet(TlvTagIds.LocationType))
                    LocationTaglist.AddValueTagByte(TlvTagIds.LocationType, value);
                //LocationTaglist.AddValueTagString(TlvTagIds.LocationID, value);
            }
        }
    }

    public class CellTowerTableContent : TableContent
    {
        public CellTowerTableContent()
            : base()
        {
            if (CellTowerEntries == null)
                CellTowerEntries = new List<CellTowerEntryCollection>();
        }

        public string CellTowerTableVer
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.CellTowerTableVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CellTowerTableVer))
                    TableTagList.AddValueTagString(TlvTagIds.CellTowerTableVer, value);
            }
        }

        public List<TagList> CellTowerTableEntry
        {
            get
            {
                return TableTagList.GetSubTags(TlvTagIds.CellTowerEntry);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.CellTowerEntry))
                    foreach (TagList item in value)
                        TableTagList.AddTagList(TlvTagIds.CellTowerEntry, item);
            }
        }

        private string CellTowerEntriesTag
        {
            get
            {
                string st = "";
                foreach (CellTowerEntryCollection item in CellTowerEntries)
                {
                    st += item.GetTableContentText();
                }
                return st;
            }
        }

        public List<CellTowerEntryCollection> CellTowerEntries { get; set; }
    }

    public class CellTowerEntryCollection : TableContent
    {
        public CellTowerEntryCollection()
            : base()
        {
        }

        public string CellTowerEntry
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.CellTowerEntry);
            }
        }

        private CellTowerEntry entries;

        public CellTowerEntry LocationEntry
        {
            get
            {
                return entries;
            }
            set
            {
                entries = value;
                if (!TableTagList.IsTagSet(TlvTagIds.CellTowerEntry))
                    TableTagList.AddValueTagString(TlvTagIds.CellTowerEntry, value.GetCellTowerTagString());
            }
        }
    }

    public class CellTowerEntry : TableContent
    {
        public CellTowerEntry()
        {
            if (CellTowerTaglist == null)
                CellTowerTaglist = new TagList();
        }

        public TagList CellTowerTaglist { get; set; }

        public string CellTowerID
        {
            get
            {
                return CellTowerTaglist.GetTagValueString(TlvTagIds.CellTowerID);
            }
            set
            {
                if (!CellTowerTaglist.IsTagSet(TlvTagIds.CellTowerID))
                    CellTowerTaglist.AddValueTagString(TlvTagIds.CellTowerID, value);
            }
        }

        public string MainLocationID
        {
            get
            {
                return CellTowerTaglist.GetTagValueString(TlvTagIds.LocationID);
            }
            set
            {
                if (!CellTowerTaglist.IsTagSet(TlvTagIds.LocationID))
                    CellTowerTaglist.AddValueTagString(TlvTagIds.LocationID, value);
            }
        }

        public List<TagList> CellTowerLocationEntry
        {
            get
            {
                return TableTagList.GetSubTags(TlvTagIds.LocationbEntry);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.LocationbEntry))
                    foreach (TagList item in value)
                        TableTagList.AddTagList(TlvTagIds.LocationbEntry, item);
            }
        }

        private string CellTowerEntriesTag
        {
            get
            {
                string st = "";
                //foreach (CellTowerLocationEntryCollection item in CellTowerLocationEntries)
                //{
                //    st += item.GetTableContentText();
                //}
                return st;
            }
        }

        public string GetCellTowerTagString()
        {
            try
            {
                byte[] tlvData = CellTowerTaglist.ToByteArray();
                return DataHelper.ByteArrayToString(tlvData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CellTowerLocationEntry> CellTowerLocationEntries { get; set; }
        //public List<CellTowerLocationEntryCollection> CellTowerLocationEntries { get; set; }
    }

    public class CellTowerLocationEntryCollection : TableContent
    {
        public CellTowerLocationEntryCollection()
            : base()
        {
        }

        private CellTowerLocationEntry entries;

        public CellTowerLocationEntry LocationEntry
        {
            get
            {
                return entries;
            }
            set
            {
                entries = value;
                if (!TableTagList.IsTagSet(TlvTagIds.LocationbEntry))
                    TableTagList.AddValueTagString(TlvTagIds.LocationbEntry, value.GetCellTowerLocationTagString());
            }
        }
    }

    public class CellTowerLocationEntry
    {
        public CellTowerLocationEntry()
        {
            if (CellTowerLocationTaglist == null)
                CellTowerLocationTaglist = new TagList();
        }

        public TagList CellTowerLocationTaglist { get; set; }

        public string LocationID
        {
            get
            {
                return CellTowerLocationTaglist.GetTagValueString(TlvTagIds.LocationID);
            }
            set
            {
                if (!CellTowerLocationTaglist.IsTagSet(TlvTagIds.LocationID))
                    CellTowerLocationTaglist.AddValueTagString(TlvTagIds.LocationID, value);
            }
        }

        public string GetCellTowerLocationTagString()
        {
            try
            {
                byte[] tlvData = CellTowerLocationTaglist.ToByteArray();
                return DataHelper.ByteArrayToString(tlvData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    #region Online Order Config table

    public class OnlineOrderConfigTable : TableContent
    {
        public OnlineOrderConfigTable()
            : base()
        {
        }

        public string OnlineOrderConfigVer
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.OnlineOrderConfigVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.OnlineOrderConfigVer))
                    TableTagList.AddValueTagString(TlvTagIds.OnlineOrderConfigVer, value);
            }
        }

        public string IdleTime
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.IdleTime);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.IdleTime))
                    TableTagList.AddValueTagString(TlvTagIds.IdleTime, value);
            }
        }

        public string PollTime
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.PollTime);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.PollTime))
                    TableTagList.AddValueTagString(TlvTagIds.PollTime, value);
            }
        }

        public string ETATimeOne
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ETATimeOne);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ETATimeOne))
                    TableTagList.AddValueTagString(TlvTagIds.ETATimeOne, value);
            }
        }

        public string ETATimeTwo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ETATimeTwo);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ETATimeTwo))
                    TableTagList.AddValueTagString(TlvTagIds.ETATimeTwo, value);
            }
        }

        public string ETATimeThree
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ETATimeThree);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ETATimeThree))
                    TableTagList.AddValueTagString(TlvTagIds.ETATimeThree, value);
            }
        }

        public string ETATimeFour
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.ETATimeFour);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.ETATimeFour))
                    TableTagList.AddValueTagString(TlvTagIds.ETATimeFour, value);
            }
        }

        public string DeclinedReasonOne
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DeclinedReasonOne);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DeclinedReasonOne))
                    TableTagList.AddValueTagString(TlvTagIds.DeclinedReasonOne, value);
            }
        }

        public string DeclinedReasonTwo
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DeclinedReasonTwo);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DeclinedReasonTwo))
                    TableTagList.AddValueTagString(TlvTagIds.DeclinedReasonTwo, value);
            }
        }

        public string DeclinedReasonThree
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DeclinedReasonThree);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DeclinedReasonThree))
                    TableTagList.AddValueTagString(TlvTagIds.DeclinedReasonThree, value);
            }
        }

        public string DeclinedReasonFour
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DeclinedReasonFour);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DeclinedReasonFour))
                    TableTagList.AddValueTagString(TlvTagIds.DeclinedReasonFour, value);
            }
        }

        public string DeclinedReasonFive
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DeclinedReasonFive);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DeclinedReasonFive))
                    TableTagList.AddValueTagString(TlvTagIds.DeclinedReasonFive, value);
            }
        }

        public string AlertTime
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.AlertTime);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.AlertTime))
                    TableTagList.AddValueTagString(TlvTagIds.AlertTime, value);
            }
        }

        public string OrderReceiptStoreTime
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.OrderReceiptStoreTime);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.OrderReceiptStoreTime))
                    TableTagList.AddValueTagString(TlvTagIds.OrderReceiptStoreTime, value);
            }
        }
    }

    #endregion Online Order Config table

    #region Online Order - Delivery Logo

    public class DeliveryLogoTableContent : TableContent
    {
        public DeliveryLogoTableContent()
            : base()
        {
        }

        public string DeliveryLogoVer
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.DeliveryLogoVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DeliveryLogoVer))
                    TableTagList.AddValueTagString(TlvTagIds.DeliveryLogoVer, value);
            }
        }

        public byte[] DeliveryLogoData
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueBytes(TlvTagIds.DeliveryLogoData);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.DeliveryLogoData))
                    TableTagList.AddValueTagBytes(TlvTagIds.DeliveryLogoData, value);
            }
        }
    }

    #endregion Online Order - Delivery Logo

    #region Online Order - Pickup Logo

    public class PickupLogoTableContent : TableContent
    {
        public PickupLogoTableContent()
            : base()
        {
        }

        public string PickupLogoVer
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.PickupLogoVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.PickupLogoVer))
                    TableTagList.AddValueTagString(TlvTagIds.PickupLogoVer, value);
            }
        }

        public byte[] PickupLogoData
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueBytes(TlvTagIds.PickupLogoData);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.PickupLogoData))
                    TableTagList.AddValueTagBytes(TlvTagIds.PickupLogoData, value);
            }
        }
    }

    #endregion Online Order - Pickup Logo

    #region Online Order - Online Order Logo

    public class OnlineOrderLogoTableContent : TableContent
    {
        public OnlineOrderLogoTableContent()
            : base()
        {
        }

        public string OnlineOrderLogoVer
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.OnlineOrderLogoVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.OnlineOrderLogoVer))
                    TableTagList.AddValueTagString(TlvTagIds.OnlineOrderLogoVer, value);
            }
        }

        public byte[] OnlineOrderLogoData
        {
            //get;
            //set;
            get
            {
                return TableTagList.GetTagValueBytes(TlvTagIds.OnlineOrderLogoData);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.OnlineOrderLogoData))
                    TableTagList.AddValueTagBytes(TlvTagIds.OnlineOrderLogoData, value);
            }
        }
    }

    #endregion Online Order - Online Order Logo

    public class GlideboxItemTableContent : TableContent
    {
        public GlideboxItemTableContent()
            : base()
        {
        }

        public string GlideboxItems
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.GlideboxItems);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.GlideboxItems))
                    TableTagList.AddValueTagString(TlvTagIds.GlideboxItems, value);
            }
        }

        public string GlideboxTableVer
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.GlideboxTableVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.GlideboxTableVer))
                    TableTagList.AddValueTagString(TlvTagIds.GlideboxTableVer, value);
            }
        }
    }

    public class FooterLogoTableContent : TableContent
    {
        public FooterLogoTableContent()
            : base()
        {
        }

        public string FooterLogoVer
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.FooterLogoVer);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.FooterLogoVer))
                    TableTagList.AddValueTagString(TlvTagIds.FooterLogoVer, value);
            }
        }

        public string FooterLogoData
        {
            get
            {
                return TableTagList.GetTagValueString(TlvTagIds.FooterLogoData);
            }
            set
            {
                if (!TableTagList.IsTagSet(TlvTagIds.FooterLogoData))
                    TableTagList.AddValueTagString(TlvTagIds.FooterLogoData, value);
            }
        }
    }
}