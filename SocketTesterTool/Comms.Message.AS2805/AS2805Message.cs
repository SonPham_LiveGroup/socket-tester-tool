﻿using Comms.Message.Common;
using System;
using System.Collections.Generic;
using System.Text;
using Util.Data;

namespace Comms.Message.AS2805
{
    /// <summary>
    /// Represents an AS2805 message object
    /// This includes requests and responses
    /// </summary>
    public class AS2805Message : CommsMessage
    {
        #region Private Variables

        /// <summary>
        /// Stores the values for each field in the AS2805 message
        /// </summary>
        private Dictionary<byte, object> _fieldValues = new Dictionary<byte, object>();

        public bool IsValidMessage => _fieldValues.Count > 0;

        #endregion Private Variables

        #region CommsMessage implementation

        /// <summary>
        /// Gets the message type description.
        /// </summary>
        /// <value>
        /// The message type description.
        /// </value>
        public override string MessageTypeDescription
        {
            get { return "AS2805"; }
        }

        /// <summary>
        /// Gets the routing id. This routing id is used to match a request message to a response
        /// message for message routing purposes. Routing ID for AS2805 is based on CAID, STAN and RRN if present
        /// </summary>
        /// <value>
        /// The routing id.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override string RoutingId
        {
            get
            {
                StringBuilder routingId = new StringBuilder();
                routingId.Append(((int)this.MessageTypeIdentifier).ToString().Substring(0, 1));
                routingId.Append(this.TerminalID);
                routingId.Append(this.STAN.ToString());

                if (_fieldValues.ContainsKey(37))
                    routingId.Append(this.RRN);

                return routingId.ToString();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is a response message by checking whether
        /// the response code field is set
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is response; otherwise, <c>false</c>.
        /// </value>
        public override bool IsResponse
        {
            get { return _fieldValues.ContainsKey(39); }
        }

        #endregion CommsMessage implementation

        #region Fields

        public MessageTypeIdentifiers MessageTypeIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the response code.
        /// </summary>
        /// <value>
        /// The response code.
        /// </value>
        public string ResponseCode
        {
            get
            {
                return (string)_fieldValues[39];
            }
            set
            {
                _fieldValues[39] = value.PadLeft(2, '0');
            }
        }

        /// <summary>
        /// Gets or sets the private in field 60.
        /// </summary>
        /// <value>
        /// The private data
        /// </value>
        public string PrivateData60
        {
            get { return (string)_fieldValues[60]; }
            set { _fieldValues[60] = value; }
        }

        /// <summary>
        /// Gets or sets the private in field 61.
        /// </summary>
        /// <value>
        /// The private data
        /// </value>
        public string PrivateData61
        {
            get { return (string)_fieldValues[61]; }
            set { _fieldValues[61] = value; }
        }

        /// <summary>
        /// Gets or sets the private in field 62.
        /// </summary>
        /// <value>
        /// The private data
        /// </value>
        public string PrivateData62
        {
            get { return (string)_fieldValues[62]; }
            set { _fieldValues[62] = value; }
        }

        /// <summary>
        /// Gets or sets the private in field 63.
        /// </summary>
        /// <value>
        /// The private data
        /// </value>
        public string PrivateData63
        {
            get { return (string)_fieldValues[63]; }
            set { _fieldValues[63] = value; }
        }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName
        {
            get { return (string)_fieldValues[101]; }
            set { _fieldValues[101] = value; }
        }

        /// <summary>
        /// Gets or sets the terminal ID (Card acceptor terminal identification. CAID)
        /// </summary>
        /// <value>
        /// The terminal ID.
        /// </value>
        public string TerminalID
        {
            get { return (string)_fieldValues[41]; }
            set { _fieldValues[41] = value; }
        }

        /// <summary>
        /// Gets or sets the STAN. (Systems trace audit number)
        /// </summary>
        /// <value>
        /// The STAN.
        /// </value>
        public int STAN
        {
            get { return (int)DataHelper.UnboxNumber(_fieldValues[11]); }
            set { _fieldValues[11] = (int)value; }
        }

        /// <summary>
        /// Gets or sets the merchant ID. (Card acceptor identification code. CAIC)
        /// </summary>
        /// <value>
        /// The merchant ID.
        /// </value>
        public string MerchantID
        {
            get { return (string)_fieldValues[42]; }
            set { _fieldValues[42] = value; }
        }

        /// <summary>
        /// Gets or sets the RRN (Retrieval reference number)
        /// </summary>
        /// <value>
        /// The RRN.
        /// </value>
        public string RRN
        {
            get { return (string)_fieldValues[37]; }
            set { _fieldValues[37] = value; }
        }

        /// <summary>
        /// Gets or sets the NII (Network international identifier)
        /// </summary>
        /// <value>
        /// The NII.
        /// </value>
        public short NII
        {
            get { return (short)DataHelper.UnboxNumber(_fieldValues[24]); }
            set { _fieldValues[24] = value; }
        }

        /// <summary>
        /// Gets or sets the processing code.
        /// </summary>
        /// <value>
        /// The processing code.
        /// </value>
        public int ProcessingCode
        {
            get { return (int)DataHelper.UnboxNumber(_fieldValues[3]); }
            set { _fieldValues[3] = value; }
        }

        /// <summary>
        /// Gets or sets the transaction amount.
        /// </summary>
        /// <value>
        /// The transaction amount.
        /// </value>
        public decimal TransactionAmount
        {
            get { return (decimal)DataHelper.UnboxNumber(_fieldValues[4]); }
            set { _fieldValues[4] = value; }
        }

        /// <summary>
        /// Gets or sets the PAN (primary account number) Max 19 chars
        /// </summary>
        /// <value>
        /// The PAN.
        /// </value>
        public long PAN
        {
            get { return (long)DataHelper.UnboxNumber(_fieldValues[2]); }
            set { _fieldValues[2] = value; }
        }

        /// <summary>
        /// Gets or sets the current message number.
        /// </summary>
        /// <value>
        /// The message number.
        /// </value>
        public short MessageNumber
        {
            get { return (short)DataHelper.UnboxNumber(_fieldValues[71]); }
            set { _fieldValues[71] = value; }
        }

        /// <summary>
        /// Gets or sets the last message number
        /// </summary>
        /// <value>
        /// The last message number
        /// </value>
        public short MessageNumberLast
        {
            get { return (short)DataHelper.UnboxNumber(_fieldValues[72]); }
            set { _fieldValues[72] = value; }
        }

        /// <summary>
        /// Gets or sets the local time. field 12 - hhmmss
        /// </summary>
        /// <value>
        /// The local time.
        /// </value>
        public DateTime LocalTime
        {
            get { return (DateTime)_fieldValues[12]; }
            set { _fieldValues[12] = value; }
        }

        /// <summary>
        /// Gets or sets the local date. field 13 - MMDD
        /// </summary>
        /// <value>
        /// The local date.
        /// </value>
        public DateTime LocalDate
        {
            get { return (DateTime)_fieldValues[13]; }
            set { _fieldValues[13] = value; }
        }

        /// <summary>
        /// Gets or sets the expiry date. field 14  - YYMM
        /// </summary>
        /// <value>
        /// The expiry date.
        /// </value>
        public DateTime ExpiryDate
        {
            get { return (DateTime)_fieldValues[14]; }
            set { _fieldValues[14] = value; }
        }

        /// <summary>
        /// Gets or sets the POS entry mode. field 22 - n3
        /// </summary>
        /// <value>
        /// The POS entry mode.
        /// </value>
        public short POSEntryMode
        {
            get { return (short)DataHelper.UnboxNumber(_fieldValues[22]); }
            set { _fieldValues[22] = value; }
        }

        /// <summary>
        /// Gets or sets the number of credits. field 74 - n10
        /// </summary>
        /// <value>
        /// The number of credits.
        /// </value>
        public int CreditsNumber
        {
            get { return (int)DataHelper.UnboxNumber(_fieldValues[74]); }
            set { _fieldValues[74] = value; }
        }

        /// <summary>
        /// Gets or sets the number of credit reversals. field 75 - n10
        /// </summary>
        /// <value>
        /// The number of credit reversals.
        /// </value>
        public int CreditsReversalNumber
        {
            get { return (int)DataHelper.UnboxNumber(_fieldValues[75]); }
            set { _fieldValues[75] = value; }
        }

        /// <summary>
        /// Gets or sets the number of debits. field 76 - n10
        /// </summary>
        /// <value>
        /// The number of debits.
        /// </value>
        public int DebitsNumber
        {
            get { return (int)DataHelper.UnboxNumber(_fieldValues[76]); }
            set { _fieldValues[76] = value; }
        }

        /// <summary>
        /// Gets or sets the number of debit reversals. field 77 - n10
        /// </summary>
        /// <value>
        /// The number of debit reversals.
        /// </value>
        public int DebitsReversalNumber
        {
            get { return (int)DataHelper.UnboxNumber(_fieldValues[77]); }
            set { _fieldValues[77] = value; }
        }

        /// <summary>
        /// Gets or sets the credit amount. field 86 - n16
        /// </summary>
        /// <value>
        /// The credit amount.
        /// </value>
        public decimal CreditsAmount
        {
            get { return (decimal)DataHelper.UnboxNumber(_fieldValues[86]); }
            set { _fieldValues[86] = value; }
        }

        /// <summary>
        /// Gets or sets the credit reversal amount. field 87 - n16
        /// </summary>
        /// <value>
        /// The credit reversal amount.
        /// </value>
        public decimal CreditsReversalAmount
        {
            get { return (decimal)DataHelper.UnboxNumber(_fieldValues[87]); }
            set { _fieldValues[87] = value; }
        }

        /// <summary>
        /// Gets or sets the debit amount. field 88 - n16
        /// </summary>
        /// <value>
        /// The debit amount.
        /// </value>
        public decimal DebitsAmount
        {
            get { return (decimal)DataHelper.UnboxNumber(_fieldValues[88]); }
            set { _fieldValues[88] = value; }
        }

        /// <summary>
        /// Gets or sets the debit reversal amount. field 89 - n16
        /// </summary>
        /// <value>
        /// The debit reversal amount.
        /// </value>
        public decimal DebitsReversalAmount
        {
            get { return (decimal)DataHelper.UnboxNumber(_fieldValues[89]); }
            set { _fieldValues[89] = value; }
        }

        /// <summary>
        /// Gets or sets the total settlement amount. field 97 - x+n16
        /// This may be negative or positive.
        /// </summary>
        /// <value>
        /// The total settlement amount.
        /// </value>
        public decimal SettlementAmount
        {
            get { return (decimal)DataHelper.UnboxNumber(_fieldValues[97]); }
            set { _fieldValues[97] = value; }
        }

        /// <summary>
        /// Sets the value of the particular field
        /// </summary>
        /// <param name="fieldNumber">The field number.</param>
        /// <param name="fieldValue">The field value.</param>
        public void SetField(byte fieldNumber, object fieldValue)
        {
            _fieldValues[fieldNumber] = fieldValue;
        }

        #endregion Fields

        #region Helper functions

        /// <summary>
        /// Gets the field value for the field number
        /// </summary>
        /// <param name="fieldNumber">The field number.</param>
        /// <returns>The field value as an object</returns>
        /// <exception cref="System.ArgumentException">Thrown if field not set</exception>
        public object GetFieldValue(byte fieldNumber)
        {
            if (_fieldValues.ContainsKey(fieldNumber))
                return _fieldValues[fieldNumber];

            throw new ArgumentException(string.Format("Field number {0} not set in message.", fieldNumber));
        }

        /// <summary>
        /// Gets the number of fields set for the message.
        /// </summary>
        /// <value>
        /// The field count.
        /// </value>
        public int FieldCount
        {
            get { return _fieldValues.Count; }
        }

        /// <summary>
        /// Determines whether the specified field is set.
        /// </summary>
        /// <param name="fieldNumber">The field number.</param>
        /// <returns>
        ///   <c>true</c> if the specified field number is set; otherwise, <c>false</c>.
        /// </returns>
        public bool HasField(byte fieldNumber)
        {
            if (_fieldValues.ContainsKey(fieldNumber))
                return true;

            return false;
        }

        public Dictionary<byte, object> GetFieldsAndValues()
        {
            return this._fieldValues;
        }

        #endregion Helper functions
    }
}