﻿using System;
using System.Text;
using Util.Data;

namespace Comms.Message.AS2805
{
    /// <summary>
    /// Represents a financial transaction routing header
    /// Can be either TPDU or CLNP.
    /// WARNING: This class is only partially implemented.
    /// </summary>
    public class Header
    {
        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        /// <value>
        /// The destination.
        /// </value>
        public string Destination { get; set; }

        public Header(byte[] buffer, HeaderTypes headType)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            if (headType == HeaderTypes.TPDU)
            {
                if (buffer[0] != 0x60)
                    throw new ArgumentException("TPDU Header must start with 0x60");

                string hexString = DataHelper.ByteArrayToHexString(buffer);

                if (hexString.Length < 10)
                    throw new ArgumentException("TPDU Header must be atleast 5 bytes long");

                Source = hexString.Substring(2, 4);
                Destination = hexString.Substring(6, 4);
            }
            else
                throw new ArgumentException(string.Format("Header type {0} not supported", headType));
        }

        public byte[] ToByteArray()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("60");
            sb.Append(Source.PadLeft(4, '0'));
            sb.Append(Destination.PadLeft(4, '0'));
            string headerTemp = sb.ToString();

            return DataHelper.HexStringToByteArray(headerTemp);
        }
    }
}