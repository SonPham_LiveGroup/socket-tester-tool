﻿using System;
using System.Configuration;

namespace Comms.Message.AS2805
{
    public class MessageConfig : ConfigurationSection
    {
        [ConfigurationProperty("headerType", DefaultValue = "None", IsRequired = true, IsKey = false)]
        public HeaderTypes HeaderType
        {
            get
            {
                return (HeaderTypes)this["headerType"];
            }
            set
            {
                this["headerType"] = (object)value;
            }
        }

        [ConfigurationProperty("packLengthType", DefaultValue = "BCD", IsRequired = true, IsKey = false)]
        public LengthIndicatorTypes PackLengthType
        {
            get
            {
                return (LengthIndicatorTypes)this["packLengthType"];
            }
            set
            {
                this["packLengthType"] = (object)value;
            }
        }

        [ConfigurationProperty("fieldConfigurations")]
        public FieldConfigElementCollection FieldConfigurations
        {
            get { return (FieldConfigElementCollection)this["fieldConfigurations"]; }
            set { this["fieldConfigurations"] = (object)value; }
        }
    }

    [Serializable]
    public class FieldConfig : ConfigurationElement
    {
        [ConfigurationProperty("fieldNumber", IsRequired = true, IsKey = true)]
        public byte FieldNumber
        {
            get
            {
                return (byte)this["fieldNumber"];
            }
            set
            {
                this["fieldNumber"] = (object)value;
            }
        }

        [ConfigurationProperty("attribute", DefaultValue = "None", IsRequired = true, IsKey = false)]
        public FieldAttributes Attribute
        {
            get
            {
                return (FieldAttributes)this["attribute"];
            }
            set
            {
                this["attribute"] = (object)value;
            }
        }

        [ConfigurationProperty("format", DefaultValue = "None", IsRequired = true, IsKey = false)]
        public FieldFormat Format
        {
            get
            {
                return (FieldFormat)this["format"];
            }
            set
            {
                this["format"] = (object)value;
            }
        }

        [ConfigurationProperty("length", DefaultValue = "6", IsRequired = true, IsKey = false)]
        public int Length
        {
            get
            {
                return (int)this["length"];
            }
            set
            {
                this["length"] = (object)value;
            }
        }
    }

    public class FieldConfigElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new FieldConfig();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FieldConfig)element).FieldNumber;
        }
    }
}