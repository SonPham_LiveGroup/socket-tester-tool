﻿using Comms.Message.Common;
using System;
using System.Configuration;
using System.Linq;
using Util.Data;

namespace Comms.Message.AS2805
{
    /// <summary>
    /// Packs/Unpacks AS2805 messages
    /// </summary>
    public class AS805Packer : CommsMessagePacker
    {
        private MessageConfig _config = new MessageConfig();

        public AS805Packer()
        {
            _config = (MessageConfig)ConfigurationManager.GetSection("AS2805PackerConfig");
        }

        public override CommsMessage Unpack(byte[] buffer, int pos, int length)
        {
            AS2805Message unpacked = new AS2805Message();

            byte[] bitmap = new byte[16];
            int bufferPosition = 0;

            // unpack message type id
            byte[] messageTypeIdArr = new byte[2];
            Array.Copy(buffer, 0, messageTypeIdArr, 0, 2);
            long messageTypeId = DataHelper.BcdToLong(messageTypeIdArr, 4);
            unpacked.MessageTypeIdentifier = (MessageTypeIdentifiers)messageTypeId;

            bufferPosition += 2;

            int maxFieldCount = 64;

            if (IsBitSet(buffer, bufferPosition, 1))
            {
                // we have an extended bitmap
                Array.Copy(buffer, bufferPosition, bitmap, 0, 16);
                bufferPosition += 16;
                maxFieldCount = 128;
            }
            else
            {
                Array.Copy(buffer, bufferPosition, bitmap, 0, 8);
                bufferPosition += 8;
            }

            for (byte i = 2; i <= maxFieldCount; i++)
            {
                if (IsBitSet(bitmap, i))
                {
                    try
                    {
                        var fConfig = from FieldConfig fc in _config.FieldConfigurations
                                      where fc.FieldNumber == i
                                      select fc;

                        if (fConfig.Count() == 0)
                            throw new Exception(string.Format("No field configuration for field {0}. Please add to app.config under AS2805PackerConfig.fieldConfigurations", i));

                        if (fConfig != null)
                            bufferPosition = UnpackField(buffer, bufferPosition, (FieldConfig)fConfig.First(), unpacked);
                    }
                    catch (NullReferenceException e)
                    {
                        throw new Exception(string.Format("No field configuration for field {0}. Please add to app.config under AS2805PackerConfig.fieldConfigurations", i));
                    }
                }
            }

            return (CommsMessage)unpacked;
        }

        private int UnpackField(byte[] source, int sourcepos, FieldConfig config, AS2805Message destination)
        {
            switch (config.Attribute)
            {
                case FieldAttributes.AlphaNumeric:
                case FieldAttributes.AlphaNumericSpecial:
                    return UnpackString(source, sourcepos, config, destination);

                case FieldAttributes.Numeric:
                case FieldAttributes.TransactionAmount:
                case FieldAttributes.TransactionAmountXN:
                    return UnpackNumeric(source, sourcepos, config, destination);

                case FieldAttributes.TrackX:
                    return UnpackTrackData(source, sourcepos, config, destination);

                case FieldAttributes.None:
                case FieldAttributes.Bits:
                case FieldAttributes.AlphaOrNumeric:
                default:
                    throw new Exception("Not implemented");
            }
        }

        private int UnpackTrackData(byte[] source, int sourcepos, FieldConfig config, AS2805Message destination)
        {
            //TODO.. just move over length for now. Cant unpack trackx as yet.
            int length = 0;
            sourcepos = UnpackLength(source, sourcepos, config, out length);

            int bytesUsed = length / 2;
            if ((length % 2) > 0)
                bytesUsed++;

            sourcepos += bytesUsed;
            return sourcepos;
        }

        private int UnpackString(byte[] source, int sourcepos, FieldConfig config, AS2805Message destination)
        {
            int length = 0;
            sourcepos = UnpackLength(source, sourcepos, config, out length);

            string fieldData = DataHelper.ByteArrayToString(source, sourcepos, length);
            destination.SetField(config.FieldNumber, (object)fieldData);
            sourcepos += length;

            return sourcepos;
        }

        private int UnpackNumeric(byte[] source, int sourcepos, FieldConfig config, AS2805Message destination)
        {
            int length = 0;

            bool creditXN = true;
            if (config.Attribute == FieldAttributes.TransactionAmountXN)
            {
                if ((char)source[sourcepos] == 'C')
                    creditXN = true;
                else
                    creditXN = false;
                sourcepos++;
            }
            sourcepos = UnpackLength(source, sourcepos, config, out length);

            int bytesUsed = 0;
            long fieldValue = DataHelper.BcdToLong(source, sourcepos, length, out bytesUsed);

            if (config.Attribute == FieldAttributes.TransactionAmountXN)
            {
                // do we need to reverse value?
                if (!creditXN)
                    fieldValue = fieldValue * -1;
            }

            long year = (long)DateTime.Now.Year;
            long month = 0;
            long day = 0;
            long hour = 0;
            long minute = 0;
            long second = 0;
            DateTime theDate;
            TimeSpan span;

            switch (config.Format)
            {
                case FieldFormat.hhmmss:
                    hour = fieldValue / 10000;
                    minute = (fieldValue - (hour * 10000)) / 100;
                    second = (fieldValue - (hour * 10000) - (minute * 100));
                    theDate = new DateTime(1, 1, 1, (int)hour, (int)minute, (int)second);
                    destination.SetField(config.FieldNumber, (object)theDate);
                    break;

                case FieldFormat.MMDDhhmmss:
                    month = DataHelper.BcdToLong(source, sourcepos, 2, out bytesUsed);
                    sourcepos++;
                    day = DataHelper.BcdToLong(source, sourcepos, 2, out bytesUsed);
                    sourcepos++;
                    hour = DataHelper.BcdToLong(source, sourcepos, 2, out bytesUsed);
                    sourcepos++;
                    minute = DataHelper.BcdToLong(source, sourcepos, 2, out bytesUsed);
                    sourcepos++;
                    second = DataHelper.BcdToLong(source, sourcepos, 2, out bytesUsed);
                    sourcepos++;
                    bytesUsed = 5; // n10

                    theDate = new DateTime((int)year, (int)month, (int)day, (int)hour, (int)minute, (int)second);

                    // this is to cater for the changeover of year in jan/feb
                    // if we get transactions batched from previous year then we need
                    // to minus a year to ensure the date is not in the future
                    // TODO: is this the right place for this??
                    span = theDate - DateTime.Now;
                    if (span.TotalDays > 180)
                        theDate = theDate.AddYears(-1);

                    destination.SetField(config.FieldNumber, (object)theDate);
                    break;

                case FieldFormat.MMDD:
                    month = fieldValue / 100;
                    day = fieldValue - (month * 100);
                    theDate = new DateTime((int)year, (int)month, (int)day);
                    span = theDate - DateTime.Now;

                    // this is to cater for the changeover of year in jan/feb
                    // if we get transactions batched from previous year then we need
                    // to minus a year to ensure the date is not in the future
                    // TODO: is this the right place for this??
                    if (span.TotalDays > 180)
                        theDate = theDate.AddYears(-1);

                    destination.SetField(config.FieldNumber, (object)theDate);
                    break;

                case FieldFormat.YYMM:
                    year = fieldValue / 100;
                    month = fieldValue - (year * 100);
                    year = year + 2000; // convert from 2 char
                    theDate = new DateTime((int)year, (int)month, 1);
                    destination.SetField(config.FieldNumber, (object)theDate);
                    break;

                case FieldFormat.None:
                    if (config.Attribute == FieldAttributes.TransactionAmount || config.Attribute == FieldAttributes.TransactionAmountXN)
                    {
                        decimal amountInDollars = (decimal)fieldValue / 100;
                        destination.SetField(config.FieldNumber, amountInDollars);
                    }
                    else
                        destination.SetField(config.FieldNumber, (object)fieldValue);

                    break;
            }

            sourcepos += bytesUsed;

            return sourcepos;
        }

        private int UnpackLength(byte[] source, int sourcepos, FieldConfig config, out int length)
        {
            string lengthString = null;

            switch (config.Format)
            {
                case FieldFormat.LLLVAR:
                    if (config.Attribute == FieldAttributes.TrackX)
                        throw new Exception("Not implemented TrackX");

                    if (this._config.PackLengthType == LengthIndicatorTypes.Ascii)
                    {
                        lengthString = DataHelper.ByteArrayToString(source, sourcepos, 3);
                        length = int.Parse(lengthString);
                        sourcepos += 3;
                    }
                    else
                    {
                        int bytesUsed = 0;
                        length = (int)DataHelper.BcdToLong(source, sourcepos, 4, out bytesUsed);
                        sourcepos += 2;
                    }
                    break;

                case FieldFormat.LLVAR:
                    if (config.Attribute == FieldAttributes.TrackX)
                    {
                        // for some reason legnth for track2 is always packed as BCD?
                        int bytesUsed = 0;
                        length = (int)DataHelper.BcdToLong(source, sourcepos, 2, out bytesUsed);
                        sourcepos += 1;
                    }
                    else
                    {
                        if (this._config.PackLengthType == LengthIndicatorTypes.Ascii)
                        {
                            lengthString = DataHelper.ByteArrayToString(source, sourcepos, 2);
                            length = int.Parse(lengthString);
                            sourcepos += 2;
                        }
                        else
                        {
                            int bytesUsed = 0;
                            length = (int)DataHelper.BcdToLong(source, sourcepos, 2, out bytesUsed);
                            sourcepos += 1;
                        }
                    }
                    break;

                case FieldFormat.None:
                case FieldFormat.hhmmss:
                case FieldFormat.MMDD:
                case FieldFormat.YYMM:
                case FieldFormat.MMDDhhmmss:
                    length = config.Length;
                    break;

                default:
                    throw new Exception("Not implemented");
            }

            return sourcepos;
        }

        public override byte[] Pack(CommsMessage message)
        {
            // TODO - what buffer size?
            byte[] buffer = new byte[4096];

            byte[] bitmap = new byte[16];
            AS2805Message requestMessage = (AS2805Message)message;

            // add message type id
            byte[] messageTypeIdArr = new byte[2];
            PackMessageTypeId(messageTypeIdArr, 0, requestMessage.MessageTypeIdentifier);

            int bufferPosition = 0;
            for (byte i = 0, j = 0; (i < 128) && (j < requestMessage.FieldCount); i++)
            {
                if (requestMessage.HasField(i))
                {
                    object fieldValue = requestMessage.GetFieldValue(i);
                    if (fieldValue != null)
                    {
                        if (i > 64)
                        {
                            // we need secondary bitmap. Set bit 1 to indicate this.
                            SetBit(ref bitmap, 1);
                        }
                        SetBit(ref bitmap, i);

                        var fConfig = from FieldConfig fc in _config.FieldConfigurations
                                      where fc.FieldNumber == i
                                      select fc;

                        if (fConfig.Count() == 0)
                            throw new Exception(string.Format("No field configuration for field {0}. Please add to app.config under AS2805PackerConfig.fieldConfigurations", i));

                        bufferPosition = PackField(buffer, bufferPosition, (FieldConfig)fConfig.First(), fieldValue);
                    }
                    j++;
                }
            }

            int bitmapSize = 8;

            // check if we need primary and secondary bitmap
            if (IsBitSet(bitmap, 1))
                bitmapSize = 16;

            byte[] finalBuffer = new byte[messageTypeIdArr.Length + bitmapSize + bufferPosition + 1];

            Array.Copy(messageTypeIdArr, 0, finalBuffer, 0, messageTypeIdArr.Length);
            Array.Copy(bitmap, 0, finalBuffer, messageTypeIdArr.Length, bitmapSize);
            Array.Copy(buffer, 0, finalBuffer, messageTypeIdArr.Length + bitmapSize, bufferPosition + 1);
            return finalBuffer;
        }

        private int PackMessageTypeId(byte[] dest, int destPos, MessageTypeIdentifiers messageType)
        {
            long messageTypeLong = (long)messageType;

            byte[] messageTypeArr = GetBcdNumberWithPadding((long)messageTypeLong, 4);

            // copy field value into dest
            Array.Copy(messageTypeArr, 0, dest, destPos, messageTypeArr.Length);
            destPos += messageTypeArr.Length;

            return destPos;
        }

        private bool IsBitSet(byte[] bitmap, byte fieldNumber)
        {
            return IsBitSet(bitmap, 0, fieldNumber);
        }

        private bool IsBitSet(byte[] bitmap, int bitmapPosition, byte fieldNumber)
        {
            return ((bitmap[((fieldNumber - 1) / 8) + bitmapPosition] << ((fieldNumber - 1) % 8) & 0x80) > 0);
        }

        private void SetBit(ref byte[] bitmap, byte fieldNumber)
        {
            bitmap[((fieldNumber - 1) / 8)] |= (byte)(0x80 >> ((fieldNumber - 1) % 8));
        }

        private int PackField(byte[] dest, int destPos, FieldConfig config, object fieldValue)
        {
            switch (config.Attribute)
            {
                case FieldAttributes.AlphaNumeric:
                case FieldAttributes.AlphaNumericSpecial:
                    return PackString(dest, destPos, config, fieldValue);

                case FieldAttributes.Numeric:
                case FieldAttributes.TransactionAmount:
                case FieldAttributes.TransactionAmountXN:
                    return PackNumeric(dest, destPos, config, fieldValue);

                case FieldAttributes.None:
                case FieldAttributes.Bits:
                case FieldAttributes.AlphaOrNumeric:
                default:
                    throw new Exception("Not implemented");
            }
        }

        private int PackString(byte[] dest, int destPos, FieldConfig config, object fieldValue)
        {
            string fieldValueString = (string)fieldValue;

            if (fieldValueString.Length > config.Length)
                throw new Exception(string.Format("Value of field {0} is greater than max length {1}", config.FieldNumber, config.Length));

            byte[] fieldValueArr = null;

            switch (config.Format)
            {
                case FieldFormat.LLVAR:
                case FieldFormat.LLLVAR:
                    // variable length portion
                    destPos = PackVariableLength(dest, destPos, config, fieldValueString.Length);
                    // data portion
                    fieldValueArr = DataHelper.StringToByteArray(fieldValueString);
                    break;

                case FieldFormat.None:
                    // data portion is fixed length
                    fieldValueArr = DataHelper.StringToByteArray(fieldValueString.PadRight(config.Length, ' '));
                    break;

                default:
                    throw new Exception("Not implemented");
            }

            // copy data portion into buffer
            Array.Copy(fieldValueArr, 0, dest, destPos, fieldValueArr.Length);
            destPos += fieldValueArr.Length;

            return destPos;
        }

        private int PackNumeric(byte[] dest, int destPos, FieldConfig config, object fieldValue)
        {
            switch (config.Format)
            {
                case FieldFormat.LLLVAR:
                case FieldFormat.LLVAR:
                    return PackVariableLengthNumeric(dest, destPos, config, fieldValue);

                case FieldFormat.MMDD:
                case FieldFormat.YYMM:
                case FieldFormat.hhmmss:
                case FieldFormat.None:
                    return PackFixedLengthNumeric(dest, destPos, config, fieldValue);

                case FieldFormat.MMDDhhmmss:
                case FieldFormat.YYMMDD:
                case FieldFormat.YYMMDDhhmmss:
                default:
                    throw new Exception("Not implemented");
            }
        }

        public static int PackFixedLengthNumeric(byte[] dest, int destPos, FieldConfig config, object fieldValue)
        {
            // get length
            int length = config.Length;

            byte[] bcdFieldValue = null;

            double fieldValueNumber = 0;

            long year = 0;
            long month = 0;
            long day = 0;
            long hour = 0;
            long minute = 0;
            long second = 0;

            switch (config.Format)
            {
                case FieldFormat.hhmmss:
                    hour = ((DateTime)fieldValue).Hour;
                    hour *= 10000;
                    minute = ((DateTime)fieldValue).Minute;
                    minute *= 100;
                    second = ((DateTime)fieldValue).Second;
                    fieldValueNumber = hour + minute + second;
                    break;

                case FieldFormat.MMDD:
                    month = ((DateTime)fieldValue).Month;
                    month *= 100;
                    day = ((DateTime)fieldValue).Day;
                    fieldValueNumber = month + day;
                    break;

                case FieldFormat.YYMM:
                    year = ((DateTime)fieldValue).Year;
                    year = year - 2000; // convert to 2 char
                    year *= 100;
                    month = ((DateTime)fieldValue).Month;
                    fieldValueNumber = month + year;
                    break;

                default:
                case FieldFormat.None:
                    fieldValueNumber = DataHelper.UnboxNumber(fieldValue);
                    break;
            }

            // convert to cents
            if (config.Attribute == FieldAttributes.TransactionAmount || config.Attribute == FieldAttributes.TransactionAmountXN)
            {
                fieldValueNumber *= 100;
            }
                

            if (config.Attribute == FieldAttributes.TransactionAmountXN)
            {
                // add D or C for negative or postive value
                if (fieldValueNumber >= 0)
                    dest[destPos] = (byte)'C';
                else
                    dest[destPos] = (byte)'D';

                destPos++;
            }

            bcdFieldValue = GetBcdNumberWithPadding((long)fieldValueNumber, length);

            if (bcdFieldValue == null)
            {
                throw new Exception(string.Format("Fixed length packing not implemented to type {0}", fieldValue.GetType().ToString()));
            }

            // copy field value into dest
            Array.Copy(bcdFieldValue, 0, dest, destPos, bcdFieldValue.Length);
            destPos += bcdFieldValue.Length;

            return destPos;
        }

        private int PackVariableLength(byte[] dest, int destPos, FieldConfig config, int length)
        {
            string lengthString = "";
            byte[] lengthArray = null;

            switch (config.Format)
            {
                case FieldFormat.LLVAR:
                    if (this._config.PackLengthType == LengthIndicatorTypes.Ascii)
                    {
                        // copy length into buffer (3 bytes ascii)
                        lengthString = length.ToString().PadLeft(2, '0');
                        lengthArray = DataHelper.StringToByteArray(lengthString);
                    }
                    else if (this._config.PackLengthType == LengthIndicatorTypes.BCD)
                    {
                        lengthArray = DataHelper.LongToBcd((long)length, 2);
                    }
                    Array.Copy(lengthArray, 0, dest, destPos, lengthArray.Length);
                    destPos += lengthArray.Length;
                    return destPos;

                case FieldFormat.LLLVAR:
                    if (this._config.PackLengthType == LengthIndicatorTypes.Ascii)
                    {
                        lengthString = length.ToString().PadLeft(3, '0');
                        lengthArray = DataHelper.StringToByteArray(lengthString);
                    }
                    else if (this._config.PackLengthType == LengthIndicatorTypes.BCD)
                    {
                        lengthArray = DataHelper.LongToBcd((long)length, 4);
                    }
                    Array.Copy(lengthArray, 0, dest, destPos, lengthArray.Length);
                    destPos += lengthArray.Length;
                    return destPos;

                default:
                    throw new Exception("Not implemented");
            }
        }

        public int PackVariableLengthNumeric(byte[] dest, int destPos, FieldConfig config, object fieldValue)
        {
            long fieldValueNumber = (long)fieldValue;

            // get length
            int length = fieldValueNumber.ToString().Length;

            destPos = PackVariableLength(dest, destPos, config, length);

            // get bcd representation of field value (dont use padding)
            byte[] bcdFieldValue = GetBcdNumberWithPadding(fieldValueNumber, length);

            // copy field value into dest
            Array.Copy(bcdFieldValue, 0, dest, destPos, bcdFieldValue.Length);
            destPos += bcdFieldValue.Length;

            return destPos;
        }

        public int BytesNeededForBcdDigits(int bcdDigits)
        {
            int bytesNeeded = bcdDigits / 2;
            if ((bcdDigits % 2) > 0)
                bytesNeeded++;

            return bytesNeeded;
        }

        /*
        public static byte[] GetBcdNumberWithPadding(long fieldValueNumber, int padToDigits)
        {
            // get bcd representation of field value
            int bcdDigits = 0;
            byte[] bcdFieldValue = DataHelper.LongToBcd(fieldValueNumber, out bcdDigits);
            if ((bcdDigits % 2) > 0)
            {
                // if its an uneven number of digits, shift arrary left one nibble and pad at end with F
                bcdFieldValue = DataHelper.BitShiftLeft(bcdFieldValue, 4);
                bcdFieldValue[bcdFieldValue.Length - 1] |= 0x0F;
            }

            if (padToDigits > 0)
            {
                if (bcdDigits < padToDigits)
                {
                    int bytesNeeded = BytesNeededForBcdDigits(padToDigits);

                    if (bytesNeeded > bcdFieldValue.Length)
                    {
                        // pad with extra FF
                        byte[] newArray = new byte[bytesNeeded];
                        Array.Copy(bcdFieldValue, 0, newArray, 0, bcdFieldValue.Length);
                        for(int i = (newArray.Length - 1); i >= bcdFieldValue.Length; i--)
                        {
                            newArray[i] = 0xFF;
                        }

                        return newArray;
                    }
                }
                return bcdFieldValue;
            }
            else
            {
                // dont apply any padding
                return bcdFieldValue;
            }
        }
        */

        public static byte[] GetBcdNumberWithPadding(long fieldValueNumber, int padToDigits)
        {
            // get bcd representation of field value
            byte[] bcdFieldValue = DataHelper.LongToBcd(fieldValueNumber, padToDigits);

            return bcdFieldValue;
        }

        private int PackNumericNoFormat(byte[] dest, int destPos, FieldConfig config, object fieldValue)
        {
            //
            throw new Exception("Not implemented");
        }
    }
}