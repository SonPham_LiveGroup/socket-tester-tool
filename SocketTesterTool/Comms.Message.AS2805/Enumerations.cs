﻿using System;

namespace Comms.Message.AS2805
{
    /// <summary>
    /// AS2805 Response Codes
    /// </summary>
    public enum ResponseCodes
    {
        Approved = 0,
        ContactIssuer = 1,
        ContactIssuerSpecialReason = 2,
        InvalidMerchant = 3,
        PickUpCard = 4,
        InvalidSTAN = 6,
        ApprovedPayOnSignature = 8,
        Approved2 = 11,
        InvalidTransaction = 12,
        InvalidAmount = 13,
        InvalidCardNumber = 14,
        ReEnterTransaction = 19,
        ReversalAcceptedOriginalNotReceived = 21,
        UnableToLocateRecordOnFile = 25,
        FormatError = 30,
        CardNotSupported = 31,
        CardExpired = 33,
        NoCreditAccount = 39,
        CardLost = 41,
        StolenCard = 43,
        InsufficientFunds = 51,
        NoChequeAccount = 52,
        NoSavingsAccount = 53,
        CardExpired2 = 54,
        InvalidPINRetry = 55,
        NoCardRecord = 56,
        TransactionInvalidToCard = 57,
        TransactionInvalidToTerminal = 58,
        SuspectedFraud = 59,
        MerchantToContactAcquirer = 60,
        DailyAmountLimitExceeded = 61,
        RestrictedCard = 62,
        SecurityViolation = 63,
        DailyFrequencyLimitExceeded = 65,
        PINTriesExceeded = 75,
        CaptureAmountExceedsPreauth = 76,
        UseFallbackForThisTransaction = 88,
        IssuerNotAvailable = 91,
        SystemMalfunction = 96
    }

    /// <summary>
    /// Types of request messages
    /// </summary>
    public enum MessageTypeIdentifiers
    {
        Unknown = 0,
        AuthorisationRequest = 100,
        AuthorisationRequestResponse = 110,
        AuthorisationAdvice = 120,
        AuthorisationAdviceResponse = 130,
        FinancialTransactionRequest = 200,
        FinancialTransactionRequestResponse = 210,
        FinancialTransactionAdvice = 220,
        FinancialTransactionAdviceResponse = 230,
        ReversalRequest = 420,
        ReversalRequestResponse = 430,
        AdminRequest = 600,
        AdminRequestResponse = 610,
        AdminAdvice = 620,
        AdminAdviceResponse = 630,
        TableRequest = 300,
        TableRequestResponse = 310,
        TableAdvice = 320,
        TableAdviceResponse = 330,
        SettlementRequest = 500,
        SettlementRequestResponse = 510,
        SettlementAdvice = 520,
        SettlementAdviceResponse = 530,
        NetworkManagementRequest = 800,
        NetworkManagementResponse = 810,
        PrivateAdministrativeAdvice = 9620,
        PrivateAdministrativeResponse = 9630,
        SettlementGetListRequest = 540,
        SettlementGetListResponse = 550,
        OnlineOrderRequest = 9625,
        OnlineOrderResponse = 9635,
        RewardPointsRequest = 240,
        RewardPointsResponse = 250,
        TransactionStatusRequest = 260,
        TransactionStatusResponse = 270,
        NotificationMessageRequest = 9640,
        NotificationMessageResponse = 9650
    }

    /// <summary>
    /// Types of fields and how they are packed
    /// </summary>
    [Serializable]
    public enum FieldFormat
    {
        /// <summary>
        /// No special formatting required
        /// </summary>
        None = 0,

        /// <summary>
        /// Field contains a 2-digit variable length indicator
        /// </summary>
        LLVAR,

        /// <summary>
        /// Field contains a 2-digit variable length indicator
        /// </summary>
        LLLVAR,

        /// <summary>
        /// Field is a date-time stamp MMDDhhmmss
        /// </summary>
        MMDDhhmmss,

        /// <summary>
        /// Field is a date MMDD
        /// </summary>
        MMDD,

        /// <summary>
        /// Field is a date YYMM
        /// </summary>
        YYMM,

        /// <summary>
        /// Field is a timestamp hhmmss
        /// </summary>
        hhmmss,

        /// <summary>
        /// Field is a date YYMMDD
        /// </summary>
        YYMMDD,

        /// <summary>
        /// Field is a date-time stamp YYMMDDhhmmss
        /// </summary>
        YYMMDDhhmmss,
    }

    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public enum FieldAttributes
    {
        /// <summary>
        /// No specific attribute
        /// </summary>
        None = 0,

        /// <summary>
        /// The field is packed as a numeric field
        /// </summary>
        Numeric,

        /// <summary>
        /// The field is packed as an ISO card track
        /// </summary>
        TrackX,

        /// <summary>
        /// The field is packed as alphanumeric
        /// </summary>
        AlphaNumeric,

        /// <summary>
        /// The field is packed as alphanumeric with special characters
        /// </summary>
        AlphaNumericSpecial,

        /// <summary>
        /// The field is packed in binary bits
        /// </summary>
        Bits,

        /// <summary>
        /// The field is packed in either alpha or numeric
        /// </summary>
        AlphaOrNumeric,

        /// <summary>
        /// The field contains 1 length byte
        /// </summary>
        XNumeric,

        /// <summary>
        /// Field is a transaction amount in cents
        /// </summary>
        TransactionAmount,

        /// <summary>
        /// Field is a transaction amount in cents with first
        /// byte being "D" for debit and "C" for credit
        /// </summary>
        TransactionAmountXN
    }

    /// <summary>
    /// How is the length represented for LLVAR and LLLVAR fields
    /// </summary>
    [Serializable]
    public enum LengthIndicatorTypes
    {
        BCD = 0,
        Ascii = 1
    }

    /// <summary>
    /// Types of As2805 headers
    /// </summary>
    [Serializable]
    public enum HeaderTypes
    {
        None,
        TPDU,
        CLNP
    }
}