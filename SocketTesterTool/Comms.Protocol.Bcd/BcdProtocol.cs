﻿using Comms.Connection.Common;
using Comms.Protocol.Common;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Util.Data;

namespace Comms.Protocol
{
    /// <summary>
    /// Implements a BCD protocol.
    /// BCD protocol has a simple 2 byte BCD length header followed by the message data
    /// </summary>
    public class BcdProtocol : ProtocolHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BcdProtocol" /> class.
        /// </summary>
        /// <param name="connectionToUse">The connection to use.</param>
        public BcdProtocol(AsyncConnectionBase connectionToUse)
            : base(connectionToUse) { }

        /// <summary>
        /// Handles a chunk of data received from the connection.
        /// This may or may not be the full frame and the BCD protocol handles multiple
        /// chunks per frame.
        /// </summary>
        /// <param name="bytesReceived">The bytes received.</param>
        protected override void HandleChunk(int bytesReceived)
        {
            try
            {
                if (_position == 0)
                {
                    if (bytesReceived < 2)
                    {
                        // expecting 2 bytes bcd header
                        // bail out
                        ProcessReceivedFrame(0);
                        return;
                    }

                    // extract bcd
                    _frameLength = (int)DataHelper.BcdToLong(_buffer, 4);
                    _position += 2;

                    int bytesWithoutBcd = bytesReceived - 2;

                    Array.Copy(_buffer, _position, _outBuffer, 0, bytesWithoutBcd);
                    _position += bytesWithoutBcd;

                    if (_frameLength > bytesWithoutBcd)
                    {
                        // we didnt get all the data, read again
                        _connection.BeginRead(_buffer, _position, _buffer.Length - _position);
                    }
                    else
                    {
                        //Trace.TraceInformation("BCDPROTOCOL: Received:\r\n{0}\r\n", DataHelper.ByteArrayToHexString(_buffer, 0, _position));
                        ProcessReceivedFrame(_frameLength);
                    }
                }
                else
                {
                    Array.Copy(_buffer, _position, _outBuffer, _position - 2, bytesReceived);
                    _position += bytesReceived;

                    if (_position >= (_frameLength + 2))
                    {
                        //Trace.TraceInformation("BCDPROTOCOL: Received:\r\n{0}\r\n", DataHelper.ByteArrayToHexString(_buffer, 0, _position));
                        ProcessReceivedFrame(_frameLength);
                    }
                    else
                    {
                        // we didnt get all the data, read again
                        _connection.BeginRead(_buffer, _position, _buffer.Length - _position);
                    }
                }
            }
            catch (Exception exc)
            {
                Trace.TraceWarning("BCDPROTOCOL: {0}", exc);
                // any other errors bail out
                ProcessReceivedFrame(0);
            }
        }

        /// <summary>
        /// Sends the frame. Adds BCD header to sendBuffer.
        /// </summary>
        /// <param name="sendBuffer">The send buffer.</param>
        /// <param name="sourcePosition">The source position to send from.</param>
        /// <param name="length">The length.</param>
        public override void SendFrame(byte[] sendBuffer, int sourcePosition, int length)
        {
            int position = 0;

            byte[] finalBuffer = new byte[length + 2];
            byte[] bcd = DataHelper.LongToBcd((long)length, 4);

            Array.Copy(bcd, 0, finalBuffer, 0, 2);
            position += 2;

            Array.Copy(sendBuffer, sourcePosition, finalBuffer, position, length);

            //Trace.TraceInformation("BCDPROTOCOL: Sending:\r\n{0}\r\n", DataHelper.ByteArrayToHexString(finalBuffer, 0, finalBuffer.Length));
            _connection.WriteAsync(finalBuffer, 0, finalBuffer.Length);
        }

        public override async Task SendFrameAsync(byte[] sendBuffer, int sourcePosition, int length)
        {
            int position = 0;

            byte[] finalBuffer = new byte[length + 2];
            byte[] bcd = DataHelper.LongToBcd((long)length, 4);

            Array.Copy(bcd, 0, finalBuffer, 0, 2);
            position += 2;

            Array.Copy(sendBuffer, sourcePosition, finalBuffer, position, length);

            //Trace.TraceInformation("BCDPROTOCOL: Sending:\r\n{0}\r\n", DataHelper.ByteArrayToHexString(finalBuffer, 0, finalBuffer.Length));
            await _connection.WriteAsync(finalBuffer, 0, finalBuffer.Length);
        }
    }
}