﻿using System;
using System.Collections.Generic;
using Util.Data;

namespace Util.Tlv
{
    /// <summary>
    /// Represents a set of TLV Tags.
    /// </summary>
    public class TagList
    {
        private List<Tag> _tags = new List<Tag>();
        //private UInt32 _totalLength = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="TagList" /> class.
        /// </summary>
        public TagList()
        {
            // new from scratch
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TagList" /> class from a byte array.
        /// </summary>
        /// <param name="buffer">The source buffer.</param>
        public TagList(byte[] buffer)
        {
            this.InitialiseFromByteArray(buffer);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TagList" /> class.
        /// </summary>
        /// <param name="tagBuffer">The tag buffer.</param>
        public TagList(string tagBuffer)
        {
            this.InitialiseFromByteArray(Util.Data.DataHelper.StringToByteArray(tagBuffer));
        }

        /// <summary>
        /// Gets the total length of all tags.
        /// </summary>
        /// <value>
        /// The total length.
        /// </value>
        public uint TotalLength
        {
            get
            {
                uint length = 0;
                foreach (Tag tmp in _tags)
                {
                    length += tmp.TlvBufferSize;
                }

                return length;
            }
        }

        /// <summary>
        /// Finds the tag matching the tag id.
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag if found. Null if not found.</returns>
        private Tag FindTag(byte[] tagId, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            if (tagId == null)
                throw new ArgumentNullException("tagId");

            foreach (Tag current in _tags)
            {
                if (DataHelper.ByteArrayCompare(current.TagId, tagId) && current.TagPrefix == tagprefix)
                    return current;
            }

            return null;
        }

        /// <summary>
        /// Finds the tag matching the tag id.
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag if found. Null if not found.</returns>
        private Tag FindTag(string tagId)
        {
            if (tagId == null)
                throw new ArgumentNullException("tagId");

            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);

            return FindTag(DataHelper.HexStringToByteArray(tagId), tagprefix);
        }

        /// <summary>
        /// Determines whether [is tag set] [the specified tag id].
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>
        ///   <c>true</c> if [is tag set] [the specified tag id]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsTagSet(string tagId)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            return IsTagSet(DataHelper.HexStringToByteArray(tagId), tagprefix);
        }

        /// <summary>
        /// Determines whether [is tag set] [the specified tag id].
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>
        ///   <c>true</c> if [is tag set] [the specified tag id]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsTagSet(byte[] tagId, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            Tag found = this.FindTag(tagId, tagprefix);

            if (found != null)
                return true;

            return false;
        }

        /// <summary>
        /// Gets the tag value as an unsigned, 32 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public UInt32 GetTagValue32Big(string tagId)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            return GetTagValue32Big(DataHelper.StringToByteArray(tagId), tagprefix);
        }

        /// <summary>
        /// Gets the tag value as an unsigned, 32 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public UInt32 GetTagValue32Big(byte[] tagId, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            Tag theTag = this.FindTag(tagId, tagprefix);
            return theTag.GetValue32Big();
        }

        /// <summary>
        /// Gets the tag value as an unsigned, 16 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public UInt32 GetTagValue16Big(string tagId)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            return GetTagValue16Big(DataHelper.StringToByteArray(tagId), tagprefix);
        }

        /// <summary>
        /// Gets the tag value as an unsigned, 16 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public UInt32 GetTagValue16Big(byte[] tagId, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            Tag theTag = this.FindTag(tagId, tagprefix);
            return theTag.GetValue16Big();
        }

        /// <summary>
        /// Gets the tag value as a byte array
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public byte[] GetTagValueBytes(string tagId)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);

            return GetTagValueBytes(DataHelper.StringToByteArray(tagId), tagprefix);
        }

        /// <summary>
        /// Gets the tag value as a byte array
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public byte[] GetTagValueBytes(byte[] tagId, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            Tag theTag = this.FindTag(tagId, tagprefix);
            return theTag.GetValueBytes();
        }

        /// <summary>
        /// Gets the tag value as a byte
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public byte GetTagValueByte(string tagId)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            return GetTagValueByte(DataHelper.StringToByteArray(tagId), tagprefix);
        }

        /// <summary>
        /// Gets the tag value as a byte
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public byte GetTagValueByte(byte[] tagId, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            Tag theTag = this.FindTag(tagId, tagprefix);
            return theTag.GetValueByte();
        }

        /// <summary>
        /// Gets the tag value as a string
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public string GetTagValueString(string tagId)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            return GetTagValueString(DataHelper.StringToByteArray(tagId), tagprefix);
        }

        /// <summary>
        /// Gets the tag value as a string
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public string GetTagValueString(byte[] tagId, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            Tag theTag = this.FindTag(tagId, tagprefix);
            if (theTag != null)
            {
                return theTag.GetValueString();
            }
            else
                return "";
        }

        /// <summary>
        /// Initialises the taglist from a byte arrary.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        private void InitialiseFromByteArray(byte[] buffer)
        {
            // populate from binary data
            UInt32 pos = 0;
            while (pos < buffer.Length)
            {
                if (buffer[pos] == 0xDF || buffer[pos] == 0x1F)
                {
                    byte prefix = buffer[pos];
                    // value tags
                    pos++;
                    UInt32 tagIdLength = Tag.GetTagIdLength(buffer, (int)pos);
                    byte[] tagId = new byte[tagIdLength];
                    Array.Copy(buffer, pos, tagId, 0, tagIdLength);
                    pos += tagIdLength;
                    UInt32 bytesRepresentingLength = 1;
                    UInt32 tagLength = Tag.GetTlvLength(buffer, pos, out bytesRepresentingLength);
                    pos += bytesRepresentingLength;
                    byte[] tagValue = new byte[tagLength];
                    Array.Copy(buffer, pos, tagValue, 0, tagLength);
                    pos += tagLength;
                    //AddTag(new ValueTag(tagId, tagValue));
                    AddTag(new ValueTag(tagId, tagValue, (TagPrefixes)prefix));
                }
                else if (buffer[pos] == 0xFF)
                {
                    // value tags
                    pos++;
                    UInt32 tagIdLength = Tag.GetTagIdLength(buffer, (int)pos);
                    byte[] tagId = new byte[tagIdLength];
                    Array.Copy(buffer, pos, tagId, 0, tagIdLength);
                    pos += tagIdLength;
                    UInt32 bytesRepresentingLength = 1;
                    UInt32 tagLength = Tag.GetTlvLength(buffer, pos, out bytesRepresentingLength);
                    pos += bytesRepresentingLength;
                    byte[] tagValue = new byte[tagLength];
                    Array.Copy(buffer, pos, tagValue, 0, tagLength);
                    pos += tagLength;
                    TagList list = new TagList(tagValue);
                    AddTag(new TagListTag(tagId, list));
                }
            }
        }

        /// <summary>
        /// Adds a tag value as an unsigned, 32 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTag32Big(string tagId, UInt32 value)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            AddValueTag32Big(DataHelper.StringToByteArray(tagId), value, tagprefix);
        }

        /// <summary>
        /// Adds a tag value as an unsigned, 32 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTag32Big(byte[] tagId, UInt32 value, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            byte[] arrayValue = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
            {
                // Reverse the byte order to correct endian
                arrayValue = DataHelper.ByteArraryReverse(arrayValue);
            }

            AddTag(new ValueTag(tagId, arrayValue, tagprefix));
        }

        /// <summary>
        /// Adds a tag value as an unsigned, 64 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTag64Big(string tagId, UInt64 value)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            AddValueTag64Big(DataHelper.StringToByteArray(tagId), value, tagprefix);
        }

        /// <summary>
        /// Adds a tag value as an unsigned, 64 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTag64Big(byte[] tagId, UInt64 value, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            byte[] arrayValue = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
            {
                // Reverse the byte order to correct endian
                arrayValue = DataHelper.ByteArraryReverse(arrayValue);
            }

            AddTag(new ValueTag(tagId, arrayValue, tagprefix));
        }

        /// <summary>
        /// Adds a tag value as an unsigned, 16 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTag16Big(string tagId, UInt16 value)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            AddValueTag16Big(DataHelper.StringToByteArray(tagId), value, tagprefix);
        }

        /// <summary>
        /// Adds a tag value as an unsigned, 16 bit, bin endian integer
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTag16Big(byte[] tagId, UInt16 value, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            byte[] arrayValue = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
            {
                // Reverse the byte order to correct endian
                arrayValue = DataHelper.ByteArraryReverse(arrayValue);
            }

            AddTag(new ValueTag(tagId, arrayValue, tagprefix));
        }

        /// <summary>
        /// Adds a tag value as a byte array
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTagBytes(string tagId, byte[] value)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            AddValueTagBytes(DataHelper.StringToByteArray(tagId), value, tagprefix);
        }

        /// <summary>
        /// Adds a tag value as a byte array
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTagBytes(byte[] tagId, byte[] value, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            AddTag(new ValueTag(tagId, value, tagprefix));
        }

        /// <summary>
        /// Adds a tag value as a byte
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTagByte(string tagId, byte value)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            AddValueTagByte(DataHelper.StringToByteArray(tagId), value, tagprefix);
        }

        /// <summary>
        /// Adds a tag value as a byte
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTagByte(byte[] tagId, byte value, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            byte[] arrayValue = new byte[1];
            arrayValue[0] = value;
            AddTag(new ValueTag(tagId, arrayValue, tagprefix));
        }

        /// <summary>
        /// Adds a tag value as a string
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTagString(string tagId, string value)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            AddValueTagString(DataHelper.StringToByteArray(tagId), value, tagprefix);
        }

        private void ExtractPrefix(string tagID, ref string onebytagID, ref TagPrefixes tagPrefix)
        {
            string _tagID = tagID.Trim();
            if (tagID.Length == 4)
            {
                string prefix = tagID.Substring(0, 2);
                UInt32 n = Convert.ToUInt32(tagID.Substring(2, 2), 16);
                string c = Convert.ToChar(n).ToString();
                tagPrefix = (TagPrefixes)DataHelper.HexStringToByte(prefix);
                onebytagID = Convert.ToChar(n).ToString();
            }
            else
                throw new Exception("Incorrect TagID");
        }

        /// <summary>
        /// Adds a tag value as a string
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void AddValueTagString(byte[] tagId, string value, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            AddTag(new ValueTag(tagId, DataHelper.StringToByteArray(value), tagprefix));
        }

        /// <summary>
        /// Sets the value of an existing tag to the string value specified
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void SetValueTagString(byte[] tagId, string value, TagPrefixes tagprefix = TagPrefixes.DF)
        {
            ValueTag tmp = (ValueTag)FindTag(tagId, tagprefix);
            tmp.Value = (object)DataHelper.StringToByteArray(value);
        }

        /// <summary>
        /// Sets the value of an existing tag to the string value specified
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="value">The value.</param>
        public void SetValueTagString(string tagId, string value)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            SetValueTagString(DataHelper.StringToByteArray(tagId), value, tagprefix);
        }

        /// <summary>
        /// Add a taglist as a subtag to this taglist.
        /// Any level of subtags are allowed.
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="toAdd">The taglist.</param>
        public void AddTagList(string tagId, TagList toAdd)
        {
            TagPrefixes tagprefix = TagPrefixes.FF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            AddTagList(DataHelper.StringToByteArray(tagId), toAdd);
        }

        /// <summary>
        /// Add a taglist as a subtag to this taglist.
        /// Any level of subtags are allowed.
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <param name="toAdd">The taglist.</param>
        public void AddTagList(byte[] tagId, TagList toAdd)
        {
            AddTag(new TagListTag(tagId, toAdd));
        }

        private void AddTag(Tag tag)
        {
            _tags.Add(tag);
        }

        /// <summary>
        /// Gets the tag value as a subtag
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public List<TagList> GetSubTags(string tagId)
        {
            TagPrefixes tagprefix = TagPrefixes.DF;
            string tag = tagId;
            ExtractPrefix(tag, ref tagId, ref tagprefix);
            return GetSubTags(DataHelper.StringToByteArray(tagId));
        }

        /// <summary>
        /// Gets the tag value as a subtag
        /// </summary>
        /// <param name="tagId">The tag id.</param>
        /// <returns>The tag value</returns>
        public List<TagList> GetSubTags(byte[] tagId)
        {
            List<TagList> tagList = new List<TagList>();

            foreach (Tag tmp in _tags)
            {
                if (DataHelper.ByteArrayCompare(tagId, tmp.TagId))
                    tagList.Add((TagList)tmp.Value);
            }

            return tagList;
        }

        /// <summary>
        /// Gets the size of the TLV buffer (total length of tags when converting to byte array)
        /// </summary>
        /// <value>
        /// The size of the TLV buffer.
        /// </value>
        public UInt32 TlvBufferSize
        {
            get
            {
                return TotalLength;
            }
        }

        /// <summary>
        /// Outputs the taglist as a byte array
        /// </summary>
        /// <returns>The taglist as a byte array</returns>
        public byte[] ToByteArray()
        {
            UInt32 pos = 0;
            byte[] buffer = new byte[this.TlvBufferSize];
            foreach (Tag tag in _tags)
            {
                Array.Copy(tag.ToByteArray(), 0, buffer, pos, tag.TlvBufferSize);
                pos += tag.TlvBufferSize;
            }
            return buffer;
        }
    }
}