﻿using System;
using Util.Data;

namespace Util.Tlv
{
    public enum TagPrefixes { DF = 0xDF, OF = 0x1F, FF = 0xFF }

    public abstract class Tag
    {
        public object Value { get; internal set; }
        public byte[] TagId { get; set; }

        //protected UInt32 _valueLength = 0;

        /// <summary>
        /// The length of the value portion of this tag
        /// </summary>
        public abstract UInt32 ValueLength { get; }

        public abstract UInt32 TlvBufferSize { get; }

        protected TagPrefixes _TagPrefix;
        public TagPrefixes TagPrefix { get { return _TagPrefix; } }

        /// <summary>
        /// Retrieve a 16-bit representation of the tag value, assuming it is in Big endian form
        /// </summary>
        /// <returns></returns>
        public UInt16 GetValue16Big()
        {
            if (!BitConverter.IsLittleEndian)
                return BitConverter.ToUInt16((byte[])Value, 0);
            else
                return BitConverter.ToUInt16(DataHelper.ByteArraryReverse((byte[])Value), 0);
        }

        /// <summary>
        /// Retrieve a 32-bit representation of the tag value, assuming it is in Big endian form
        /// </summary>
        /// <returns></returns>
        public UInt32 GetValue32Big()
        {
            if (!BitConverter.IsLittleEndian)
                return BitConverter.ToUInt32((byte[])Value, 0);
            else
                return BitConverter.ToUInt32(DataHelper.ByteArraryReverse((byte[])Value), 0);
        }

        /// <summary>
        /// Retrieve a 64-bit representation of the tag value, assuming it is in Big endian form
        /// </summary>
        /// <returns></returns>
        public UInt64 GetValu64Big()
        {
            if (!BitConverter.IsLittleEndian)
                return BitConverter.ToUInt64((byte[])Value, 0);
            else
                return BitConverter.ToUInt64(DataHelper.ByteArraryReverse((byte[])Value), 0);
        }

        /// <summary>
        /// Retrieve the bytes stored in the value portion of the tag.
        /// </summary>
        /// <returns></returns>
        public byte[] GetValueBytes()
        {
            return (byte[])Value;
        }

        /// <summary>
        /// Retrieve the bytes stored in the value portion of the tag.
        /// </summary>
        /// <returns></returns>
        public byte GetValueByte()
        {
            return ((byte[])Value)[0];
        }

        /// <summary>
        /// Retrieve the tag value as a string
        /// </summary>
        /// <returns></returns>
        public string GetValueString()
        {
            return DataHelper.ByteArrayToString((byte[])Value);
        }

        public static byte GetTlvLengthBuffer(UInt32 length, byte[] outputBuffer, int outputIndex)
        {
            byte tlvBufferLength = 1; // default
            int bufferPos = outputIndex;

            if (length < 127)
            {
                // 1 byte length
                if (outputBuffer != null)
                    outputBuffer[bufferPos++] = (byte)length;
            }
            else if (length > 127)
            {
                // first byte contains the number of bytes to be used for the length

                if (length > 127 && length <= 255)
                {
                    // 1 byte for length
                    tlvBufferLength = 2;
                    if (outputBuffer != null)
                    {
                        outputBuffer[bufferPos++] = 0x81;
                        outputBuffer[bufferPos++] = (byte)length;
                    }
                }
                else if (length > 255 && length <= 65535)
                {
                    // 2 byte for length
                    tlvBufferLength = 3;
                    if (outputBuffer != null)
                    {
                        outputBuffer[bufferPos++] = 0x82;
                        byte[] numberBuffer = BitConverter.GetBytes((UInt16)length);

                        if (BitConverter.IsLittleEndian)
                            Array.Reverse(numberBuffer);

                        Array.Copy(numberBuffer, 0, outputBuffer, bufferPos, 2);
                        bufferPos += 2;
                    }
                }
                else
                    throw new Exception("length not implemented");
            }

            return tlvBufferLength;
        }

        public static UInt32 GetTagIdLength(byte[] tagId, int pos)
        {
            UInt32 count = 1;
            int length = tagId.Length - pos;

            //comment this code out because we assume all Tag length is 1
            /*
            for (int i = 0; (i < length) && ((tagId[pos++] & 0x80) > 0); i++)
            {
                count++;
            }
             * */

            return count;
        }

        public static UInt32 GetTlvLength(byte[] tlvBuffer, UInt32 pos)
        {
            UInt32 throwAway = 0;
            return GetTlvLength(tlvBuffer, pos, out throwAway);
        }

        public static UInt32 GetTlvLength(byte[] tlvBuffer, UInt32 pos, out UInt32 bytesRepresentingLength)
        {
            byte first = tlvBuffer[pos++];
            bytesRepresentingLength = 1;

            if (first < 127)
            {
                return (UInt32)first;
            }
            else
            {
                // first byte is length
                if (first == 0x81)
                {
                    bytesRepresentingLength = 2;
                    return (UInt32)tlvBuffer[pos++];
                }
                else if (first == 0x82)
                {
                    // 2 byte length
                    bytesRepresentingLength = 3;
                    byte[] numberBuffer = new byte[2];
                    Array.Copy(tlvBuffer, pos, numberBuffer, 0, 2);

                    if (BitConverter.IsLittleEndian)
                        Array.Reverse(numberBuffer);

                    UInt16 tmpInt16 = BitConverter.ToUInt16(numberBuffer, 0);
                    pos += 2;
                    return (UInt32)tmpInt16;
                }
                else
                    throw new Exception("length not implemented");
            }
        }

        public abstract byte[] ToByteArray();
    }

    public class ValueTag : Tag
    {
        //public ValueTag(byte[] tagId, byte[] value)
        //{
        //    Value = value;
        //    TagId = tagId;
        //    _TagPrefix = TagPrefixes.DF;
        //    //_valueLength = (UInt32)value.Length;
        //}
        public ValueTag(byte[] tagId, byte[] value, TagPrefixes prefix = TagPrefixes.DF)
        {
            Value = value;
            TagId = tagId;
            _TagPrefix = prefix;
            //_valueLength = (UInt32)value.Length;
        }

        public override uint ValueLength
        {
            get
            {
                return (uint)((byte[])Value).Length;
            }
        }

        public byte[] FullTagId
        {
            get
            {
                byte[] tmp = new byte[TagId.Length + 1];
                //tmp[0] = 0xDF;
                tmp[0] = (Byte)this.TagPrefix;
                Array.Copy(TagId, 0, tmp, 1, TagId.Length);
                return tmp;
            }
        }

        public override byte[] ToByteArray()
        {
            byte[] outBuffer = new byte[TlvBufferSize];
            int pos = 0;
            //outBuffer[pos++] = 0xDF;
            outBuffer[pos++] = (Byte)this.TagPrefix;
            Array.Copy(this.TagId, 0, outBuffer, pos, this.TagId.Length);
            pos += this.TagId.Length;
            pos += Tag.GetTlvLengthBuffer(this.ValueLength, outBuffer, pos);
            byte[] arrayValue = (byte[])Value;
            Array.Copy(arrayValue, 0, outBuffer, pos, arrayValue.Length);
            pos += arrayValue.Length;
            return outBuffer;
        }

        /// <summary>
        /// The size of this tag including header, tag, length header and value
        /// </summary>
        public override UInt32 TlvBufferSize
        {
            get
            {
                if (TagId == null)
                    return 0;
                return (UInt32)(1 + this.TagId.Length + Tag.GetTlvLengthBuffer(ValueLength, null, 0) + ValueLength);
            }
        }
    }

    public class TagListTag : Tag
    {
        public TagListTag(byte[] tagId, TagList value)
        {
            Value = value;
            TagId = tagId;
            //_valueLength = (UInt32)value.TlvBufferSize;
        }

        public override uint ValueLength
        {
            get
            {
                return (uint)((TagList)Value).TlvBufferSize;
            }
        }

        public byte[] FullTagId
        {
            get
            {
                byte[] tmp = new byte[TagId.Length + 1];
                tmp[0] = 0xFF;
                Array.Copy(TagId, 0, tmp, 1, TagId.Length);
                return tmp;
            }
        }

        public override byte[] ToByteArray()
        {
            byte[] outBuffer = new byte[TlvBufferSize];
            int pos = 0;
            outBuffer[pos++] = 0xFF;
            Array.Copy(this.TagId, 0, outBuffer, pos, this.TagId.Length);
            pos += this.TagId.Length;
            pos += Tag.GetTlvLengthBuffer(this.ValueLength, outBuffer, pos);
            byte[] arrayValue = ((TagList)Value).ToByteArray();
            Array.Copy(arrayValue, 0, outBuffer, pos, arrayValue.Length);
            pos += arrayValue.Length;
            return outBuffer;
        }

        /// <summary>
        /// The size of this tag including header, tag, length header and value
        /// </summary>
        public override UInt32 TlvBufferSize
        {
            get
            {
                uint tmp = (uint)(this.TagId.Length + Tag.GetTlvLengthBuffer(this.ValueLength, null, 0));
                return ((TagList)Value).TlvBufferSize + tmp + 1;
            }
        }
    }
}