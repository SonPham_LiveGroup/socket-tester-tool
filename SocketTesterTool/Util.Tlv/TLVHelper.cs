﻿using System;

namespace Util.Tlv
{
    /// <summary>
    /// Helper which helps to parse string based tags into other data types.
    /// </summary>
    public static class TLVHelper
    {
        public static DateTime GetDateTimeFromStringTag(TagList list, string tagId, string format)
        {
            string dateString = list.GetTagValueString(tagId);
            return DateTime.ParseExact(dateString, format, null);
        }

        public static Int32 GetIntValueFromStringTag(TagList list, string tagId)
        {
            string IntValue = list.GetTagValueString(tagId);
            int result = 0;
            int.TryParse(IntValue, out result);
            return Convert.ToInt32(result);
        }

        public static UInt16 GetUInt16ValueFromStringTag(TagList list, string tagId)
        {
            string IntValue = list.GetTagValueString(tagId);
            UInt16 result = 0;
            UInt16.TryParse(IntValue, out result);
            return Convert.ToUInt16(result);
        }

        public static Decimal GetDecimalValueFromStringTag(TagList list, string tagId, int precision)
        {
            string DecimalValue = list.GetTagValueString(tagId);
            decimal pre = 1;
            for (int n = 1; n < precision; n++)
                pre = pre * 10;
            return Convert.ToDecimal(DecimalValue) / pre;
        }

        public static double GetDoubleValueFromStringTag(TagList list, string tagId, int precision)
        {
            string DecimalValue = list.GetTagValueString(tagId);
            double pre = 1;
            for (int n = 1; n < precision; n++)
                pre = pre * 10;
            return Convert.ToDouble(DecimalValue) / pre;
        }
    }
}