﻿using System;
using System.Diagnostics;
using System.IO;

namespace Util.TraceListeners
{
    public class RollingXmlWriterTraceListener : XmlWriterTraceListener
    {
        private string baseFileName;
        private string currentfileName;
        private DateTime currentDay = DateTime.Now;
        private bool _writerInitialised = false;

        public RollingXmlWriterTraceListener(string filename)
            : base(filename)
        {
            InitFilename(filename);
        }

        /*
        public RollingXmlWriterTraceListener(string filename, string name)
            : base(filename, name)
        {
            InitFilename(filename);
        }
        */

        /*
        public RollingXmlWriterTraceListener(TextWriter writer)
            : base(writer)
        {
            int i = 0;
        }

        public RollingXmlWriterTraceListener(Stream stream)
            : base(stream)
        {
            int i = 0;
        }

        public RollingXmlWriterTraceListener(Stream stream, string name)
            : base(stream, name)
        {
            int i = 0;
        }

        public RollingXmlWriterTraceListener(TextWriter writer, string name)
            : base(writer, name)
        {
            int i = 0;
        }
        */

        private void InitFilename(string filename)
        {
            // dont call base.
            baseFileName = filename;
            currentfileName = GetFilename();
        }

        private string GetFilename()
        {
            string dir = Path.GetDirectoryName(baseFileName);

            if (dir == string.Empty)
            {
                // use same directory as executable
                dir = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            }

            string file = Path.GetFileName(baseFileName);
            file = DateTime.Now.ToString("yyyyMMdd") + file;

            return Path.Combine(dir, file);
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
        {
            if (currentDay.DayOfYear != DateTime.Now.DayOfYear)
            {
                currentfileName = GetFilename();
                try
                {
                    this.Writer.Close();
                }
                catch { }
                this.Writer = new StreamWriter(currentfileName, true);
                currentDay = DateTime.Now;
            }
            else
            {
                if (!_writerInitialised)
                {
                    this.Writer = new StreamWriter(currentfileName, true);
                    _writerInitialised = true;
                }
            }

            base.TraceEvent(eventCache, source, eventType, id, format, args);
        }
    }
}