﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Util.TraceListeners
{
    public class TextBoxTraceListener : TraceListener
    {
        private TextBox _target;
        private StringSendDelegate _invokeWrite;
        public bool ScrollToEnd { get; set; }

        public TextBoxTraceListener(TextBox target)
        {
            _target = target;
            _invokeWrite = new StringSendDelegate(SendString);
        }

        public override void Write(string message)
        {
            try
            {
                _target.Invoke(_invokeWrite, new object[] { string.Format("{0} {1}", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff"), message) });
            }
            catch { }
        }

        public override void WriteLine(string message)
        {
            try
            {
                _target.Invoke(_invokeWrite, new object[] { message + Environment.NewLine });
            }
            catch { }
        }

        private delegate void StringSendDelegate(string message);

        private void SendString(string message)
        {
            // No need to lock text box as this function will only
            // ever be executed from the UI thread
            //_target.Text += message;
            if (_target.Text.Length > 50000)
            {
                _target.Text = _target.Text.Substring(25000, _target.Text.Length - 25000);
            }

            _target.AppendText(" ");
            _target.AppendText(message);
            if (ScrollToEnd)
            {
                _target.SelectionStart = _target.Text.Length;
                _target.ScrollToCaret();
            }
        }
    }
}