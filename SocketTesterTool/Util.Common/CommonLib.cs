﻿using System;
using System.Net.Mail;
using System.Reflection;

namespace Util.Common
{
    public class StringDescription : System.Attribute
    {
        private string _value;

        public StringDescription(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }
    }

    public class StringValue : System.Attribute
    {
        private string _value;

        public StringValue(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }
    }

    public static class StringEnum
    {
        public static string GetStringValue(Enum value)
        {
            string output = null;
            Type type = value.GetType();

            //Check first in our cached results...

            //Look for our 'StringValue'

            //in the field's custom attributes

            FieldInfo fi = type.GetField(value.ToString());
            StringValue[] attrs =
               fi.GetCustomAttributes(typeof(StringValue),
                                       false) as StringValue[];
            if (attrs.Length > 0)
            {
                output = attrs[0].Value;
            }

            return output;
        }

        public static string GetStringDescription(Enum value)
        {
            string output = null;
            Type type = value.GetType();

            //Check first in our cached results...

            //Look for our 'StringValue'

            //in the field's custom attributes

            FieldInfo fi = type.GetField(value.ToString());
            StringDescription[] attrs =
               fi.GetCustomAttributes(typeof(StringDescription),
                                       false) as StringDescription[];
            if (attrs.Length > 0)
            {
                output = attrs[0].Value;
            }

            return output;
        }

        public static object Parse(Type type, string stringValue)
        {
            return Parse(type, stringValue, false);
        }

        public static object Parse(Type type, string stringValue, bool ignoreCase)
        {
            object output = null;
            string enumStringValue = null;

            ValidateAsEnumClass(type);

            //Look for our string value associated with fields in this enum
            foreach (FieldInfo fi in type.GetFields())
            {
                //Check for our custom attribute
                StringValue[] attrs = fi.GetCustomAttributes(typeof(StringValue), false) as StringValue[];
                if (attrs != null && attrs.Length > 0)
                    enumStringValue = attrs[0].Value;

                //Check for equality then select actual enum value.
                if (string.Compare(enumStringValue, stringValue, ignoreCase) == 0)
                {
                    output = Enum.Parse(type, fi.Name);
                    break;
                }
            }

            return output;
        }

        private static void ValidateAsEnumClass(Type enumType)
        {
            if (!enumType.IsEnum)
                throw new ArgumentException(String.Format("Supplied type must be an Enum.  Type was {0}", enumType));
        }
    }

    public static class CommonHelper
    {
        /// <summary>
        /// Copy an object to destination object, only matching fields will be copied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceObject">An object with matching fields of the destination object</param>
        /// <param name="destObject">Destination object, must already be created</param>
        public static void DeepCopy<T>(T sourceObject, ref T destObject)
        {
            //	If either the source, or destination is null, return
            if (sourceObject == null || destObject == null)
                return;

            //	Get the type of each object
            Type sourceType = sourceObject.GetType();
            Type targetType = destObject.GetType();

            //	Loop through the source properties
            foreach (PropertyInfo p in sourceType.GetProperties())
            {
                //	Get the matching property in the destination object
                PropertyInfo targetObj = targetType.GetProperty(p.Name);
                //	If there is none, skip
                if (targetObj == null)
                    continue;

                //	Set the value in the destination
                targetObj.SetValue(destObject, p.GetValue(sourceObject, null), null);
            }
        }

        public static void Casting<B, D>(B sourceObject, ref D destObject)
        {
            //	If either the source, or destination is null, return
            if (sourceObject == null || destObject == null)
                return;

            //	Get the type of each object
            Type sourceType = sourceObject.GetType();
            Type targetType = destObject.GetType();

            //	Loop through the source properties
            foreach (PropertyInfo p in sourceType.GetProperties())
            {
                //	Get the matching property in the destination object
                PropertyInfo targetObj = targetType.GetProperty(p.Name);
                //	If there is none, skip
                if (targetObj == null)
                    continue;

                //	Set the value in the destination
                if (targetObj.PropertyType == p.PropertyType)
                    targetObj.SetValue(destObject, p.GetValue(sourceObject, null), null);
            }
        }

        public static decimal StrToDec(string text, decimal defaultvalue)
        {
            decimal temp = 0;
            Decimal.TryParse(text, out temp);
            return temp;
        }

        public static double StrToDouble(string text, double defaultvalue)
        {
            double temp = 0;
            Double.TryParse(text, out temp);
            return temp;
        }

        public static int StrToInt(string text, int defaultvalue)
        {
            int temp = 0;
            Int32.TryParse(text, out temp);
            return temp;
        }
    }

    public class SMTPMailSender
    {
        private string sMTPServer = "10.100.2.15";

        public string SMTPServer
        {
            get { return sMTPServer; }
            set { sMTPServer = value; }
        }

        private string sMTPUserID = "support@livegroup.com.au";

        public string SMTPUserID
        {
            get { return sMTPUserID; }
            //set { sMTPUserID = value; }
        }

        private string sMTPPassword = "";

        public string SMTPPassword
        {
            get { return sMTPPassword; }
            set { sMTPPassword = value; }
        }

        private int sMTPPort = 25;

        public int SMTPPort
        {
            get { return sMTPPort; }
            set { sMTPPort = value; }
        }

        private string mailFrom = "";//"support@livegroup.com.au";

        public string MailFrom
        {
            get { return mailFrom; }
            set { mailFrom = value; }
        }

        private string mailTo = "";

        public string MailTo
        {
            get { return mailTo; }
            set { mailTo = value; }
        }

        private string mailSubject = "";

        public string MailSubject
        {
            get { return mailSubject; }
            set { mailSubject = value; }
        }

        private string mailBody = "";

        public string MailBody
        {
            get { return mailBody; }
            set { mailBody = value; }
        }

        private string mailCC = "";

        public string MailCC
        {
            get { return mailCC; }
            set { mailCC = value; }
        }

        public SMTPMailSender()
        {
            this.sMTPUserID = "support@livegroup.com.au"; // Properties.Settings.Default.SMTPUserID;
            this.sMTPServer = "10.100.2.15"; //Properties.Settings.Default.SMTPServer;
            this.sMTPPort = 25;// Properties.Settings.Default.SMPTPPort;
            this.mailFrom = "support@livegroup.com.au";// Properties.Settings.Default.DefaultEmailFrom;
        }

        private AttachmentCollection attachments;

        public AttachmentCollection Attachements
        {
            get { return this.attachments; }
        }

        public void SendMail()
        {
            try
            {
                MailAddress smtpAddress = new MailAddress(sMTPUserID);
                MailAddress fromaddress = new MailAddress(mailFrom);
                MailAddress toaddress = new MailAddress(mailTo);

                MailMessage mail = new MailMessage();
                mail.From = fromaddress;
                mail.To.Add(toaddress);
                mail.Subject = mailSubject;
                mail.Body = mailBody;
                mail.IsBodyHtml = true;
                if (mailCC != "")
                    mail.CC.Add(mailCC);
                SmtpClient client = new SmtpClient(sMTPServer, sMTPPort);
                //Attachment item = new Attachment(
                //mail.Attachments.Add(new Attachment(
                if (attachments != null && attachments.Count > 0)
                    foreach (Attachment at in attachments)
                        mail.Attachments.Add(at);
                client.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendMail(AttachmentCollection attachmentcol)
        {
            try
            {
                MailAddress smtpAddress = new MailAddress(sMTPUserID);
                MailAddress fromaddress = new MailAddress(mailFrom);
                MailAddress toaddress = new MailAddress(mailTo);

                MailMessage mail = new MailMessage();
                mail.From = fromaddress;
                mail.To.Add(toaddress);
                mail.Subject = mailSubject;
                mail.Body = mailBody;
                mail.IsBodyHtml = true;
                if (mailCC != "")
                    mail.CC.Add(mailCC);
                SmtpClient client = new SmtpClient(sMTPServer, sMTPPort);
                //Attachment item = new Attachment(
                if (attachments != null && attachments.Count > 0)
                    foreach (Attachment at in attachmentcol)
                        mail.Attachments.Add(at);
                client.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}