﻿using System;
using System.IO;
using System.Text;

namespace Util.Data
{
    public static class DataHelper
    {
        /// <summary>
        /// Converts a byte array into a hex string
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <returns>A hex string</returns>
        public static string ByteArrayToHexString(byte[] buffer)
        {
            return ByteArrayToHexString(buffer, 0, buffer.Length);
        }

        public static string ByteArrayToHexString(byte[] buffer, int pos, int length)
        {
            StringBuilder sb = new StringBuilder(buffer.Length * 2);
            for (int i = 0; i < length; i++)
            {
                sb.AppendFormat("{0:x2}", buffer[i + pos]);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Returns a reversed byte array without affecting
        /// the source
        /// </summary>
        /// <param name="source">The byte array</param>
        /// <returns>The reverse of the source</returns>
        public static byte[] ByteArraryReverse(byte[] source)
        {
            byte[] reverse = new byte[source.Length];
            source.CopyTo(reverse, 0);
            Array.Reverse(reverse);
            return reverse;
        }

        /// <summary>
        /// Converts a hexadecimal string to a byte array
        /// </summary>
        /// <param name="hexString">The hex string.</param>
        /// <returns>A byte array</returns>
        public static byte[] HexStringToByteArray(string hexString)
        {
            int byteLength = hexString.Length / 2;
            byte[] bytes = new byte[byteLength];

            int j = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                string hex = new String(new Char[] { hexString[j], hexString[j + 1] });
                bytes[i] = HexStringToByte(hex);
                j = j + 2;
            }
            return bytes;
        }

        /// <summary>
        /// Converts 1 or 2 character hex string into equivalant byte value
        /// </summary>
        /// <param name="hex">1 or 2 character string</param>
        /// <returns>The byte representing the single byte</returns>
        public static byte HexStringToByte(string hexString)
        {
            if (hexString.Length > 2 || hexString.Length == 0)
                throw new ArgumentException("hexString must be 1 or 2 characters in length");

            byte newByte = byte.Parse(hexString, System.Globalization.NumberStyles.HexNumber);
            return newByte;
        }

        /// <summary>
        /// Converts an ASCII string to a byte arrary
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>A byte array representing the ASCII string</returns>
        public static byte[] StringToByteArray(string source)
        {
            byte[] buffer = new byte[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                buffer[i] = (byte)source[i];
            }

            return buffer;
        }

        /// <summary>
        /// Compares two byte arrays.
        /// </summary>
        /// <param name="first">The first.</param>
        /// <param name="second">The second.</param>
        /// <returns>True if arrays are identical</returns>
        /// <exception cref="System.ArgumentNullException">first</exception>
        /// <exception cref="System.ArgumentNullException">second</exception>
        public static bool ByteArrayCompare(byte[] first, byte[] second)
        {
            if (first == null)
                throw new ArgumentNullException("first");

            if (second == null)
                throw new ArgumentNullException("second");

            if (first.Length != second.Length)
                return false;

            for (int i = 0; i < first.Length; i++)
            {
                if (first[i] != second[i])
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Converts a byte array into an ASCII string
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns>An ASCII string</returns>
        public static string ByteArrayToString(byte[] source)
        {
            return ByteArrayToString(source, 0, source.Length);
        }

        /// <summary>
        /// Converts a byte array into an ASCII string
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="pos">The pos in the source.</param>
        /// <param name="length">The length.</param>
        /// <returns>
        /// An ASCII string
        /// </returns>
        public static string ByteArrayToString(byte[] source, int pos, int length)
        {
            StringBuilder sb = new StringBuilder(length);
            for (int i = 0; i < length; i++)
                sb.Append((char)source[pos + i]);

            return sb.ToString();
        }

        /// <summary>
        /// Converts a number to a BCD (binary coded decimal) byte array
        /// EG: 1234 would become a 2 byte array 0x12, 0x23
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>A byte array</returns>
        public static byte[] LongToBcd(long value, out int bcdDigits)
        {
            string valueString = value.ToString();
            bcdDigits = valueString.Length;
            int bytes = bcdDigits / 2;
            if ((bcdDigits % 2) > 0)
                bytes++;

            byte[] ret = new byte[bytes];
            for (int i = 0; i < bytes; i++)
            {
                ret[i] = (byte)(value % 10);
                value /= 10;
                ret[i] |= (byte)((value % 10) << 4);
                value /= 10;
            }

            if (BitConverter.IsLittleEndian)
                Array.Reverse(ret);

            return ret;
        }

        public static long BcdToLong(byte[] bcdArr, int bcdDigits)
        {
            int bytes = 0;
            return BcdToLong(bcdArr, 0, bcdDigits, out bytes);
        }

        public static long BcdToLong(byte[] bcdArr, int pos, int bcdDigits, out int bytesUsed)
        {
            long result = 0;

            bytesUsed = bcdDigits / 2;
            if ((bcdDigits % 2) > 0)
                bytesUsed++;

            //if (BitConverter.IsLittleEndian)
            //Array.Reverse(bcdArr);

            for (int i = 0; i < bytesUsed; i++)
            {
                byte bcd = bcdArr[pos + i];
                result *= 100;
                result += bcd & 0x0F;
                result += (((bcd & 0xF0) >> 4) * 10);
            }

            return result;
        }

        /// <summary>
        /// Converts a number to a BCD (binary coded decimal) byte array
        /// EG: 1234 would become a 2 byte array 0x12, 0x23
        /// Pads left to padToDigits
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="padToDigits">How many digits to pad to.</param>
        /// <returns>A byte array</returns>
        public static byte[] LongToBcd(long value, int padToDigits)
        {
            int bytes = padToDigits / 2;
            if ((padToDigits % 2) > 0)
                bytes++;

            byte[] ret = new byte[bytes];
            for (int i = 0; i < bytes; i++)
            {
                ret[i] = (byte)(value % 10);
                value /= 10;
                ret[i] |= (byte)((value % 10) << 4);
                value /= 10;
            }

            if (BitConverter.IsLittleEndian)
                Array.Reverse(ret);

            return ret;
        }

        /// <summary>
        /// Shifts a byte arrary bitcount bits to the left
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="bitcount">The bitcount.</param>
        /// <returns>A shifted byte array</returns>
        public static byte[] BitShiftLeft(byte[] value, int bitcount)
        {
            byte[] temp = new byte[value.Length];
            if (bitcount >= 8)
            {
                Array.Copy(value, bitcount / 8, temp, 0, temp.Length - (bitcount / 8));
            }
            else
            {
                Array.Copy(value, temp, temp.Length);
            }
            if (bitcount % 8 != 0)
            {
                for (int i = 0; i < temp.Length; i++)
                {
                    temp[i] <<= bitcount % 8;
                    if (i < temp.Length - 1)
                    {
                        temp[i] |= (byte)(temp[i + 1] >> 8 - bitcount % 8);
                    }
                }
            }
            return temp;
        }

        public static double UnboxNumber(object numeric)
        {
            if (numeric is long)
                return (double)(long)numeric;

            if (numeric is int)
                return (double)(int)numeric;

            if (numeric is short)
                return (double)(short)numeric;

            if (numeric is byte)
                return (double)(byte)numeric;

            if (numeric is float)
                return (double)(float)numeric;

            if (numeric is decimal)
                return (double)(decimal)numeric;

            return (double)numeric;
        }

        public static byte[] DebugDataToByteArray(string debugData)
        {
            StringBuilder sb = new StringBuilder();

            using (StringReader reader = new StringReader(debugData))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Length > 52)
                    {
                        int pos = line.IndexOf(':');

                        if ((pos != -1) && (pos < 5))
                        {
                            int posHex = pos + 1;
                            string finalLine = line.Substring(posHex, 48);
                            finalLine = finalLine.Replace(" ", string.Empty);
                            sb.Append(finalLine);
                        }
                    }
                }
            }

            string final = sb.ToString();

            return HexStringToByteArray(final);
        }
    }
}