﻿namespace Util.Threads
{
    /// <summary>
    /// The type of thread used within ThreadWorker.
    /// </summary>
    public enum WorkerType
    {
        /// <summary>
        /// Use smart thread pool. Good for short running tasks (default)
        /// </summary>
        SmartThreadPool,

        /// <summary>
        /// The a standard thread. There will be some overhead
        /// in creating the thread so only use this for longer running tasks.
        /// </summary>
        StandardThread,

        /// <summary>
        /// Use the standard thread pool (not recommended for short running tasks)
        /// </summary>
        CLRThreadPool
    }
}