﻿using Amib.Threading;
using System;
using System.Diagnostics;
using System.Threading;

namespace Util.Threads
{
    /// <summary>
    /// Wrapper which makes creating different types
    /// of threads easier. Takes care of thread exceptions without
    /// the caller being affected and ensures all resources are released.
    ///
    /// Uses SmartThreadPool internally for responsive threadpool.
    /// </summary>
    public abstract class ThreadWorker
    {
        /// <summary>
        /// Number of active worker instances.
        /// </summary>
        private static int _activeThreads = 0;

        /// <summary>
        /// The smart thread pool shared by all workers.
        /// </summary>
        private static SmartThreadPool pool;

        /// <summary>
        /// Initializes the <see cref="ThreadWorker" /> class. Creates a SmartThreadPool to be used by instances.
        /// </summary>
        static ThreadWorker()
        {
            STPStartInfo info = new STPStartInfo();
            info.AreThreadsBackground = true;
            info.MaxWorkerThreads = 80;
            info.MinWorkerThreads = Environment.ProcessorCount * 2;
            info.IdleTimeout = 300000;
            pool = new SmartThreadPool(info);
        }

        /// <summary>
        /// Gets the number of active workers.
        /// </summary>
        /// <value>
        /// The active workers.
        /// </value>
        public static int ActiveWorkers
        {
            get { return _activeThreads; }
        }

        private readonly object _stopLock = new object();

        private bool _stopped = true;
        private bool _stopping = false;

        private ManualResetEvent _finishedEvent;

        /// <summary>
        /// Gets the reset event. Allows a caller to wait for the thread to finish. By using FinishedEvent.WaitOne
        /// </summary>
        /// <value>
        /// The finished event.
        /// </value>
        public ManualResetEvent FinishedEvent
        {
            get { return _finishedEvent; }
        }

        /// <summary>
        /// The thread type to use for this worker.
        /// </summary>
        private WorkerType _threadType = WorkerType.SmartThreadPool;

        /// <summary>
        /// Gets or sets the type of the thread.
        /// </summary>
        /// <value>
        /// The type of the thread.
        /// </value>
        public WorkerType ThreadType
        {
            get { return _threadType; }
            set { _threadType = value; }
        }

        /// <summary>
        /// Starts the thread.
        /// </summary>
        /// <exception cref="System.Exception"></exception>
        public virtual void Start()
        {
            try
            {
                Interlocked.Increment(ref _activeThreads);
                _finishedEvent = new ManualResetEvent(false);

                switch (_threadType)
                {
                    case WorkerType.SmartThreadPool:
                        pool.QueueWorkItem(new WorkItemCallback(_Run), WorkItemPriority.Normal);
                        break;

                    case WorkerType.StandardThread:
                        Thread workerThread = new Thread(new ThreadStart(_Run));
                        workerThread.Start();
                        break;

                    default:
                        throw new Exception(string.Format("ThreadWorker: Threadtype not implemented {0}", _threadType));
                }
            }
            catch (Exception e)
            {
                Trace.TraceError("THREADWORKER: Exception {0}", e);
            }
            finally
            {
                _finishedEvent.Set();
            }
        }

        /// <summary>
        /// Tells the worker to stop.
        /// The thread may not stop instantly.
        /// </summary>
        public virtual void Stop()
        {
            lock (_stopLock)
            {
                _stopping = true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ThreadWorker" /> is stopping.
        /// </summary>
        /// <value>
        ///   <c>true</c> if stopping; otherwise, <c>false</c>.
        /// </value>
        public bool Stopping
        {
            get
            {
                lock (_stopLock)
                {
                    return _stopping;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ThreadWorker" /> is stopped.
        /// </summary>
        /// <value>
        ///   <c>true</c> if stopped; otherwise, <c>false</c>.
        /// </value>
        public bool Stopped
        {
            get
            {
                lock (_stopLock)
                {
                    return _stopped;
                }
            }
        }

        private void _Run()
        {
            _Run(this);
        }

        private object _Run(Object state)
        {
            try
            {
                _finishedEvent = new ManualResetEvent(false);

                lock (_stopLock)
                {
                    _stopped = false;
                    _stopping = false;
                }

                Run();
            }
            catch (Exception e)
            {
                Trace.TraceError("THREADWORKER: Exception {0}", e);
            }
            finally
            {
                lock (_stopLock)
                {
                    _stopped = true;
                    _stopping = false;
                }

                Interlocked.Decrement(ref _activeThreads);
                _finishedEvent.Set();
            }

            return null;
        }

        /// <summary>
        /// Should be implemented by child classes. This is where all the thread work is done.
        ///
        /// If your worker needs to run until stopped, include while(!Stopping){ // do work }
        /// within your implementation of Run()
        /// </summary>
        protected abstract void Run();
    }
}