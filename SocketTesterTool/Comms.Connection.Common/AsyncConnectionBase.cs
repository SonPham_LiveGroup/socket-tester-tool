﻿namespace Comms.Connection.Common
{
    /// <summary>
    /// Represents a connection which supports asyncronous read operations
    /// </summary>
    public abstract class AsyncConnectionBase : ConnectionBase
    {
        #region Events

        /// <summary>
        /// Delegate for receiving the results of an async read
        /// </summary>
        /// <param name="bytesReceived">The bytes received.</param>
        public delegate void DataReceived(int bytesReceived);

        /// <summary>
        /// Occurs when data is received after BeginRead is called.
        /// </summary>
        public event DataReceived OnDataReceived;

        #endregion Events

        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncConnectionBase" /> class.
        /// </summary>
        protected AsyncConnectionBase()
        {
            _supportsAsyncRead = true;
            OnDataReceived = null;
        }

        /// <summary>
        /// Processes data received from an async read.
        /// OnDataReceived should be set at this point.
        /// </summary>
        /// <param name="bytesReceived">The bytes received.</param>
        protected void ProcessDataReceived(int bytesReceived)
        {
            if (OnDataReceived != null)
                OnDataReceived(bytesReceived);
        }

        /// <summary>
        /// Begins an async read.
        /// OnDataReceived event should be set before this to
        /// ensure a result is received back to the caller.
        /// </summary>
        /// <param name="intoBuffer">Read into buffer.</param>
        /// <param name="atOffset">At offset.</param>
        /// <param name="length">For length.</param>
        public abstract void BeginRead(byte[] intoBuffer, int atOffset, int length);
    }
}