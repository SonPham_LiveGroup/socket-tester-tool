﻿using System.Threading.Tasks;

namespace Comms.Connection.Common
{
    /// <summary>
    /// Represents a communications connection.
    /// All connections should inherit from this.
    ///
    /// This allows us to use different connections types
    /// with protocol handlers etc without the need to know
    /// the underlying working of the connection.
    /// </summary>
    public abstract class ConnectionBase
    {
        /// <summary>
        /// Gets or sets the connection status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public ConnectionStatus Status { get; protected set; }

        /// <summary>
        /// Does this connection support asyncronous read/write operations
        /// </summary>
        protected bool _supportsAsyncRead = false;

        /// <summary>
        /// Reads from the connection into the a byte array.
        /// </summary>
        /// <param name="into">Read into array.</param>
        /// <param name="pos">At pos.</param>
        /// <param name="length">How may bytes to read.</param>
        public abstract void Read(byte[] into, int pos, int length);

        /// <summary>
        /// Writes to the connection from a byte array.
        /// </summary>
        /// <param name="from">From array.</param>
        /// <param name="pos">At pos.</param>
        /// <param name="length">How many bytes to write.</param>
        public abstract void Write(byte[] from, int pos, int length);

        public abstract Task WriteAsync(byte[] from, int pos, int length);

        /// <summary>
        /// Connects the underlying connection.
        /// </summary>
        public abstract void Connect();

        /// <summary>
        /// Disconnected the underlying connection.
        /// </summary>
        public abstract void Disconnect();
    }
}