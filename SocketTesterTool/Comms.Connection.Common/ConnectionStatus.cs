﻿namespace Comms.Connection.Common
{
    /// <summary>
    /// The status of a connection
    /// </summary>
    public enum ConnectionStatus
    {
        /// <summary>
        /// The connection is currently disconnected
        /// </summary>
        Disconnected,

        /// <summary>
        /// The connection is currently connected
        /// </summary>
        Connected,

        /// <summary>
        /// The connection is currently connecting
        /// </summary>
        Connecting
    }
}